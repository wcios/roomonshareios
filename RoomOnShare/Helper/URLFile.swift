//
//  URLFile.swift
//  RoomOnShare
//
//  Created by mac on 28/07/18.
//  Copyright © 2018 WestCoast. All rights reserved.
//

import Foundation

class Url_File {
    
    static let BaseUrl = "http://18.223.28.186/api/"
    
    static let Login = Url_File.BaseUrl + "login"
    static let SocilaLogin = Url_File.BaseUrl + "register"
    static let SignUp = Url_File.BaseUrl + "register"
    static let VerifyOtp = Url_File.BaseUrl + "verify_otp"
    static let ResendOtp = Url_File.BaseUrl + "resend_otp"
    static let UpdateMobileNumber = Url_File.BaseUrl + "update_mobile"
    static let ForgotPassword = Url_File.BaseUrl + "password/email"
    static let AboutYou = Url_File.BaseUrl + "about_you"
    static let ProfilePic = Url_File.BaseUrl + "update_profile_pic"
    
    static let Looking_YourDescription = Url_File.BaseUrl + "looking_home"
    static let Looking_IntroduceYourSelf = Url_File.BaseUrl + "looking_introduceyourself"
    static let Looking_PreferredLocation = Url_File.BaseUrl + "preferred_location"
    static let Looking_HomePreferrence = Url_File.BaseUrl + "looking_home_preferences"
    static let AddYourPhoto = Url_File.BaseUrl + "offering_add_photo"
    static let FlatematePreferrence = Url_File.BaseUrl + "offering_flatmate_prefrences"
    static let Offering_AboutOccupants = Url_File.BaseUrl + "offering_about_occupants"
    static let url_offering_home = Url_File.BaseUrl + "offering_home"
    
    static let MyListing = Url_File.BaseUrl + "looking_list"
    static let ChangeStatus = Url_File.BaseUrl + "is_active"
    static let DeleteLookingProfile = Url_File.BaseUrl + "looking_list/delete"
    
    static let LikeRoom = Url_File.BaseUrl + "favorite_rooms/store"
    static let LikeRoommat = Url_File.BaseUrl + "favorite_roommates"
    
    static let MatchesRoom = Url_File.BaseUrl + "show_room_matches_filter"
    static let MatchesRoommate = Url_File.BaseUrl + "show_roommates_matches_filter"
    static let FeaturedRoommates = Url_File.BaseUrl + "featured_roommates"
    static let FavouriteRoom = Url_File.BaseUrl + "my_favourite_rooms"
    static let FavouriteRoommate = Url_File.BaseUrl + "my_favourite_roommate"

    static let ChangeEmail = Url_File.BaseUrl + "change_email"
    static let ChangeCountry = Url_File.BaseUrl + "change_country"
    static let ResetPassword = Url_File.BaseUrl + ""
    static let SettingSendOtp = Url_File.BaseUrl + ""
    static let VerifyEmail = Url_File.BaseUrl + "verify_email"
    static let VerifyProfile = Url_File.BaseUrl + "verify_profile"
    
    //Appoinment slot api
    static let url_appoinment_detail = Url_File.BaseUrl + "appoinment_detail"
    static let url_appoinment = Url_File.BaseUrl + "appoinment"
    static let url_appoinment_confirmed = Url_File.BaseUrl + "appoinment_confirmed"
    static let url_getAllAppoinment = Url_File.BaseUrl + "all_appoinment"
    
    
    
    
    
    static let CreateGroup = Url_File.BaseUrl + "creategroup"
    static let AddMemberInGroup = Url_File.BaseUrl + "addusertogroup"
    static let GroupMemberList = Url_File.BaseUrl + "group_member_lists"    
    static let DeleteGroupMember = Url_File.BaseUrl + "deleteGroupUser"
    static let DeleteGroup = Url_File.BaseUrl + "deleteGroup"
    static let GroupList = Url_File.BaseUrl + "group_lists"
    static let GroupDetail = Url_File.BaseUrl + "group_detail"
    static let CreateExpance = Url_File.BaseUrl + "create_expense"
    
    //Upgrade service
    static let getServicPrice_Url = Url_File.BaseUrl + "package"

}



