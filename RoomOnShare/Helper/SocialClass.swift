//
//  SocialClass.swift
//  RoomOnShare
//
//  Created by mac on 03/08/18.
//  Copyright © 2018 WestCoast. All rights reserved.
//

import UIKit
import FBSDKCoreKit
import FBSDKLoginKit
import GoogleSignIn
import LinkedinSwift

class SocialClass: NSObject {

    class func Facebook(_ sender: FBSDKLoginButton, Vc: UIViewController, completion:@escaping (_ result: NSDictionary)->()) {
        var FBUserdata : NSDictionary = NSDictionary.init()
        if (FBSDKAccessToken.current() != nil){
            sender.readPermissions = ["public_profile", "email"]
        }
        let fbLoginManager : FBSDKLoginManager = FBSDKLoginManager()
        fbLoginManager.logIn(withReadPermissions: ["email"], from: Vc) {
            (result, error) in
            if (error == nil) {
                let fbloginresult : FBSDKLoginManagerLoginResult = result!
                if fbloginresult.grantedPermissions != nil {
                    if (fbloginresult.grantedPermissions.contains("email")){
                        //FBUserdata = self.getFBUserData()
                        if ((FBSDKAccessToken.current()) != nil) {
                            FBSDKGraphRequest(graphPath: "me", parameters: ["fields": "id, name, first_name, last_name, picture.type(large), email"]).start(completionHandler: { (connection, result,error) -> Void in
                                if (error == nil){
                                    print(result!)
                                    FBUserdata = result as! NSDictionary
                                    completion(FBUserdata)
                                   // let userDict = dict.value(forKey:"id") as! String
                                    //print(userDict)
                                }
                            })
                        }
                        
                        let myFbToken = (FBSDKAccessToken.current().tokenString)
                        print(myFbToken!)
                        print(FBSDKAccessToken.current().tokenString)
                        fbLoginManager.logOut()
                    }
                }
            }
        }
        
        //return FBUserdata
    }
    
    class func getFBUserData() -> NSDictionary {
        var dict : NSDictionary!
        if ((FBSDKAccessToken.current()) != nil) {
            FBSDKGraphRequest(graphPath: "me", parameters: ["fields": "id, name, first_name, last_name, picture.type(large), email"]).start(completionHandler: { (connection, result,error) -> Void in
                if (error == nil){
                    print(result!)
                    dict = result as! NSDictionary
                    let userDict = dict.value(forKey:"id") as! String
                    print(userDict)
                }
            })
        }
        return dict
    }
    
    
//    class func LinkedIn(vc : UIViewController, completion:@escaping (_ result: NSDictionary)->())  {
//        
//        var LinkedIndata : NSDictionary = NSDictionary.init()
//        
//         let linkedinHelper = LinkedinSwiftHelper(configuration: LinkedinSwiftConfiguration(clientId: "81zhiuczjmy4v9", clientSecret: "5nKx1TeI0JEYjsmF", state: "linkedin\(Int(Date().timeIntervalSince1970))", permissions: ["r_basicprofile", "r_emailaddress"], redirectUrl: "https://www.linkedin.com/oauth/v2/accessToken"))
//        
//        linkedinHelper.authorizeSuccess({ [unowned self] (lsToken) -> Void in
//            print("token----\(lsToken)")
//            // let strToken = lsToken.accessToken
//            }, error: { [unowned vc] (error) -> Void in
//                //self.writeConsoleLine("Encounter error: \(error.localizedDescription)")
//            }, cancel: { [unowned vc] () -> Void in
//                // self.writeConsoleLine("User Cancelled!")
//        })
//        
//        linkedinHelper.requestURL("https://api.linkedin.com/v1/people/~:(id,first-name,last-name,email-address,picture-url,picture-urls::(original),positions,date-of-birth,phone-numbers,location)?format=json", requestType: LinkedinSwiftRequestGet, success: { (response) -> Void in
//            print("Request success with response: \(response)")
//             LinkedIndata = response as! NSDictionary
//            completion(LinkedIndata)
//            DispatchQueue.main.async {
//                //  let login = self.storyboard?.instantiateViewController(withIdentifier: "HOMEViewController") as! HOMEViewController
//                //self.present(login, animated: true, completion: nil)
//            }
//        }) { [unowned vc] (error) -> Void in
//        }
//    }
    
}
