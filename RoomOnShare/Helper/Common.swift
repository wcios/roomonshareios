
import UIKit

class ErrorEntity: NSObject {
    var errorMessage = ""
    convenience init(withMessage: String) {
        self.init()
        self.errorMessage = withMessage
    }
}

class ResponseEntity: NSObject {
    var message = "Some Error"
    var success = true
    convenience init(withMessage: String) {
        self.init()
        
        self.message = withMessage
        
        
    }
}
class Common: NSObject
{
    
    static let Title = "Room On Share"
    // MARK: - Email Validation
    class func isValidEmail(testStr:String) -> Bool {
        let emailRegEx = "[A-Z0-9a-z._%+-]+@[A-Za-z0-9.-]+\\.[A-Za-z]{2,}"
        let emailTest = NSPredicate(format:"SELF MATCHES %@", emailRegEx)
        return emailTest.evaluate(with: testStr)
    }
    class func isValidateNumber(value: String) -> Bool {
        let PHONE_REGEX = "^((\\+)|(00))[0-9]{6,14}$"
        let phoneTest = NSPredicate(format: "SELF MATCHES %@", PHONE_REGEX)
        let result =  phoneTest.evaluate(with: value)
        return result
    }
    
    class func isCheckPasswordLength(password: String)  -> Bool{
        if password.count >= 6{
            return true
        }else{
            return false
        }
    }
    
    class func isCheckOTPLength(password: String)  -> Bool{
        if password.count == 4{
            return true
        }else{
            return false
        }
    }
    
    class func isCheckPasswordLength(password: String , confirmPassword : String) -> Bool {
        if password.count <= 7 && confirmPassword.count <= 7{
            return true
        }else{
            return false
        }
    }
    
    class func ShowAlert( Title : String, Message : String, VC: UIViewController ){
        
        let alert = UIAlertController(title: Title, message: Message, preferredStyle: UIAlertControllerStyle.alert)
        let okAction = UIAlertAction(title: "OK", style: UIAlertActionStyle.default) { (result : UIAlertAction) -> Void in
            print("OK")
        }
        //alert.view.tintColor = UIColor.init(red: 0.0/255.0, green: 128.0/255.0, blue: 0.0/255.0, alpha: 1.0)
        //alert.view.layer.cornerRadius = 12
        //alert.view.layer.masksToBounds = true
        //alert.view.layer.borderWidth = 3
        //alert.view.layer.borderColor = UIColor.init(red: 0.0/255.0, green: 128.0/255.0, blue: 0.0/255.0, alpha: 1.0).cgColor
        alert.addAction(okAction)
        alert.self.show(VC, sender: self)
        VC.present(alert, animated: true, completion: nil)
    }

    // MARK: - get Day date Month From Date String
    class func getDateDetailFrom(strDate:String) -> String{
        if strDate.isEmpty{
            return ""
        }
        let dateFormatterGet = DateFormatter()
        dateFormatterGet.dateFormat = "YYYY-MM-dd'T'HH:mm:ss'Z'"
        var date = Date()
        date = dateFormatterGet.date(from: strDate)!
        dateFormatterGet.dateFormat = "EEEE MMM dd yyyy hh-a"
        let strReturn:String = dateFormatterGet.string(from: date)
        return strReturn
    }
    // MARK: - Convert  String To date
    
    class func getDateFromString(strDate:String) -> Date
    {
        let dateFormatterGet = DateFormatter()
        dateFormatterGet.dateFormat = "YYYY-MM-dd'T'HH:mm:ss.SSSZ"
       // dateFormatterGet.timeZone = TimeZone(identifier: )
        var date = Date()
        date = dateFormatterGet.date(from: strDate)!
         return date
    }
    
  
    // MARK: - Creates a UIColor from a Hex string.
    
    static func hexStringToUIColor (_ hex:String) -> UIColor
    {
        var cString:String = hex.trimmingCharacters(in: .whitespacesAndNewlines).uppercased()
        
        if (cString.hasPrefix("#")) {
            cString.remove(at: cString.startIndex)
        }
        
        if ((cString.count) != 6) {
            return UIColor.gray
        }
        
        var rgbValue:UInt32 = 0
        Scanner(string: cString).scanHexInt32(&rgbValue)
        
        return UIColor(
            red: CGFloat((rgbValue & 0xFF0000) >> 16) / 255.0,
            green: CGFloat((rgbValue & 0x00FF00) >> 8) / 255.0,
            blue: CGFloat(rgbValue & 0x0000FF) / 255.0,
            alpha: CGFloat(1.0)
        )
    }

    // MARK: - showHUD with message
    class func showHUD()
    {
       // HUDManager.sharedInstance.showHud()
    }
    // MARK: - hide HUD
    class func hideHUD()
    {
      //  HUDManager.sharedInstance.hideHud()
    }
//    class func sizeForText(text: String, font: UIFont, maxSize: CGSize) -> CGSize
//    {
//        let attrString = NSAttributedString.init(string: text, attributes: [NSAttributedStringKey.font:font])
//        let rect = attrString.boundingRect(with: maxSize, options: NSStringDrawingOptions.usesLineFragmentOrigin, context: nil)
//        let size = CGSize(width: rect.size.width, height: rect.size.height)
//        return size
//    }
    
    ///// Push
    class func PushMethod(VC: UIViewController, identifier : String){
        let Vc = VC.storyboard?.instantiateViewController(withIdentifier: identifier)
        VC.navigationController?.pushViewController(Vc!, animated: false)
    }
    
    ////Pop
    class func PopMethod(VC: UIViewController){
        _ = VC.navigationController?.popViewController(animated: true)
    }
    
    ////Pop
    class func PopRootMethod(VC: UIViewController){
        _ = VC.navigationController?.popToRootViewController(animated: true)
    }
    
    //// Present
    class func PresentMethod(VC: UIViewController, PresentVC : UIViewController){
        VC.present(    PresentVC , animated: true, completion: nil)
    }
    
    //// Dismiss
    class func DismissMethod(VC: UIViewController){
        VC.dismiss(animated: true, completion: nil)
    }
    
    class func CheckArray(array : Array<Any>) -> Bool {
        var Available : Bool
        
        if (array.count == 0)  {
            Available = false
        }else if (array.isEmpty){
            Available = false
        }
        else{
            Available = true
        }
        return Available
    }
    
    class func CheckMutableArray(array : NSMutableArray) -> Bool {
        var Available : Bool
        
        if (array.count == 0)  {
            Available = false
        }else{
            Available = true
        }
        return Available
    }
    
    
//    class func CheckString(checkString : String?) -> Bool {
//        
//        if (checkString == nil) {
//            return false
//        }
//        else if checkString?.characters.count == 0 {
//            return false
//        }
//        else{
//            return true
//        }
//    }
    
    class func getCurrentDate() -> String {
        let date = Date()
        let formatter = DateFormatter()
        formatter.dateFormat = "dd MMM yyyy"
        let result = formatter.string(from: date)
        return result
    }
    
    class func getCurrentTime() -> String {
        let date = Date()
        let formatter = DateFormatter()
        formatter.dateFormat = "HH:mm a"
        let result = formatter.string(from: date)
        return result
    }
    
    class func ChangeDateFormat(Date: String, fromFormat: String, toFormat: String ) -> String  {
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = fromFormat
        let newdate = dateFormatter.date(from: Date)
        dateFormatter.dateFormat = toFormat
        return dateFormatter.string(from: newdate!)
    }
    
    class func CheckUnceck(arrButton : [UIButton], Selected : Int) {
        for button in arrButton {
            let tag = button.tag
            if Selected == tag{
                button.setImage(UIImage(named: "radio_fill"), for: .normal)
            }else{
                button.setImage(UIImage(named: "radio_blank"), for: .normal)
            }
        }
    }
    
    ////////////////  Camera Gallery start ///////////////////////
    class func ActionSheetForGallaryAndCamera(Picker : UIImagePickerController, VC: UIViewController) {
        
        let actionSheetController: UIAlertController = UIAlertController(title: nil, message: "", preferredStyle: .actionSheet)
        
        let cancelActionButton = UIAlertAction(title: "Cancel", style: .cancel) { _ in
            print("Cancel")
        }
        actionSheetController.addAction(cancelActionButton)
        
        let saveActionButton = UIAlertAction(title: "Gallery", style: .default)
        { _ in
            /////////// gallery //////////
            self.openGallary(Picker: Picker, VC: VC)
        }
        actionSheetController.addAction(saveActionButton)
        
        let deleteActionButton = UIAlertAction(title: "Camera", style: .default)
        { _ in
            /////////// camera //////////
            self.openCamera(Picker: Picker, VC: VC)
        }
        actionSheetController.addAction(deleteActionButton)
        
        VC.present(actionSheetController, animated: true, completion: nil)
    }
    
    
    class func openGallary(Picker : UIImagePickerController, VC: UIViewController)
    {
        Picker.allowsEditing = false
        Picker.sourceType = UIImagePickerControllerSourceType.photoLibrary
        VC.present(Picker, animated: true, completion: nil)
    }
    
    class func openCamera(Picker : UIImagePickerController, VC: UIViewController)
    {
        if(UIImagePickerController .isSourceTypeAvailable(UIImagePickerControllerSourceType.camera)){
            Picker.allowsEditing = false
            Picker.sourceType = UIImagePickerControllerSourceType.camera
            Picker.cameraCaptureMode = .photo
            VC.present(Picker, animated: true, completion: nil)
        }else{
           // self.ShowAlert(Title: "Camera Not Found", Message: "This device has no Camera", VC: VC)
        }
    }
    ///////////////////// End ///////////////////////
    
    class func setupTabViewController(Vc : UIViewController) {
        let storyboard = UIStoryboard(name: "Home", bundle: nil)
        
        let firstViewController = storyboard.instantiateViewController(withIdentifier: "HomeViewController")
        let firstNavigationController = UINavigationController(rootViewController: firstViewController)
        
        let secondViewController = storyboard.instantiateViewController(withIdentifier: "MatchesViewController")
        let secondNavigationController = UINavigationController(rootViewController: secondViewController)
        
        let thirdViewController = storyboard.instantiateViewController(withIdentifier: "MessagesViewController")
        let thirdNavigationController = UINavigationController(rootViewController: thirdViewController)
        
        let fourViewController = storyboard.instantiateViewController(withIdentifier: "SplitRentBillViewController")
        let fourNavigationController = UINavigationController(rootViewController: fourViewController)
        
        let fiveViewController = storyboard.instantiateViewController(withIdentifier: "MoreOptionsViewController")
        let fiveNavigationController = UINavigationController(rootViewController: fiveViewController)
        
        
        let tabBarController = RDVTabBarController()
        tabBarController.viewControllers = [firstNavigationController, secondNavigationController, thirdNavigationController,fourNavigationController,fiveNavigationController]
        
        //self.viewController = tabBarController
        
        customizeTabBarForController(tabBarController)
        
//        let tabBarController = HHTabBarView.shared.referenceUITabBarController
//        tabBarController.setViewControllers([firstNavigationController, secondNavigationController, thirdNavigationController, fourNavigationController, fiveNavigationController], animated: false)
//        //self.viewController = tabBarController
//
//        setupHHTabBarView()
       // customizeTabBarForController(tabBarController)
        Vc.navigationController?.pushViewController(tabBarController, animated: false)
        
    }
    
    
//    class func setupHHTabBarView() {
//        
//        //Default & Selected Background Color
//        let defaultTabColor = UIColor.white
//        let selectedTabColor = UIColor(red: 28/255, green: 158/255, blue: 247/255, alpha: 1.0)
//        let tabFont = UIFont.init(name: "Helvetica-Light", size: 12.0)
//        let spacing: CGFloat = 3.0
//        
//        let hhTabBarView = HHTabBarView.shared
//        
//        //Create Custom Tabs
//        //Note: As tabs are subclassed of UIButton so you can modify it as much as possible.
//        
//        var tabs = [HHTabButton]()
//        let titles = ["myListing", "matches", "chat", "share&split", "more"]
//        let icons = [UIImage(named: "CmyListing")!, UIImage(named: "Cmatches")!, UIImage(named: "Cchat")!, UIImage(named: "Cshare&split")!, UIImage(named: "Cmore")!]
//        
//        //var index = 0
//        
//        for index in 0...4 {
//            let tab = HHTabButton(withTitle: titles[index], tabImage: icons[index], index: index)
//            tab.titleLabel?.font = tabFont
//            tab.titleLabel?.textColor = UIColor.lightGray
//            tab.setHHTabBackgroundColor(color: defaultTabColor, forState: .normal)
//            tab.setHHTabBackgroundColor(color: selectedTabColor, forState: .selected)
//            tab.imageToTitleSpacing = spacing
//            tab.imageVerticalAlignment = .top
//            tab.imageHorizontalAlignment = .center
//            tabs.append(tab)
//        }
//        
////        for item in items {
////            //item.setBackgroundSelectedImage(finishedImage, unselectedImage: unfinishedImage)
////            let selectedimage = UIImage(named: "\(tabBarItemImagessel[index])")
////            let unselectedimage = UIImage(named: "\(tabBarItemImages[index])")
////            item.setFinishedSelectedImage(selectedimage, unselectedImage: unselectedimage)
////            index += 1
////        }
//        
//        //Set HHTabBarView position.
//        hhTabBarView.tabBarViewPosition = .bottom
//        hhTabBarView.tabBarViewTopPositionValue = 64
//        hhTabBarView.tabBarTabs = tabs
//        hhTabBarView.defaultIndex = 0
//        hhTabBarView.tabChangeAnimationType = .none
//        
//        //Handle Tab Changes
//        hhTabBarView.onTabTapped = { (tabIndex) in
//            print("Selected Tab Index:\(tabIndex)")
//        }
//    }
    
    class func customizeTabBarForController(_ tabBarController: RDVTabBarController) {
        guard let items = tabBarController.tabBar.items else {
            return
        }
        
        let tabBarItemImages    = ["myListing", "matches", "chat" , "share&split", "more"]
        let tabBarItemImagessel = ["CmyListing", "Cmatches", "Cchat" , "Cshare&split", "Cmore"]
        
        var index = 0
        for item in items {
            //item.setBackgroundSelectedImage(finishedImage, unselectedImage: unfinishedImage)
            let selectedimage = UIImage(named: "\(tabBarItemImagessel[index])")
            let unselectedimage = UIImage(named: "\(tabBarItemImages[index])")
            item.setFinishedSelectedImage(selectedimage, unselectedImage: unselectedimage)
            index += 1
        }
    }
}
    
  


protocol MyPickerViewProtocol {
    func myPickerDidSelectRow(Index:Int?, Tag:Int)
}

class MyPickerView: UIPickerView, UIPickerViewDataSource, UIPickerViewDelegate {
    
     var MyPickerDelegate:MyPickerViewProtocol?
     var  arrItems  = [String]()
    var Ctag : Int = 0
    
    
    convenience init(withDictionary response:[String], Tag: Int) {
        self.init()
        self.arrItems = response
        self.Ctag = Tag
    }
    
    func numberOfComponents(in pickerView: UIPickerView) -> Int {
        return 1
    }
    
    func pickerView(_ pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int {
        return arrItems.count
    }
    
    func pickerView(_ pickerView: UIPickerView, titleForRow row: Int, forComponent component: Int) -> String? {
        return arrItems[row]
    }
    
    func pickerView(_ pickerView: UIPickerView, didSelectRow row: Int, inComponent component: Int) {
        self.MyPickerDelegate?.myPickerDidSelectRow(Index: row, Tag: self.Ctag)
    }
}






