//
//  CountryAndCountryCode.swift
//  RoomOnShare
//
//  Created by mac on 20/08/18.
//  Copyright © 2018 WestCoast. All rights reserved.
//

import UIKit

class countrydetail: NSObject {
    class func readCountryJson() -> [country] {
        var arr : [country] = [country]()
        do {
            if let file = Bundle.main.url(forResource: "countrylist", withExtension: "json") {
                let data = try Data(contentsOf: file)
                let json = try JSONSerialization.jsonObject(with: data, options: [])
                if let object = json as? [String: Any] {
                    // json is a dictionary
                    print(object)
                } else if let object = json as? [Any] {
                    // json is an array
                    for item in object {
                        let dic = country.init(withDictionary: item as! [String : Any])
                        arr.append(dic)
                    }
                    //arr = object
                    return arr
                } else {
                    print("JSON is invalid")
                }
            } else {
                print("no file")
            }
        } catch {
            print(error.localizedDescription)
        }
        return arr
    }
    
//    class func readCountryCodeJson() -> [countryCode] {
//        var arr : [countryCode] = []
//        do {
//            if let file = Bundle.main.url(forResource: "countrycode", withExtension: "json") {
//                let data = try Data(contentsOf: file)
//                let json = try JSONSerialization.jsonObject(with: data, options: [])
//                if let object = json as? [String: Any] {
//                    // json is a dictionary
//                    print(object)
//                } else if let object = json as? [Any] {
//                    // json is an array
//                    for item in object {
//                        let dic = countryCode.init(withDictionary: item as! [String : Any])
//                        arr.append(dic)
//                    }
//                    // arr = object
//                    return arr
//                } else {
//                    print("JSON is invalid")
//                }
//            } else {
//                print("no file")
//            }
//        } catch {
//            print(error.localizedDescription)
//        }
//        return arr
//    }
}

class countries: NSObject {
    let countries : [country] = []
}

class country: NSObject {
    
    var Country_code = ""
    var Country_name = ""
    var Country_dialcode = ""
    var Country_latitude = 00.00
    var Country_longitude = 00.00
    var Country_icon = ""
    var currency_symbol = ""
    var currency_code = ""

    convenience init(withDictionary response:[String:Any]) {
        self.init()
        
        self.Country_code = (response["Alpha-2 code"] as? String)!
        self.Country_name = (response["Country name"] as? String)!
        self.Country_latitude = (response["Latitude (average)"] as? Double)!
        self.Country_longitude = (response["Longitude (average)"] as? Double)!
        self.Country_icon = (response["Icon"] as? String)!
        self.Country_dialcode = (response["dial_code"] as? String)!
        self.currency_code = (response["currency_code"] as? String)!
        self.currency_symbol = (response["currency_symbol"] as? String)!
    }
}


//class countriesCode: NSObject {
//    let countriesCode : [countryCode] = []
//}
//
//class countryCode: NSObject {
//    var Country_code = ""
//    convenience init(withDictionary response:[String:Any]) {
//        self.init()
//        self.Country_code = (response["dial_code"] as? String)!
//    }
//}
//"name":"Afghanistan",
//"dial_code":"+93",
//"code":"AF"

