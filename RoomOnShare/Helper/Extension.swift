//
//  Extension.swift
//  RoomOnShare
//
//  Created by mac on 20/08/18.
//  Copyright © 2018 WestCoast. All rights reserved.
//

import Foundation
import UIKit
extension UIViewController {
    
    func UIColorFromHex(rgbValue:UInt32, alpha:Double=1.0)->UIColor {
        let red = CGFloat((rgbValue & 0xFF0000) >> 16)/256.0
        let green = CGFloat((rgbValue & 0xFF00) >> 8)/256.0
        let blue = CGFloat(rgbValue & 0xFF)/256.0
        return UIColor(red:red, green:green, blue:blue, alpha:CGFloat(alpha))
    }
}

extension UIImageView {
    func tintImageColor(color : UIColor) {
        self.image = self.image!.withRenderingMode(UIImageRenderingMode.alwaysTemplate)
        self.tintColor = color
    }
}

extension String{
    func getTodayTime() -> String{
        
        let date = Date()
        let calender = Calendar.current
        let components = calender.dateComponents([.hour,.minute,.second], from: date)
        
        
        let hour = components.hour
        let minute = components.minute
        let second = components.second
        
        let today_string = String(hour!)  + ":" + String(minute!) + ":" +  String(second!)
        
        return today_string
        
    }
    
    func getCurrentDate() -> String{
        let date = Date()
        let calender = Calendar.current
        let components = calender.dateComponents([.year,.month,.day], from: date)
        
        let year = components.year
        let month = components.month
        let day = components.day
        
        
        let today_string = String(day!) + "/" + String(month!) + "/" + String(year!)
        return today_string
        
    }
    
    func changeTimeFormat(strDate : String) -> String{
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "HH:mm:ss"
        let date = dateFormatter.date(from: strDate)
        
        dateFormatter.dateFormat = "h:mm a"
        let date24 = dateFormatter.string(from: date!)
        
        return date24
    }
    
    func changeTimeFormatWidoutSec(strDate : String) -> String{
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "HH:mm"
        let date = dateFormatter.date(from: strDate)
        
        dateFormatter.dateFormat = "HH:mm a"
        let date24 = dateFormatter.string(from: date!)
        
        return date24
    }
    
    func changeDateFormat(strDate : String) -> String{
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "yyyy-MM-dd"
        let date = dateFormatter.date(from: strDate)
        
        
        dateFormatter.dateFormat = "EEEE, MMM d"
        let date24 = dateFormatter.string(from: date!)
        
        return date24
    }
}
