//
//  ArrayFile.swift
//  RoomOnShare
//
//  Created by mac on 28/08/18.
//  Copyright © 2018 WestCoast. All rights reserved.
//

import Foundation

class Array_File {
    
    /// WalkThrough////
    static let WarrName = ["Rent Easier", "Rent Easier", "Rent Easier", "Rent Easier"]
    static let WarrHeading = ["Search Apartments", "Find Roommates", "Send Messages", "Share Expense"]
    static let WarrDesc = ["Request or invite to book a room and set up secure 1st rent payment in a few steps","Request or invite to book a room and set up secure 1st rent payment in a few steps","Request or invite to book a room and set up secure 1st rent payment in a few steps", "Request or invite to book a room and set up secure 1st rent payment in a few steps"]
    static let Warrimage = ["WalkThrough1", "WalkThrough2","WalkThrough3","WalkThrough4"]
    //////
    
    ///tab bar menu
    static let arrOptions = ["Edit Profile",
                             "My Appointments",
                             "Upgrade Services",
                             "Invite/Refer friends",
                             "Rewards",
                             "Settings",
                             "Feedback",
                             "Help",
                             "Contact us",
                             "Logout"]
    
    static let arrMenuOptionsImg = ["bedEdit",
                                    "notificationColor",
                                    "serviceProce",
                                    "invite&refer",
                                    "rewards",
                                    "setting",
                                    "feedback",
                                    "help",
                                    "contactus",
                                    "logout"]
    
    
    static let arrAdults = ["One Person",
                            "Couple",
                            "2+ friends"]
    
    static let arrGender = ["Male",
                            "Female",
                            "Other"]
    
    static let arrSexuality = ["Straight",
                               "Lesbin/bi",
                               "Gay/bi"]
    
    static let arrGenderSexuality = ["Female Straight",
                                     "Male Straight",
                                     "Female Lesbin/bi",
                                     "Male Gay/bi"]
    
    static let arrAgeGroup = ["18 to 24 Years",
                              "25 to 30 years",
                              "31 to 35 Years",
                              "36 to 40 Years",
                              "41 years or over"]
    
    static let arrEmployment = ["Fully employed",
                                "Work part-time",
                                "Studying",
                                "Self-funded",
                                "In job transition"]
    
    static let arrLength_of_stay = ["1 month or less",
                                    "2 to 3 months",
                                    "4 to 5 months",
                                    "6 months plus"]
    
     static let arrBedroom_type = ["Private Bedroom",
                                   "Shared Bedroom"]
    
    
    static let arrfilter = ["Best Match",
                            "Price",
                            "Recent"]
    
    
    ///Offering
    
    static let arrHomeDescriptionOptionImgs = [
        [
            "image": "animal-&-pats",
            "name": "Air Conditioner"
        ],
        [
            "image": "art-&-culture",
            "name": "Balcony"
        ],[
            "image": "bar-&-pubs",
            "name": "BBQ Facilities"
        ],
          [
            "image": "business",
            "name": "CC TV"
        ],[
            "image": "car-&-motobikes",
            "name": "Ceiling Fan"
        ],
          [
            "image": "cummunity-work",
            "name": "City Views"
        ],[
            "image": "computer-&-Internet",
            "name": "Clothes washer"
        ],
          [
            "image": "cook",
            "name": "Clothes Dryer"
        ],[
            "image": "creative-interest",
            "name": "GYM"
        ],
          [
            "image": "current-affairs",
            "name": "Living Area Heating"
        ],[
            "image": "fashion",
            "name": "Lift & Elevator"
        ],
          [
            "image": "health",
            "name": "Neighbour Views"
        ],[
            "image": "movies",
            "name": "Broadband Internet"
        ],
          [
            "image": "music",
            "name": "Satellite or Cable View"
        ],[
            "image": "natural-therapies",
            "name": "Storage Room"
        ],
          [
            "image": "pd",
            "name": "Study of Office"
        ],[
            "image": "reading",
            "name": "Swimming Pool"
        ],
          [
            "image": "shopping",
            "name": "Water View"
        ],[
            "image": "social",
            "name": "Court Yard,Garden or Yard"
        ],
          [
            "image": "spiri",
            "name": "Dish Washer"
        ],[
            "image": "sports",
            "name": "Sauna"
        ],
          [
            "image": "tv",
            "name": "Spa or Hot Tub"
        ],[
            "image": "travel",
            "name": "Sauna"
        ],
          [
            "image": "video-game",
            "name": "Spa or Hot Tub"
        ],[
            "image": "writing",
            "name": "Dish Washer"
        ]
    ]
    
    static let arrHomeType = ["House","Appartment"]
    static let  arrHomeSize = ["1 bedder/studio","2 bedrooms","3 bedrooms","4+ bedrooms"]
    
    static let arrroomFeaturesImgs = [
        [
            "image": "car-&-motobikes",
            "name": "Wardrobe or Rack"
        ],
        [
            "image": "cummunity-work",
            "name": "Built-in Wardrobe"
        ],[
            "image": "computer-&-Internet",
            "name": "Walk-in Wardrobe"
        ],
          [
            "image": "cook",
            "name": "Air Conditioner"
        ],[
            "image": "creative-interest",
            "name": "Ceiling Fan"
        ],
          [
            "image": "current-affairs",
            "name": "Double Glassed Window"
        ],[
            "image": "fashion",
            "name": "TV aerial outlet"
        ],
          [
            "image": "health",
            "name": "Desk available"
        ],[
            "image": "movies",
            "name": "Used seperate rooms"
        ],
          [
            "image": "music",
            "name": "Adjoining rooms"
        ],[
            "image": "natural-therapies",
            "name": "Own Outdoor Area"
        ],[
            "image": "natural-therapies",
            "name": "Good view outlook"
        ]
    ]
    
   static let arrMinWeek = ["1 week","2 weeks","3 weeks","1 month","2 months","3 months","4 months","5 months","6 months"]
    
   static let arrMaxWeek = ["2 weeks","3 weeks","1 month","2 months","3 months","4 months","5 months","6 months","Indefinite"]
    
   static let arrBedrromrentWeek = ["per week","per month"]
   static let arrDeposit = ["2 weeks","1 month","discuss","no deposit"]
   static let arrBedroomtype = ["Private Bedroom", "Shared Bedroom"]
   static let arrBedroomSize = ["Small(single bed size)" , "Medium(double bed size)","Large(queen bed size)"]
   static let arrBedroomFurniture = ["With a bed" , "Without a bed","With or Without a bed"]
   static let arrNoOfBeds = ["2","3","4","5"]
   static let arrLastActive = ["All","30 Days","7 Days"]
    
    
    //more interest
    static let arrIntesrestOptionImgs = [
        [
            "image": "animal-&-pats",
            "name": "Animals & pets"
        ],
        [
            "image": "art-&-culture",
            "name": "Art & Culture"
        ],[
            "image": "bar-&-pubs",
            "name": "Bar Pubs or Clubs"
        ],
          [
            "image": "business",
            "name": "Business"
        ],[
            "image": "car-&-motobikes",
            "name": "Cars & Motorbikes"
        ],
          [
            "image": "cummunity-work",
            "name": "Community work"
        ],[
            "image": "computer-&-Internet",
            "name": "Computer & Internet"
        ],
          [
            "image": "cook",
            "name": "Cooking & Food"
        ],[
            "image": "creative-interest",
            "name": "Creative Interest"
        ],
          [
            "image": "current-affairs",
            "name": "Current Affairs"
        ],[
            "image": "fashion",
            "name": "Fashion"
        ],
          [
            "image": "health",
            "name": "Health & Fitness"
        ],[
            "image": "movies",
            "name": "Movie Goer"
        ],
          [
            "image": "music",
            "name": "Music"
        ],[
            "image": "natural-therapies",
            "name": "Natural Therapies"
        ],
          [
            "image": "pd",
            "name": "Personal Development"
        ],[
            "image": "reading",
            "name": "Reading"
        ],
          [
            "image": "shopping",
            "name": "Shopping"
        ],[
            "image": "social",
            "name": "Socialising"
        ],
          [
            "image": "spiri",
            "name": "Spirituality"
        ],[
            "image": "sports",
            "name": "Sports"
        ],
          [
            "image": "tv",
            "name": "Television"
        ],[
            "image": "travel",
            "name": "Travel"
        ],
          [
            "image": "video-game",
            "name": "Video Games"
        ],[
            "image": "writing",
            "name": "Writing"
        ]
    ]
    
    static let arrGroupList = [
        [
            "image": "group1",
            "name": "Group1"
        ],
        [
            "image": "group2",
            "name": "Group2"
        ],[
            "image": "group3",
            "name": "Group3"
        ],
          [
            "image": "group4",
            "name": "Group4"
        ]]
    
    static let arrExpenseTypeList = [
        [
            "image": "Rent",
            "name": "expense1"
        ],
        [
            "image": "electricity",
            "name": "expense2"
        ],[
            "image": "maid",
            "name": "expense3"
        ],
          [
            "image": "other",
            "name": "expense4"
        ]]
}




