
import UIKit

class SharedPreference: NSObject {
    
    fileprivate let kUserID = "__u_id"
    fileprivate let kUserEmail = "__u_email"
    fileprivate let KUserAuthtoken = "__u_authtoken"
    fileprivate let kUserFullName = "__u_f_nm"
    fileprivate let kUserMobileNumber = "__u_mobilenumber"
    fileprivate let kUserAge = "__u_age"
    fileprivate let kUserGender = "__u_gender"
    fileprivate let kUserProfilePic = "__u_f_userprofile"
    fileprivate let kUserCountry = "__u_country"
    fileprivate let kUserCountryCode = "__u_countrycode"
    fileprivate let kUserLatitude = "__u_latitude"
    fileprivate let kUserlongitude = "__u_longitude"
    fileprivate let kEmailVerified = "__u_emailverified"
    fileprivate let kMobileVerified = "__u_mobileverified"
    fileprivate let kUserActive = "__u_useractive"
    fileprivate let kContactPrivate = "__u_contactprivate"
    fileprivate let kUserLoginType = "__u_userlogintype"

    let kIsUserLogin = "IsUserLogin"
    let kLookingId = "IsLookingId"
    let kOfferingId = "IsLookingId"
    
    fileprivate let defaults = UserDefaults.standard
    static let sharedInstance = SharedPreference()
    
    class func saveUserData(_ user: UserData) {
        sharedInstance.saveUserData(user)
    }


    fileprivate func saveUserData(_ user: UserData) {

        defaults.setValue(user.user_id  , forKey: kUserID)
        defaults.setValue(user.user_authtoken  , forKey: KUserAuthtoken)
        defaults.setValue(user.user_fullname, forKey: kUserFullName)
        defaults.setValue(user.user_email, forKey: kUserEmail)
        defaults.setValue(user.user_mobile_no, forKey: kUserMobileNumber)
        defaults.setValue(user.user_age, forKey: kUserAge)
        defaults.setValue(user.user_gender, forKey: kUserGender)
        defaults.setValue(user.user_profilepic, forKey: kUserProfilePic)
        defaults.setValue(user.user_country, forKey: kUserCountry)
        defaults.setValue(user.user_country_code, forKey: kUserCountryCode)
        defaults.setValue(user.user_latitude, forKey: kUserLatitude)
        defaults.setValue(user.user_longitude, forKey: kUserlongitude)
        defaults.setValue(user.email_verified, forKey: kEmailVerified)
        defaults.setValue(user.mobile_verified, forKey: kMobileVerified)
        defaults.setValue(user.contact_is_private, forKey: kContactPrivate)
        defaults.setValue(user.user_is_active, forKey: kUserActive)
        defaults.setValue(user.user_login_type, forKey: kUserLoginType)
        
      //  UIImageView().kf.setImage(with: NSURL(string: user.stylemee_user_img_url.getURLstring)! as URL, placeholder: #imageLiteral(resourceName: "placeholder") , options: nil, progressBlock: nil, completionHandler: { image, error, cacheType, imageURL in
        //    if image != nil {
          //      SharedPreference.storeUserImage(UIImageJPEGRepresentation(image!, 0.5)!)
           // }
       // })

        defaults.synchronize()
   }
    
    

//    class func clearUserData(){
//        sharedInstance.clearUserData()
//        sharedInstance.setUserImage(Data())
//    }
    
//    fileprivate func clearUserData(){
//        self.saveUserData(UserEntity())
//
//    }
//
   class func getUserData() -> UserData{
    return sharedInstance.getUserData()
  }

    fileprivate  func getUserData() -> UserData {

        let user:UserData       =  UserData()
        user.user_id            =  defaults.value(forKey: kUserID)  as? Int ?? 0
        user.user_authtoken     =  defaults.value(forKey: KUserAuthtoken) as? String  ?? ""
        user.user_fullname      =  defaults.value(forKey: kUserFullName) as? String  ?? ""
        user.user_email         =  defaults.value(forKey: kUserEmail) as? String  ?? ""
        user.user_mobile_no     =  defaults.value(forKey: kUserMobileNumber) as? String ?? ""
        user.user_age           =  defaults.value(forKey: kUserAge) as? Int ?? 0
        user.user_gender        =  defaults.value(forKey: kUserGender) as? Int ?? 0
        user.user_profilepic    =  defaults.value(forKey: kUserProfilePic) as? String ?? ""
        user.user_country       =  defaults.value(forKey: kUserCountry) as? String ?? ""
        user.user_country_code  =  defaults.value(forKey: kUserCountryCode) as? String ?? ""
        user.user_latitude      =  defaults.value(forKey: kUserLatitude) as? Double ?? 0
        user.user_longitude     =  defaults.value(forKey: kUserlongitude) as? Double ?? 0
        user.email_verified     =  defaults.value(forKey: kEmailVerified) as? Bool ?? false
        user.mobile_verified    =  defaults.value(forKey: kMobileVerified) as? Bool ?? false
        user.contact_is_private =  defaults.value(forKey: kContactPrivate) as? Bool ?? false
        user.user_is_active     =  defaults.value(forKey: kUserActive) as? Bool ?? false
        user.user_login_type    =  defaults.value(forKey: kUserLoginType)  as? Int ?? 0
        return user
    }
    
    
    
    class func ContactUserData(name : String, email : String, age : Int, gender : Int, country : String, country_code : String, advice : String, mobile_V : Bool, email_v : Bool ){
       sharedInstance.ContactUserData(name: name, email: email, age: age, gender: gender, country: country, country_code: country_code, mobile_V: mobile_V, email_v: email_v)
    }
    
    fileprivate func ContactUserData(name : String, email : String, age : Int, gender : Int, country : String, country_code : String, mobile_V : Bool, email_v : Bool ){
        defaults.setValue(name , forKey: kUserFullName)
        defaults.setValue(email  , forKey: kUserEmail)
        defaults.setValue(age, forKey: kUserAge)
        defaults.setValue(gender, forKey: kUserGender)
        defaults.setValue(country, forKey: kUserCountry)
        defaults.setValue(country_code, forKey: kUserCountryCode)
        defaults.setValue(mobile_V, forKey: kMobileVerified)
        defaults.setValue(email_v, forKey: kEmailVerified)
        defaults.synchronize()
    }
    
    class func profilepic(pic : String){
        sharedInstance.profilepic(pic: pic)
    }
    
    fileprivate func profilepic(pic : String){
        defaults.setValue(pic , forKey: kUserProfilePic)
        defaults.synchronize()
    }
    
    class func MobileVerify(mob : Bool){
        sharedInstance.MobileVerify(mob: mob)
    }
    
    fileprivate func MobileVerify(mob : Bool){
        defaults.setValue(mob , forKey: kMobileVerified)
        defaults.synchronize()
    }
    
    class func SetMobileNumber(mob : String){
        sharedInstance.MobileNumber(mob: mob)
    }
    
    fileprivate func MobileNumber(mob : String){
        defaults.setValue(mob , forKey: kUserMobileNumber)
        defaults.synchronize()
    }
    
    class func Setcountry(country : String){
        sharedInstance.Setcountry(country: country)
    }
    
    fileprivate func Setcountry(country : String){
        defaults.setValue(country , forKey: kUserCountry)
        defaults.synchronize()
    }
    
    class func getIsUserLogin() -> Bool {        
        return sharedInstance.UserLogin() ?? false
    }
    fileprivate func UserLogin() -> Bool?{
        return defaults.value(forKey: kIsUserLogin) as? Bool;
    }
    
    class func setIsUserLogin(_ isLogin: Bool) {
        sharedInstance.IsUserLogin(isLogin)
    }
    fileprivate func IsUserLogin(_ token: Bool){
        defaults.set(token, forKey: kIsUserLogin);
    }
   
   
    class func getLooking_id() -> Int {
        return sharedInstance.getLooking_id() ?? 0
    }
    fileprivate func getLooking_id() -> Int?{
        return defaults.value(forKey: kLookingId) as? Int;
    }
    
    class func setLooking_id(_ looking_id: Int) {
        sharedInstance.setLooking_id(looking_id)
    }
    fileprivate func setLooking_id(_ lookingid: Int){
        defaults.set(lookingid, forKey:kLookingId );
    }
    
    class func getOffering_id() -> Int {
        return sharedInstance.getOffering_id() ?? 0
    }
    fileprivate func getOffering_id() -> Int?{
        return defaults.value(forKey: kOfferingId) as? Int;
    }
    
    class func setOffering_id(_ offring_id: Int) {
        sharedInstance.setOffering_id(offring_id)
    }
    fileprivate func setOffering_id(_ offringid: Int){
        defaults.set(offringid, forKey:kOfferingId );
    }
   
    
    
   // device token
//    class func storeDeviceToken(_ token: String) {
//        sharedInstance.setDeviceToken(token)
//    }
//    class func deviceToken() -> String {
//        return sharedInstance.getDeviceToken() ?? ""
//    }
//    fileprivate func setDeviceToken(_ token: String){
//        defaults.set(token, forKey: kDeviceToken);
//    }
//    fileprivate func getDeviceToken() -> String?{
//        return defaults.value(forKey: kDeviceToken) as? String;
//    }
//
//    // AuthToken
//    class func storeAuthToken(_ token: String) {
//        sharedInstance.setAuthToken(token)
//    }
//    class func authToken() -> String {
//        return sharedInstance.getAuthToken() ?? ""
//    }
//    fileprivate func setAuthToken(_ token: String){
//        defaults.set(token, forKey: kAuthKey);
//    }
//    fileprivate func getAuthToken() -> String?{
//        return defaults.value(forKey: kAuthKey) as? String;
//    }
//
//
//    // UDID
//    class func storeUDID(_ token: String) {
//        sharedInstance.setUDID(token)
//    }
//    class func UDID() -> String {
//        return sharedInstance.getUDID() ?? "123456789"
//    }
//    fileprivate func setUDID(_ token: String){
//        defaults.set(token, forKey: kUDID);
//    }
//    fileprivate func getUDID() -> String?{
//        return defaults.value(forKey: kUDID) as? String;
//    }
//
//     // user Image
//    class func storeUserImage(_ img: Data) {
//        sharedInstance.setUserImage(img)
//    }
//    class func userImage() -> Data {
//        return sharedInstance.getUserImage() ?? Data()
//    }
//    fileprivate func setUserImage(_ img: Data){
//        defaults.set(img, forKey: kUserImage)
//    }
//    fileprivate func getUserImage() -> Data?{
//        return defaults.value(forKey: kUserImage) as? Data
//    }
    
    
    

    // Filter data
//    class func storeFilterData (_ data: FilterData) {
//
//        let encodedData: Data = NSKeyedArchiver.archivedData(withRootObject: [data])
//        sharedInstance.setFilterData(encodedData)
//    }
//    class func filterData() -> FilterData {
//        let filterData = NSKeyedUnarchiver.unarchiveObject(with: sharedInstance.getFilterData()!) as! FilterData
//        return filterData
//    }
//    fileprivate func setFilterData(_ data: Data){
//        defaults.set(data, forKey: KFilterData);
//    }
//    fileprivate func getFilterData() -> Data?{
//        return defaults.value(forKey: KFilterData) as? Data
//    }
    
    
}
