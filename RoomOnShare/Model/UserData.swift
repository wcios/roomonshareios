//
//  UserData.swift
//  RoomOnShare
//
//  Created by mac on 13/08/18.
//  Copyright © 2018 WestCoast. All rights reserved.
//

import UIKit

class UserData: NSObject {
    
    var user_id = 0
    var user_authtoken = ""
    var user_fullname = ""
    var user_email = ""
    var user_mobile_no = ""
    var user_age = 0
    var user_gender = 0
    var user_country = ""
    var user_country_code = ""
    var user_country_latitude = 00.00
    var user_country_longitude = 00.00
    var user_latitude = 00.00
    var user_longitude = 00.00
    var user_contactadvice = ""
    var user_profilepic = ""
    var email_verified = false
    var mobile_verified = false
    var admin_verified = false
    var user_is_active = false
    var contact_is_private = false
    var facebook_id = ""
    var google_id = ""
    var linked_id = ""
    var user_login_type = 0
    

    convenience init(withDictionary response:[String:Any]) {
        self.init()
        
        self.user_login_type    = response["login_type"] as? Int ?? 0
        self.user_id       = response["user_id"] as? Int ?? 0
        self.user_authtoken = response["auth_token"] as? String ?? ""
        self.user_fullname = response["name"] as? String ?? ""
        self.user_email    = response["email"] as? String ?? ""
        self.user_mobile_no = response["mobile"] as? String ?? ""
        self.user_profilepic = response["profile_pic"] as? String ?? ""
        self.user_age      = response["age"] as? Int ?? 0
        self.user_gender   = response["gender"] as? Int ?? 0
        self.user_country  = response["country"] as? String ?? ""
        self.user_country_code = response["country_code"] as? String ?? ""
        self.facebook_id = response["facebook_id"] as? String ?? ""
        self.google_id = response["google_id"] as? String ?? ""
        self.linked_id = response["linkedin_id"] as? String ?? ""
        self.user_contactadvice = response["contact_advice"] as? String ?? ""

        self.user_country_latitude = response["country_latitude"] as? Double ?? 0
        self.user_country_longitude = response["country_longitude"] as? Double ?? 0
        self.user_latitude = response["user_latitude"] as? Double ?? 0
        self.user_longitude = response["user_longitude"] as? Double ?? 0
        
        self.email_verified = response["email_verified"] as? Bool ?? false
        self.mobile_verified = response["mobile_verified"] as? Bool ?? false
        self.admin_verified = response["is_admin_verified"] as? Bool ?? false
        self.user_is_active = response["is_active"] as? Bool ?? false
        self.contact_is_private = response["is_contact_private"] as? Bool ?? false
        
        
//        if let user_id = response["user_id"] as? Int
//        {
//            self.user_id = user_id
//        }
//        if let user_fullname = response["full_name"] as? String
//        {
//            self.user_fullname = user_fullname
//        }
//        if let user_email = response["email"] as? String
//        {
//            self.user_email = user_email
//        }
//
//        if let user_mobile_no = response["mobile"] as? String
//        {
//            self.user_mobile_no = user_mobile_no
//        }
//
//        if let user_profilepic = response["profile_pic"] as? String
//        {
//            self.user_profilepic = user_profilepic
//        }
//
//        if let user_age = response["age"] as? String
//        {
//            self.user_age = user_age
//        }
//
//        if let user_gender = response["stylemee_user_last_name"] as? String
//        {
//            self.user_gender = user_gender
//        }
//
//        if let user_country = response["country"] as? String
//        {
//            self.user_country = user_country
//        }
//
//        if let user_country_code = response["stylemee_user_last_name"] as? String
//        {
//            self.user_country_code = user_country_code
//        }
//
//        if let user_country_latitude = response["country_latitude"] as? String
//        {
//            self.user_country_latitude = user_country_latitude
//        }
//
//        if let user_country_longitude = response["country_longitude"] as? String
//        {
//            self.user_country_longitude = user_country_longitude
//        }
//
//        if let user_latitude = response["user_latitude"] as? String
//        {
//            self.user_latitude = user_latitude
//        }
//
//        if let user_longitude = response["user_longitude"] as? String
//        {
//            self.user_longitude = user_longitude
//        }
//
//        if let facebook_id = response["facebook_id"] as? String
//        {
//            self.facebook_id = facebook_id
//        }
//
//        if let google_id = response["google_id"] as? String
//        {
//            self.google_id = google_id
//        }
//
//        if let linked_id = response["linkedin_id"] as? String
//        {
//            self.linked_id = linked_id
//        }
//
//        if let user_contactadvice = response["contact_advice"] as? String
//        {
//            self.user_contactadvice = user_contactadvice
//        }
//
//        if let email_verified = response["email_verified"] as? Bool
//        {
//            self.email_verified = email_verified
//        }
//
//        if let mobile_verified = response["mobile_verified"] as? Bool
//        {
//            self.mobile_verified = mobile_verified
//        }
//
//        if let user_is_active = response["is_active"] as? Bool
//        {
//            self.user_is_active = user_is_active
//        }
//
//        if let contact_is_private = response["is_contact_private"] as? Bool
//        {
//            self.contact_is_private = contact_is_private
//        }
    }
}


//    {
//        "status": true,
//        "message": "Login Successfully.",
//        "response": {
//            "user_id": 1,
//            "full_name": "sonali",
//            "email": "abhi@gmail.com",
//            "mobile": "7788994455",
//            "age": 25,
//            "city": "",
//            "state": "",
//            "country": "",
//            "country_longitude": null,
//            "country_latitude": null,
//            "email_verified": 0,
//            "mobile_verified": 0,
//            "profile_pic": "",
//            "user_longitude": null,
//            "user_latitude": null,
//            "facebook_id": "",
//            "google_id": "",
//            "linkedin_id": "",
//            "contact_advice": "",
//            "is_contact_private": 0,
//            "is_active": false,
//            "auth_token": ""
//        }
//    }



