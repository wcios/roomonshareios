//
//  ProfileFlatmateDetail.swift
//  RoomOnShare
//
//  Created by mac on 03/09/18.
//  Copyright © 2018 WestCoast. All rights reserved.
//

import Foundation
class ProfileFlatmateDetailData: NSObject {
    
    var arrImages = [Images]()
    
    var homedescription  = OfferingHome()
    var bedroomdescription = [BedroomDescription]()
    var aboutoccupant = AboutOccupant()
    var Flatmatepreference = FlatmatePreference()
    var Userdata = UserData()

    var email_verified = false
    var fb_verified = false
    var google_verified = false
    var linkedin_verified = false
    var mobile_verified = false
    var user_is_active = false
    
    convenience init(withDictionary response:[String:Any]) {
        self.init()
        
        if let dicimages = response["photos"] as? NSDictionary {
            let cover_arr = dicimages.value(forKey: "upload_photo") as! [[String:Any]]
            for data in cover_arr {
                self.arrImages.append(Images.init(withDictionary:data))
            }
        }
        
        if let offeringhome = response["offering_home"] as? [String:Any] {
            self.homedescription = OfferingHome.init(withDictionary: offeringhome)
        }
        
        if let offering = response["offering_home"] as? NSDictionary {
            let arrbedroom = offering.value(forKey: "bedroom") as! [[String:Any]]
            for data in arrbedroom {
                self.bedroomdescription.append(BedroomDescription.init(withDictionary: data))
            }
        }
        
        if let aboutoccupent = response["about_occupant"] as? [String:Any] {
            self.aboutoccupant = AboutOccupant.init(withDictionary: aboutoccupent)
        }
        
        if let flatmate = response["offering_flatmate_prefrences"] as? [String:Any] {
            self.Flatmatepreference = FlatmatePreference.init(withDictionary: flatmate)
        }
        
        if let userdata = response["user"] as? [String:Any] {
            self.Userdata = UserData.init(withDictionary: userdata)
        }
        
        /////// verify /////
        if let email_verified = response[""] as? Bool {
            self.email_verified = email_verified
        }
        
        if let fb_verified = response[""] as? Bool {
            self.fb_verified = fb_verified
        }
        
        if let google_verified = response[""] as? Bool {
            self.google_verified = google_verified
        }
        
        if let linkedin_verified = response[""] as? Bool {
            self.linkedin_verified = linkedin_verified
        }
        
        if let mobile_verified = response[""] as? Bool {
            self.mobile_verified = mobile_verified
        }
        
        if let user_is_active = response[""] as? Bool {
            self.user_is_active = user_is_active
        }
    }
}


class OfferingHome : NSObject{
    
    var home_address = ""
    var home_location = ""
    var latitude = 00.00
    var longitude = 00.00
    var home_type = 0
    var home_size = 0
    var home_feature : Array<Any> = []
    var parking_facitity = 0
    var home_description = ""
    var bill = 0
    var views = 0
    var is_active : Bool = false
    var bill_included = ""
    
    convenience init(withDictionary response:[String:Any]) {
        self.init()
        
        if let home_address = response["home_address"] as? String {
            self.home_address = home_address
        }
        
        if let home_location = response["home_location"] as? String {
            self.home_location = home_location
        }
        
        if let latitude = response["latitude"] as? Double {
            self.latitude = latitude
        }
        
        if let longitude = response["longitude"] as? Double {
            self.longitude = longitude
        }
        
        if let home_type = response["home_type"] as? Int {
            self.home_type = home_type
        }
        
        if let home_size = response["home_size"] as? Int {
            self.home_size = home_size
        }
        
        if let home_feature = response["home_features"] as? String {
            self.home_feature = self.separate(arr: home_feature.components(separatedBy: ","))
        }
        
        if let parking_facitity = response["parking_facility"] as? Int {
            self.parking_facitity = parking_facitity
        }
        
        if let bill = response["bills"] as? Int {
            self.bill = bill
        }
        
        if let is_active = response["is_active"] as? Bool {
            self.is_active = is_active
        }
        
        if let views = response["profile_views"] as? Int {
            self.views = views
        }
        
        if let home_description = response["home_description"] as? String {
            self.home_description = home_description
        }
        
        if let bill_included = response["bills_included"] as? String {
            self.bill_included = bill_included
        }
    }
    
    func separate(arr : [String]) -> Array<Any>{
        var arrInt : Array<Any> = []
        for item in arr{
           let str = item.replacingOccurrences(of: " ", with: "")
            arrInt.append(Int(str)!)
        }
        return arrInt
    }
}



class BedroomDescription : NSObject{
   
    var bedroom_Id = 0
    var avalibality_date = ""
    var minimum_period = 0
    var maximum_period = 0
    var single_person_rent = 0
    var couple_rent = 0
    var rent_duration = 0
    var deposit = 0
    var bill = 0
    var badroom_type = 0
    var badroom_size = 0
    var badroom_furniture = 0
    var bathroom_facitity = 0
    var room_feature : Array<Any> = []
    var no_of_beds = 0
    var other_information = ""
    var country_code = ""
    
    convenience init(withDictionary response:[String:Any]) {
        self.init()
        
        ////// bedroom description ///
        
        if let bedroom_Id = response["id"] as? Int {
            self.bedroom_Id = bedroom_Id
        }
        
        if let avalibality_date = response["available_date"] as? String {
            self.avalibality_date = avalibality_date
        }
        
        if let minimum_period = response["minimum_period"] as? Int {
            self.minimum_period = minimum_period
        }
        
        if let maximum_period = response["maximum_period"] as? Int {
            self.maximum_period = maximum_period
        }
        
        if let single_person_rent = response["single_person_rent"] as? Int {
            self.single_person_rent = single_person_rent
        }
        
        if let couple_rent = response["couple_rent"] as? Int {
            self.couple_rent = couple_rent
        }
        
        if let rent_duration = response["rent_duration"] as? Int {
            self.rent_duration = rent_duration
        }
        
        if let deposit = response["deposit"] as? Int {
            self.deposit = deposit
        }
        
        if let bill = response["bills"] as? Int {
            self.bill = bill
        }
        
        if let badroom_type = response["bedroom_type"] as? Int {
            self.badroom_type = badroom_type
        }
        
        if let badroom_size = response["bedroom_size"] as? Int {
            self.badroom_size = badroom_size
        }
        
        if let badroom_furniture = response["bedroom_furniture"] as? Int {
            self.badroom_furniture = badroom_furniture
        }
        
        if let bathroom_facitity = response["bathroom_facilities"] as? String {
            self.bathroom_facitity = Int(bathroom_facitity)!
        }
        
        if let room_feature = response["room_features"] as? String {
            self.room_feature = self.separate(arr: room_feature.components(separatedBy: ","))
        }
        
        if let no_of_beds = response["no_of_beds"] as? Int {
            self.no_of_beds = no_of_beds
        }
        
        if let other_information = response["other_information"] as? String {
            self.other_information = other_information
        }
        
        if let country_code = response["country_code"] as? String {
            self.country_code = country_code
        }
    }
    
    func separate(arr : [String]) -> Array<Any>{
        var arrInt : Array<Any> = []
        for item in arr{
            let str = item.replacingOccurrences(of: " ", with: "")
            arrInt.append(Int(str)!)
        }
        return arrInt
    }
}



class AboutOccupant : NSObject{
    
    var AO_gendersexuality : Array<Any> = []
    var AO_agegroup : Array<Any> = []
    var AO_smoking : Array<Any> = []
    var AO_pets = 0
    var interest : Array<Any> = []
    var personal_infro = ""
    var personal_qualite = ""
    var is_anyone_living : Bool = false
    
    convenience init(withDictionary response:[String:Any]) {
        self.init()
        
        //////// about occupant ///
        
        if let AO_gendersexuality = response["gender_and_sexuality"] as? String {
            if AO_gendersexuality != ""{
                self.AO_gendersexuality = self.separate(arr: AO_gendersexuality.components(separatedBy: ","))
            }
        }
        
        if let AO_agegroup = response["age_group"] as? String {
            if AO_agegroup != ""{
                self.AO_agegroup = self.separate(arr: AO_agegroup.components(separatedBy: ","))
            }
        }
        
        if let AO_smoking = response["smoking"] as? String {
            if AO_smoking != ""{
                self.AO_smoking = self.separate(arr: AO_smoking.components(separatedBy: ","))
            }
        }
        
        if let AO_pets = response["pets"] as? Int {
            self.AO_pets = AO_pets
        }
        
        if let is_anyone_living = response["is_anyone_living"] as? Bool {
            self.is_anyone_living = is_anyone_living
        }
        
        if let interest = response["main_interest"] as? String {
            if interest != ""{
                self.interest = self.separate(arr: interest.components(separatedBy: ","))
            }
        }
        
        if let personal_infro = response["personal_introduction"] as? String {
            self.personal_infro = personal_infro
        }
        
        if let personal_qualite = response["personal_qualities"] as? String {
            self.personal_qualite = personal_qualite
        }
        
    }
    
    func separate(arr : [String]) -> Array<Any>{
        var arrInt : Array<Any> = []
        for item in arr{
            let str = item.replacingOccurrences(of: " ", with: "")
            arrInt.append(Int(str)!)
        }
        return arrInt
    }
}


