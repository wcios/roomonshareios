//
//  LookingProfileDetailData.swift
//  RoomOnShare
//
//  Created by mac on 03/09/18.
//  Copyright © 2018 WestCoast. All rights reserved.
//

import Foundation


class Images: NSObject {
    var url = ""
    var id = 0
    convenience init(withDictionary response:[String:Any]) {
        self.init()
        
        if let url = response["photo"] as? String
        {
            self.url = url
        }
        
        if let id = response["add_photo_id"] as? Int
        {
            self.id = id
        }
    }
}

class LookingProfileDetailData: NSObject {
    
    var arrImages = [Images]()
    var yourDescription  = YourDescription()
    var Introduceyourself = IntroduceYourSelf()
    var Preferedlocation = PreferedLocation()
    var Homepreference = HomePreference()
    var Flatmatepreference = FlatmatePreference()
    var Userdata = UserData()
    
    var email_verified = false
    var fb_verified = false
    var google_verified = false
    var linkedin_verified = false
    var mobile_verified = false
    var user_is_active = false
    
    convenience init(withDictionary response:[String:Any]) {
        self.init()
        
        if let dicimages = response["photos"] as? NSDictionary {
            let cover_arr = dicimages.value(forKey: "upload_photo") as! [[String:Any]]
            for data in cover_arr {
                self.arrImages.append(Images.init(withDictionary:data))
            }
        }
        
        if let yourdescription = response["looking_your_description"] as? [String:Any] {
            self.yourDescription = YourDescription.init(withDictionary: yourdescription)
        }
        
        if let introduceyourself = response["looking_introduce_yourself"] as? [String:Any] {
            self.Introduceyourself = IntroduceYourSelf.init(withDictionary: introduceyourself)
        }
        
        if let Preferedlocation = response["prefered_location"] as? [String:Any] {
            self.Preferedlocation = PreferedLocation.init(withDictionary: Preferedlocation)
        }
        
        if let Homepreference = response["looking_home_preferences"] as? [String : Any] {
            self.Homepreference = HomePreference.init(withDictionary: Homepreference)
        }
        
        if let Flatmatepreference = response["looking_flatmate_prefrences"] as? [String:Any] {
            self.Flatmatepreference = FlatmatePreference.init(withDictionary: Flatmatepreference)
        }
        
        if let userdata = response["user"] as? [String:Any] {
            self.Userdata = UserData.init(withDictionary: userdata)
        }
        
        /////// verify /////
        if let email_verified = response[""] as? Bool {
            self.email_verified = email_verified
        }
        
        if let fb_verified = response[""] as? Bool {
            self.fb_verified = fb_verified
        }
        
        if let google_verified = response[""] as? Bool {
            self.google_verified = google_verified
        }
        
        if let linkedin_verified = response[""] as? Bool {
            self.linkedin_verified = linkedin_verified
        }
        
        if let mobile_verified = response[""] as? Bool {
            self.mobile_verified = mobile_verified
        }
        
        if let user_is_active = response[""] as? Bool {
            self.user_is_active = user_is_active
        }
    }
    
    func separate(arr : [String]) -> Array<Any>{
        var arrInt : Array<Any> = []
        for item in arr{
            arrInt.append(Int(item)!)
        }
        return arrInt
    }
}


class YourDescription : NSObject{

    var no_of_adult = 0
    var gender = 0
    var sexuality = 0
    var age_group = 0
    var employment_situation = 0
    var smoking_type = 0
    var pets_type = 0
    var views = 0
    var is_active : Bool = false
    var interest : Array<Any> = []
    
    convenience init(withDictionary response:[String:Any]) {
        self.init()
        
        if let no_of_adult = response["no_of_adults"] as? Int {
            self.no_of_adult = no_of_adult
        }
        
        if let gender = response["gender"] as? Int {
            self.gender = gender
        }
        
        if let sexuality = response["sexuality"] as? Int {
            self.sexuality = sexuality
        }
        
        if let age_group = response["age_group"] as? Int {
            self.age_group = age_group
        }
        
        if let employment_situation = response["employment_situation"] as? Int {
            self.employment_situation = employment_situation
        }
        
        if let smoking_type = response["smoking"] as? Int {
            self.smoking_type = smoking_type
        }
        
        if let pets_type = response["pets"] as? Int {
            self.pets_type = pets_type
        }
        
        if let views = response["profile_views"] as? Int {
            self.views = views
        }
        
        if let is_active = response["is_active"] as? Bool {
            self.is_active = is_active
        }
        
        if let interest = response["main_interest"] as? String {
            if interest != ""{
                self.interest = self.separate(arr: interest.components(separatedBy: ","))
            }
        }
    }
    
    func separate(arr : [String]) -> Array<Any>{
        var arrInt : Array<Any> = []
        for item in arr{
            arrInt.append(Int(item)!)
        }
        return arrInt
    }
}


class IntroduceYourSelf : NSObject{
    
    var avalibality_date = ""
    var prefered_length_stay = 0
    var important_personal_quality = ""
    
    convenience init(withDictionary response:[String:Any]) {
        self.init()
        if let avalibality_date = response["available_date"] as? String {
            self.avalibality_date = avalibality_date
        }
        
        if let prefered_length_stay = response["stay_duration"] as? String {
            self.prefered_length_stay = Int(prefered_length_stay)!
        }
        
        if let important_personal_quality = response["personal_qualities"] as? String {
            self.important_personal_quality = important_personal_quality
        }
    }
}

class PreferedLocation  : NSObject{
    
    var latitude = 00.00
    var longitude = 00.00
    var address = ""
    
    convenience init(withDictionary response:[String:Any]) {
        self.init()
        
        if let latitude = response["latitude"] as? Double {
            self.latitude = latitude
        }
        
        if let longitude = response["longitude"] as? Double {
            self.longitude = longitude
        }
        
        if let address = response["address"] as? String {
            self.address = address
        }
    }
}

class HomePreference : NSObject{
    
    var rent_budget = 0
    var home_size : Array<Any> = []
    var badroom_type = 0
    var badroom_size : Array<Any> = []
    var badroom_facitity = 0
    var parking_facitity = 0
    
    convenience init(withDictionary response:[String:Any]) {
        self.init()
        
        if let rent_budget = response["rent_budget"] as? String {
            self.rent_budget = Int(Double(rent_budget)!)
        }
        
        if let home_size = response["home_size"] as? String {
            self.home_size = self.separate(arr: home_size.components(separatedBy: ","))
        }
        
        if let badroom_type = response["bedroom_type"] as? String {
            self.badroom_type = Int(badroom_type)!
        }
        
        if let badroom_size = response["bedroom_size"] as? String {
            self.badroom_size = self.separate(arr: badroom_size.components(separatedBy: ","))
        }
        
        if let badroom_facitity = response["bathroom_facilities"] as? String {
            self.badroom_facitity = Int(badroom_facitity)!
        }
        
        if let parking_facitity = response["parking_facility"] as? String {
            self.parking_facitity = Int(parking_facitity)!
        }
    }
    
    func separate(arr : [String]) -> Array<Any>{
        var arrInt : Array<Any> = []
        for item in arr{
            arrInt.append(Int(item)!)
        }
        return arrInt
    }
}

class FlatmatePreference : NSObject{
    
    var F_gendersexuality : Array<Any> = []
    var F_agegroup : Array<Any> = []
    var F_smoking : Array<Any> = []
    var F_pets : Array<Any> = []
    
    convenience init(withDictionary response:[String:Any]) {
        self.init()
        if let F_gendersexuality = response["gender_and_sexuality"] as? String {
            self.F_gendersexuality = self.separate(arr: F_gendersexuality.components(separatedBy: ","))
        }
        
        if let F_agegroup = response["age_group"] as? String {
            self.F_agegroup = self.separate(arr: F_agegroup.components(separatedBy: ","))
        }
        
        if let F_smoking = response["smoking"] as? String {
            self.F_smoking = self.separate(arr: F_smoking.components(separatedBy: ","))
        }
        
        if let F_pets = response["pets"] as? String {
            self.F_pets = self.separate(arr: F_pets.components(separatedBy: ","))
        }
    }
    func separate(arr : [String]) -> Array<Any>{
        var arrInt : Array<Any> = []
        for item in arr{
            arrInt.append(Int(item)!)
        }
        return arrInt
    }
}




//{
//    Id = 2;
//    "looking_flatmate_prefrences" =                 {
//        "age_group" = "1,2";
//        "created_at" = "2018-09-05 11:31:19";
//        "gender_and_sexuality" = "1,2,3";
//        id = 8;
//        "looking_home_id" = 40;
//        pets = 1;
//        "profile_id" = 1;
//        smoking = "1,3";
//        "updated_at" = "2018-09-05 11:31:19";
//        "user_id" = 21;
//    };
//    "looking_home_preferences" =                 {
//        "bathroom_facilities" = 2;
//        "bedroom_size" = "2,3";
//        "bedroom_type" = 1;
//        "created_at" = "2018-09-04 07:37:58";
//        "home_size" = "1,2,3";
//        id = 10;
//        "looking_home_id" = 23;
//        "parking_facility" = 2;
//        "profile_id" = 2;
//        "rent_budget" = 1;
//        "updated_at" = "2018-09-04 08:23:43";
//        "user_id" = 21;
//    };
//    "looking_introduce_yourself" =                 {
//        "available_date" = "06 Sep 2018";
//        "created_at" = "2018-09-03 14:26:59";
//        id = 3;
//        "looking_home_id" = 23;
//        "personal_qualities" = "dad dad";
//        "profile_id" = 2;
//        "stay_duration" = 2;
//        "updated_at" = "2018-09-05 05:49:28";
//        "user_id" = 21;
//    };
//    "looking_your_description" =                 {
//        "age_group" = 1;
//        "created_at" = "2018-08-28 09:43:30";
//        "emplyoment_situation" = 0;
//        gender = 0;
//        id = 2;
//        "main_interset" = "";
//        "no_of_adults" = 0;
//        pets = 1;
//        "profile_id" = 2;
//        sexuality = 0;
//        smoking = 1;
//        "updated_at" = "2018-08-28 09:43:30";
//        "user_id" = 21;
//    };
//    photos =                 {
//        "upload_photo" =                     (
//            {
//                "add_photo_id" = 9;
//                "created_at" = "2018-09-05 06:21:00";
//                id = 18;
//                "looking_home_id" = 23;
//                photo = "photogallry/Nn3G8O23fRG2QjZczUWrT0M7EVWeto38sZTS9dPt.jpeg";
//                "updated_at" = "2018-09-05 06:21:00";
//            }
//        );
//    };
//    "prefered_location" =                 {
//        address = "Testour, Tunisia";
//        "created_at" = "2018-09-04 10:05:00";
//        id = 3;
//        latitude = "22.7196";
//        longitude = "75.8577";
//        "looking_home_id" = 23;
//        "profile_id" = 2;
//        "updated_at" = "2018-09-04 10:17:57";
//        "user_id" = 21;
//    };
