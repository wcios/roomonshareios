import UIKit
import GoogleMaps
import GooglePlaces
import CoreLocation

class profileFlatmateViewController: UIViewController {

    @IBOutlet weak var scrollViewImgs: UIScrollView!{
        didSet{
            scrollViewImgs.delegate = self
        }
    }
    
    @IBOutlet weak var lblHeader: UILabel!
    @IBOutlet var imgViews : [UIImageView]!
    @IBOutlet weak var lblAddress : UILabel!
    @IBOutlet weak var lblLocation: UILabel!
    @IBOutlet weak var lblPrice : UILabel!
    @IBOutlet weak var lblHometype: UILabel!
    @IBOutlet weak var lblhomeSize: UILabel!
    @IBOutlet weak var lblParking: UILabel!
    @IBOutlet weak var imgHometype: UIImageView!
    @IBOutlet weak var imgParkingtype: UIImageView!
    @IBOutlet weak var imgBathroom: UIImageView!
    @IBOutlet weak var lblbathroom: UILabel!
    
    //Bedroom
    @IBOutlet weak var lblBedroomType: UILabel!
    @IBOutlet weak var lblRentSingle : UILabel!
    @IBOutlet weak var lblRentCouple : UILabel!
    @IBOutlet weak var lblDateAvail : UILabel!
    @IBOutlet weak var lblStayLength : UILabel!
    @IBOutlet weak var lblBedroomSize : UILabel!
    @IBOutlet weak var lblBedroomFurniture : UILabel!
    @IBOutlet weak var lblNoOfBeds : UILabel!
    @IBOutlet var btnBathroom : [UIButton]!
    @IBOutlet weak var collRoomFeatures : UICollectionView!
    @IBOutlet weak var lblSecurity : UILabel!
    var id_Bathroom = 0
    
    //Home Description
    @IBOutlet weak var lblHomeDescription : UILabel!
    @IBOutlet weak var lblBills : UILabel!
    @IBOutlet weak var collHomeFeatures : UICollectionView!
    @IBOutlet weak var lblstatus : UILabel!
    @IBOutlet weak var btnStatus: UIButton!
    var is_status : Bool = false
    
    //About Occupants
    @IBOutlet weak var lblGender : UILabel!
    @IBOutlet weak var lblAge : UILabel!
    @IBOutlet weak var lblEmployment : UILabel!
    @IBOutlet weak var lblAboutOccupants : UILabel!
    @IBOutlet var btnSmoking : [UIButton]!
    @IBOutlet var btnPets : [UIButton]!
    @IBOutlet weak var collMainInterst : UICollectionView!
    var id_Pets = 0
    var id_Smoking = 0
    
    //Flatmate Prefernce
    @IBOutlet weak var lblGender_Flat : UILabel!
    @IBOutlet weak var lblAge_Flat : UILabel!
    @IBOutlet var btnSmoking_Flat : [UIButton]!
    @IBOutlet var btnPets_Flat : [UIButton]!
    var id_Pets_Flat = 0
    var id_Smoking_Flat = 0
    
    //MAp
    @IBOutlet weak var viewMap: GMSMapView!
    var locationManager:CLLocationManager!
    var latitude : Double = 0000
    var longitude : Double = 0000
    
    var slides:[imageSliderView] = [];
    var arrcountries : [country] = [country]()
    //var imagesArray = [UIImage(named: "Banner")!, UIImage(named: "WalkThrough1")!, UIImage(named: "Banner")!,UIImage(named: "WalkThrough1")!,UIImage(named: "Banner")!]
    
    var dicOfferingDetail : NSDictionary = [:]
    var arrbedroomfeature : Array<Any> = []
    var arrhomefeature : Array<Any> = []
    var arrMaininterest : Array<Any> = []
    var imagesArray : [Images] = []
    var userdata = SharedPreference.getUserData()
    
    var OfferingData = ProfileFlatmateDetailData()

    override func viewDidLoad() {
        super.viewDidLoad()
        determineMyCurrentLocation()
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        self.navigationController?.isNavigationBarHidden = true
        //HHTabBarView.toggleShowOrHide(<#T##HHTabBarView#>)
       // rdv_tabBarController?.setTabBarHidden(false, animated: true)
        
        print(self.dicOfferingDetail)
        
        self.ShowData(dicData: self.dicOfferingDetail)
    }
   
    func ShowData(dicData : NSDictionary) {
        if dicData.value(forKey: "profile_id") as! Int == 1 {
            /////////////// offering ///////////
            
            self.OfferingData = ProfileFlatmateDetailData.init(withDictionary: dicData as! [String : Any])
            
            /// Images
            self.imagesArray = OfferingData.arrImages
            self.createSlides()
            setupSlideScrollView(slides: slides)
            for i in 0..<imagesArray.count{
                let img = imagesArray[i]
                imgViews[i].sd_setImage(with: URL(string: img.url), placeholderImage: UIImage(named: "Banner"))
            }
            
            print(OfferingData.homedescription.is_active)
            
            self.is_status = OfferingData.homedescription.is_active
            if self.is_status{
                self.lblstatus.text = "Active"
                self.lblstatus.textColor = self.UIColorFromHex(rgbValue: 0x44aa46, alpha: 1.0)
                self.btnStatus.setTitle("Deactive", for: .normal)
                self.btnStatus.backgroundColor = self.UIColorFromHex(rgbValue: 0xD44936, alpha: 1.0)
            }else{
                self.lblstatus.text = "Deactive"
                self.lblstatus.textColor = self.UIColorFromHex(rgbValue: 0xD44936, alpha: 1.0)
                self.btnStatus.setTitle("Active", for: .normal)
                self.btnStatus.backgroundColor = self.UIColorFromHex(rgbValue: 0x44aa46, alpha: 1.0)
            }
            
            /////// bedroom /////
            if OfferingData.bedroomdescription.count != 0{
                var count = 0
                var check : Bool = true
                for item in OfferingData.bedroomdescription{
                    if item.bathroom_facitity == 1{
                        if check{
                            count = count + 1
                            check = false
                        }
                    }else{
                        count = count + 1
                    }
                }
                print(count)
                self.lblbathroom.text = "\(count) bath"
                let dicBedroom = OfferingData.bedroomdescription[0]
                self.lblBedroomType.text = Array_File.arrBedroomtype[dicBedroom.badroom_type - 1]
                
                self.arrcountries = countrydetail.readCountryJson()
                for item in self.arrcountries {
                    if(self.userdata.user_country == item.Country_code){
                        let curr = item.currency_code
                        self.lblPrice.text = "\(curr) \(dicBedroom.single_person_rent)"
                        self.lblRentSingle.text = String("Single \(curr) \(dicBedroom.single_person_rent) Per Month")
                        self.lblRentCouple.text = String("Couple \(curr) \(dicBedroom.couple_rent) Per Month")
                    }
                }
                
                self.lblDateAvail.text = Common.ChangeDateFormat(Date: dicBedroom.avalibality_date, fromFormat: "yyyy-MM-dd", toFormat: "dd MMM")
                
                self.lblStayLength.text = Array_File.arrLength_of_stay[dicBedroom.rent_duration - 1]
                
                
                
                
                self.arrbedroomfeature = dicBedroom.room_feature
                if dicBedroom.badroom_type == 2{
                    self.lblNoOfBeds.text = Array_File.arrNoOfBeds[dicBedroom.no_of_beds - 1]
                }else{
                    self.lblBedroomSize.text = Array_File.arrBedroomSize[dicBedroom.badroom_size - 1]
                    self.lblBedroomFurniture.text = Array_File.arrBedroomFurniture[dicBedroom.badroom_furniture - 1]
                }
                self.collRoomFeatures.reloadData()
                self.lblSecurity.text =  Array_File.arrDeposit[dicBedroom.deposit - 1]
                self.ShowCheckUnceck(arrButton: self.btnBathroom, Selected: dicBedroom.bathroom_facitity)
            }
            ////// home description /////
            
            self.lblAddress.text = self.OfferingData.homedescription.home_address
            self.lblLocation.text = self.OfferingData.homedescription.home_location
            //self.lblParking.text = //Array_File.arrHomeType[self.OfferingData.homedescription.parking_facitity - 1]
            if self.OfferingData.homedescription.parking_facitity == 1{
                self.imgParkingtype.image = #imageLiteral(resourceName: "covered-parking")
                //self.lblParking.text = "Covered Off Street"
            }else if self.OfferingData.homedescription.parking_facitity == 2 {
                self.imgParkingtype.image = #imageLiteral(resourceName: "Parking")
                //self.lblParking.text = "Uncovered Off Street"
            }else{
                self.imgParkingtype.image = #imageLiteral(resourceName: "no-parking")
                //self.lblParking.text = "No Off Street Parking"
            }
            self.lblParking.text = "Parking"
            self.lblHometype.text = Array_File.arrHomeType[self.OfferingData.homedescription.home_type - 1]
            if self.OfferingData.homedescription.home_type == 2{
                self.imgHometype.image = #imageLiteral(resourceName: "apartment")
            }else{
                self.imgHometype.image = #imageLiteral(resourceName: "Dhouse")
            }
            
            //self.lblhomeSize.text = Array_File.arrHomeSize[self.OfferingData.homedescription.home_size - 1]
            self.lblHomeDescription.text = self.OfferingData.homedescription.home_description
            self.lblBills.text = self.OfferingData.homedescription.bill_included
            self.arrhomefeature = self.OfferingData.homedescription.home_feature
            self.collHomeFeatures.reloadData()
            
            /////  about occupants ///
            
            let gender : NSMutableArray = []
            for item in self.OfferingData.aboutoccupant.AO_gendersexuality{
                gender.add(Array_File.arrGenderSexuality[item as! Int - 1])
            }
            self.lblGender.text = gender.componentsJoined(by: ",")
            
            let age : NSMutableArray = []
            for item in self.OfferingData.aboutoccupant.AO_agegroup{
                age.add(Array_File.arrAgeGroup[item as! Int - 1])
            }
            self.lblAge.text = age.componentsJoined(by: ",")
            
            let arrsmok = self.OfferingData.aboutoccupant.AO_smoking
            for item in arrsmok{
                self.ShowCheckUnceck(arrButton: self.btnSmoking, Selected: item as! Int)
            }
            print(OfferingData.aboutoccupant.AO_pets)
            self.ShowCheckUnceck(arrButton: self.btnPets, Selected: OfferingData.aboutoccupant.AO_pets)
            
            self.lblAboutOccupants.text = OfferingData.aboutoccupant.personal_infro
            self.arrMaininterest = self.OfferingData.aboutoccupant.interest
            if self.arrMaininterest.count != 0{
                self.collMainInterst.reloadData()
            }
            
            ////// flatmate preferance /////
            let Fgender : NSMutableArray = []
            for item in OfferingData.Flatmatepreference.F_gendersexuality{
                Fgender.add(Array_File.arrGenderSexuality[item as! Int - 1])
            }
            self.lblGender_Flat.text = Fgender.componentsJoined(by: ",")
            
            let Fage : NSMutableArray = []
            for item in OfferingData.Flatmatepreference.F_agegroup{
                Fage.add(Array_File.arrAgeGroup[item as! Int - 1])
            }
            self.lblAge_Flat.text = Fage.componentsJoined(by: ",")
            
            let Farrsmok = OfferingData.Flatmatepreference.F_smoking
            for item in Farrsmok{
                self.ShowCheckUnceck(arrButton: self.btnSmoking_Flat, Selected: item as! Int)
            }
            
            let Fpets = OfferingData.Flatmatepreference.F_pets
            for item in Fpets{
                self.ShowCheckUnceck(arrButton: self.btnPets_Flat, Selected: item as! Int)
            }
        
            let lati = OfferingData.homedescription.latitude
            let long = OfferingData.homedescription.longitude
            self.ShowMarker(lati: lati, long: long)
        }
    }
    
    @IBAction func clickOnDeactivateBtn(_ sender : UIButton){
        if self.lblstatus.text == "Deactive"{
            self.is_status = true
            self.ChangeStatusService()
        }else{
            self.is_status = false
            self.ChangeStatusService()
        }
    }
    
    
    func ChangeStatusService() {
        self.startActivityIndicator()
        let param = ["profile_id"    : 1,
                     "looking_home_id" : self.dicOfferingDetail.value(forKey: "Id") as! Int,
                     "is_active" : self.is_status
            ] as [String : Any]
        print(param)
        
        CommunicationManager().getResponseForPost(strUrl: Url_File.ChangeStatus, parameters: param as NSDictionary) { ( result , data) in
            
            if(result == "success") {
                let dataDict = data as! NSDictionary
                print(dataDict)
                if self.is_status{
                    self.lblstatus.text = "Active"
                    self.lblstatus.textColor = self.UIColorFromHex(rgbValue: 0x44aa46, alpha: 1.0)
                    self.btnStatus.setTitle("Deactive", for: .normal)
                    self.btnStatus.backgroundColor = self.UIColorFromHex(rgbValue: 0xD44936, alpha: 1.0)
                }else{
                    self.lblstatus.text = "Deactive"
                    self.lblstatus.textColor = self.UIColorFromHex(rgbValue: 0xD44936, alpha: 1.0)
                    self.btnStatus.setTitle("Active", for: .normal)
                    self.btnStatus.backgroundColor = self.UIColorFromHex(rgbValue: 0x44aa46, alpha: 1.0)
                }
                
                self.stopActivityIndicator()
            }else if (result == "error") {
                let strMessage = (data as! NSDictionary)["message"] as! String
                Common.ShowAlert(Title: Common.Title, Message: strMessage, VC: self)
                self.stopActivityIndicator()
            }else if (result == "Network") {
                let strMessage = (data as! String)
                Common.ShowAlert(Title: Common.Title, Message: strMessage, VC: self)
                self.stopActivityIndicator()
            }
        }
    }
    
    func ShowCheckUnceck(arrButton : [UIButton], Selected : Int) {
        for button in arrButton {
            let tag = button.tag
            if Selected == tag{
                button.isSelected = true
            }
        }
    }
    
    @IBAction func clickOnAddBedroomBtn(_ sender : UIButton){
        
    }
    
    @IBAction func clickOnEditProprtyBtn(_ sender : UIButton){
        
         SharedPreference.setOffering_id(self.dicOfferingDetail.value(forKey: "Id") as! Int)
        
        /// offering home
        offeringHomeDescriptionVC.Shomeaddress = self.OfferingData.homedescription.home_address
        offeringHomeDescriptionVC.Shomelocation = self.OfferingData.homedescription.home_location
        offeringHomeDescriptionVC.Shometype = self.OfferingData.homedescription.home_type
        offeringHomeDescriptionVC.Shomesize = self.OfferingData.homedescription.home_size
        offeringHomeDescriptionVC.Shomefeature = self.OfferingData.homedescription.home_feature
        offeringHomeDescriptionVC.SParking = self.OfferingData.homedescription.parking_facitity
        offeringHomeDescriptionVC.Shomedescription = self.OfferingData.homedescription.home_description
        offeringHomeDescriptionVC.Sbill  = self.OfferingData.homedescription.bill
        offeringHomeDescriptionVC.Sbillinclude = self.OfferingData.homedescription.bill_included
        
        userDef.set(nil, forKey: "bedroomarray")
        var arr : Array<Any> = []
        for i in 0...OfferingData.bedroomdescription.count - 1 {
            let dicBedroom = OfferingData.bedroomdescription[i]
            
            let roomfeature : NSMutableArray = []
            for item in dicBedroom.room_feature{
                roomfeature.add(item)
            }
            let roomfeatur = roomfeature.componentsJoined(by: ",")
                        
            let dict = [
                "id" : dicBedroom.bedroom_Id,
                "available_date": dicBedroom.avalibality_date,
                "minimum_period": dicBedroom.minimum_period,
                "maximum_period": dicBedroom.maximum_period,
                "single_person_rent": "\(dicBedroom.single_person_rent)",
                "couple_rent": "\(dicBedroom.couple_rent)",
                "rent_duration": dicBedroom.rent_duration,
                "bills": dicBedroom.bill,
                "deposit": dicBedroom.deposit,
                "bedroom_type": dicBedroom.badroom_type,
                "bedroom_size": dicBedroom.badroom_size,
                "bedroom_furniture": dicBedroom.badroom_furniture,
                "bathroom_facilities": dicBedroom.bathroom_facitity,
                "room_features": roomfeatur ,
                "no_of_beds": dicBedroom.no_of_beds,
                "other_information": dicBedroom.other_information,
                "country_code": dicBedroom.country_code
                ] as [String : Any]
            print(dict)
            arr.append(dict)
        }
        userDef.set(arr, forKey: "bedroomarray")
        
        ///// about
        AboutOccupantsViewController.Sid_Gender = self.OfferingData.aboutoccupant.AO_gendersexuality
        AboutOccupantsViewController.Sid_AgeGroup = self.OfferingData.aboutoccupant.AO_agegroup
        AboutOccupantsViewController.Sid_Smoking = self.OfferingData.aboutoccupant.AO_smoking
        AboutOccupantsViewController.Sid_Pet  = self.OfferingData.aboutoccupant.AO_pets
        AboutOccupantsViewController.SPersonalInfo = self.OfferingData.aboutoccupant.personal_infro
        AboutOccupantsViewController.SPersonalquality = self.OfferingData.aboutoccupant.personal_qualite
        AboutOccupantsViewController.SarrMoreinterest = self.OfferingData.aboutoccupant.interest
        
        /// images
        let images = self.dicOfferingDetail.value(forKey: "photos") as! NSDictionary
        AddPhotoViewController.SarrPhoto = images.value(forKey: "upload_photo") as! Array<Any>
        
        ////// flatemate
        FlatmatePreferenceViewController.Sid_Gender = OfferingData.Flatmatepreference.F_gendersexuality //
        FlatmatePreferenceViewController.Sid_AgeGroup = OfferingData.Flatmatepreference.F_agegroup
        FlatmatePreferenceViewController.Sid_Pet = OfferingData.Flatmatepreference.F_pets
        FlatmatePreferenceViewController.Sid_Smoking = OfferingData.Flatmatepreference.F_smoking
        
        let storyboard = UIStoryboard(name: "Main", bundle: nil)
        let vc = storyboard.instantiateViewController(withIdentifier: "offeringHomeDescriptionVC") as! offeringHomeDescriptionVC
        self.navigationController?.pushViewController(vc, animated: true)        
    }
    
    @IBAction func BackMethod(_ sender: Any) {
        Common.PopMethod(VC: self)
    }
}

//MArk - Image slider code
extension profileFlatmateViewController : UIScrollViewDelegate{
    
    func createSlides(){
        for i in 0..<imagesArray.count{
            let slide1 = Bundle.main.loadNibNamed("imageSliderView", owner: self, options: nil)?.first as! imageSliderView
            let img = imagesArray[i]
            slide1.imageView.sd_setImage(with: URL(string: img.url), placeholderImage: UIImage(named: "Banner"))            
            slide1.btnNExt.addTarget(self, action: #selector(clickOnNExtArrowBtn(_:)), for: .touchUpInside)
            slide1.btnPrevious.addTarget(self, action: #selector(clickOnPreviousArrowBtn(_:)), for: .touchUpInside)
            
            if(i==0){
                slide1.btnPrevious.isHidden=true
            }
            if(i==4){
                slide1.btnNExt.isHidden=true
            }
            
            slides.append(slide1)
        }
        print(slides.count)
    }
    
    func setupSlideScrollView(slides : [imageSliderView]) {
        scrollViewImgs.contentSize = CGSize(width: scrollViewImgs.frame.width * CGFloat(slides.count), height:175)
        scrollViewImgs.isPagingEnabled = true
        for i in 0 ..< slides.count {
            slides[i].frame = CGRect(x: scrollViewImgs.frame.width * CGFloat(i), y: 0, width: scrollViewImgs.frame.width, height: scrollViewImgs.frame.height)
            scrollViewImgs.addSubview(slides[i])
        }
    }
 
    @objc func clickOnPreviousArrowBtn(_ sender : UIButton) {
        if scrollViewImgs.contentOffset.x > 0 {
            sender.isHidden=false
            scrollViewImgs.contentOffset.x -=  scrollViewImgs.bounds.width
        }else{
            sender.isHidden=true
        }
    }
    
    @objc func clickOnNExtArrowBtn(_ sender : UIButton){
        let scrollContnt = scrollViewImgs.bounds.width * CGFloat(slides.count-1)
        if (scrollViewImgs.contentOffset.x) < (scrollContnt) {
            sender.isHidden=false
            scrollViewImgs.contentOffset.x +=  scrollViewImgs.bounds.width
        }else{
            sender.isHidden=true
        }
        
    }
}



//Map code
//MArk - location

extension profileFlatmateViewController : CLLocationManagerDelegate{
    func determineMyCurrentLocation() {
        locationManager = CLLocationManager()
        locationManager.delegate = self
        locationManager.desiredAccuracy = kCLLocationAccuracyBest
        locationManager.requestAlwaysAuthorization()
        
        if CLLocationManager.locationServicesEnabled() {
            locationManager.startUpdatingLocation()
            //locationManager.startUpdatingHeading()
        }
    }
    
    func locationManager(_ manager: CLLocationManager, didUpdateLocations locations: [CLLocation]){
        let userLocation:CLLocation = locations[0] as CLLocation
        
        print("user latitude = \(userLocation.coordinate.latitude)")
        print("user longitude = \(userLocation.coordinate.longitude)")
        
        self.latitude = userLocation.coordinate.latitude
        self.longitude = userLocation.coordinate.longitude
    }
    
    func locationManager(_ manager: CLLocationManager, didFailWithError error: Error){
        print("Error \(error)")
    }
    
    func ShowMarker(lati : Double, long : Double){
        let camera = GMSCameraPosition.camera(withLatitude: lati, longitude: long, zoom: 6.0)
        self.viewMap.camera = camera
        
        // Creates a marker in the center of the map.
        let marker = GMSMarker()
        marker.position = CLLocationCoordinate2D(latitude: lati, longitude: long)
        marker.map = self.viewMap
    }
}


//MArk - Collectionview code
extension profileFlatmateViewController : UICollectionViewDelegate , UICollectionViewDataSource , UICollectionViewDelegateFlowLayout{
    
    func numberOfSections(in collectionView: UICollectionView) -> Int {
        return 1
        
    }
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int{
        var count:Int?
        if(collectionView == self.collRoomFeatures){
           count = self.arrbedroomfeature.count
        }
        
        if(collectionView == self.collHomeFeatures){
            count = self.arrhomefeature.count
        }
        if(collectionView == self.collMainInterst){
            count = self.arrMaininterest.count
        }
        return count!
       
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        
        let cell : UICollectionViewCell = UICollectionViewCell()
        
        if collectionView == self.collRoomFeatures {
            let cell = collRoomFeatures.dequeueReusableCell(withReuseIdentifier: "collRoomFeatureCell", for: indexPath) as! collRoomFeatureCell
            
            let arrmain = Array_File.arrroomFeaturesImgs
            for j in 0...arrmain.count - 1{
                let id = self.arrbedroomfeature[indexPath.row] as! Int
                if j == id{
                    cell.isSelected=true
                  //  self.collRoomFeatures.selectItem(at: indexPath, animated: true, scrollPosition: UICollectionViewScrollPosition.top)
                    cell.imgInterets.image = UIImage(named:  Array_File.arrroomFeaturesImgs[j]["image"]!)?.imageWithInsets(insetDimen: 8)
                    cell.imgInterets.tintImageColor(color: UIColorFromHex(rgbValue: 0x05C6CF, alpha: 1.0))
                    cell.lblInteretsName.text = Array_File.arrroomFeaturesImgs[j]["name"]!
                }
            }
            return cell
        }
            
         if collectionView == self.collHomeFeatures{
            let cell = collHomeFeatures.dequeueReusableCell(withReuseIdentifier: "collHomeFeatureCell", for: indexPath) as! collHomeFeatureCell
            let arrmain = Array_File.arrHomeDescriptionOptionImgs
            for j in 0...arrmain.count - 1{
                let id = self.arrhomefeature[indexPath.row] as! Int
                if j == id{
                    cell.isSelected=true
                   // self.collRoomFeatures.selectItem(at: indexPath, animated: true, scrollPosition: UICollectionViewScrollPosition.top)
                    cell.imgInterets.image = UIImage(named:  Array_File.arrHomeDescriptionOptionImgs[j]["image"]!)?.imageWithInsets(insetDimen: 8)
                    cell.imgInterets.tintImageColor(color: UIColorFromHex(rgbValue: 0x05C6CF, alpha: 1.0))
                    cell.lblInteretsName.text = Array_File.arrHomeDescriptionOptionImgs[j]["name"]!
                }
            }
             return cell
        }
        
         if collectionView == self.collMainInterst{
            let cell = collMainInterst.dequeueReusableCell(withReuseIdentifier: "collMainInterstCell", for: indexPath) as! collMainInterstCell
            
            let arrmain = Array_File.arrIntesrestOptionImgs
            for j in 0...arrmain.count - 1{
                let id = self.arrMaininterest[indexPath.row] as! Int
                if j == id {
                    cell.isSelected=true
                   // self.collRoomFeatures.selectItem(at: indexPath, animated: true, scrollPosition: UICollectionViewScrollPosition.top)
                    cell.imgInterets.image = UIImage(named:  Array_File.arrIntesrestOptionImgs[j]["image"]!)?.imageWithInsets(insetDimen: 8)
                    cell.imgInterets.tintImageColor(color: UIColorFromHex(rgbValue: 0x05C6CF, alpha: 1.0))
                    cell.lblInteretsName.text = Array_File.arrIntesrestOptionImgs[j]["name"]!
                }
            }
             return cell
        }
        
        return cell        
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        let yourWidth = collectionView.bounds.width/4.0
        return CGSize(width: yourWidth, height: 110.0)
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, insetForSectionAt section: Int) -> UIEdgeInsets {
        return UIEdgeInsets.zero
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumInteritemSpacingForSectionAt section: Int) -> CGFloat {
        return 0
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumLineSpacingForSectionAt section: Int) -> CGFloat {
        return 0
    }
}

class collMainInterstCell : UICollectionViewCell{
    
    @IBOutlet weak var imgInterets : UIImageView!
    @IBOutlet weak var lblInteretsName : UILabel!
}

class collRoomFeatureCell : UICollectionViewCell{
    
    @IBOutlet weak var imgInterets : UIImageView!
    @IBOutlet weak var lblInteretsName : UILabel!
}
class collHomeFeatureCell : UICollectionViewCell{
    
    @IBOutlet weak var imgInterets : UIImageView!
    @IBOutlet weak var lblInteretsName : UILabel!
}



//{
//    Id = 16;
//    "offering_flatmate_prefrences" =                 {
//        "age_group" = "1,2";
//        "created_at" = "2018-09-11 08:13:43";
//        "gender_and_sexuality" = "1,2";
//        id = 5;
//        "offering_home_id" = 16;
//        pets = "1,2";
//        "profile_id" = 1;
//        smoking = "1,2";
//        "updated_at" = "2018-09-11 08:13:43";
//        "user_id" = 2;
//    };
//    "offering_home" =                 {
//        bedroom =                     (
//            {
//                "available_date" = "2018-09-13";
//                "bathroom_facilities" = 2;
//                "bedroom_features" = "2,1,6,8";
//                "bedroom_furniture" = 1;
//                "bedroom_size" = 1;
//                "bedroom_type" = 1;
//                bills = 1;
//                "couple_rent" = 200;
//                "created_at" = "2018-09-11 08:11:41";
//                deposit = 1;
//                id = 16;
//                "maximum_period" = 2;
//                "minimum_period" = 2;
//                "no_of_beds" = 0;
//                "offering_home_id" = 16;
//                "other_information" = QWERTY;
//                "profile_id" = 1;
//                "rent_duration" = 1;
//                "room_features" = "2,1,6,8";
//                "single_person_rent" = 150;
//                "updated_at" = "2018-09-11 08:11:41";
//                "user_id" = 2;
//        },
//            {
//                "available_date" = "2018-09-13";
//                "bathroom_facilities" = 2;
//                "bedroom_features" = "2,3,11,10";
//                "bedroom_furniture" = 2;
//                "bedroom_size" = 1;
//                "bedroom_type" = 1;
//                bills = 2;
//                "couple_rent" = 200;
//                "created_at" = "2018-09-11 08:11:41";
//                deposit = 1;
//                id = 17;
//                "maximum_period" = 1;
//                "minimum_period" = 2;
//                "no_of_beds" = 0;
//                "offering_home_id" = 16;
//                "other_information" = test;
//                "profile_id" = 1;
//                "rent_duration" = 1;
//                "room_features" = "2,3,11,10";
//                "single_person_rent" = 150;
//                "updated_at" = "2018-09-11 08:11:41";
//                "user_id" = 2;
//        }
//        );
//        bills = 2;
//        "bills_included" = ytytguygy;
//        "created_at" = "2018-09-11 08:11:41";
//        "home_address" = sadfsdfg;
//        "home_description" = ytrtyug;
//        "home_features" = "2,3,4";
//        "home_location" = "Pawar Villa, N-430, Singapore Green View, Risi Nagar, Talawali Chanda, Indore, Madhya Pradesh 452007, India";
//        "home_size" = 2;
//        "home_type" = 1;
//        id = 16;
//        latitude = "22.7195796";
//        longitude = "75.8577274";
//        "offering_home_id" = 16;
//        "parking_facility" = 1;
//        "profile_id" = 1;
//        "updated_at" = "2018-09-11 08:11:41";
//        "user_id" = 2;
//    };
//    photos =                 {
//        "upload_photo" =                     (
//            {
//                "created_at" = "2018-09-11 08:13:27";
//                id = 11;
//                "offering_home_id" = 16;
//                photo = "http://18.223.28.186/photogallry/photogallry/6pt783GibHIO6Uklxy86Afdgnfp9k317TGcJTaoO.jpeg";
//                "photo_id" = 6;
//                "updated_at" = "2018-09-11 08:13:27";
//        },
//            {
//                "created_at" = "2018-09-11 08:13:27";
//                id = 12;
//                "offering_home_id" = 16;
//                photo = "http://18.223.28.186/photogallry/photogallry/k7fRC8ILTpgmT957JT7zowUyB5MnFZtj8GbSqDXg.jpeg";
//                "photo_id" = 6;
//                "updated_at" = "2018-09-11 08:13:27";
//        }
//        );
//    };
//    "profile_id" = 1;
//}
//);
//};
//status = 1;
//}


