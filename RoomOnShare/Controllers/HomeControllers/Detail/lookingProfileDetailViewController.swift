import UIKit
import GoogleMaps
import GooglePlaces
import CoreLocation

class lookingProfileDetailViewController: UIViewController {
    
    //View first
    @IBOutlet weak var scrollView: UIScrollView!{
        didSet{
            scrollView.delegate = self
        }
    }
    
    @IBOutlet weak var lblHeader: UILabel!    
    @IBOutlet var imgViews : [UIImageView]!
    @IBOutlet weak var lblDate : UILabel!
    @IBOutlet weak var lblTimeStay : UILabel!
    @IBOutlet weak var lblPrice : UILabel!
    @IBOutlet weak var lblstatus : UILabel!
    @IBOutlet weak var lblProgressDetail : UILabel!
    @IBOutlet weak var progressBar : UIProgressView!    
    @IBOutlet weak var btnStatus: UIButton!
    
    @IBOutlet weak var btn2: UIButton!
    @IBOutlet weak var btn3: UIButton!
    @IBOutlet weak var btn4: UIButton!    
    @IBOutlet weak var btn5: UIButton!
    
    
    //View sec
    @IBOutlet weak var lblNoOfAdults : UILabel!
    @IBOutlet weak var lblGender : UILabel!
    @IBOutlet weak var lblAge : UILabel!
    @IBOutlet var btnSmoking : [UIButton]!
    @IBOutlet var btnPets : [UIButton]!
    @IBOutlet weak var lblEmployement : UILabel!
    @IBOutlet weak var lblAboutPerson : UILabel!
    @IBOutlet weak var lblImpQualities : UILabel!
    @IBOutlet weak var collInterestItems : UICollectionView!
  
    //Home Preference
    @IBOutlet weak var lblBedroomtype : UILabel!
    @IBOutlet weak var lblHomeSize : UILabel!
    @IBOutlet weak var lblBedroomSize : UILabel!
    @IBOutlet weak var lblBedroomFurniture : UILabel!
    @IBOutlet weak var lblNoOfBeds : UILabel!
    @IBOutlet var btnBathroom : [UIButton]!
    @IBOutlet var btnParking : [UIButton]!
    
    //Flatmate Prefernce
    @IBOutlet weak var lblGender_Flat : UILabel!
    @IBOutlet weak var lblAge_Flat : UILabel!
    @IBOutlet var btnSmoking_Flat : [UIButton]!
    @IBOutlet var btnPets_Flat : [UIButton]!
    
    //MAp
    @IBOutlet weak var viewMap: GMSMapView!
    var locationManager:CLLocationManager!
    
    var is_status : Bool = false
    var dicLookingDetail : NSDictionary = [:]
    var arrMainintrest : Array<Any> = []
    
    var slides:[imageSliderView] = [];
    var imagesArray : [Images] = []
    var userdata = SharedPreference.getUserData()
    
    var LookingData = LookingProfileDetailData()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        determineMyCurrentLocation()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        self.navigationController?.isNavigationBarHidden = true
        rdv_tabBarController?.setTabBarHidden(false, animated: true)
        self.btn2.isHidden = true
        self.btn3.isHidden = true
        self.btn4.isHidden = true
        self.btn5.isHidden = true
        self.ShowData(dicData: self.dicLookingDetail)
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    
    func ShowData(dicData : NSDictionary) {
        if dicData.value(forKey: "profile_id") as! Int == 2 {
            /////////////// Looking ///////////
            
            self.lblHeader.text = userdata.user_fullname
            self.LookingData = LookingProfileDetailData.init(withDictionary: dicData as! [String : Any])
            let profilecomplete = Float((Float(dicData.value(forKey: "profile_completed") as! Int)) / 100.0)
            self.progressBar.setProgress(profilecomplete, animated: true)
            self.lblProgressDetail.text = "Your profile is \(dicData.value(forKey: "profile_completed") as! Int)% complete"
            
            /// Images
            self.imagesArray = LookingData.arrImages
            self.createSlides()
            setupSlideScrollView(slides: slides)
            for i in 0..<imagesArray.count{
                let img = imagesArray[i]
                imgViews[i].sd_setImage(with: URL(string: img.url), placeholderImage: UIImage(named: "Banner"))
            }
            
            ///// user ///
            let user = LookingData.Userdata
            
            if user.mobile_verified && user.email_verified{
                self.btn2.isHidden = false
            }
            if user.facebook_id != ""{
                self.btn3.isHidden = false
            }
            if user.google_id != ""{
                self.btn4.isHidden = false
            }
            if user.linked_id != ""{
                self.btn5.isHidden = false
            }
        
            //// Intro
            if LookingData.Introduceyourself.prefered_length_stay != 0{
                self.lblDate.text = Common.ChangeDateFormat(Date: LookingData.Introduceyourself.avalibality_date, fromFormat: "yyyy-MM-dd", toFormat: "dd MMM")
                self.lblTimeStay.text = Array_File.arrLength_of_stay[(LookingData.Introduceyourself.prefered_length_stay) - 1]
                self.lblImpQualities.text = LookingData.Introduceyourself.important_personal_quality
            }
            /// your desc
            self.is_status = LookingData.yourDescription.is_active
            if self.is_status{
                self.lblstatus.text = "Active"
                self.lblstatus.textColor = self.UIColorFromHex(rgbValue: 0x44aa46, alpha: 1.0)
                self.btnStatus.setTitle("Deactive", for: .normal)
                self.btnStatus.backgroundColor = self.UIColorFromHex(rgbValue: 0xD44936, alpha: 1.0)
            }else{
                self.lblstatus.text = "Deactive"
                self.lblstatus.textColor = self.UIColorFromHex(rgbValue: 0xD44936, alpha: 1.0)
                self.btnStatus.setTitle("Active", for: .normal)
                self.btnStatus.backgroundColor = self.UIColorFromHex(rgbValue: 0x44aa46, alpha: 1.0)
            }
            
            self.lblNoOfAdults.text = Array_File.arrAdults[LookingData.yourDescription.no_of_adult - 1]
            self.lblGender.text = String("\(Array_File.arrGender[LookingData.yourDescription.gender - 1]) \(Array_File.arrSexuality[LookingData.yourDescription.sexuality - 1])")
            self.lblAge.text = Array_File.arrAgeGroup[LookingData.yourDescription.age_group - 1]
            self.ShowCheckUnceck(arrButton: self.btnSmoking, Selected: LookingData.yourDescription.smoking_type) //
            self.ShowCheckUnceck(arrButton: self.btnPets, Selected: LookingData.yourDescription.pets_type) //
            self.lblEmployement.text = Array_File.arrEmployment[LookingData.yourDescription.employment_situation - 1]
            self.arrMainintrest = LookingData.yourDescription.interest
            self.collInterestItems.reloadData()
            
            /// home preference
            if LookingData.Homepreference.badroom_type != 0{
                self.lblPrice.text = String("$ \(LookingData.Homepreference.rent_budget) per month")
                self.lblBedroomtype.text = Array_File.arrBedroomtype[LookingData.Homepreference.badroom_type - 1]
                
                let bedroom : NSMutableArray = []
                for item in LookingData.Homepreference.badroom_size{
                    bedroom.add(Array_File.arrBedroomSize[item as! Int - 1])
                }
                self.lblBedroomSize.text = bedroom.componentsJoined(by: ",")
                let home : NSMutableArray = []
                for item in LookingData.Homepreference.home_size{
                    home.add(Array_File.arrHomeSize[item as! Int - 1])
                }
                self.lblHomeSize.text = home.componentsJoined(by: ",")
                
                self.ShowCheckUnceck(arrButton: self.btnBathroom, Selected: LookingData.Homepreference.badroom_facitity)
                self.ShowCheckUnceck(arrButton: self.btnParking, Selected: LookingData.Homepreference.parking_facitity)
            }
            ///// flatmate
            let gender : NSMutableArray = []
            for item in LookingData.Flatmatepreference.F_gendersexuality{
                gender.add(Array_File.arrGenderSexuality[item as! Int - 1])
            }            
            self.lblGender_Flat.text = gender.componentsJoined(by: ",")
                
            let age : NSMutableArray = []
            for item in LookingData.Flatmatepreference.F_agegroup{
                age.add(Array_File.arrAgeGroup[item as! Int - 1])
            }
            self.lblAge_Flat.text = age.componentsJoined(by: ",")
            
            let arrsmok = LookingData.Flatmatepreference.F_smoking
            for item in arrsmok{
                self.ShowCheckUnceck(arrButton: self.btnSmoking_Flat, Selected: item as! Int)
            }
            
            let pets = LookingData.Flatmatepreference.F_pets
            for item in pets{
                self.ShowCheckUnceck(arrButton: self.btnPets_Flat, Selected: item as! Int)
            }
            
            ///// Location ////
            if LookingData.Preferedlocation.latitude != 00.00{
                let lati = LookingData.Preferedlocation.latitude
                let long = LookingData.Preferedlocation.longitude
                self.ShowMarker(lati: lati, long: long)
            }
        }
    }
    
    func ShowCheckUnceck(arrButton : [UIButton], Selected : Int) {
        for button in arrButton {
            let tag = button.tag
            if Selected == tag{
                button.isSelected = true
            }
        }
    }
    
    @IBAction func clickOnDeactivateBtn(_ sender : UIButton){
        if self.lblstatus.text == "Deactive"{
          self.is_status = true
            self.ChangeStatusService()
        }else{
          self.is_status = false
             self.ChangeStatusService()
        }
    }
    
    @IBAction func clickOnEditProfileBtn(_ sender : UIButton){
        self.SetUpEditData()
    }
    
    func SetUpEditData() {
        SharedPreference.setLooking_id(self.dicLookingDetail.value(forKey: "Id") as! Int)
        
        LookingDescriptionViewController.SAdults = LookingData.yourDescription.no_of_adult
        LookingDescriptionViewController.SGender = LookingData.yourDescription.gender
        LookingDescriptionViewController.SSextulity = LookingData.yourDescription.sexuality
        LookingDescriptionViewController.SAgeGroup = LookingData.yourDescription.age_group
        LookingDescriptionViewController.SEmployment = LookingData.yourDescription.employment_situation        
        LookingDescriptionViewController.Sid_Pet = LookingData.yourDescription.pets_type
        LookingDescriptionViewController.Sid_Smoking = LookingData.yourDescription.smoking_type
        LookingDescriptionViewController.SstrMoreinterest = LookingData.yourDescription.interest
        
        if self.lblDate.text != "Not available"{
            print(LookingData.Introduceyourself.avalibality_date)
            IntroduceSelfViewController.date = LookingData.Introduceyourself.avalibality_date
            IntroduceSelfViewController.stay = LookingData.Introduceyourself.prefered_length_stay
            IntroduceSelfViewController.quality = self.lblImpQualities.text!
        }else{
            IntroduceSelfViewController.date = ""
        }
        
        if self.dicLookingDetail.value(forKey: "photos") != nil{
            let images = self.dicLookingDetail.value(forKey: "photos") as! NSDictionary
            AddPhotoViewController.SarrPhoto = images.value(forKey: "upload_photo") as! Array<Any>
        }
        
        PreferredLoactionViewController.Slatitude = LookingData.Preferedlocation.latitude
        PreferredLoactionViewController.Slongitude = LookingData.Preferedlocation.longitude
        PreferredLoactionViewController.Saddress = LookingData.Preferedlocation.address
        
        HomePreferencesViewController.SRent = LookingData.Homepreference.rent_budget
        HomePreferencesViewController.SBedroom_type = LookingData.Homepreference.badroom_type
        HomePreferencesViewController.SBathroom_facilities = LookingData.Homepreference.badroom_facitity
        HomePreferencesViewController.SParking_facility = LookingData.Homepreference.parking_facitity
        HomePreferencesViewController.SHome_size =   LookingData.Homepreference.home_size
        HomePreferencesViewController.SBedroom_size = LookingData.Homepreference.badroom_size
        
        FlatmatePreferenceViewController.Sid_Gender = LookingData.Flatmatepreference.F_gendersexuality //
        FlatmatePreferenceViewController.Sid_AgeGroup = LookingData.Flatmatepreference.F_agegroup
        FlatmatePreferenceViewController.Sid_Pet = LookingData.Flatmatepreference.F_pets
        FlatmatePreferenceViewController.Sid_Smoking = LookingData.Flatmatepreference.F_smoking
        
        let storyboard = UIStoryboard(name: "Main", bundle: nil)
        let vc = storyboard.instantiateViewController(withIdentifier: "LookingDescriptionViewController") as! LookingDescriptionViewController
        self.navigationController?.pushViewController(vc, animated: true)
        
    }
    
    @IBAction func BackMethod(_ sender: Any) {
        Common.PopMethod(VC: self)
    }
    
    func ChangeStatusService() {
        self.startActivityIndicator()                                
        let param = ["profile_id"    : 2,
                 "looking_home_id" : self.dicLookingDetail.value(forKey: "Id") as! Int,
                 "is_active" : self.is_status
            ] as [String : Any]
        print(param)
        
        CommunicationManager().getResponseForPost(strUrl: Url_File.ChangeStatus, parameters: param as NSDictionary) { ( result , data) in
            
            if(result == "success") {
                let dataDict = data as! NSDictionary
                print(dataDict)
                if self.is_status{
                    self.lblstatus.text = "Active"
                    self.lblstatus.textColor = self.UIColorFromHex(rgbValue: 0x44aa46, alpha: 1.0)
                    self.btnStatus.setTitle("Deactive", for: .normal)
                    self.btnStatus.backgroundColor = self.UIColorFromHex(rgbValue: 0xD44936, alpha: 1.0)
                }else{
                    self.lblstatus.text = "Deactive"
                    self.lblstatus.textColor = self.UIColorFromHex(rgbValue: 0xD44936, alpha: 1.0)
                    self.btnStatus.setTitle("Active", for: .normal)
                    self.btnStatus.backgroundColor = self.UIColorFromHex(rgbValue: 0x44aa46, alpha: 1.0)
                }
                
                self.stopActivityIndicator()
            }else if (result == "error") {
                let strMessage = (data as! NSDictionary)["message"] as! String
                Common.ShowAlert(Title: Common.Title, Message: strMessage, VC: self)
                self.stopActivityIndicator()
            }else if (result == "Network") {
                let strMessage = (data as! String)
                Common.ShowAlert(Title: Common.Title, Message: strMessage, VC: self)
                self.stopActivityIndicator()
            }
        }
    }
}

//MArk - Image slider code
extension lookingProfileDetailViewController : UIScrollViewDelegate{
    func createSlides(){
        for i in 0..<imagesArray.count{
            let slide1 = Bundle.main.loadNibNamed("imageSliderView", owner: self, options: nil)?.first as! imageSliderView
            let img = imagesArray[i]
            slide1.imageView.sd_setImage(with: URL(string: img.url), placeholderImage: UIImage(named: "Banner"))
            slide1.btnNExt.addTarget(self, action: #selector(clickOnNExtArrowBtn(_:)), for: .touchUpInside)
            slide1.btnPrevious.addTarget(self, action: #selector(clickOnPreviousArrowBtn(_:)), for: .touchUpInside)
            if(i==0){
                slide1.btnPrevious.isHidden=true
            }
            if(i==4){
                slide1.btnNExt.isHidden=true
            }
            slides.append(slide1)
        }
        print(slides.count)
    }
    
    func setupSlideScrollView(slides : [imageSliderView]) {
        scrollView.contentSize = CGSize(width: scrollView.frame.width * CGFloat(slides.count), height:175)
        scrollView.isPagingEnabled = true
        for i in 0 ..< slides.count {
            slides[i].frame = CGRect(x: scrollView.frame.width * CGFloat(i), y: 0, width: scrollView.frame.width, height: scrollView.frame.height)
            scrollView.addSubview(slides[i])
        }
    }
    
    @objc func clickOnPreviousArrowBtn(_ sender : UIButton) {
        if scrollView.contentOffset.x > 0 {
            sender.isHidden=false
            scrollView.contentOffset.x -=  scrollView.bounds.width
        }else{
            sender.isHidden=true
        }
    }
    
    @objc func clickOnNExtArrowBtn(_ sender : UIButton){
        let scrollContnt = scrollView.bounds.width * CGFloat(slides.count-1)
        if (scrollView.contentOffset.x) < (scrollContnt) {
            sender.isHidden=false
            scrollView.contentOffset.x +=  scrollView.bounds.width
        }else{
            sender.isHidden=true
        }
    }
}


//MArk - Collectionview Ineterst code
extension lookingProfileDetailViewController : UICollectionViewDelegate , UICollectionViewDataSource , UICollectionViewDelegateFlowLayout{
    
    func numberOfSections(in collectionView: UICollectionView) -> Int {
        return 1
    }
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return self.arrMainintrest.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        
        let cell = collInterestItems.dequeueReusableCell(withReuseIdentifier: "showInterstCollCell", for: indexPath) as! showInterstCollCell
        let arrmain = Array_File.arrIntesrestOptionImgs
    
        for j in 0...arrmain.count - 1{
            let id = self.arrMainintrest[indexPath.row] as! Int
            if j == id {
                cell.isSelected=true
                self.collInterestItems.selectItem(at: indexPath, animated: true, scrollPosition: UICollectionViewScrollPosition.top)                               
                cell.imgInterets.image = UIImage(named: Array_File.arrIntesrestOptionImgs[id]["image"]!)?.imageWithInsets(insetDimen: 8)
                cell.imgInterets.tintImageColor(color: UIColorFromHex(rgbValue: 0x05C6CF, alpha: 1.0))
                cell.lblInteretsName.text = Array_File.arrIntesrestOptionImgs[id]["name"]!
                break
            }
        }
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        let yourWidth = collectionView.bounds.width/4.0
        return CGSize(width: yourWidth, height: 110.0)
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, insetForSectionAt section: Int) -> UIEdgeInsets {
        return UIEdgeInsets.zero
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumInteritemSpacingForSectionAt section: Int) -> CGFloat {
        return 0
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumLineSpacingForSectionAt section: Int) -> CGFloat {
        return 0
    }
}

class showInterstCollCell : UICollectionViewCell{
    @IBOutlet weak var imgInterets : UIImageView!
    @IBOutlet weak var lblInteretsName : UILabel!
}


//Map code
//MArk - location

extension lookingProfileDetailViewController : CLLocationManagerDelegate{
    func determineMyCurrentLocation() {
        locationManager = CLLocationManager()
        locationManager.delegate = self
        locationManager.desiredAccuracy = kCLLocationAccuracyBest
        locationManager.requestAlwaysAuthorization()
        if CLLocationManager.locationServicesEnabled() {
            locationManager.startUpdatingLocation()
            //locationManager.startUpdatingHeading()
        }
    }
    
    func locationManager(_ manager: CLLocationManager, didUpdateLocations locations: [CLLocation]){
        let userLocation:CLLocation = locations[0] as CLLocation
        print("user latitude = \(userLocation.coordinate.latitude)")
        print("user longitude = \(userLocation.coordinate.longitude)")
    }
    
    func locationManager(_ manager: CLLocationManager, didFailWithError error: Error){
        print("Error \(error)")
    }
    
    func ShowMarker(lati : Double, long : Double){
        let camera = GMSCameraPosition.camera(withLatitude: lati, longitude: long, zoom: 18.0)
        self.viewMap.camera = camera
        
        // Creates a marker in the center of the map.
        let marker = GMSMarker()
        marker.position = CLLocationCoordinate2D(latitude: lati, longitude: long)
        marker.map = self.viewMap
    }
}


//    func scrollView(_ scrollView: UIScrollView, didScrollToPercentageOffset percentageHorizontalOffset: CGFloat) {
//        if(pageControl.currentPage == 0) {
//            //Change background color to toRed: 103/255, fromGreen: 58/255, fromBlue: 183/255, fromAlpha: 1
//            //Change pageControl selected color to toRed: 103/255, toGreen: 58/255, toBlue: 183/255, fromAlpha: 0.2
//            //Change pageControl unselected color to toRed: 255/255, toGreen: 255/255, toBlue: 255/255, fromAlpha: 1
//
//            let pageUnselectedColor: UIColor = fade(fromRed: 255/255, fromGreen: 255/255, fromBlue: 255/255, fromAlpha: 1, toRed: 103/255, toGreen: 58/255, toBlue: 183/255, toAlpha: 1, withPercentage: percentageHorizontalOffset * 3)
//            pageControl.pageIndicatorTintColor = pageUnselectedColor
//
//
//            let bgColor: UIColor = fade(fromRed: 103/255, fromGreen: 58/255, fromBlue: 183/255, fromAlpha: 1, toRed: 255/255, toGreen: 255/255, toBlue: 255/255, toAlpha: 1, withPercentage: percentageHorizontalOffset * 3)
//            slides[pageControl.currentPage].backgroundColor = bgColor
//
//            let pageSelectedColor: UIColor = fade(fromRed: 81/255, fromGreen: 36/255, fromBlue: 152/255, fromAlpha: 1, toRed: 103/255, toGreen: 58/255, toBlue: 183/255, toAlpha: 1, withPercentage: percentageHorizontalOffset * 3)
//            pageControl.currentPageIndicatorTintColor = pageSelectedColor
//        }
//    }

//    func callLookingProfileDetailGetApi(){
//        self.startActivityIndicator()
//
//        let userid = SharedPreference.getUserData().user_id
//        let param = ["user_id" : userid,
//                     "profile_id" : 1,
//                     ] as [String : Any]
//        print(param)
//        CommunicationManager().getResponseForPost(strUrl: "", parameters: param as NSDictionary) { ( result , data) in
//            if(result == "success") {
//                let dataDict = data as! NSDictionary
//                print(dataDict)
//                _ = dataDict["response"] as! NSDictionary
//                self.stopActivityIndicator()
//            }else if (result == "error") {
//                let strMessage = (data as! NSDictionary)["message"] as! String
//                Common.ShowAlert(Title: Common.Title, Message: strMessage, VC: self)
//                self.stopActivityIndicator()
//            }else if (result == "Network") {
//                let strMessage = (data as! String)
//                Common.ShowAlert(Title: Common.Title, Message: strMessage, VC: self)
//                self.stopActivityIndicator()
//            }
//        }
//    }

//    func GoNext() {
//        Common.PushMethod(VC: self, identifier: "")
//    }
