import UIKit
import GoogleMaps
import GooglePlaces
import CoreLocation


class PropertyDetailViewController: UIViewController {

    @IBOutlet weak var scrollViewImgs: UIScrollView!{
        didSet{
            scrollViewImgs.delegate = self
        }
    }
    @IBOutlet weak var lblHeader: UILabel!
    @IBOutlet var imgViews : [UIImageView]!
    @IBOutlet weak var lblAddress : UILabel!
    @IBOutlet weak var lblLocation : UILabel!
    @IBOutlet weak var lblPrice : UILabel!
    
    @IBOutlet var imgProfilepic : UIImageView!
    @IBOutlet weak var lblNAme : UILabel!
    @IBOutlet weak var lblDetails : UILabel!
    
    //Bedroom
    @IBOutlet weak var lblRentSingle : UILabel!
    @IBOutlet weak var lblRentCouple : UILabel!
    @IBOutlet weak var lblDateAvail : UILabel!
    @IBOutlet weak var lblStayLength : UILabel!
    @IBOutlet weak var lblBedroomType : UILabel!
    @IBOutlet weak var lblBedroomSize : UILabel!
    @IBOutlet weak var lblBedroomFurniture : UILabel!
    @IBOutlet weak var lblNoOfBeds : UILabel!
    @IBOutlet var btnBathroom : [UIButton]!
    @IBOutlet weak var collRoomFeatures : UICollectionView!
    @IBOutlet weak var lblSecurity : UILabel!
    
    //Home Description
    @IBOutlet weak var lblHomeDescription : UILabel!
    @IBOutlet weak var lblBills : UILabel!
    @IBOutlet weak var collHomeFeatures : UICollectionView!
    
    //About Occupants
    @IBOutlet weak var lblGender : UILabel!
    @IBOutlet weak var lblAge : UILabel!
    @IBOutlet weak var lblEmployment : UILabel!
    @IBOutlet weak var lblAboutOccupants : UILabel!
    @IBOutlet var btnSmoking : [UIButton]!
    @IBOutlet var btnPets : [UIButton]!
    @IBOutlet weak var collMainInterst : UICollectionView!
    
    //Flatmate Prefernce
    @IBOutlet weak var lblGender_Flat : UILabel!
    @IBOutlet weak var lblAge_Flat : UILabel!
    @IBOutlet var btnSmoking_Flat : [UIButton]!
    @IBOutlet var btnPets_Flat : [UIButton]!
    
    //MAp
    @IBOutlet weak var viewMap: GMSMapView!
    var locationManager:CLLocationManager!
    var latitude : Double = 0000
    var longitude : Double = 0000

    var slides:[imageSliderView] = [];
    var arrbedroomfeature : Array<Any> = []
    var arrhomefeature : Array<Any> = []
    var arrMaininterest : Array<Any> = []
    var dicPropertyDetail : NSDictionary = [:]
    var OfferingData = ProfileFlatmateDetailData()
    var imagesArray : [Images] = []
    
    override func viewDidLoad() {
        super.viewDidLoad()      
        determineMyCurrentLocation()
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    override func viewWillAppear(_ animated: Bool) {
        self.navigationController?.isNavigationBarHidden = true
       // rdv_tabBarController?.setTabBarHidden(false, animated: true)
        self.ShowData(dicData: self.dicPropertyDetail)
    }
    
    @IBAction func BackMethod(_ sender: Any) {
        Common.PopMethod(VC: self)
    }
    
    func ShowData(dicData : NSDictionary) {
        
        print(dicData)
            /////////////// offering ///////////
            
            self.OfferingData = ProfileFlatmateDetailData.init(withDictionary: dicData as! [String : Any])
            
            /// Images
            self.imagesArray = OfferingData.arrImages
            self.createSlides()
            setupSlideScrollView(slides: slides)
            for i in 0..<imagesArray.count{
                let img = imagesArray[i]
                imgViews[i].sd_setImage(with: URL(string: img.url), placeholderImage: UIImage(named: "Banner"))
            }
            
            
            ///// user ///
            let user = OfferingData.Userdata
            self.lblNAme.text = user.user_fullname
            self.lblDetails.text = "\(user.user_age) Years \( Array_File.arrGender[user.user_gender - 1])"
            self.imgProfilepic.sd_setImage(with: URL(string: user.user_profilepic), placeholderImage: UIImage(named: "UserPlaceholder"))
            /////// bedroom /////
            
            let dicBedroom = OfferingData.bedroomdescription[0]
            print(dicBedroom.badroom_type)
            self.lblBedroomType.text = Array_File.arrBedroomtype[dicBedroom.badroom_type - 1]
            self.lblPrice.text = "$ \(dicBedroom.single_person_rent)"
            self.lblRentSingle.text = String("Single $ \(dicBedroom.single_person_rent) Per Month")
            self.lblRentCouple.text = String("Couple $ \(dicBedroom.couple_rent) Per Month")
            self.lblDateAvail.text = Common.ChangeDateFormat(Date: dicBedroom.avalibality_date, fromFormat: "yyyy-MM-dd", toFormat: "dd MMM yyyy")
            self.lblStayLength.text = Array_File.arrLength_of_stay[dicBedroom.maximum_period - 1]
            self.lblBedroomSize.text = Array_File.arrBedroomSize[dicBedroom.badroom_size - 1]
            self.lblBedroomFurniture.text = Array_File.arrBedroomFurniture[dicBedroom.badroom_furniture - 1]
            self.arrbedroomfeature = dicBedroom.room_feature
            if dicBedroom.badroom_type == 2{
                self.lblNoOfBeds.text = Array_File.arrNoOfBeds[dicBedroom.no_of_beds - 1]
            }
            self.collRoomFeatures.reloadData()
            self.lblSecurity.text =  Array_File.arrDeposit[dicBedroom.deposit - 1]
            self.ShowCheckUnceck(arrButton: self.btnBathroom, Selected: dicBedroom.bathroom_facitity)
            
            ////// home description /////
            
            self.lblAddress.text = self.OfferingData.homedescription.home_address
            self.lblLocation.text = self.OfferingData.homedescription.home_location
           // self.lblhomeSize.text = Array_File.arrHomeSize[self.OfferingData.homedescription.home_size - 1]
            self.lblHomeDescription.text = self.OfferingData.homedescription.home_description
            self.lblBills.text = self.OfferingData.homedescription.bill_included
            self.arrhomefeature = self.OfferingData.homedescription.home_feature
            self.collHomeFeatures.reloadData()
            
            /////  about occupants ///
        
        if self.OfferingData.aboutoccupant.is_anyone_living{
            let gender : NSMutableArray = []
            for item in self.OfferingData.aboutoccupant.AO_gendersexuality{
                gender.add(Array_File.arrGenderSexuality[item as! Int - 1])
            }
            self.lblGender.text = gender.componentsJoined(by: ",")
            
            let age : NSMutableArray = []
            for item in self.OfferingData.aboutoccupant.AO_agegroup{
                age.add(Array_File.arrAgeGroup[item as! Int - 1])
            }
            self.lblAge.text = age.componentsJoined(by: ",")
            
            let arrsmok = self.OfferingData.aboutoccupant.AO_smoking
            for item in arrsmok{
                self.ShowCheckUnceck(arrButton: self.btnSmoking, Selected: item as! Int)
            }
            print(OfferingData.aboutoccupant.AO_pets)
            self.ShowCheckUnceck(arrButton: self.btnPets, Selected: OfferingData.aboutoccupant.AO_pets)
            
            self.lblAboutOccupants.text = OfferingData.aboutoccupant.personal_infro
            self.arrMaininterest = self.OfferingData.aboutoccupant.interest
            if self.arrMaininterest.count != 0{
                self.collMainInterst.reloadData()
            }
        }
            
            ////// flatmate preferance /////
            let Fgender : NSMutableArray = []
            for item in OfferingData.Flatmatepreference.F_gendersexuality{
                Fgender.add(Array_File.arrGenderSexuality[item as! Int - 1])
            }
            self.lblGender_Flat.text = Fgender.componentsJoined(by: ",")
            
            let Fage : NSMutableArray = []
            for item in OfferingData.Flatmatepreference.F_agegroup{
                Fage.add(Array_File.arrAgeGroup[item as! Int - 1])
            }
            self.lblAge_Flat.text = Fage.componentsJoined(by: ",")
            
            let Farrsmok = OfferingData.Flatmatepreference.F_smoking
            for item in Farrsmok{
                self.ShowCheckUnceck(arrButton: self.btnSmoking_Flat, Selected: item as! Int)
            }
            
            let Fpets = OfferingData.Flatmatepreference.F_pets
            for item in Fpets{
                self.ShowCheckUnceck(arrButton: self.btnPets_Flat, Selected: item as! Int)
            }
            
            let lati = OfferingData.homedescription.latitude
            let long = OfferingData.homedescription.longitude
            self.ShowMarker(lati: lati, long: long)
        
    }
    
    func ShowCheckUnceck(arrButton : [UIButton], Selected : Int) {
        for button in arrButton {
            let tag = button.tag
            if Selected == tag{
                button.isSelected = true
            }
        }
    }
    
    @IBAction func clickOnCallBtn(_ sender : UIButton){
        guard let number = URL(string: "tel:8516880786") else { return }
        if #available(iOS 10.0, *) {
            UIApplication.shared.open(number)
        } else {
            // Fallback on earlier versions
            UIApplication.shared.openURL(number)
        }
    }
    
    @IBAction func clickOnChatBtn(_ sender : UIButton){
        
    }
    
    @IBAction func clickOnAppoinmentBtn(_ sender : UIButton){
        
    }
}

//MArk - Image slider code
extension PropertyDetailViewController : UIScrollViewDelegate{
    
    func createSlides(){
        for i in 0..<imagesArray.count{
            let slide1 = Bundle.main.loadNibNamed("imageSliderView", owner: self, options: nil)?.first as! imageSliderView
            let img = imagesArray[i]
            slide1.imageView.sd_setImage(with: URL(string: img.url), placeholderImage: UIImage(named: "Banner"))
            slide1.btnNExt.addTarget(self, action: #selector(clickOnNExtArrowBtn(_:)), for: .touchUpInside)
            slide1.btnPrevious.addTarget(self, action: #selector(clickOnPreviousArrowBtn(_:)), for: .touchUpInside)
            
            if(i==0){
                slide1.btnPrevious.isHidden=true
            }
            if(i==4){
                slide1.btnNExt.isHidden=true
            }
            
            slides.append(slide1)
        }
        print(slides.count)
    }
    
    
    func setupSlideScrollView(slides : [imageSliderView]) {
        scrollViewImgs.contentSize = CGSize(width: scrollViewImgs.frame.width * CGFloat(slides.count), height:175)
        scrollViewImgs.isPagingEnabled = true                
        for i in 0 ..< slides.count {
            slides[i].frame = CGRect(x: scrollViewImgs.frame.width * CGFloat(i), y: 0, width: scrollViewImgs.frame.width, height: scrollViewImgs.frame.height)
            scrollViewImgs.addSubview(slides[i])
        }
    }
    
    @objc func clickOnPreviousArrowBtn(_ sender : UIButton) {
        if scrollViewImgs.contentOffset.x > 0 {
            sender.isHidden=false
            scrollViewImgs.contentOffset.x -=  scrollViewImgs.bounds.width
        }else{
            sender.isHidden=true
        }
    }
    
    @objc func clickOnNExtArrowBtn(_ sender : UIButton){
        let scrollContnt = scrollViewImgs.bounds.width * CGFloat(slides.count-1)
        if (scrollViewImgs.contentOffset.x) < (scrollContnt) {
            sender.isHidden=false
            scrollViewImgs.contentOffset.x +=  scrollViewImgs.bounds.width
        }else{
            sender.isHidden=true
        }
        
    }
}

//Map code
//MArk - location

extension PropertyDetailViewController : CLLocationManagerDelegate{
    func determineMyCurrentLocation() {
        locationManager = CLLocationManager()
        locationManager.delegate = self
        locationManager.desiredAccuracy = kCLLocationAccuracyBest
        locationManager.requestAlwaysAuthorization()
        
        if CLLocationManager.locationServicesEnabled() {
            locationManager.startUpdatingLocation()
        }
    }
    
    func locationManager(_ manager: CLLocationManager, didUpdateLocations locations: [CLLocation]){
        let userLocation:CLLocation = locations[0] as CLLocation
        
        print("user latitude = \(userLocation.coordinate.latitude)")
        print("user longitude = \(userLocation.coordinate.longitude)")
        
        self.latitude = userLocation.coordinate.latitude
        self.longitude = userLocation.coordinate.longitude
        
        self.ShowMarker(lati: self.latitude, long: self.longitude)
    }
    
    func locationManager(_ manager: CLLocationManager, didFailWithError error: Error){
        print("Error \(error)")
    }
    
    func ShowMarker(lati : Double, long : Double){
        let camera = GMSCameraPosition.camera(withLatitude: lati, longitude: long, zoom: 6.0)
        self.viewMap.camera = camera
        
        // Creates a marker in the center of the map.
        let marker = GMSMarker()
        marker.position = CLLocationCoordinate2D(latitude: lati, longitude: long)
        marker.map = self.viewMap
    }
}


//MArk - Collectionview code
extension PropertyDetailViewController : UICollectionViewDelegate , UICollectionViewDataSource , UICollectionViewDelegateFlowLayout{
    
    func numberOfSections(in collectionView: UICollectionView) -> Int {
        return 1
    }
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int{
        var count:Int?
        if(collectionView == self.collRoomFeatures){
            count = self.arrbedroomfeature.count
        }        
        if(collectionView == self.collHomeFeatures){
            count = self.arrhomefeature.count
        }
        if(collectionView == self.collMainInterst){
            count = self.arrMaininterest.count
        }
        return count!
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        
        let cell : UICollectionViewCell = UICollectionViewCell()
        
        if collectionView == self.collRoomFeatures {
            let cell = collRoomFeatures.dequeueReusableCell(withReuseIdentifier: "collRoomFeatureCell", for: indexPath) as! collRoomFeatureCell
            
            let arrmain = Array_File.arrroomFeaturesImgs
            for j in 0...arrmain.count - 1{
                let id = self.arrbedroomfeature[indexPath.row] as! Int
                if j == id{
                    cell.isSelected=true
                    self.collRoomFeatures.selectItem(at: indexPath, animated: true, scrollPosition: UICollectionViewScrollPosition.top)
                    cell.imgInterets.image = UIImage(named:  Array_File.arrroomFeaturesImgs[j]["image"]!)?.imageWithInsets(insetDimen: 8)
                    cell.imgInterets.tintImageColor(color: UIColorFromHex(rgbValue: 0x05C6CF, alpha: 1.0))
                    cell.lblInteretsName.text = Array_File.arrroomFeaturesImgs[j]["name"]!
                }
            }
            return cell
        }
        
        if collectionView == self.collHomeFeatures{
            let cell = collHomeFeatures.dequeueReusableCell(withReuseIdentifier: "collHomeFeatureCell", for: indexPath) as! collHomeFeatureCell
            let arrmain = Array_File.arrroomFeaturesImgs
            for j in 0...arrmain.count - 1{
                let id = self.arrhomefeature[indexPath.row] as! Int
                if j == id{
                    cell.isSelected=true
                    self.collRoomFeatures.selectItem(at: indexPath, animated: true, scrollPosition: UICollectionViewScrollPosition.top)
                    cell.imgInterets.image = UIImage(named:  Array_File.arrroomFeaturesImgs[j]["image"]!)?.imageWithInsets(insetDimen: 8)
                    cell.imgInterets.tintImageColor(color: UIColorFromHex(rgbValue: 0x05C6CF, alpha: 1.0))
                    cell.lblInteretsName.text = Array_File.arrroomFeaturesImgs[j]["name"]!
                }
            }
            return cell
        }
        
        if collectionView == self.collMainInterst{
            let cell = collMainInterst.dequeueReusableCell(withReuseIdentifier: "collMainInterstCell", for: indexPath) as! collMainInterstCell
            
            let arrmain = Array_File.arrIntesrestOptionImgs
            for j in 0...arrmain.count - 1{
                let id = self.arrMaininterest[indexPath.row] as! Int
                if j == id {
                    cell.isSelected=true
                    self.collRoomFeatures.selectItem(at: indexPath, animated: true, scrollPosition: UICollectionViewScrollPosition.top)
                    cell.imgInterets.image = UIImage(named:  Array_File.arrIntesrestOptionImgs[j]["image"]!)?.imageWithInsets(insetDimen: 8)
                    cell.imgInterets.tintImageColor(color: UIColorFromHex(rgbValue: 0x05C6CF, alpha: 1.0))
                    cell.lblInteretsName.text = Array_File.arrIntesrestOptionImgs[j]["name"]!
                }
            }
            return cell
        }
        
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        let yourWidth = collectionView.bounds.width/4.0
        return CGSize(width: yourWidth, height: 110.0)
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, insetForSectionAt section: Int) -> UIEdgeInsets {
        return UIEdgeInsets.zero
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumInteritemSpacingForSectionAt section: Int) -> CGFloat {
        return 0
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumLineSpacingForSectionAt section: Int) -> CGFloat {
        return 0
    }
}


