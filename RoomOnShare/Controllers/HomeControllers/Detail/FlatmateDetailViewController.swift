//
//  FlatmateDetailViewController.swift
//  RoomOnShare
//
//  Created by mac on 31/08/18.
//  Copyright © 2018 WestCoast. All rights reserved.
//

import UIKit

class FlatmateDetailViewController: UIViewController {

    //View first
    @IBOutlet weak var scrollView: UIScrollView!{
        didSet{
            scrollView.delegate = self
        }
    }
    @IBOutlet var imgViews : [UIImageView]!
    
    @IBOutlet weak var lblDate : UILabel!
    @IBOutlet weak var lblTimeStay : UILabel!
    @IBOutlet weak var lblPrice : UILabel!
    
    @IBOutlet weak var lblName : UILabel!
    @IBOutlet weak var lblContactNo : UILabel!
    @IBOutlet weak var lblOnlinestatus : UILabel!
    @IBOutlet weak var lblProgressDetail : UILabel!
    @IBOutlet weak var progressBar : UIProgressView!
    
    @IBOutlet weak var img1: UIImageView!
    @IBOutlet weak var img2: UIImageView!
    @IBOutlet weak var img3: UIImageView!
    @IBOutlet weak var img4: UIImageView!
    @IBOutlet weak var img5: UIImageView!
    @IBOutlet weak var btnLike: UIButton!
    
    @IBOutlet weak var lblNoofadult: UILabel!
    @IBOutlet weak var lblGender : UILabel!
    @IBOutlet weak var lblAge : UILabel!
    @IBOutlet var btnSmoking : [UIButton]!
    @IBOutlet var btnPets : [UIButton]!
    @IBOutlet weak var lblEmployement : UILabel!
    @IBOutlet weak var lblAboutPerson : UILabel!
    @IBOutlet weak var lblImpQualities : UILabel!
    @IBOutlet weak var lblFoodSharingFurniture : UILabel!
    @IBOutlet weak var collInterestItems : UICollectionView!

    //Home Preference
    @IBOutlet weak var lblBedroomtype : UILabel!
    @IBOutlet weak var lblHomeSize : UILabel!
    @IBOutlet weak var lblBedroomSize : UILabel!
    @IBOutlet weak var lblBedroomFurniture : UILabel!
    @IBOutlet var btnBathroom : [UIButton]!
    @IBOutlet var btnParking : [UIButton]!
    
    //Flatmate Prefernce
    @IBOutlet weak var lblGender_Flat : UILabel!
    @IBOutlet weak var lblAge_Flat : UILabel!
    @IBOutlet var btnSmoking_Flat : [UIButton]!
    @IBOutlet var btnPets_Flat : [UIButton]!
    
    //Featured roommates
    @IBOutlet weak var collFeaturedRoomMates : UICollectionView!
    
    var arrMainintrest : Array<Any> = []
    var arrfeatureroommat : Array<Any> = []
    var slides:[imageSliderView] = [];
    var imagesArray : [Images] = []
    var LookingData = LookingProfileDetailData()
    var dicFlatmateProfile : NSDictionary = [:]
    var isFavorite : Bool = false
    var LookingId : Int = 0
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view.
    }
    
    override func viewWillAppear(_ animated: Bool) {
        self.navigationController?.isNavigationBarHidden = true
        rdv_tabBarController?.setTabBarHidden(false, animated: true)
        self.img1.isHidden = true
        self.img2.isHidden = true
        self.img3.isHidden = true
        self.img4.isHidden = true
        self.img5.isHidden = true
        self.ShowData(dicData: self.dicFlatmateProfile)
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    func ShowData(dicData : NSDictionary) {
        print(dicData)
        self.LookingData = LookingProfileDetailData.init(withDictionary: dicData as! [String : Any])
        
        let profilecomplete = Float((Float(dicData.value(forKey: "matches_percentage") as! Int)) / 100.0)
        self.progressBar.setProgress(profilecomplete, animated: true)
        self.lblProgressDetail.text = "\(dicData.value(forKey: "matches_percentage") as! Int)% Matched"
        
        if dicData.value(forKey: "is_favourite") as! Bool{
            self.btnLike.setImage(UIImage(named: "Clike"), for: .normal)
        }
        
        
        
        //// Images
        self.imagesArray = LookingData.arrImages
        self.createSlides()
        setupSlideScrollView(slides: slides)
        for i in 0..<imagesArray.count{
            let img = imagesArray[i]
            imgViews[i].sd_setImage(with: URL(string: img.url), placeholderImage: UIImage(named: "Banner"))
        }
        
        ///// user ///
        let user = LookingData.Userdata
        self.lblName.text = user.user_fullname
        self.lblContactNo.text = user.user_mobile_no
        if user.admin_verified{
            self.img1.isHidden = false
        }
        if user.mobile_verified && user.email_verified{
            self.img2.isHidden = false
        }
        if user.facebook_id != ""{
            self.img3.isHidden = false
        }
        if user.google_id != ""{
            self.img4.isHidden = false
        }
        if user.linked_id != ""{
            self.img5.isHidden = false
        }
        
        //// Intro
        self.lblDate.text = self.ChangeDateFormat(Date: LookingData.Introduceyourself.avalibality_date)
        self.lblTimeStay.text = Array_File.arrLength_of_stay[(LookingData.Introduceyourself.prefered_length_stay) - 1]
        self.lblImpQualities.text = LookingData.Introduceyourself.important_personal_quality
        
        /// your desc
        self.lblNoofadult.text = Array_File.arrAdults[LookingData.yourDescription.no_of_adult - 1]
        self.lblGender.text = String("\(Array_File.arrGender[LookingData.yourDescription.gender - 1]) \(Array_File.arrSexuality[LookingData.yourDescription.sexuality - 1])")
        self.lblAge.text = Array_File.arrAgeGroup[LookingData.yourDescription.age_group - 1]
        self.ShowCheckUnceck(arrButton: self.btnSmoking, Selected: LookingData.yourDescription.smoking_type) //
        self.ShowCheckUnceck(arrButton: self.btnPets, Selected: LookingData.yourDescription.pets_type) //
        self.lblEmployement.text = Array_File.arrEmployment[LookingData.yourDescription.employment_situation - 1]
        self.arrMainintrest = LookingData.yourDescription.interest
        self.collInterestItems.reloadData()
        
        /// home preference
        self.lblPrice.text = String("$ \(LookingData.Homepreference.rent_budget) per month")
        self.lblBedroomtype.text = Array_File.arrBedroomtype[LookingData.Homepreference.badroom_type - 1]
        
        let bedroom : NSMutableArray = []
        for item in LookingData.Homepreference.badroom_size{
            bedroom.add(Array_File.arrBedroomSize[item as! Int - 1])
        }
        self.lblBedroomSize.text = bedroom.componentsJoined(by: ",")
        let home : NSMutableArray = []
        for item in LookingData.Homepreference.home_size{
            home.add(Array_File.arrHomeSize[item as! Int - 1])
        }
        self.lblHomeSize.text = home.componentsJoined(by: ",")
        
        self.ShowCheckUnceck(arrButton: self.btnBathroom, Selected: LookingData.Homepreference.badroom_facitity)
        self.ShowCheckUnceck(arrButton: self.btnParking, Selected: LookingData.Homepreference.parking_facitity)
        
        /// flatmate
        let gender : NSMutableArray = []
        for item in LookingData.Flatmatepreference.F_gendersexuality{
            gender.add(Array_File.arrGenderSexuality[item as! Int - 1])
        }
        self.lblGender_Flat.text = gender.componentsJoined(by: ",")
        
        let age : NSMutableArray = []
        for item in LookingData.Flatmatepreference.F_agegroup{
            age.add(Array_File.arrAgeGroup[item as! Int - 1])
        }
        self.lblAge_Flat.text = age.componentsJoined(by: ",")
        
        let arrsmok = LookingData.Flatmatepreference.F_smoking
        for item in arrsmok{
            self.ShowCheckUnceck(arrButton: self.btnSmoking_Flat, Selected: item as! Int)
        }
        
        let pets = LookingData.Flatmatepreference.F_pets
        for item in pets{
            self.ShowCheckUnceck(arrButton: self.btnPets_Flat, Selected: item as! Int)
        }
        
        
        
        self.FeaturedRoommateService()
    }
    
    func ShowCheckUnceck(arrButton : [UIButton], Selected : Int) {
        for button in arrButton {
            let tag = button.tag
            if Selected == tag{
                button.isSelected = true
            }
        }
    }
    func ChangeDateFormat(Date: String) -> String  {
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "yyyy-MM-dd"
        let newdate = dateFormatter.date(from: Date)
        dateFormatter.dateFormat = "MMM dd"
        return dateFormatter.string(from: newdate!)
    }

    @IBAction func BackMethod(_ sender: Any) {
        Common.PopMethod(VC: self)
    }
    
    @IBAction func FavoriteMethod(_ sender: Any) {
        let look = self.dicFlatmateProfile.value(forKey: "looking_home_preferences") as! NSDictionary
        self.LookingId = look.value(forKey: "looking_home_id") as! Int
        if self.dicFlatmateProfile.value(forKey: "is_favourite") as! Bool{
            self.btnLike.setImage(UIImage(named: "like"), for: .normal)
            self.isFavorite = false
        }else{
            self.btnLike.setImage(UIImage(named: "Clike"), for: .normal)
            self.isFavorite = true
        }
        self.FavoriteService()
    }
    
    @IBAction func clickOnAppoinmntBtn(_ sender : UIButton){
        let look = self.dicFlatmateProfile.value(forKey: "looking_home_preferences") as! NSDictionary
        self.LookingId = look.value(forKey: "looking_home_id") as! Int
        let story = AppStoryboard.Home.instance
        let vc = story.instantiateViewController(withIdentifier: "viewTimeViewController")as! viewTimeViewController
        vc.looking_id = self.LookingId
        self.navigationController?.pushViewController(vc, animated: true)
    }
    
    ////////////// Favorite Service ///////////
    func FavoriteService(){
        self.startActivityIndicator()
        var param : [String:Any] = [:]
        param = [ "is_favourite"  : self.isFavorite,
                  "looking_home_id" : self.LookingId
            ] as [String : Any]
        print(param)
        CommunicationManager().getResponseForPost(strUrl: Url_File.LikeRoommat , parameters: param as NSDictionary) { ( result , data) in
            if(result == "success") {
                let dataDict = data as! NSDictionary
                print(dataDict)
                let dic = dataDict["response"] as! NSDictionary
                print(dic)
                self.stopActivityIndicator()
            }else if (result == "error") {
                let strMessage = (data as! NSDictionary)["message"] as! String
                Common.ShowAlert(Title: Common.Title, Message: strMessage, VC: self)
                self.stopActivityIndicator()
            }else if (result == "Network") {
                let strMessage = (data as! String)
                Common.ShowAlert(Title: Common.Title, Message: strMessage, VC: self)
                self.stopActivityIndicator()
            }
        }
    }
    
    ////////////// Favorite Service ///////////
    func FeaturedRoommateService(){
        self.startActivityIndicator()
        var param : [String:Any] = [:]
        param = [ "user_id"  : SharedPreference.getUserData().user_id
            ] as [String : Any]
        print(param)
        CommunicationManager().getResponseForPost(strUrl: Url_File.FeaturedRoommates , parameters: param as NSDictionary) { ( result , data) in
            if(result == "success") {
                let dataDict = data as! NSDictionary
                print(dataDict)
                let dic = dataDict["response"] as! NSDictionary
                self.arrfeatureroommat = dic.value(forKey: "my_list") as! Array<Any>
                self.collFeaturedRoomMates.reloadData()
                print(dic)
                self.stopActivityIndicator()
            }else if (result == "error") {
                let strMessage = (data as! NSDictionary)["message"] as! String
                Common.ShowAlert(Title: Common.Title, Message: strMessage, VC: self)
                self.stopActivityIndicator()
            }else if (result == "Network") {
                let strMessage = (data as! String)
                Common.ShowAlert(Title: Common.Title, Message: strMessage, VC: self)
                self.stopActivityIndicator()
            }
        }
    }
}

//MArk - Image slider code///////////
extension FlatmateDetailViewController : UIScrollViewDelegate{
    
    func createSlides(){
        for i in 0..<imagesArray.count{
            let slide1 = Bundle.main.loadNibNamed("imageSliderView", owner: self, options: nil)?.first as! imageSliderView
            let img = imagesArray[i]
            slide1.imageView.sd_setImage(with: URL(string: img.url), placeholderImage: UIImage(named: "Banner"))
            slide1.btnNExt.addTarget(self, action: #selector(clickOnNExtArrowBtn(_:)), for: .touchUpInside)
            slide1.btnPrevious.addTarget(self, action: #selector(clickOnPreviousArrowBtn(_:)), for: .touchUpInside)
            if(i==0){
                slide1.btnPrevious.isHidden=true
            }
            if(i==4){
                slide1.btnNExt.isHidden=true
            }
            slides.append(slide1)
        }
        print(slides.count)
    }
    
    
    func setupSlideScrollView(slides : [imageSliderView]) {
        scrollView.contentSize = CGSize(width: scrollView.frame.width * CGFloat(slides.count), height:175)
        scrollView.isPagingEnabled = true
        for i in 0 ..< slides.count {
            slides[i].frame = CGRect(x: scrollView.frame.width * CGFloat(i), y: 0, width: scrollView.frame.width, height: scrollView.frame.height)
            scrollView.addSubview(slides[i])
        }
    }
    
    @objc func clickOnPreviousArrowBtn(_ sender : UIButton) {
        if scrollView.contentOffset.x > 0 {
            sender.isHidden=false
            scrollView.contentOffset.x -=  scrollView.bounds.width
        }else{
            sender.isHidden=true
        }
    }
    
    @objc func clickOnNExtArrowBtn(_ sender : UIButton){
        let scrollContnt = scrollView.bounds.width * CGFloat(slides.count-1)
        if (scrollView.contentOffset.x) < (scrollContnt) {
            sender.isHidden=false
            scrollView.contentOffset.x +=  scrollView.bounds.width
        }else{
            sender.isHidden=true
        }
    }
}


////MArk - Collectionview code
extension FlatmateDetailViewController : UICollectionViewDelegate , UICollectionViewDataSource , UICollectionViewDelegateFlowLayout{
    
    func numberOfSections(in collectionView: UICollectionView) -> Int {
        return 1
    }
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int{
        var count:Int?
        if(collectionView == self.collInterestItems){
            count = self.arrMainintrest.count
        }
        
        if(collectionView == self.collFeaturedRoomMates){
            count = self.arrfeatureroommat.count
        }
        return count!
        
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        
        let cell : UICollectionViewCell = UICollectionViewCell()
        
        if collectionView == self.collInterestItems {
            let cell = collInterestItems.dequeueReusableCell(withReuseIdentifier: "collInterestItemsCell", for: indexPath) as! collInterestItemsCell
            
            let arrmain = Array_File.arrIntesrestOptionImgs
            
            for j in 0...arrmain.count - 1{
                let id = self.arrMainintrest[indexPath.row] as! Int
                if j == id {
                    cell.isSelected=true
                    self.collInterestItems.selectItem(at: indexPath, animated: true, scrollPosition: UICollectionViewScrollPosition.top)
                    cell.imgInterets.image = UIImage(named: Array_File.arrIntesrestOptionImgs[id]["image"]!)?.imageWithInsets(insetDimen: 8)
                    cell.imgInterets.tintImageColor(color: UIColorFromHex(rgbValue: 0x05C6CF, alpha: 1.0))
                    cell.lblInteretsName.text = Array_File.arrIntesrestOptionImgs[id]["name"]!
                    break
                }
            }
            return cell
        }
        
        if collectionView == self.collFeaturedRoomMates{
            let cell = collFeaturedRoomMates.dequeueReusableCell(withReuseIdentifier: "collFeaturedRoomMatesCell", for: indexPath) as! collFeaturedRoomMatesCell
            
            let dic = self.arrfeatureroommat[indexPath.row] as! NSDictionary
            let LookingData = LookingProfileDetailData.init(withDictionary: dic as! [String : Any])
            let arrimg = LookingData.arrImages
            if arrimg.count != 0{
                let dicimg = arrimg[0]
                cell.imgRoomMate.sd_setImage(with: URL(string: dicimg.url), placeholderImage: UIImage(named: "Banner"))
            }else{
                cell.imgRoomMate.image = UIImage(named: "Banner")
            }
            cell.lblRoomMateName.text = LookingData.Userdata.user_fullname
            let gender = LookingData.Userdata.user_gender
            cell.lblage.text = "\(LookingData.Userdata.user_age) Years \(Array_File.arrGender[gender])"
            cell.lblPrice.text = String("$ \(LookingData.Homepreference.rent_budget)")
            return cell
        }
        return cell
        
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        
        var size : CGSize?
        if(collectionView == self.collInterestItems){
            let yourWidth = collectionView.bounds.width/4.0
            size = CGSize(width: yourWidth, height: 110.0)
            
        }
        
        if(collectionView == self.collFeaturedRoomMates){
            let yourWidth = collectionView.bounds.width/2.0
            size = CGSize(width: yourWidth, height: 130.0)
        }
        
        return size!
        
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, insetForSectionAt section: Int) -> UIEdgeInsets {
        return UIEdgeInsets.zero
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumInteritemSpacingForSectionAt section: Int) -> CGFloat {
        return 0
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumLineSpacingForSectionAt section: Int) -> CGFloat {
        return 0
    }
}

class collInterestItemsCell : UICollectionViewCell{
    
    @IBOutlet weak var imgInterets : UIImageView!
    @IBOutlet weak var lblInteretsName : UILabel!
}

class collFeaturedRoomMatesCell : UICollectionViewCell{
    
    @IBOutlet weak var lblPrice: UILabel!        
    @IBOutlet weak var lblage: UILabel!
    @IBOutlet weak var imgRoomMate : UIImageView!
    @IBOutlet weak var lblRoomMateName : UILabel!
}

//{
//    message = "Show feature roommates lists.";
//    response =     {
//        "my_list" =         (
//            {
//                "is_favourite" = 0;
//                "looking_flatmate_prefrences" =                 {
//                    "age_group" = "1,2";
//                    "created_at" = "2018-09-10 09:59:24";
//                    "gender_and_sexuality" = "1,2";
//                    id = 2;
//                    "looking_home_id" = 1;
//                    pets = "1,2";
//                    "profile_id" = 1;
//                    smoking = "1,2";
//                    "updated_at" = "2018-09-10 09:59:24";
//                    "user_id" = 2;
//                };
//                "looking_home_preferences" =                 {
//                    "bathroom_facilities" = 2;
//                    "bedroom_size" = "1,2";
//                    "bedroom_type" = 1;
//                    "created_at" = "2018-09-10 09:58:51";
//                    "home_size" = "1,2";
//                    id = 1;
//                    "looking_home_id" = 1;
//                    "parking_facility" = 1;
//                    "profile_id" = 2;
//                    "rent_budget" = 445;
//                    "updated_at" = "2018-09-10 10:38:13";
//                    "user_id" = 1;
//                };
//                "looking_introduce_yourself" =                 {
//                    "available_date" = "2018-10-10";
//                    "created_at" = "2018-09-10 09:57:25";
//                    id = 1;
//                    "looking_home_id" = 1;
//                    "personal_qualities" = test;
//                    "profile_id" = 2;
//                    "stay_duration" = 2;
//                    "updated_at" = "2018-09-11 06:28:49";
//                    "user_id" = 1;
//                };
//                "looking_your_description" =                 {
//                    "age_group" = 2;
//                    "created_at" = "2018-09-10 09:57:13";
//                    "employment_situation" = 1;
//                    gender = 1;
//                    id = 1;
//                    "is_active" = 1;
//                    "looking_home_id" = 1;
//                    "main_interest" = "";
//                    "no_of_adults" = 1;
//                    pets = 1;
//                    "profile_id" = 2;
//                    sexuality = 1;
//                    smoking = 1;
//                    "updated_at" = "2018-09-11 06:28:45";
//                    "user_id" = 1;
//                };
//                "matches_percentage" = 60;
//                photos =                 {
//                    "upload_photo" =                     (
//                        {
//                            "created_at" = "2018-09-10 09:57:43";
//                            id = 1;
//                            "looking_home_id" = 1;
//                            photo = "http://18.223.28.186/photogallry/photogallry/PqoYjdJTTvzr8FLGf78DeMsPgxMpvT6PoqqnA7nU.jpeg";
//                            "photo_id" = 1;
//                            "updated_at" = "2018-09-10 09:57:43";
//                    },
//                        {
//                            "created_at" = "2018-09-10 09:57:43";
//                            id = 2;
//                            "looking_home_id" = 1;
//                            photo = "http://18.223.28.186/photogallry/photogallry/mCSIQtdHkrPoiEKoFNVN2yHxULslq4qxFv5yVvaD.jpeg";
//                            "photo_id" = 2;
//                            "updated_at" = "2018-09-10 09:57:43";
//                    }
//                    );
//                };
//                "prefered_location" =                 {
//                    address = "Indore, Madhya Pradesh, India";
//                    "created_at" = "2018-09-10 09:57:54";
//                    id = 1;
//                    latitude = "22.7196";
//                    longitude = "75.85769999999999";
//                    "looking_home_id" = 1;
//                    "profile_id" = 2;
//                    "updated_at" = "2018-09-11 06:29:01";
//                    "user_id" = 2;
//                };
//                "profile_completed" = 100;
//                user =                 {
//                    age = 0;
//                    city = "";
//                    "contact_advice" = "";
//                    country = "";
//                    "country_code" = "";
//                    "country_latitude" = 0;
//                    "country_longitude" = 0;
//                    "created_at" = "2018-09-10 02:28:54";
//                    email = "akansha.n@westcoastinfotech.com";
//                    "email_verified" = 1;
//                    "facebook_id" = "";
//                    "fcm_token" = FDX;
//                    gender = 0;
//                    "google_id" = "";
//                    id = 1;
//                    "is_active" = 0;
//                    "is_admin_verified" = 0;
//                    "is_contact_private" = 0;
//                    "is_facebook_verified" = 0;
//                    "is_google_verified" = 0;
//                    "is_linkedin_verified" = 0;
//                    "linkedin_id" = "";
//                    "login_type" = 1;
//                    mobile = 97896523255;
//                    "mobile_verified" = 0;
//                    name = "";
//                    "profile_pic" = "";
//                    "refer_by" = "";
//                    "reference_code" = "";
//                    state = "";
//                    status = 0;
//                    "updated_at" = "2018-09-24 11:56:48";
//                    "user_id" = 1;
//                    "user_latitude" = 0;
//                    "user_longitude" = 0;
//                };
//        },
//            {
//                "appoinment_id" = 8;
//                "is_favourite" = 0;
//                "looking_flatmate_prefrences" =                 {
//                    "age_group" = "1,2";
//                    "created_at" = "2018-09-25 09:46:15";
//                    "gender_and_sexuality" = "1,2";
//                    id = 17;
//                    "looking_home_id" = 17;
//                    pets = "1,2";
//                    "profile_id" = 2;
//                    smoking = "1,2";
//                    "updated_at" = "2018-09-25 09:46:15";
//                    "user_id" = 3;
//                };
//                "looking_home_preferences" =                 {
//                    "bathroom_facilities" = 1;
//                    "bedroom_size" = "1,2";
//                    "bedroom_type" = 1;
//                    "created_at" = "2018-09-25 09:46:08";
//                    "home_size" = "1,2";
//                    id = 16;
//                    "looking_home_id" = 17;
//                    "parking_facility" = 1;
//                    "profile_id" = 2;
//                    "rent_budget" = 228;
//                    "updated_at" = "2018-09-25 09:46:08";
//                    "user_id" = 3;
//                };
//                "looking_introduce_yourself" =                 {
//                    "available_date" = "2018-09-26";
//                    "created_at" = "2018-09-25 09:45:33";
//                    id = 16;
//                    "looking_home_id" = 17;
//                    "personal_qualities" = test;
//                    "profile_id" = 2;
//                    "stay_duration" = 1;
//                    "updated_at" = "2018-09-25 09:45:33";
//                    "user_id" = 3;
//                };
//                "looking_your_description" =                 {
//                    "age_group" = 1;
//                    "created_at" = "2018-09-25 09:45:25";
//                    "employment_situation" = 1;
//                    gender = 2;
//                    id = 17;
//                    "is_active" = 1;
//                    "looking_home_id" = 17;
//                    "main_interest" = "10,6,5";
//                    "no_of_adults" = 1;
//                    pets = 1;
//                    "profile_id" = 2;
//                    sexuality = 1;
//                    smoking = 1;
//                    "updated_at" = "2018-09-26 05:55:10";
//                    "user_id" = 3;
//                };
//                "matches_percentage" = 50;
//                photos =                 {
//                    "upload_photo" =                     (
//                    );
//                };
//                "prefered_location" =                 {
//                    address = "Chhoti Gwaltoli, Indore, Madhya Pradesh 452007, India";
//                    "created_at" = "2018-09-25 09:46:01";
//                    id = 16;
//                    latitude = "22.717019";
//                    longitude = "75.86843710000001";
//                    "looking_home_id" = 17;
//                    "profile_id" = 2;
//                    "updated_at" = "2018-09-25 09:46:01";
//                    "user_id" = 3;
//                };
//                "profile_completed" = 83;
//                user =                 {
//                    age = 21;
//                    city = "";
//                    "contact_advice" = "";
//                    country = BN;
//                    "country_code" = IN;
//                    "country_latitude" = 0;
//                    "country_longitude" = 0;
//                    "created_at" = "2018-09-10 09:58:00";
//                    email = "bansal@gmail.com";
//                    "email_verified" = 0;
//                    "facebook_id" = "";
//                    "fcm_token" = "f03MnR8j-wE:APA91bHVODCXhvB3QR3488q-fm46vNht7f6dl57dQvUVjd9TS0BRpcyvpBNYrvbr7Rj1PMoA75bnl-sBt5bSNtUm6ddVUHUolD-JcMwdgg1nry-ATUL8oFkbSR5WL08bI8kGhCbe_N4C";
//                    gender = 2;
//                    "google_id" = "";
//                    id = 3;
//                    "is_active" = 0;
//                    "is_admin_verified" = 0;
//                    "is_contact_private" = 0;
//                    "is_facebook_verified" = 0;
//                    "is_google_verified" = 0;
//                    "is_linkedin_verified" = 0;
//                    "linkedin_id" = "";
//                    "login_type" = 1;
//                    mobile = 7987191186;
//                    "mobile_verified" = 1;
//                    name = "julee ban";
//                    "profile_pic" = "http://18.223.28.186/upload/profile_pic/1536578821.jpg";
//                    "refer_by" = "";
//                    "reference_code" = "";
//                    state = "";
//                    status = 0;
//                    "updated_at" = "2018-09-28 11:13:30";
//                    "user_id" = 3;
//                    "user_latitude" = 0;
//                    "user_longitude" = 0;
//                };
//        }
//        );
//    };
//    status = 1;
//}

