import UIKit

class MessagesViewController: UIViewController {
    
    @IBOutlet weak var viewNoMessage : UIView!
    @IBOutlet weak var tblMessageList : UITableView!

    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.viewNoMessage.isHidden=true
        
        self.tblMessageList.tableFooterView=UIView()
        self.tblMessageList.estimatedRowHeight = 200
        self.tblMessageList.rowHeight = UITableViewAutomaticDimension
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        self.navigationController?.isNavigationBarHidden = true
        rdv_tabBarController?.setTabBarHidden(false, animated: true)
    }
}

extension MessagesViewController : UITableViewDelegate , UITableViewDataSource{
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 5
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = self.tblMessageList.dequeueReusableCell(withIdentifier: "messageTblCell") as! messageTblCell
        
        cell.btnRightarrow.tag=indexPath.row
        cell.btnRightarrow.addTarget(self, action: #selector(clickOnNextBtn(_:)), for: .touchUpInside)
        
        return cell
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 122
    }
    
    @objc func clickOnNextBtn(_ sender : UIButton){
        let storyboard = UIStoryboard(name: "Home", bundle: nil)
        let vc = storyboard.instantiateViewController(withIdentifier: "ChatViewController") as! ChatViewController
        self.navigationController?.pushViewController(vc, animated: true)
        
    }
    
}

class messageTblCell: UITableViewCell {
    
    @IBOutlet weak var btnRightarrow : UIButton!
}
