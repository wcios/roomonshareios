import UIKit

class HomeViewController: UIViewController {
    
    @IBOutlet weak var lblHeading: UILabel!
    @IBOutlet weak var tblHome: UITableView!
    @IBOutlet weak var lblNote: UILabel!    
    
    var arrMyListing : Array<Any> = []
    var userdata = SharedPreference.getUserData()
    
    override func viewDidLoad() {
        
       // print(userdata.user_email)
        
        super.viewDidLoad()
        if userdata.email_verified == false{
         self.EmailPopup()
        }
        tblHome.register(UINib(nibName: "LookingProfileTableViewCell", bundle: nil), forCellReuseIdentifier: "LookingProfile")
        
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
  
    override func viewWillAppear(_ animated: Bool) {
        self.navigationController?.isNavigationBarHidden = true
        rdv_tabBarController?.setTabBarHidden(false, animated: true)
        self.MyListingService()
    }
    
    @IBAction func btnCreateProfileMethod(_ sender: Any) {
        if userdata.email_verified == false{
            self.EmailPopup()
        }else{
            let storyboard = UIStoryboard(name: "Main", bundle: nil)
            let vc = storyboard.instantiateViewController(withIdentifier: "CreateProifleViewController") as UIViewController
            self.navigationController?.pushViewController(vc, animated: true)
        }
    }
    
    @IBAction func btnMessageMethod(_ sender: Any) {
        
    }
    
    @IBAction func btnFavoriteMethod(_ sender: Any) {
        Common.PushMethod(VC: self , identifier:  "matchFavouriteViewController")
    }
    
    
    //MyListing
    func MyListingService() {
        
        self.startActivityIndicator()

        CommunicationManager().getResponseFor(strUrl: Url_File.MyListing, parameters: nil, completion: { ( result , data) in
            
            if(result == "success") {
                CreateProifleViewController.looking_id = 0
                self.stopActivityIndicator()
                let dataDict = data as! NSDictionary
                let dic = dataDict["response"] as! NSDictionary
                self.arrMyListing = dic.value(forKey: "my_list") as! Array<Any>
                print(self.arrMyListing)
                
                if(self.arrMyListing.count==0){
                    self.lblNote.text = "You can list your profile on Room On Share using toolbar icon"
                    self.tblHome.isHidden = true
                    self.lblNote.isHidden=false
                }else{
                    self.tblHome.isHidden = false
                    self.lblNote.isHidden=true
                    self.tblHome.reloadData()
                }
            }else if (result == "error") {
                let strMessage = (data as! NSDictionary)["message"] as! String
                Common.ShowAlert(Title: Common.Title, Message: strMessage, VC: self)
                self.stopActivityIndicator()
            }else if (result == "Network") {
                let strMessage = (data as! String)
                Common.ShowAlert(Title: Common.Title, Message: strMessage, VC: self)
                self.stopActivityIndicator()
            }
        })
    }
    ////////////// Delete Service ///////////
    func DeleteLookingService(profileid : Int, Id : Int){
        self.startActivityIndicator()
        var param : [String : Any] = [:]
        if profileid == 2{
            param = ["profile_id" : 2,
                     "looking_home_id" : Id
                ] as [String : Any]
        }else{
            param = ["profile_id" : 1,
                     "offering_home_id" : Id
                ] as [String : Any]
        }
        
        print(param)
        
        CommunicationManager().getResponseForPost(strUrl: Url_File.DeleteLookingProfile, parameters: param as NSDictionary) { ( result , data) in            
            if(result == "success") {
                let dataDict = data as! NSDictionary
                print(dataDict)
                self.MyListingService()
                self.stopActivityIndicator()
            }else if (result == "error") {
                let strMessage = (data as! NSDictionary)["message"] as! String
                Common.ShowAlert(Title: Common.Title, Message: strMessage, VC: self)
                self.stopActivityIndicator()
            }else if (result == "Network") {
                let strMessage = (data as! String)
                Common.ShowAlert(Title: Common.Title, Message: strMessage, VC: self)
                self.stopActivityIndicator()
            }
        }
    }
    
    ////////////// Email popup ///////////
    func EmailPopup() {
        let alert = UIAlertController(title: "Verify Your Email Id", message:
            "We have send an email to your registered email id.Please click on verification link to verify your email.", preferredStyle: UIAlertControllerStyle.alert)
        let Ignore = UIAlertAction(title: "IGNORE", style: UIAlertActionStyle.default) { (action) in
    
        }
        
        let resend = UIAlertAction(title: "RESEND", style: UIAlertActionStyle.default) { (action) in
            self.VerifyEmailService()
        }
        
        alert.addAction(Ignore)
        alert.addAction(resend)
        self.present(alert, animated: true, completion: nil)
    }
    
    ////////////// Verify Email Service ///////////
    func VerifyEmailService(){
        self.startActivityIndicator()
        CommunicationManager().getResponseForPost(strUrl: Url_File.VerifyEmail , parameters: nil) { ( result , data) in
            if(result == "success") {
                let dataDict = data as! NSDictionary
                print(dataDict)
                let strMessage = dataDict["message"] as! String
                Common.ShowAlert(Title: Common.Title, Message: strMessage, VC: self)
                self.stopActivityIndicator()
            }else if (result == "error") {
                let strMessage = (data as! NSDictionary)["message"] as! String
                Common.ShowAlert(Title: Common.Title, Message: strMessage, VC: self)
                self.stopActivityIndicator()
            }else if (result == "Network") {
                let strMessage = (data as! String)
                Common.ShowAlert(Title: Common.Title, Message: strMessage, VC: self)
                self.stopActivityIndicator()
            }
        }
    }
}

////////////// Table view  ///////////
extension HomeViewController: UITableViewDelegate, UITableViewDataSource {
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
   func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.arrMyListing.count
    }

    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = self.tblHome.dequeueReusableCell(withIdentifier: "LookingProfile") as! LookingProfileTableViewCell
        
        let img_pro = SharedPreference.getUserData().user_profilepic
        cell.imgProfile.sd_setImage(with: URL(string: img_pro), placeholderImage: UIImage(named: "UserPlaceholder"))
        let dic = self.arrMyListing[indexPath.row] as! NSDictionary
        print(dic.value(forKey: "profile_id") as! Int)
        
        if dic.value(forKey: "profile_completed") as! Int == 100 {
            cell.lblStatus.backgroundColor = self.UIColorFromHex(rgbValue: 0x032AB43, alpha: 1.0)
            cell.lblStatus.text = "UPDATED"
        }else{
            cell.lblStatus.backgroundColor = self.UIColorFromHex(rgbValue: 0xE77E23, alpha: 1.0)
            cell.lblStatus.text = "INCOMPLETE"
        }        
        cell.btnDelete.tag = indexPath.row
        cell.btnEdit.tag = indexPath.row
        cell.btnMatches.tag = indexPath.row
         cell.btnMsg.tag = indexPath.row
        cell.btnDelete.addTarget(self, action: #selector(DeleteMethod(btn:)), for: .touchUpInside)
        cell.btnEdit.addTarget(self, action: #selector(EditMethod(btn:)), for: .touchUpInside)
        cell.btnMatches.addTarget(self, action: #selector(MatchesMethod(btn:)), for: .touchUpInside)
        cell.btnMsg.addTarget(self, action: #selector(AppoinmnetMethod(btn:)), for: .touchUpInside)
        
        if dic.value(forKey: "profile_id") as! Int == 2 {
            /////////////// Looking ///////////
            CreateProifleViewController.looking_id = 1
            let LookingData = LookingProfileDetailData.init(withDictionary: dic as! [String : Any])
            cell.viewoffer.isHidden = true
            cell.lblName.text = userdata.user_fullname
            let gender = Array_File.arrGender[userdata.user_gender - 1]
            cell.lblAddress.text = String("\(userdata.user_age) Years \(gender)")
            
            if LookingData.Introduceyourself.avalibality_date !=  ""{
                cell.lblDate.text = Common.ChangeDateFormat(Date: LookingData.Introduceyourself.avalibality_date, fromFormat: "yyyy-MM-dd", toFormat: "dd MMM")
                cell.lblAvailable.text = Array_File.arrLength_of_stay[LookingData.Introduceyourself.prefered_length_stay - 1]
            }
            
            let profile_view = LookingData.yourDescription.views
            if(profile_view == 0){
                cell.lblviewsCount.text = "\(profile_view) view"
            }else{
                cell.lblviewsCount.text = "\(profile_view) views"
            }
            
            let is_status = LookingData.yourDescription.is_active
            if is_status{
                cell.lblActive.text = "ACTIVE"
                cell.lblActive.textColor = UIColorFromHex(rgbValue: 0x44aa46, alpha: 1.0)
                
            }else{
                cell.lblActive.text = "DEACTIVE"
                cell.lblActive.textColor = UIColorFromHex(rgbValue: 0xD44936, alpha: 1.0)
            }
        }else{
            /////////////// Offering ///////////
            let offering = ProfileFlatmateDetailData.init(withDictionary: dic as! [String : Any])
            cell.viewoffer.isHidden = false
            cell.lblAvailable.text = ""
            cell.lblName.text = offering.homedescription.home_address
            cell.lblAddress.text = offering.homedescription.home_location
            
            let profile_view = offering.homedescription.views
            if(profile_view == 0){
                cell.lblviewsCount.text = "\(profile_view) view"
            }else{
                cell.lblviewsCount.text = "\(profile_view) views"
            }
            
            let status = offering.homedescription.is_active
            if status{
                cell.lblActive.text = "ACTIVE"
                cell.lblActive.textColor = UIColorFromHex(rgbValue: 0x44aa46, alpha: 1.0)
            }else{
                cell.lblActive.text = "DEACTIVE"
                cell.lblActive.textColor = UIColorFromHex(rgbValue: 0xD44936, alpha: 1.0)
            }
            if offering.bedroomdescription.count != 0{
                let dicBedroom = offering.bedroomdescription[0]
                cell.lblDate.text = Common.ChangeDateFormat(Date: dicBedroom.avalibality_date, fromFormat: "yyyy-MM-dd", toFormat: "dd MMM")
            }
            cell.lblviewsCount.text = String("100 Views")
            if offering.homedescription.home_type == 2{
                cell.imgHometype.image = #imageLiteral(resourceName: "apartment")
            }else{
                cell.imgHometype.image = #imageLiteral(resourceName: "Dhouse")
            }
            
            if offering.homedescription.parking_facitity == 1{
                cell.imgParkingtype.image = #imageLiteral(resourceName: "covered-parking")
            }else if offering.homedescription.parking_facitity == 2{
                cell.imgParkingtype.image = #imageLiteral(resourceName: "Parking")
            }else{
                cell.imgParkingtype.image = #imageLiteral(resourceName: "no-parking")
            }
            
        }
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let dic = self.arrMyListing[indexPath.row] as! NSDictionary
        if dic.value(forKey: "profile_id") as! Int == 2 {
            let story = UIStoryboard(name: "Detail", bundle: nil)
            let profile = story.instantiateViewController(withIdentifier: "lookingProfileDetailViewController") as! lookingProfileDetailViewController
            profile.dicLookingDetail = self.arrMyListing[indexPath.row] as! NSDictionary
            self.navigationController?.pushViewController(profile, animated: true)
        }else{
            let story = UIStoryboard(name: "Detail", bundle: nil)
            let profile = story.instantiateViewController(withIdentifier: "profileFlatmateViewController") as! profileFlatmateViewController
            profile.dicOfferingDetail = self.arrMyListing[indexPath.row] as! NSDictionary
            self.navigationController?.pushViewController(profile, animated: true)
        }
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 165
    }
    
    ////////////// delete button ///////////
    @objc func DeleteMethod(btn : UIButton) {
        
        let Index = btn.tag
        let dic = self.arrMyListing[Index] as! NSDictionary
        print(dic)
        
        let alert = UIAlertController(title: Common.Title, message:
            "Are you sure, you want to delete this profile", preferredStyle: UIAlertControllerStyle.alert)
        let okAction = UIAlertAction(title: "Yes", style: UIAlertActionStyle.default) { (action) in
            if dic.value(forKey: "profile_id") as! Int == 2 {
                let LookingId =  dic.value(forKey: "Id") as! Int
                self.DeleteLookingService(profileid: 2, Id: LookingId)
            }else{
                let offeringId =  dic.value(forKey: "Id") as! Int
                self.DeleteLookingService(profileid: 1, Id: offeringId)
            }
        }
        
        let CancleAction = UIAlertAction(title: "No", style: UIAlertActionStyle.default) { (action) in
            
        }
        
        alert.addAction(okAction)
        alert.addAction(CancleAction)
        self.present(alert, animated: true, completion: nil)
    }
    
    ////////////// edit button ///////////
    @objc func EditMethod(btn : UIButton) {
        let Index = btn.tag
        let dic = self.arrMyListing[Index] as! NSDictionary
        if dic.value(forKey: "profile_id") as! Int == 2 {
            let story = UIStoryboard(name: "Detail", bundle: nil)
            let profile = story.instantiateViewController(withIdentifier: "lookingProfileDetailViewController") as! lookingProfileDetailViewController
            profile.dicLookingDetail = dic
            self.navigationController?.pushViewController(profile, animated: true)
        }else{
            let story = UIStoryboard(name: "Detail", bundle: nil)
            let profile = story.instantiateViewController(withIdentifier: "profileFlatmateViewController") as! profileFlatmateViewController
            profile.dicOfferingDetail = dic
            self.navigationController?.pushViewController(profile, animated: true)
        }
    }
    
    ////////////// Matches button ///////////
    @objc func MatchesMethod(btn : UIButton) {
        let Index = btn.tag
        let dic = self.arrMyListing[Index] as! NSDictionary
        if dic.value(forKey: "profile_id") as! Int == 2 {
            let story = UIStoryboard(name: "Home", bundle: nil)
            let profile = story.instantiateViewController(withIdentifier: "RoomMatchesViewController") as! RoomMatchesViewController
            self.navigationController?.pushViewController(profile, animated: true)
        }else{
            let id = (dic.value(forKey: "offering_home") as! NSDictionary)["offering_home_id"] as! Int
            print(id)
            let story = UIStoryboard(name: "Home", bundle: nil)
            let profile = story.instantiateViewController(withIdentifier: "RoommateMatchesViewController") as! RoommateMatchesViewController
            profile.offering_id = String(id)
            self.navigationController?.pushViewController(profile, animated: true)
        }
    }
    
    
    @objc func AppoinmnetMethod(btn : UIButton) {
        
        let story = UIStoryboard(name: "Home", bundle: nil)
        let profile = story.instantiateViewController(withIdentifier: "myAppoinmnetViewController") as! myAppoinmnetViewController
        self.navigationController?.pushViewController(profile, animated: true)
    }
}
