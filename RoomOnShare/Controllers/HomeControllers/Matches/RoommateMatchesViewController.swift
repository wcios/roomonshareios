//
//  RoommateMatchesViewController.swift
//  
//
//  Created by mac on 29/09/18.
//

import UIKit
import GooglePlacePicker

class RoommateMatchesViewController: UIViewController {

    @IBOutlet weak var tblRoommateMatches: UITableView!
    @IBOutlet weak var lblStatus : UILabel!
    @IBOutlet weak var btnFilter: UIButton!
    
    @IBOutlet weak var viewRoommate: UIView!
    @IBOutlet weak var txtLastActive: UITextField!
    @IBOutlet weak var txtRLocation: UITextField!
    @IBOutlet weak var txtGender: UITextField!
    @IBOutlet weak var txtAge: UITextField!
    @IBOutlet weak var btnGoogle: UIButton!
    @IBOutlet weak var btnFacebook: UIButton!
    @IBOutlet weak var btnLinkedin: UIButton!
    @IBOutlet var btnRSmoking: [UIButton]!
    @IBOutlet var btnRPets: [UIButton]!
    @IBOutlet weak var txtMoveinDate: UITextField!
    @IBOutlet weak var sliderRrent: UISlider!
    @IBOutlet weak var lblRrent: UILabel!
    
    var locationManager:CLLocationManager!
    var userdata = SharedPreference.getUserData()
    var arrMatches : Array<Any> = []    
    var arrSmoking : NSMutableArray = []
    var arrPet : NSMutableArray = []
    var arrAge : [String] = []
    var id_Pet  = ""
    var id_Smoking = ""
    var availableDate = ""
    var Rent = ""
    var latitude  = ""
    var longitude = ""
    var age = ""
    var gender = ""
    var G_id = ""
    var FB_id = ""
    var LN_id = ""
    var last_active = "all"
    var offering_id  = ""
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    override func viewWillAppear(_ animated: Bool) {
        self.viewRoommate.isHidden = true
        self.lblStatus.isHidden = true
        self.navigationController?.isNavigationBarHidden = true
        rdv_tabBarController?.setTabBarHidden(true, animated: true)
        
        self.determineMyCurrentLocation()
        self.txtLastActive.delegate = self
        self.txtAge.delegate = self
        self.txtGender.delegate = self
        self.txtRLocation.delegate = self
        self.txtMoveinDate.delegate = self
        
        for i in 18..<101{
            self.arrAge.append(String(i))
        }
        self.MatchesRoommateService()
        self.tblRoommateMatches.register(UINib(nibName: "roomMateTblCell", bundle: nil), forCellReuseIdentifier: "roomMateTblCell")
    }
    
    override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {        
        self.viewRoommate.isHidden = true
    }
    
    @IBAction func BackMethod(_ sender: Any) {
        Common.PopMethod(VC: self)
    }
    
    ////////////// Filter ///////////
    @IBAction func FilterMethod(_ sender: Any) {
        self.viewRoommate.isHidden = false
    }
    
    //////////////// romemate filter //////
    
    @IBAction func GoogleMethod(_ sender: Any) {
        if self.btnGoogle.isSelected{
            self.btnGoogle.isSelected = false
        }else{
            self.btnGoogle.isSelected = true
            self.G_id = "true"
        }
    }
    
    @IBAction func FacebookMethod(_ sender: Any) {
        if self.btnFacebook.isSelected{
            self.btnFacebook.isSelected = false
        }else{
            self.btnFacebook.isSelected = true
            self.FB_id = "true"
        }
    }
    
    @IBAction func LinkedInMethod(_ sender: Any) {
        if self.btnLinkedin.isSelected{
            self.btnLinkedin.isSelected = false
        }else{
            self.btnLinkedin.isSelected = true
            self.LN_id = "true"
        }
    }
    
    @IBAction func SliderRrent(_ sender: UISlider) {
        let rent = Int(sender.value)
        self.Rent = String(rent)
        self.lblRrent.text = "$\(self.Rent)/month"
        print(rent)
    }
    
    @IBAction func RSmokingMethod(_ sender: UIButton) {
        let Selectedtag = sender.tag
        if sender.isSelected {
            sender.isSelected = false
            arrSmoking.remove(Selectedtag)
        }else{
            sender.isSelected = true
            arrSmoking.add(Selectedtag)
        }
        self.id_Smoking = arrSmoking.componentsJoined(by: ",")
        print(self.id_Smoking)
    }
    
    @IBAction func RPetsMethod(_ sender: UIButton) {
        let Selectedtag = sender.tag
        if sender.isSelected {
            sender.isSelected = false
            arrPet.remove(Selectedtag)
        }else{
            sender.isSelected = true
            arrPet.add(Selectedtag)
        }
        self.id_Pet = arrPet.componentsJoined(by: ",")
        print(self.id_Pet)
    }
    
    @IBAction func RApplyMethod(_ sender: Any) {
        self.viewRoommate.isHidden = true
        self.MatchesRoommateService()
    }
    
    @IBAction func RResetMethod(_ sender: Any) {
        self.sliderRrent.setValue(0, animated: true)
        self.txtLastActive.text = Array_File.arrLastActive[0]
        self.txtMoveinDate.text = "Move in date"
        self.txtAge.text = "Select age"
        self.txtRLocation.text = "Select Location"
        self.txtGender.text = "Select gender"
        
        self.Rent = ""
        self.last_active = "All"
        self.gender = ""
        self.age = ""
        self.latitude = ""
        self.longitude = ""
        self.id_Pet = ""
        self.id_Smoking = ""
        self.availableDate = ""
        self.FB_id = ""
        self.G_id = ""
        self.LN_id = ""
        
        
        for item in self.btnRPets{
            item.isSelected = false
        }
        for item in self.btnRSmoking{
            item.isSelected = false
        }
    }
    
    ////////////// MatchesRoommate Service ///////////
    func MatchesRoommateService(){
        self.startActivityIndicator()
        var param : [String:Any] = [:]
        param = [ "offering_home_id"  : self.offering_id,
                  "last_active"  : self.last_active,
                  "budget" : self.Rent,
                  "location_latitude"  : self.latitude,
                  "location_longitude" : self.longitude,
                  "gender" : self.gender,
                  "available_date" : self.availableDate,
                  "age" : self.age ,
                  "smoking" : id_Smoking,
                  "pets" : id_Pet,
                  "facebook_verification" : self.FB_id,
                  "google_verification" : self.G_id,
                  "linkedin_verification" : self.LN_id
            ] as [String : Any]
        
        print(param)
        
        CommunicationManager().getResponseForPost(strUrl: Url_File.MatchesRoommate , parameters: param as NSDictionary) { ( result , data) in
            if(result == "success") {
                self.stopActivityIndicator()
                
                let dataDict = data as! NSDictionary
                print(dataDict)
                let dic = dataDict["response"] as! NSDictionary
                self.arrMatches = dic.value(forKey: "my_list") as! Array<Any>
                print(dic)
                if(self.arrMatches.count == 0){
                    self.lblStatus.isHidden = false
                    self.tblRoommateMatches.isHidden = true
                    self.lblStatus.text = "No Roommate matches available."
                }else{
                     self.lblStatus.isHidden = true
                    self.tblRoommateMatches.isHidden = false
                    self.tblRoommateMatches.reloadData()
                }
                
           }else if (result == "error") {
                let strMessage = (data as! NSDictionary)["message"] as! String
                Common.ShowAlert(Title: Common.Title, Message: strMessage, VC: self)
                self.stopActivityIndicator()
            }else if (result == "Network") {
                let strMessage = (data as! String)
                Common.ShowAlert(Title: Common.Title, Message: strMessage, VC: self)
                self.stopActivityIndicator()
            }
        }
    }
}


////////////// Tableview ///////////
extension RoommateMatchesViewController: UITableViewDelegate, UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.arrMatches.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let dic = self.arrMatches[indexPath.row] as! NSDictionary
        
        let cell = self.tblRoommateMatches.dequeueReusableCell(withIdentifier: "roomMateTblCell") as! roomMateTblCell
        let LookingData = LookingProfileDetailData.init(withDictionary: dic as! [String : Any])
        let arrimg = LookingData.arrImages
        if arrimg.count != 0{
            let dicimg = arrimg[0]
            cell.imgProfile.sd_setImage(with: URL(string: dicimg.url), placeholderImage: UIImage(named: "Banner"))
        }else{
            cell.imgProfile.image = UIImage(named: "Banner")
        }
        
        cell.imgV1.isHidden = true
        cell.imgV2.isHidden = true
        cell.imgV3.isHidden = true
        cell.imgV4.isHidden = true
        cell.imgV5.isHidden = true
        
        let user = LookingData.Userdata
        
        if user.admin_verified {
            cell.imgV1.isHidden = false
        }
        if user.mobile_verified && user.email_verified {
            cell.imgV2.isHidden = false
        }
        if user.facebook_id != "" {
            cell.imgV3.isHidden = false
        }
        if user.google_id != "" {
            cell.imgV4.isHidden = false
        }
        if user.linked_id != "" {
            cell.imgV5.isHidden = false
        }
        
        cell.lblName.text = LookingData.Userdata.user_fullname
        let gender = LookingData.Userdata.user_gender
        cell.lblAgeGender.text = "\(LookingData.Userdata.user_age) Years \(Array_File.arrGender[gender]) "
        cell.lblViews.text = "\(100) Views"
        cell.lblDate.text = "\(self.ChangeDateFormat(Date: LookingData.Introduceyourself.avalibality_date) )    \(Array_File.arrLength_of_stay[LookingData.Introduceyourself.prefered_length_stay])"
        return cell
    }
        
    func ChangeDateFormat(Date: String) -> String  {
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "yyyy-MM-dd"
        let newdate = dateFormatter.date(from: Date)
        dateFormatter.dateFormat = "MMM dd"
        return dateFormatter.string(from: newdate!)
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let story = UIStoryboard(name: "Detail", bundle: nil)
        let profile = story.instantiateViewController(withIdentifier: "FlatmateDetailViewController") as! FlatmateDetailViewController
        profile.dicFlatmateProfile = self.arrMatches[indexPath.row] as! NSDictionary
        self.navigationController?.pushViewController(profile, animated: true)
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 250
    }
}

////////////// textFileld delegate ///////////
extension RoommateMatchesViewController : UITextFieldDelegate, MyPickerViewProtocol{
    func textFieldDidBeginEditing(_ textField: UITextField) {
        if textField == self.txtMoveinDate {
            let datePickerView = UIDatePicker()
            datePickerView.datePickerMode = .date
            datePickerView.minimumDate = datePickerView.date
            self.txtMoveinDate.inputView = datePickerView
            datePickerView.addTarget(self, action: #selector(handleDatePicker(sender:)), for: .valueChanged)
        }else if textField == self.txtRLocation  {
            let lat = Double(self.latitude)
            let lon = Double(self.longitude)
            let center = CLLocationCoordinate2D(latitude: lat!, longitude: lon!)
            let northEast = CLLocationCoordinate2D(latitude: center.latitude + 0.001, longitude: center.longitude + 0.001)
            let southWest = CLLocationCoordinate2D(latitude: center.latitude - 0.001, longitude: center.longitude - 0.001)
            let viewport = GMSCoordinateBounds(coordinate: northEast, coordinate: southWest)
            
            let config = GMSPlacePickerConfig(viewport: viewport)
            let placePicker = GMSPlacePickerViewController(config: config)
            placePicker.delegate = self
            self.present(placePicker, animated: true, completion: nil)
        }else if textField == self.txtLastActive {
            let myPickerView =  MyPickerView.init(withDictionary: Array_File.arrLastActive, Tag:3)
            myPickerView.delegate = myPickerView
            myPickerView.MyPickerDelegate = self
            self.txtLastActive.inputView = myPickerView
        }else if textField == self.txtGender {
            let myPickerView =  MyPickerView.init(withDictionary: Array_File.arrGender, Tag:4)
            myPickerView.delegate = myPickerView
            myPickerView.MyPickerDelegate = self
            self.txtGender.inputView = myPickerView
        }else if textField == self.txtAge {
            let myPickerView =  MyPickerView.init(withDictionary: self.arrAge, Tag:5)
            myPickerView.delegate = myPickerView
            myPickerView.MyPickerDelegate = self
            self.txtAge.inputView = myPickerView
        }
        
    }
    func myPickerDidSelectRow(Index:Int?, Tag:Int) {
        if Tag == 3 {
            if Index == 0{
                self.last_active = "all"
            }else if Index == 1{
                self.last_active = "7"
            }else if Index == 2{
                self.last_active = "30"
            }
            self.txtLastActive.text =  Array_File.arrLastActive[Index!]
        }else if Tag == 4 {
            self.gender = String(Index! + 1)
            self.txtGender.text = Array_File.arrGender[Index!]
        }else if Tag == 5 {
            self.age = self.arrAge[Index!]
            self.txtAge.text = self.arrAge[Index!]
        }
    }
    
    @objc func handleDatePicker(sender: UIDatePicker) {
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "dd MMM yyyy"
        self.txtMoveinDate.text = dateFormatter.string(from: sender.date)
        self.availableDate = Common.ChangeDateFormat(Date: self.txtMoveinDate.text!, fromFormat: "dd MMM yyyy", toFormat: "yyyy-MM-dd")
        print(self.availableDate)
    }
}

////////////// Current location ///////////
extension RoommateMatchesViewController : CLLocationManagerDelegate {
    func determineMyCurrentLocation() {
        locationManager = CLLocationManager()
        locationManager.delegate = self
        locationManager.desiredAccuracy = kCLLocationAccuracyBest
        locationManager.requestAlwaysAuthorization()
        if CLLocationManager.locationServicesEnabled() {
            locationManager.startUpdatingLocation()
        }
        locationManager.startUpdatingLocation()
    }
    
    func locationManager(_ manager: CLLocationManager, didUpdateLocations locations: [CLLocation]) {
        let userLocation:CLLocation = locations[0] as CLLocation
        self.latitude = String(userLocation.coordinate.latitude)
        self.longitude = String(userLocation.coordinate.longitude)
        print("user latitude = \(userLocation.coordinate.latitude)")
        print("user longitude = \(userLocation.coordinate.longitude)")
        locationManager.stopUpdatingLocation()
    }
    
    func locationManager(_ manager: CLLocationManager, didFailWithError error: Error)
    {
        print("Error \(error.localizedDescription)")
    }
}

////////////// PlacePicker ///////////
extension RoommateMatchesViewController : GMSPlacePickerViewControllerDelegate{
    
    func placePicker(_ viewController: GMSPlacePickerViewController, didPick place: GMSPlace) {
        
        self.txtRLocation.text=place.formattedAddress!
        self.latitude = String(place.coordinate.latitude)
        self.longitude = String(place.coordinate.longitude)
        
        print(place.coordinate.latitude)
        print(place.coordinate.longitude)
        viewController.dismiss(animated: true, completion: nil)
    }
    
    func placePickerDidCancel(_ viewController: GMSPlacePickerViewController) {
        // Dismiss the place picker, as it cannot dismiss itself.
        viewController.dismiss(animated: true, completion: nil)
        print("No place selected")
    }
}
