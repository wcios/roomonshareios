

import UIKit

class matchFavouriteViewController: UIViewController {
    
    @IBOutlet weak var tblFavrourite : UITableView!
    @IBOutlet weak var btnRoom : UIButton!
    @IBOutlet weak var btnRoomMate : UIButton!

    var strCheckTable : String = "roomMate"
    var userdata = SharedPreference.getUserData()
    var arrFavourite : Array<Any> = []
    var isFavorite : Bool = false
    var OfferingId : Int = 0
    
    override func viewDidLoad() {
        super.viewDidLoad()
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    override func viewWillAppear(_ animated: Bool) {
        self.navigationController?.isNavigationBarHidden = true
        rdv_tabBarController?.setTabBarHidden(false, animated: true)
        self.btnRoom.setImage(UIImage(named: "room"), for: .normal)
        self.btnRoomMate.setImage(UIImage(named: "Croommat"), for: .normal)  
        tblFavrourite.register(UINib(nibName: "roomMateTblCell", bundle: nil), forCellReuseIdentifier: "roomMateTblCell")
        self.FavouriteRoommateService()
    }
    
    @IBAction func BackMethod(_ sender: Any) {
        Common.PopMethod(VC: self)
    }
    
    @IBAction func clickOnBtnRoom(_ sender : UIButton){
        strCheckTable="room"
        self.btnRoomMate.setTitleColor(UIColor.lightGray, for: .normal)
        self.btnRoom.setTitleColor(UIColor(red: 5/255, green: 198/255, blue: 207/255, alpha: 1.0), for: .normal)
        self.btnRoom.setImage(UIImage(named: "Croom"), for: .normal)
        self.btnRoomMate.setImage(UIImage(named: "roommat"), for: .normal)
        self.FavouriteRoomService()
        tblFavrourite.register(UINib(nibName: "roomTblCell", bundle: nil), forCellReuseIdentifier: "roomTblCell")
    }
    
    @IBAction func clickOnBtnRoomMate(_ sender : UIButton){
        strCheckTable="roomMate"
        
        self.btnRoom.setTitleColor(UIColor.lightGray, for: .normal)
        self.btnRoomMate.setTitleColor(UIColor(red: 5/255, green: 198/255, blue: 207/255, alpha: 1.0) , for: .normal)
        self.btnRoom.setImage(UIImage(named: "room"), for: .normal)
        self.btnRoomMate.setImage(UIImage(named: "Croommat"), for: .normal)
        self.FavouriteRoommateService()
        tblFavrourite.register(UINib(nibName: "roomMateTblCell", bundle: nil), forCellReuseIdentifier: "roomMateTblCell")        
    }
    
    //FavouriteRoom
    func FavouriteRoomService() {
        
        self.startActivityIndicator()
        
        CommunicationManager().getResponseFor(strUrl: Url_File.FavouriteRoom, parameters: nil, completion: { ( result , data) in
            if(result == "success") {
                let dataDict = data as! NSDictionary
                print(dataDict)
                let dic = dataDict["response"] as! NSDictionary
                self.arrFavourite = dic.value(forKey: "my_list") as! Array<Any>
                self.tblFavrourite.reloadData()
                self.stopActivityIndicator()
            }else if (result == "error") {
                let strMessage = (data as! NSDictionary)["message"] as! String
                Common.ShowAlert(Title: Common.Title, Message: strMessage, VC: self)
                self.stopActivityIndicator()
            }else if (result == "Network") {
                let strMessage = (data as! String)
                Common.ShowAlert(Title: Common.Title, Message: strMessage, VC: self)
                self.stopActivityIndicator()
            }
        })
    }
    
    //FavouriteRoommate
    func FavouriteRoommateService() {
        
        self.startActivityIndicator()
        
        CommunicationManager().getResponseFor(strUrl: Url_File.FavouriteRoommate, parameters: nil, completion: { ( result , data) in
            
            if(result == "success") {
                let dataDict = data as! NSDictionary
                print(dataDict)
                let dic = dataDict["response"] as! NSDictionary
                self.arrFavourite = dic.value(forKey: "my_list") as! Array<Any>
                self.tblFavrourite.reloadData()
                self.stopActivityIndicator()
            }else if (result == "error") {
                let strMessage = (data as! NSDictionary)["message"] as! String
                Common.ShowAlert(Title: Common.Title, Message: strMessage, VC: self)
                self.stopActivityIndicator()
            }else if (result == "Network") {
                let strMessage = (data as! String)
                Common.ShowAlert(Title: Common.Title, Message: strMessage, VC: self)
                self.stopActivityIndicator()
            }
        })
    }
    
    ////////////// Favorite Service ///////////
    func FavoriteService(){
        self.startActivityIndicator()
        var param : [String:Any] = [:]
        param = [ "is_favourite"  : self.isFavorite,
                  "offering_home_id" : self.OfferingId
            ] as [String : Any]
        
        print(param)
        
        CommunicationManager().getResponseForPost(strUrl: Url_File.LikeRoom , parameters: param as NSDictionary) { ( result , data) in
            if(result == "success") {
                let dataDict = data as! NSDictionary
                print(dataDict)
                let dic = dataDict["response"] as! NSDictionary
                print(dic)
                self.FavouriteRoomService()
                self.stopActivityIndicator()
            }else if (result == "error") {
                let strMessage = (data as! NSDictionary)["message"] as! String
                Common.ShowAlert(Title: Common.Title, Message: strMessage, VC: self)
                self.stopActivityIndicator()
            }else if (result == "Network") {
                let strMessage = (data as! String)
                Common.ShowAlert(Title: Common.Title, Message: strMessage, VC: self)
                self.stopActivityIndicator()
            }
        }
    }

}

extension matchFavouriteViewController: UITableViewDelegate, UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if(strCheckTable=="room"){
            return self.arrFavourite.count
        }
        else if(strCheckTable=="roomMate"){
            return self.arrFavourite.count
        }
        return 1
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = UITableViewCell()
        let dic = self.arrFavourite[indexPath.row] as! NSDictionary
        
        if(strCheckTable=="room"){
            let cell = self.tblFavrourite.dequeueReusableCell(withIdentifier: "roomTblCell") as! roomTblCell
            let OfferingData = ProfileFlatmateDetailData.init(withDictionary: dic as! [String : Any])
            
            cell.lblName.text = OfferingData.homedescription.home_address
            cell.lblAddress.text = OfferingData.homedescription.home_location
            
            if dic.value(forKey: "profile_completed") as! Int == 100 {
                cell.lblStatus.backgroundColor = self.UIColorFromHex(rgbValue: 0x032AB43, alpha: 1.0)
                cell.lblStatus.text = "UPDATED"
            }else{
                cell.lblStatus.backgroundColor = self.UIColorFromHex(rgbValue: 0xE77E23, alpha: 1.0)
                cell.lblStatus.text = "INCOMPLETE"
            }
            
            let bedroom = OfferingData.bedroomdescription
            if bedroom.count > 0{
                let bed = bedroom[0]
                cell.lblDate.text = self.ChangeDateFormat(Date: bed.avalibality_date)
                cell.lblPrice.text = "$ \(bed.single_person_rent)"
            }
            
            let user = OfferingData.Userdata
            cell.imgProfile.sd_setImage(with: URL(string: user.user_profilepic), placeholderImage: UIImage(named: "UserPlaceholder"))
            
            cell.imgV1.isHidden = true
            cell.imgV2.isHidden = true
            cell.imgV3.isHidden = true
            cell.imgV4.isHidden = true
            cell.imgV5.isHidden = true
            if user.admin_verified {
                cell.imgV1.isHidden = false
            }
            if user.mobile_verified && user.email_verified {
                cell.imgV2.isHidden = false
            }
            if user.facebook_id != "" {
                cell.imgV3.isHidden = false
            }
            if user.google_id != "" {
                cell.imgV4.isHidden = false
            }
            if user.linked_id != "" {
                cell.imgV5.isHidden = false
            }
            
            let arrimg = OfferingData.arrImages
            if arrimg.count != 0{
                let dicimg = arrimg[0]
                cell.imgRoom.sd_setImage(with: URL(string: dicimg.url), placeholderImage: UIImage(named: "Banner"))
            }else{
                cell.imgProfile.image = UIImage(named: "Banner")
            }
            
            if OfferingData.homedescription.home_type == 2{
                cell.imgHometype.image = #imageLiteral(resourceName: "apartment")
            }else{
                cell.imgHometype.image = #imageLiteral(resourceName: "Dhouse")
            }
            
            if OfferingData.homedescription.parking_facitity == 1{
                cell.imgParkingtype.image = #imageLiteral(resourceName: "covered-parking")
            }else if OfferingData.homedescription.parking_facitity == 2{
                cell.imgParkingtype.image = #imageLiteral(resourceName: "Parking")
            }else{
                cell.imgParkingtype.image = #imageLiteral(resourceName: "no-parking")
            }
            
            if dic.value(forKey: "is_favourite") as! Bool{
                cell.btnLike.setImage(UIImage(named: "Clike"), for: .normal)
            }else{
                cell.btnLike.setImage(UIImage(named: "like"), for: .normal)
            }
            
            cell.btnLike.tag = indexPath.row
            cell.btnLike.addTarget(self, action: #selector(FavoriteMethod(btn:)), for: .touchUpInside)
            return cell
        }
        else if(strCheckTable=="roomMate"){
            let cell = self.tblFavrourite.dequeueReusableCell(withIdentifier: "roomMateTblCell") as! roomMateTblCell
            let LookingData = LookingProfileDetailData.init(withDictionary: dic as! [String : Any])
            let arrimg = LookingData.arrImages
            if arrimg.count != 0{
                let dicimg = arrimg[0]
                cell.imgProfile.sd_setImage(with: URL(string: dicimg.url), placeholderImage: UIImage(named: "Banner"))
            }else{
                cell.imgProfile.image = UIImage(named: "Banner")
            }
            
            cell.imgV1.isHidden = true
            cell.imgV2.isHidden = true
            cell.imgV3.isHidden = true
            cell.imgV4.isHidden = true
            cell.imgV5.isHidden = true
            
            let user = LookingData.Userdata
            
            if user.admin_verified {
                cell.imgV1.isHidden = false
            }
            if user.mobile_verified && user.email_verified {
                cell.imgV2.isHidden = false
            }
            if user.facebook_id != "" {
                cell.imgV3.isHidden = false
            }
            if user.google_id != "" {
                cell.imgV4.isHidden = false
            }
            if user.linked_id != "" {
                cell.imgV5.isHidden = false
            }
            
            cell.lblName.text = LookingData.Userdata.user_fullname
            let gender = LookingData.Userdata.user_gender
            cell.lblAgeGender.text = "\(LookingData.Userdata.user_age) Years \(Array_File.arrGender[gender]) "
            cell.lblViews.text = "\(100) Views"
            cell.lblDate.text = "\(self.ChangeDateFormat(Date: LookingData.Introduceyourself.avalibality_date) )    \(Array_File.arrLength_of_stay[LookingData.Introduceyourself.prefered_length_stay])"
            return cell
        }
        return cell
    }
    
    @objc func FavoriteMethod(btn : UIButton) {
        let Index = btn.tag
        let dic = self.arrFavourite[Index] as! NSDictionary
        let offer = dic.value(forKey: "offering_home") as! NSDictionary
        self.OfferingId = offer.value(forKey: "offering_home_id") as! Int
        if dic.value(forKey: "is_favourite") as! Bool{
            self.isFavorite = false
        }else{
            self.isFavorite = true
        }
        self.FavoriteService()
    }
    
    func ChangeDateFormat(Date: String) -> String  {
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "yyyy-MM-dd"
        let newdate = dateFormatter.date(from: Date)
        dateFormatter.dateFormat = "MMM dd"
        return dateFormatter.string(from: newdate!)
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let dic = self.arrFavourite[indexPath.row] as! NSDictionary
        if strCheckTable == "roomMate"{
            let story = UIStoryboard(name: "Detail", bundle: nil)
            let profile = story.instantiateViewController(withIdentifier: "FlatmateDetailViewController") as! FlatmateDetailViewController
            profile.dicFlatmateProfile = self.arrFavourite[indexPath.row] as! NSDictionary
            self.navigationController?.pushViewController(profile, animated: true)
        }else{
            let story = UIStoryboard(name: "Detail", bundle: nil)
            let profile = story.instantiateViewController(withIdentifier: "PropertyDetailViewController") as! PropertyDetailViewController
            profile.dicPropertyDetail = self.arrFavourite[indexPath.row] as! NSDictionary
            self.navigationController?.pushViewController(profile, animated: true)
        }
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 250
    }
}
