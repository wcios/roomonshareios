//
//  RoomMatchesViewController.swift
//  RoomOnShare
//
//  Created by mac on 29/09/18.
//  Copyright © 2018 WestCoast. All rights reserved.
//

import UIKit
import GooglePlacePicker

class RoomMatchesViewController: UIViewController {

    @IBOutlet weak var tblRoomMatches: UITableView!
    @IBOutlet weak var lblStatus: UILabel!
    @IBOutlet weak var btnFilter: UIButton!
    
    @IBOutlet weak var viewRoom: UIView!
    @IBOutlet weak var txtBestmatch: UITextField!
    @IBOutlet weak var sliderRent: UISlider!
    @IBOutlet weak var lblRent: UILabel!
    @IBOutlet weak var txtLocation: UITextField!
    @IBOutlet weak var txtDateavailable: UITextField!
    @IBOutlet weak var txtAgegroup: UITextField!
    @IBOutlet weak var collHomefeature: UICollectionView!
    @IBOutlet var btnSmoking: [UIButton]!
    @IBOutlet var btnPets: [UIButton]!
    
    var arrMatches : Array<Any> = []
    
    var locationManager:CLLocationManager!
    var userdata = SharedPreference.getUserData()
    var _selectedCells : NSMutableArray = []
    var arrSmoking : NSMutableArray = []
    var arrPet : NSMutableArray = []
    
    var isFavorite : Bool = false
    var OfferingId : Int = 0
    
    var id_Pet  = ""
    var id_Smoking = ""
    var availableDate = ""
    var sort_by = "by_best_matches"
    var Rent = ""
    var agegroup = ""
    var latitude  = ""
    var longitude = ""
    var strHomeFeatures : String = ""
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

    override func viewWillAppear(_ animated: Bool) {
        self.navigationController?.isNavigationBarHidden = true
        rdv_tabBarController?.setTabBarHidden(true, animated: true)
        
        self.viewRoom.isHidden = true
        self.lblStatus.isHidden = true
        self.collHomefeature.allowsMultipleSelection=true
        self.determineMyCurrentLocation()
        
        self.txtAgegroup.delegate = self
        self.txtLocation.delegate = self
        self.txtBestmatch.delegate = self
        self.txtDateavailable.delegate = self
        
        self.MatchesRoomService()
        self.tblRoomMatches.register(UINib(nibName: "roomTblCell", bundle: nil), forCellReuseIdentifier: "roomTblCell")
    }
    
    override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {
        self.viewRoom.isHidden = true
    }
    
    @IBAction func BackMethod(_ sender: Any) {
        Common.PopMethod(VC: self)
    }
    
    @IBAction func FilterMethod(_ sender: Any) {
        self.viewRoom.isHidden = false
    }
    
    ///////////////// room filter //////
    @IBAction func SliderMethod(_ sender: UISlider) {
        let rent = Int(sender.value)
        self.Rent = String(rent)
        self.lblRent.text = "$\(self.Rent)/month"
        print(rent)
    }
    
    @IBAction func SmokingMethod(_ sender: UIButton) {
        let Selectedtag = sender.tag
        if sender.isSelected {
            sender.isSelected = false
            arrSmoking.remove(Selectedtag)
        }else{
            sender.isSelected = true
            arrSmoking.add(Selectedtag)
        }
        self.id_Smoking = arrSmoking.componentsJoined(by: ",")
        print(self.id_Smoking)
    }
    
    @IBAction func PetsMethod(_ sender: UIButton) {
        let Selectedtag = sender.tag
        if sender.isSelected {
            sender.isSelected = false
            arrPet.remove(Selectedtag)
        }else{
            sender.isSelected = true
            arrPet.add(Selectedtag)
        }
        self.id_Pet = arrPet.componentsJoined(by: ",")
        print(self.id_Pet)
    }
    
    @IBAction func ApplyMethod(_ sender: Any) {
        self.viewRoom.isHidden = true
        self.MatchesRoomService()
    }
    
    @IBAction func ResetMethod(_ sender: Any) {
        self.sliderRent.setValue(0, animated: true)
        self.txtBestmatch.text = Array_File.arrfilter[0]
        self.txtDateavailable.text = "Date Availability"
        self.txtAgegroup.text = "Select age group"
        self.txtLocation.text = "Select Location"
        self._selectedCells  = []
        self.collHomefeature.reloadData()
        
        self.Rent = ""
        self.agegroup = ""
        self.latitude = ""
        self.longitude = ""
        self.id_Pet = ""
        self.id_Smoking = ""
        self.availableDate = ""
        
        for item in self.btnPets{
            item.isSelected = false
        }
        for item in self.btnSmoking{
            item.isSelected = false
        }
    }
    //////////////////////////////////////
    
    
    ////////////// RoomMatches Service ///////////
    func MatchesRoomService(){
        self.startActivityIndicator()
        var param : [String:Any] = [:]
        param = [ "sort_by"  : self.sort_by,
                  "budget" : self.Rent,
                  "location_latitude"  : self.latitude,
                  "location_longitude" : self.longitude,
                  "available_date" : self.availableDate,
                  "age_group" : self.agegroup ,
                  "home_features" : self.strHomeFeatures,
                  "smoking" : id_Smoking,
                  "pets" : id_Pet
            ] as [String : Any]
        
        print(param)
        
        CommunicationManager().getResponseForPost(strUrl: Url_File.MatchesRoom , parameters: param as NSDictionary) { ( result , data) in
            if(result == "success") {
                self.stopActivityIndicator()
                let dataDict = data as! NSDictionary
                print(dataDict)
                let dic = dataDict["response"] as! NSDictionary
                self.arrMatches = dic.value(forKey: "my_list") as! Array<Any>
                print(dic)
                if(self.arrMatches.count == 0){
                    self.lblStatus.isHidden = false
                    self.tblRoomMatches.isHidden = true
                    self.lblStatus.text = "No Room matches available."
                }else{
                    self.lblStatus.isHidden = true
                    self.tblRoomMatches.isHidden = false
                    self.tblRoomMatches.reloadData()
                }
                
            }else if (result == "error") {
                let strMessage = (data as! NSDictionary)["message"] as! String
                Common.ShowAlert(Title: Common.Title, Message: strMessage, VC: self)
                self.stopActivityIndicator()
            }else if (result == "Network") {
                let strMessage = (data as! String)
                Common.ShowAlert(Title: Common.Title, Message: strMessage, VC: self)
                self.stopActivityIndicator()
            }
        }
    }
    
    ////////////// Favorite Service ///////////
    func FavoriteService(){
        self.startActivityIndicator()
        var param : [String:Any] = [:]
        param = [ "is_favourite"  : self.isFavorite,
                  "offering_home_id" : self.OfferingId
            ] as [String : Any]
        
        print(param)
        
        CommunicationManager().getResponseForPost(strUrl: Url_File.LikeRoom , parameters: param as NSDictionary) { ( result , data) in
            if(result == "success") {
                let dataDict = data as! NSDictionary
                print(dataDict)
                let dic = dataDict["response"] as! NSDictionary
                print(dic)
                self.MatchesRoomService()
                self.stopActivityIndicator()
            }else if (result == "error") {
                let strMessage = (data as! NSDictionary)["message"] as! String
                Common.ShowAlert(Title: Common.Title, Message: strMessage, VC: self)
                self.stopActivityIndicator()
            }else if (result == "Network") {
                let strMessage = (data as! String)
                Common.ShowAlert(Title: Common.Title, Message: strMessage, VC: self)
                self.stopActivityIndicator()
            }
        }
    }
}

////////////// Tableview ///////////
extension RoomMatchesViewController: UITableViewDelegate, UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.arrMatches.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        
        let dic = self.arrMatches[indexPath.row] as! NSDictionary
        
        let cell = self.tblRoomMatches.dequeueReusableCell(withIdentifier: "roomTblCell") as! roomTblCell
        let OfferingData = ProfileFlatmateDetailData.init(withDictionary: dic as! [String : Any])
        
        cell.lblName.text = OfferingData.homedescription.home_address
        cell.lblAddress.text = OfferingData.homedescription.home_location
        
        if dic.value(forKey: "profile_completed") as! Int == 100 {
            cell.lblStatus.backgroundColor = self.UIColorFromHex(rgbValue: 0x032AB43, alpha: 1.0)
            cell.lblStatus.text = "UPDATED"
        }else{
            cell.lblStatus.backgroundColor = self.UIColorFromHex(rgbValue: 0xE77E23, alpha: 1.0)
            cell.lblStatus.text = "INCOMPLETE"
        }
        
        let bedroom = OfferingData.bedroomdescription
        if bedroom.count > 0{
            let bed = bedroom[0]
            cell.lblDate.text = self.ChangeDateFormat(Date: bed.avalibality_date)
            cell.lblPrice.text = "$ \(bed.single_person_rent)"
        }
        
        let user = OfferingData.Userdata
        cell.imgProfile.sd_setImage(with: URL(string: user.user_profilepic), placeholderImage: UIImage(named: "UserPlaceholder"))
        
        cell.imgV1.isHidden = true
        cell.imgV2.isHidden = true
        cell.imgV3.isHidden = true
        cell.imgV4.isHidden = true
        cell.imgV5.isHidden = true
        if user.admin_verified {
            cell.imgV1.isHidden = false
        }
        if user.mobile_verified && user.email_verified {
            cell.imgV2.isHidden = false
        }
        if user.facebook_id != "" {
            cell.imgV3.isHidden = false
        }
        if user.google_id != "" {
            cell.imgV4.isHidden = false
        }
        if user.linked_id != "" {
            cell.imgV5.isHidden = false
        }
        
        let arrimg = OfferingData.arrImages
        if arrimg.count != 0{
            let dicimg = arrimg[0]
            cell.imgRoom.sd_setImage(with: URL(string: dicimg.url), placeholderImage: UIImage(named: "Banner"))
        }else{
            cell.imgProfile.image = UIImage(named: "Banner")
        }
        
        if OfferingData.homedescription.home_type == 2{
            cell.imgHometype.image = #imageLiteral(resourceName: "apartment")
        }else{
            cell.imgHometype.image = #imageLiteral(resourceName: "Dhouse")
        }
        
        if OfferingData.homedescription.parking_facitity == 1{
            cell.imgParkingtype.image = #imageLiteral(resourceName: "covered-parking")
        }else if OfferingData.homedescription.parking_facitity == 2{
            cell.imgParkingtype.image = #imageLiteral(resourceName: "Parking")
        }else{
            cell.imgParkingtype.image = #imageLiteral(resourceName: "no-parking")
        }
        
        if dic.value(forKey: "is_favourite") as! Bool{
            cell.btnLike.setImage(UIImage(named: "Clike"), for: .normal)
        }else{
            cell.btnLike.setImage(UIImage(named: "like"), for: .normal)
        }
        
        cell.btnLike.tag = indexPath.row
        cell.btnLike.addTarget(self, action: #selector(FavoriteMethod(btn:)), for: .touchUpInside)
        return cell
    }
    
    @objc func FavoriteMethod(btn : UIButton) {
        let Index = btn.tag
        let dic = self.arrMatches[Index] as! NSDictionary
        let offer = dic.value(forKey: "offering_home") as! NSDictionary
        self.OfferingId = offer.value(forKey: "offering_home_id") as! Int
        if dic.value(forKey: "is_favourite") as! Bool{
            self.isFavorite = false
        }else{
            self.isFavorite = true
        }
        self.FavoriteService()
    }
    
    func ChangeDateFormat(Date: String) -> String  {
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "yyyy-MM-dd"
        let newdate = dateFormatter.date(from: Date)
        dateFormatter.dateFormat = "MMM dd"
        return dateFormatter.string(from: newdate!)
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {        
        let story = UIStoryboard(name: "Detail", bundle: nil)
        let profile = story.instantiateViewController(withIdentifier: "PropertyDetailViewController") as! PropertyDetailViewController
        profile.dicPropertyDetail = self.arrMatches[indexPath.row] as! NSDictionary
        self.navigationController?.pushViewController(profile, animated: true)
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 250
    }
}

////////////// textFileld delegate ///////////
extension RoomMatchesViewController : UITextFieldDelegate, MyPickerViewProtocol{
    func textFieldDidBeginEditing(_ textField: UITextField) {
        if textField == self.txtAgegroup{
            let myPickerView =  MyPickerView.init(withDictionary: Array_File.arrAgeGroup, Tag:1)
            myPickerView.delegate = myPickerView
            myPickerView.MyPickerDelegate = self
            self.txtAgegroup.inputView = myPickerView
        }else if textField == self.txtBestmatch{
            let myPickerView =  MyPickerView.init(withDictionary: Array_File.arrfilter, Tag:2)
            myPickerView.delegate = myPickerView
            myPickerView.MyPickerDelegate = self
            self.txtBestmatch.inputView = myPickerView
        }else if textField == self.txtDateavailable{
            let datePickerView = UIDatePicker()
            datePickerView.datePickerMode = .date
            datePickerView.minimumDate = datePickerView.date
            self.txtDateavailable.inputView = datePickerView
            datePickerView.addTarget(self, action: #selector(handleDatePicker(sender:)), for: .valueChanged)
        }else if textField == self.txtLocation{
            let lat = Double(self.latitude)
            let lon = Double(self.longitude)
            let center = CLLocationCoordinate2D(latitude: lat!, longitude: lon!)
            let northEast = CLLocationCoordinate2D(latitude: center.latitude + 0.001, longitude: center.longitude + 0.001)
            let southWest = CLLocationCoordinate2D(latitude: center.latitude - 0.001, longitude: center.longitude - 0.001)
            let viewport = GMSCoordinateBounds(coordinate: northEast, coordinate: southWest)
            
            let config = GMSPlacePickerConfig(viewport: viewport)
            let placePicker = GMSPlacePickerViewController(config: config)
            placePicker.delegate = self
            self.present(placePicker, animated: true, completion: nil)
        }
    }
    func myPickerDidSelectRow(Index:Int?, Tag:Int) {
        if Tag == 1{
            self.agegroup = String(Index! + 1)
            self.txtAgegroup.text = Array_File.arrAgeGroup[Index!]
        } else if Tag == 2{
            if Index == 0{
                self.sort_by = "by_best_matches"
            }else if Index == 1{
                self.sort_by = "by_price"
            }else if Index == 2{
                self.sort_by = "by_recent"
            }
            self.txtBestmatch.text = Array_File.arrfilter[Index!]
        }
    }
    
    @objc func handleDatePicker(sender: UIDatePicker) {
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "dd MMM yyyy"
        self.txtDateavailable.text = dateFormatter.string(from: sender.date)        
        self.availableDate = Common.ChangeDateFormat(Date: self.txtDateavailable.text!, fromFormat: "dd MMM yyyy", toFormat: "yyyy-MM-dd")
        print(self.availableDate)
    }
}

////////////// Current location ///////////
extension RoomMatchesViewController : CLLocationManagerDelegate {
    func determineMyCurrentLocation() {
        locationManager = CLLocationManager()
        locationManager.delegate = self
        locationManager.desiredAccuracy = kCLLocationAccuracyBest
        locationManager.requestAlwaysAuthorization()
        if CLLocationManager.locationServicesEnabled() {
            locationManager.startUpdatingLocation()
        }
        locationManager.startUpdatingLocation()
    }
    
    func locationManager(_ manager: CLLocationManager, didUpdateLocations locations: [CLLocation]) {
        let userLocation:CLLocation = locations[0] as CLLocation
        self.latitude = String(userLocation.coordinate.latitude)
        self.longitude = String(userLocation.coordinate.longitude)
        print("user latitude = \(userLocation.coordinate.latitude)")
        print("user longitude = \(userLocation.coordinate.longitude)")
        locationManager.stopUpdatingLocation()
    }
    
    func locationManager(_ manager: CLLocationManager, didFailWithError error: Error)
    {
        print("Error \(error.localizedDescription)")
    }
}

////////////// Collectionview delegates ///////////
extension RoomMatchesViewController : UICollectionViewDelegate , UICollectionViewDataSource ,UICollectionViewDelegateFlowLayout{
    
    func numberOfSections(in collectionView: UICollectionView) -> Int {
        return 1
    }
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        
        return Array_File.arrHomeDescriptionOptionImgs.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        
        let cell = collHomefeature.dequeueReusableCell(withReuseIdentifier: "homeFeaturesCollCell", for: indexPath) as! homeFeaturesCollCell
        let dict = Array_File.arrHomeDescriptionOptionImgs[indexPath.row]
        cell.imgFeatures.image=UIImage(named: (dict["image"])!)?.imageWithInsets(insetDimen: 8)
        cell.lblIconName.text=(dict["name"]!)
        
        if _selectedCells.contains(indexPath.row + 1) {
            cell.isSelected=true
            collHomefeature.selectItem(at: indexPath, animated: true, scrollPosition: UICollectionViewScrollPosition.top)
            cell.imgFeatures.tintImageColor(color: UIColorFromHex(rgbValue: 0x05C6CF, alpha: 1.0))
        }
        else{
            cell.isSelected=false
            cell.imgFeatures.tintImageColor(color: .lightGray)
        }
        
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath){
        _selectedCells.add(indexPath.row + 1)
        print(_selectedCells)
        self.strHomeFeatures = _selectedCells.componentsJoined(by: ",")
        collHomefeature.reloadItems(at: [indexPath])
    }
    
    func collectionView(_ collectionView: UICollectionView, didDeselectItemAt indexPath: IndexPath) {
        _selectedCells.remove(indexPath.row + 1)
        print(_selectedCells)
        self.strHomeFeatures = _selectedCells.componentsJoined(by: ",")
        collHomefeature.reloadItems(at: [indexPath])
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        let yourWidth = collectionView.bounds.width/3.0
        return CGSize(width: yourWidth, height: 100.0)
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, insetForSectionAt section: Int) -> UIEdgeInsets {
        return UIEdgeInsets.zero
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumInteritemSpacingForSectionAt section: Int) -> CGFloat {
        return 0
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumLineSpacingForSectionAt section: Int) -> CGFloat {
        return 0
    }
}

////////////// PlacePicker ///////////
extension RoomMatchesViewController : GMSPlacePickerViewControllerDelegate{
    
    func placePicker(_ viewController: GMSPlacePickerViewController, didPick place: GMSPlace) {
        
        self.txtLocation.text=place.formattedAddress!
        self.latitude = String(place.coordinate.latitude)
        self.longitude = String(place.coordinate.longitude)
        
        print(place.coordinate.latitude)
        print(place.coordinate.longitude)
        viewController.dismiss(animated: true, completion: nil)
    }
    
    func placePickerDidCancel(_ viewController: GMSPlacePickerViewController) {
        // Dismiss the place picker, as it cannot dismiss itself.
        viewController.dismiss(animated: true, completion: nil)
        print("No place selected")
    }
}
