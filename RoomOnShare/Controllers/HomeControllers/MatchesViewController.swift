

import UIKit
import GooglePlacePicker

class MatchesViewController: UIViewController {
    
    @IBOutlet weak var tblMatches: UITableView!
    @IBOutlet weak var btnRoom : UIButton!
    @IBOutlet weak var btnRoomMate : UIButton!
    ///// room ///////
    @IBOutlet weak var viewRoom: UIView!
    @IBOutlet weak var txtBestmatch: UITextField!
    @IBOutlet weak var sliderRent: UISlider!
    @IBOutlet weak var lblRent: UILabel!
    @IBOutlet weak var txtLocation: UITextField!
    @IBOutlet weak var txtDateavailable: UITextField!
    @IBOutlet weak var txtAgegroup: UITextField!
    @IBOutlet weak var collHomefeature: UICollectionView!
    @IBOutlet var btnSmoking: [UIButton]!
    @IBOutlet var btnPets: [UIButton]!
    ///////////////////////////////
    
    ///// roommate /////
    @IBOutlet weak var viewRoommate: UIView!
    @IBOutlet weak var txtLastActive: UITextField!
    @IBOutlet weak var txtRLocation: UITextField!
    @IBOutlet weak var txtGender: UITextField!
    @IBOutlet weak var txtAge: UITextField!
    @IBOutlet weak var btnGoogle: UIButton!
    @IBOutlet weak var btnFacebook: UIButton!
    @IBOutlet weak var btnLinkedin: UIButton!
    @IBOutlet var btnRSmoking: [UIButton]!
    @IBOutlet var btnRPets: [UIButton]!
    @IBOutlet weak var txtMoveinDate: UITextField!
    @IBOutlet weak var sliderRrent: UISlider!
    @IBOutlet weak var lblRrent: UILabel!
    //////////////////////
    var locationManager:CLLocationManager!
    var strCheckTable : String = "roomMate"
    var userdata = SharedPreference.getUserData()
    var arrMatches : Array<Any> = []
    var _selectedCells : NSMutableArray = []
    var strHomeFeatures : String = ""
    var arrSmoking : NSMutableArray = []
    var arrPet : NSMutableArray = []
    var isFavorite : Bool = false
    var OfferingId : Int = 0
    
    var id_Pet  = ""
    var id_Smoking = ""
    var availableDate = ""
    var sort_by = "by_best_matches"
    var Rent = ""
    var agegroup = ""
    var latitude  = ""
    var longitude = ""
    
    var arrAge : [String] = []
    var age = ""
    var gender = ""
    var G_id = ""
    var FB_id = ""
    var LN_id = ""
    var last_active = "all"
    
    override func viewDidLoad() {
        super.viewDidLoad()
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    override func viewWillAppear(_ animated: Bool) {
        self.viewRoom.isHidden = true
        self.viewRoommate.isHidden = true
        
        self.navigationController?.isNavigationBarHidden = true
        rdv_tabBarController?.setTabBarHidden(false, animated: true)
        self.collHomefeature.allowsMultipleSelection=true
        self.determineMyCurrentLocation()
        
        self.txtAgegroup.delegate = self
        self.txtLocation.delegate = self
        self.txtBestmatch.delegate = self
        self.txtDateavailable.delegate = self
        
        self.txtLastActive.delegate = self
        self.txtAge.delegate = self
        self.txtGender.delegate = self
        self.txtRLocation.delegate = self
        self.txtMoveinDate.delegate = self
        
        for i in 18..<101{
            self.arrAge.append(String(i))
        }
        
        self.MatchesRoommateService()
        self.btnRoom.setImage(UIImage(named: "room"), for: .normal)
        self.btnRoomMate.setImage(UIImage(named: "Croommat"), for: .normal)
        tblMatches.register(UINib(nibName: "roomMateTblCell", bundle: nil), forCellReuseIdentifier: "roomMateTblCell")
    }
    
    override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {
        self.viewRoom.isHidden = true
        self.viewRoommate.isHidden = true
    }
    
    ////////////// Room ///////////
    @IBAction func clickOnBtnRoom(_ sender : UIButton){
        strCheckTable="room"
        self.btnRoomMate.setTitleColor(UIColor.lightGray, for: .normal)
        self.btnRoom.setTitleColor(UIColor(red: 5/255, green: 198/255, blue: 207/255, alpha: 1.0), for: .normal)
        self.btnRoom.setImage(UIImage(named: "Croom"), for: .normal)
        self.btnRoomMate.setImage(UIImage(named: "roommat"), for: .normal)
        self.MatchesRoomService()
        tblMatches.register(UINib(nibName: "roomTblCell", bundle: nil), forCellReuseIdentifier: "roomTblCell")
     
    }
    
    ////////////// Roommate ///////////
    @IBAction func clickOnBtnRoomMate(_ sender : UIButton){
        strCheckTable="roomMate"
        
        self.btnRoom.setTitleColor(UIColor.lightGray, for: .normal)
        self.btnRoomMate.setTitleColor(UIColor(red: 5/255, green: 198/255, blue: 207/255, alpha: 1.0) , for: .normal)
        self.btnRoom.setImage(UIImage(named: "room"), for: .normal)
        self.btnRoomMate.setImage(UIImage(named: "Croommat"), for: .normal)
        self.MatchesRoommateService()
        tblMatches.register(UINib(nibName: "roomMateTblCell", bundle: nil), forCellReuseIdentifier: "roomMateTblCell")
    }
    
    ////////////// Back ///////////
    @IBAction func BackMethod(_ sender: Any) {
        self.navigationController?.popViewController(animated: true)
    }
    
    ////////////// Filter ///////////
    @IBAction func FilterMethod(_ sender: Any) {
        if self.strCheckTable == "roomMate"{
            self.viewRoom.isHidden = true
            self.viewRoommate.isHidden = false
        }else{
            self.viewRoom.isHidden = false
            self.viewRoommate.isHidden = true
        }
    }
    
    ///////////////// room filter //////
    @IBAction func SliderMethod(_ sender: UISlider) {
        let rent = Int(sender.value)
        self.Rent = String(rent)
        self.lblRent.text = "$\(self.Rent)/month"
        print(rent)
    }
    
    @IBAction func SmokingMethod(_ sender: UIButton) {
        let Selectedtag = sender.tag
        if sender.isSelected {
            sender.isSelected = false
            arrSmoking.remove(Selectedtag)
        }else{
            sender.isSelected = true
            arrSmoking.add(Selectedtag)
        }
        self.id_Smoking = arrSmoking.componentsJoined(by: ",")
        print(self.id_Smoking)
    }
    
    @IBAction func PetsMethod(_ sender: UIButton) {
        let Selectedtag = sender.tag
        if sender.isSelected {
            sender.isSelected = false
            arrPet.remove(Selectedtag)
        }else{
            sender.isSelected = true
            arrPet.add(Selectedtag)
        }
        self.id_Pet = arrPet.componentsJoined(by: ",")
        print(self.id_Pet)
    }
    
    @IBAction func ApplyMethod(_ sender: Any) {
        self.viewRoom.isHidden = true
        self.MatchesRoomService()
    }
    
    @IBAction func ResetMethod(_ sender: Any) {
        self.sliderRent.setValue(0, animated: true)
        self.txtBestmatch.text = Array_File.arrfilter[0]
        self.txtDateavailable.text = "Date Availability"
        self.txtAgegroup.text = "Select age group"
        self.txtLocation.text = "Select Location"
        self._selectedCells  = []
        self.collHomefeature.reloadData()
        
        self.Rent = ""
        self.agegroup = ""
        self.latitude = ""
        self.longitude = ""
        self.id_Pet = ""
        self.id_Smoking = ""
        self.availableDate = ""
        
        for item in self.btnPets{
            item.isSelected = false
        }
        for item in self.btnSmoking{
            item.isSelected = false
        }
    }
    //////////////////////////////////////
    
    //////////////// romemate filter //////
    
    @IBAction func GoogleMethod(_ sender: Any) {
        if self.btnGoogle.isSelected{
           self.btnGoogle.isSelected = false
        }else{
           self.btnGoogle.isSelected = true
            self.G_id = "true"
        }
    }
    
    @IBAction func FacebookMethod(_ sender: Any) {
        if self.btnFacebook.isSelected{
            self.btnFacebook.isSelected = false
        }else{
            self.btnFacebook.isSelected = true
            self.FB_id = "true"
        }
    }
    
    @IBAction func LinkedInMethod(_ sender: Any) {
        if self.btnLinkedin.isSelected{
            self.btnLinkedin.isSelected = false
        }else{
            self.btnLinkedin.isSelected = true
            self.LN_id = "true"
        }
    }
    
    @IBAction func SliderRrent(_ sender: UISlider) {
        let rent = Int(sender.value)
        self.Rent = String(rent)
        self.lblRrent.text = "$\(self.Rent)/month"
        print(rent)
    }
    
    @IBAction func RSmokingMethod(_ sender: UIButton) {
        let Selectedtag = sender.tag
        if sender.isSelected {
            sender.isSelected = false
            arrSmoking.remove(Selectedtag)
        }else{
            sender.isSelected = true
            arrSmoking.add(Selectedtag)
        }
        self.id_Smoking = arrSmoking.componentsJoined(by: ",")
        print(self.id_Smoking)
    }
    
    @IBAction func RPetsMethod(_ sender: UIButton) {
        let Selectedtag = sender.tag
        if sender.isSelected {
            sender.isSelected = false
            arrPet.remove(Selectedtag)
        }else{
            sender.isSelected = true
            arrPet.add(Selectedtag)
        }
        self.id_Pet = arrPet.componentsJoined(by: ",")
        print(self.id_Pet)
    }
    
    @IBAction func RApplyMethod(_ sender: Any) {
        self.viewRoommate.isHidden = true
        self.MatchesRoommateService()
    }
    
    @IBAction func RResetMethod(_ sender: Any) {
        self.sliderRrent.setValue(0, animated: true)
        self.txtLastActive.text = Array_File.arrLastActive[0]
        self.txtMoveinDate.text = "Move in date"
        self.txtAge.text = "Select age"
        self.txtRLocation.text = "Select Location"
        self.txtGender.text = "Select gender"
        
        self.Rent = ""
        self.last_active = "All"
        self.gender = ""
        self.age = ""
        self.latitude = ""
        self.longitude = ""
        self.id_Pet = ""
        self.id_Smoking = ""
        self.availableDate = ""
        self.FB_id = ""
        self.G_id = ""
        self.LN_id = ""
        
        
        for item in self.btnRPets{
            item.isSelected = false
        }
        for item in self.btnRSmoking{
            item.isSelected = false
        }
    }
    
    //////////////////////////
    ////////
    
    
    ////////////// RoomMatches Service ///////////
    func MatchesRoomService(){
        self.startActivityIndicator()
        var param : [String:Any] = [:]
        param = [ "sort_by"  : self.sort_by,
                  "budget" : self.Rent,
                  "location_latitude"  : self.latitude,
                  "location_longitude" : self.longitude,
                  "available_date" : self.availableDate,
                  "age_group" : self.agegroup ,
                  "home_features" : self.strHomeFeatures,
                  "smoking" : id_Smoking,
                  "pets" : id_Pet
            ] as [String : Any]
        
        print(param)
        
        CommunicationManager().getResponseForPost(strUrl: Url_File.MatchesRoom , parameters: param as NSDictionary) { ( result , data) in
            if(result == "success") {
                let dataDict = data as! NSDictionary
                print(dataDict)
                let dic = dataDict["response"] as! NSDictionary
                self.arrMatches = dic.value(forKey: "my_list") as! Array<Any>
                print(dic)
                self.tblMatches.reloadData()
                self.stopActivityIndicator()
            }else if (result == "error") {
                let strMessage = (data as! NSDictionary)["message"] as! String
                Common.ShowAlert(Title: Common.Title, Message: strMessage, VC: self)
                self.stopActivityIndicator()
            }else if (result == "Network") {
                let strMessage = (data as! String)
                Common.ShowAlert(Title: Common.Title, Message: strMessage, VC: self)
                self.stopActivityIndicator()
            }
        }
    }
    
    ////////////// MatchesRoommate Service ///////////
    func MatchesRoommateService(){
        self.startActivityIndicator()
        var param : [String:Any] = [:]
        param = [ "offering_home_id"  : "",
                  "last_active"  : self.last_active,
                  "budget" : self.Rent,
                  "location_latitude"  : self.latitude,
                  "location_longitude" : self.longitude,
                  "gender" : self.gender,
                  "available_date" : self.availableDate,
                  "age" : self.age ,
                  "smoking" : id_Smoking,
                  "pets" : id_Pet,
                  "facebook_verification" : self.FB_id,
                  "google_verification" : self.G_id,
                  "linkedin_verification" : self.LN_id
            ] as [String : Any]

        print(param)
        
        CommunicationManager().getResponseForPost(strUrl: Url_File.MatchesRoommate , parameters: param as NSDictionary) { ( result , data) in
            if(result == "success") {
                let dataDict = data as! NSDictionary
                print(dataDict)
                let dic = dataDict["response"] as! NSDictionary
                self.arrMatches = dic.value(forKey: "my_list") as! Array<Any>
                print(dic)
                self.tblMatches.reloadData()
                self.stopActivityIndicator()
            }else if (result == "error") {
                let strMessage = (data as! NSDictionary)["message"] as! String
                Common.ShowAlert(Title: Common.Title, Message: strMessage, VC: self)
                self.stopActivityIndicator()
            }else if (result == "Network") {
                let strMessage = (data as! String)
                Common.ShowAlert(Title: Common.Title, Message: strMessage, VC: self)
                self.stopActivityIndicator()
            }
        }
    }
    
    ////////////// Favorite Service ///////////
    func FavoriteService(){
        self.startActivityIndicator()
        var param : [String:Any] = [:]
        param = [ "is_favourite"  : self.isFavorite,
                  "offering_home_id" : self.OfferingId
            ] as [String : Any]
        
        print(param)
       
        CommunicationManager().getResponseForPost(strUrl: Url_File.LikeRoom , parameters: param as NSDictionary) { ( result , data) in
            if(result == "success") {
                let dataDict = data as! NSDictionary
                print(dataDict)
                let dic = dataDict["response"] as! NSDictionary
                print(dic)
                self.MatchesRoomService()
                self.stopActivityIndicator()
            }else if (result == "error") {
                let strMessage = (data as! NSDictionary)["message"] as! String
                Common.ShowAlert(Title: Common.Title, Message: strMessage, VC: self)
                self.stopActivityIndicator()
            }else if (result == "Network") {
                let strMessage = (data as! String)
                Common.ShowAlert(Title: Common.Title, Message: strMessage, VC: self)
                self.stopActivityIndicator()
            }
        }
    }
}

////////////// Tableview ///////////
extension MatchesViewController: UITableViewDelegate, UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if(strCheckTable=="room"){
            return self.arrMatches.count
        }
        else if(strCheckTable=="roomMate"){
            return self.arrMatches.count
        }
        return 1
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = UITableViewCell()
        
        let dic = self.arrMatches[indexPath.row] as! NSDictionary
        
        if(strCheckTable=="room"){
            let cell = self.tblMatches.dequeueReusableCell(withIdentifier: "roomTblCell") as! roomTblCell
            let OfferingData = ProfileFlatmateDetailData.init(withDictionary: dic as! [String : Any])
            
            cell.lblName.text = OfferingData.homedescription.home_address
            cell.lblAddress.text = OfferingData.homedescription.home_location
            
            if dic.value(forKey: "profile_completed") as? Int == 100 {
                cell.lblStatus.backgroundColor = self.UIColorFromHex(rgbValue: 0x032AB43, alpha: 1.0)
                cell.lblStatus.text = "UPDATED"
            }else{
                cell.lblStatus.backgroundColor = self.UIColorFromHex(rgbValue: 0xE77E23, alpha: 1.0)
                cell.lblStatus.text = "INCOMPLETE"
            }
            
            let bedroom = OfferingData.bedroomdescription
            if bedroom.count > 0{
                let bed = bedroom[0]
                cell.lblDate.text = self.ChangeDateFormat(Date: bed.avalibality_date)
                cell.lblPrice.text = "$ \(bed.single_person_rent)"
            }
        
            let user = OfferingData.Userdata
            cell.imgProfile.sd_setImage(with: URL(string: user.user_profilepic), placeholderImage: UIImage(named: "UserPlaceholder"))
            
            cell.imgV1.isHidden = true
            cell.imgV2.isHidden = true
            cell.imgV3.isHidden = true
            cell.imgV4.isHidden = true
            cell.imgV5.isHidden = true
            if user.admin_verified {
                cell.imgV1.isHidden = false
            }
            if user.mobile_verified && user.email_verified {
                cell.imgV2.isHidden = false
            }
            if user.facebook_id != "" {
                cell.imgV3.isHidden = false
            }
            if user.google_id != "" {
                cell.imgV4.isHidden = false
            }
            if user.linked_id != "" {
                cell.imgV5.isHidden = false
            }
                        
            let arrimg = OfferingData.arrImages
            if arrimg.count != 0{
                let dicimg = arrimg[0]
                cell.imgRoom.sd_setImage(with: URL(string: dicimg.url), placeholderImage: UIImage(named: "Banner"))
            }else{
                cell.imgProfile.image = UIImage(named: "Banner")
            }
            
            if OfferingData.homedescription.home_type == 2{
                cell.imgHometype.image = #imageLiteral(resourceName: "apartment")
            }else{
                cell.imgHometype.image = #imageLiteral(resourceName: "Dhouse")
            }
            
            if OfferingData.homedescription.parking_facitity == 1{
                cell.imgParkingtype.image = #imageLiteral(resourceName: "covered-parking")
            }else if OfferingData.homedescription.parking_facitity == 2{
                cell.imgParkingtype.image = #imageLiteral(resourceName: "Parking")
            }else{
                cell.imgParkingtype.image = #imageLiteral(resourceName: "no-parking")
            }
            
            if dic.value(forKey: "is_favourite") as! Bool{
                cell.btnLike.setImage(UIImage(named: "Clike"), for: .normal)
            }else{
                cell.btnLike.setImage(UIImage(named: "like"), for: .normal)
            }
            
            cell.btnLike.tag = indexPath.row
            cell.btnLike.addTarget(self, action: #selector(FavoriteMethod(btn:)), for: .touchUpInside)
            return cell
        }
        else if(strCheckTable=="roomMate"){
            let cell = self.tblMatches.dequeueReusableCell(withIdentifier: "roomMateTblCell") as! roomMateTblCell
            let LookingData = LookingProfileDetailData.init(withDictionary: dic as! [String : Any])
            let arrimg = LookingData.arrImages
            if arrimg.count != 0{
                let dicimg = arrimg[0]
                cell.imgProfile.sd_setImage(with: URL(string: dicimg.url), placeholderImage: UIImage(named: "Banner"))
            }else{
                cell.imgProfile.image = UIImage(named: "Banner")
            }
            
            cell.imgV1.isHidden = true
            cell.imgV2.isHidden = true
            cell.imgV3.isHidden = true
            cell.imgV4.isHidden = true
            cell.imgV5.isHidden = true
            
            let user = LookingData.Userdata
            
            if user.admin_verified {
                cell.imgV1.isHidden = false
            }
            if user.mobile_verified && user.email_verified {
                cell.imgV2.isHidden = false
            }
            if user.facebook_id != "" {
                cell.imgV3.isHidden = false
            }
            if user.google_id != "" {
                cell.imgV4.isHidden = false
            }
            if user.linked_id != "" {
                cell.imgV5.isHidden = false
            }
            
            cell.lblName.text = LookingData.Userdata.user_fullname
            let gender = LookingData.Userdata.user_gender
            cell.lblAgeGender.text = "\(LookingData.Userdata.user_age) Years \(Array_File.arrGender[gender]) "
            cell.lblViews.text = "\(100) Views"
            cell.lblDate.text = "\(self.ChangeDateFormat(Date: LookingData.Introduceyourself.avalibality_date) )    \(Array_File.arrLength_of_stay[LookingData.Introduceyourself.prefered_length_stay])"
            return cell
        }
        return cell
    }
    
    @objc func FavoriteMethod(btn : UIButton) {
        let Index = btn.tag
        let dic = self.arrMatches[Index] as! NSDictionary
        let offer = dic.value(forKey: "offering_home") as! NSDictionary
        self.OfferingId = offer.value(forKey: "offering_home_id") as! Int
        if dic.value(forKey: "is_favourite") as! Bool{
            self.isFavorite = false
        }else{
            self.isFavorite = true
        }
        self.FavoriteService()
    }
    
    func ChangeDateFormat(Date: String) -> String  {
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "yyyy-MM-dd"
        let newdate = dateFormatter.date(from: Date)
        dateFormatter.dateFormat = "MMM dd"
        return dateFormatter.string(from: newdate!)
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let dic = self.arrMatches[indexPath.row] as! NSDictionary
        if strCheckTable == "roomMate"{
            let story = UIStoryboard(name: "Detail", bundle: nil)
            let profile = story.instantiateViewController(withIdentifier: "FlatmateDetailViewController") as! FlatmateDetailViewController
            profile.dicFlatmateProfile = self.arrMatches[indexPath.row] as! NSDictionary
            self.navigationController?.pushViewController(profile, animated: true)
        }else{
            let story = UIStoryboard(name: "Detail", bundle: nil)
            let profile = story.instantiateViewController(withIdentifier: "PropertyDetailViewController") as! PropertyDetailViewController
            profile.dicPropertyDetail = self.arrMatches[indexPath.row] as! NSDictionary
            self.navigationController?.pushViewController(profile, animated: true)
        }
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 250
    }
}

////////////// textFileld delegate ///////////
extension MatchesViewController : UITextFieldDelegate, MyPickerViewProtocol{
    func textFieldDidBeginEditing(_ textField: UITextField) {
        if textField == self.txtAgegroup{
            let myPickerView =  MyPickerView.init(withDictionary: Array_File.arrAgeGroup, Tag:1)
            myPickerView.delegate = myPickerView
            myPickerView.MyPickerDelegate = self
            self.txtAgegroup.inputView = myPickerView
        }else if textField == self.txtBestmatch{
            let myPickerView =  MyPickerView.init(withDictionary: Array_File.arrfilter, Tag:2)
            myPickerView.delegate = myPickerView
            myPickerView.MyPickerDelegate = self
            self.txtBestmatch.inputView = myPickerView
        }else if textField == self.txtDateavailable || textField == self.txtMoveinDate {
            let datePickerView = UIDatePicker()
            datePickerView.datePickerMode = .date
            datePickerView.minimumDate = datePickerView.date
            self.txtDateavailable.inputView = datePickerView
            self.txtMoveinDate.inputView = datePickerView
            datePickerView.addTarget(self, action: #selector(handleDatePicker(sender:)), for: .valueChanged)
        }else if textField == self.txtLocation || textField == self.txtRLocation  {
            let lat = Double(self.latitude)
            let lon = Double(self.longitude)
            let center = CLLocationCoordinate2D(latitude: lat!, longitude: lon!)
            let northEast = CLLocationCoordinate2D(latitude: center.latitude + 0.001, longitude: center.longitude + 0.001)
            let southWest = CLLocationCoordinate2D(latitude: center.latitude - 0.001, longitude: center.longitude - 0.001)
            let viewport = GMSCoordinateBounds(coordinate: northEast, coordinate: southWest)
            
            let config = GMSPlacePickerConfig(viewport: viewport)
            let placePicker = GMSPlacePickerViewController(config: config)
            placePicker.delegate = self
            self.present(placePicker, animated: true, completion: nil)
        }else if textField == self.txtLastActive {
            let myPickerView =  MyPickerView.init(withDictionary: Array_File.arrLastActive, Tag:3)
            myPickerView.delegate = myPickerView
            myPickerView.MyPickerDelegate = self
            self.txtLastActive.inputView = myPickerView
        }else if textField == self.txtGender {
            let myPickerView =  MyPickerView.init(withDictionary: Array_File.arrGender, Tag:4)
            myPickerView.delegate = myPickerView
            myPickerView.MyPickerDelegate = self
            self.txtGender.inputView = myPickerView
        }else if textField == self.txtAge {
            let myPickerView =  MyPickerView.init(withDictionary: self.arrAge, Tag:5)
            myPickerView.delegate = myPickerView
            myPickerView.MyPickerDelegate = self
            self.txtAge.inputView = myPickerView
        }
        
    }
    func myPickerDidSelectRow(Index:Int?, Tag:Int) {
        if Tag == 1{
            self.agegroup = String(Index! + 1)
            self.txtAgegroup.text = Array_File.arrAgeGroup[Index!]
        } else if Tag == 2{
            if Index == 0{
                self.sort_by = "by_best_matches"
            }else if Index == 1{
                self.sort_by = "by_price"
            }else if Index == 2{
                self.sort_by = "by_recent"
            }
            self.txtBestmatch.text = Array_File.arrfilter[Index!]
        }else if Tag == 3 {
            if Index == 0{
                self.last_active = "all"
            }else if Index == 1{
                self.last_active = "7"
            }else if Index == 2{
                self.last_active = "30"
            }
            self.txtLastActive.text =  Array_File.arrLastActive[Index!]
        }else if Tag == 4 {
            self.gender = String(Index! + 1)
            self.txtGender.text = Array_File.arrGender[Index!]
        }else if Tag == 5 {
            self.age = self.arrAge[Index!]
            self.txtAge.text = self.arrAge[Index!]
        }
    }
    
    @objc func handleDatePicker(sender: UIDatePicker) {
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "dd MMM yyyy"
        self.txtDateavailable.text = dateFormatter.string(from: sender.date)
        self.txtMoveinDate.text = dateFormatter.string(from: sender.date)
        self.availableDate = Common.ChangeDateFormat(Date: self.txtDateavailable.text!, fromFormat: "dd MMM yyyy", toFormat: "yyyy-MM-dd")
        print(self.availableDate)        
    }
}

////////////// Current location ///////////
extension MatchesViewController : CLLocationManagerDelegate {
    func determineMyCurrentLocation() {
        locationManager = CLLocationManager()
        locationManager.delegate = self
        locationManager.desiredAccuracy = kCLLocationAccuracyBest
        locationManager.requestAlwaysAuthorization()
        if CLLocationManager.locationServicesEnabled() {
            locationManager.startUpdatingLocation()
        }
        locationManager.startUpdatingLocation()
    }

    func locationManager(_ manager: CLLocationManager, didUpdateLocations locations: [CLLocation]) {
        let userLocation:CLLocation = locations[0] as CLLocation
        self.latitude = String(userLocation.coordinate.latitude)
        self.longitude = String(userLocation.coordinate.longitude)
        print("user latitude = \(userLocation.coordinate.latitude)")
        print("user longitude = \(userLocation.coordinate.longitude)")
        locationManager.stopUpdatingLocation()
    }
    
    func locationManager(_ manager: CLLocationManager, didFailWithError error: Error)
    {
        print("Error \(error.localizedDescription)")
    }
}

////////////// Collectionview delegates ///////////
extension MatchesViewController : UICollectionViewDelegate , UICollectionViewDataSource ,UICollectionViewDelegateFlowLayout{
    
    func numberOfSections(in collectionView: UICollectionView) -> Int {
        return 1
    }
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        
        return Array_File.arrHomeDescriptionOptionImgs.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        
        let cell = collHomefeature.dequeueReusableCell(withReuseIdentifier: "homeFeaturesCollCell", for: indexPath) as! homeFeaturesCollCell
        let dict = Array_File.arrHomeDescriptionOptionImgs[indexPath.row]
        cell.imgFeatures.image=UIImage(named: (dict["image"])!)?.imageWithInsets(insetDimen: 8)
        cell.lblIconName.text=(dict["name"]!)
        
        if _selectedCells.contains(indexPath.row + 1) {
            cell.isSelected=true
            collHomefeature.selectItem(at: indexPath, animated: true, scrollPosition: UICollectionViewScrollPosition.top)
            cell.imgFeatures.tintImageColor(color: UIColorFromHex(rgbValue: 0x05C6CF, alpha: 1.0))
        }
        else{
            cell.isSelected=false
            cell.imgFeatures.tintImageColor(color: .lightGray)
        }
        
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath){
        _selectedCells.add(indexPath.row + 1)
        print(_selectedCells)
        self.strHomeFeatures = _selectedCells.componentsJoined(by: ",")
        collHomefeature.reloadItems(at: [indexPath])
    }
    
    func collectionView(_ collectionView: UICollectionView, didDeselectItemAt indexPath: IndexPath) {
        _selectedCells.remove(indexPath.row + 1)
        print(_selectedCells)
        self.strHomeFeatures = _selectedCells.componentsJoined(by: ",")
        collHomefeature.reloadItems(at: [indexPath])
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        let yourWidth = collectionView.bounds.width/3.0
        return CGSize(width: yourWidth, height: 100.0)
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, insetForSectionAt section: Int) -> UIEdgeInsets {
        return UIEdgeInsets.zero
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumInteritemSpacingForSectionAt section: Int) -> CGFloat {
        return 0
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumLineSpacingForSectionAt section: Int) -> CGFloat {
        return 0
    }
}

////////////// PlacePicker ///////////
extension MatchesViewController : GMSPlacePickerViewControllerDelegate{
    
    func placePicker(_ viewController: GMSPlacePickerViewController, didPick place: GMSPlace) {
        
        self.txtLocation.text=place.formattedAddress!
        self.latitude = String(place.coordinate.latitude)
        self.longitude = String(place.coordinate.longitude)
        
        print(place.coordinate.latitude)
        print(place.coordinate.longitude)
        viewController.dismiss(animated: true, completion: nil)
    }
    
    func placePickerDidCancel(_ viewController: GMSPlacePickerViewController) {
        // Dismiss the place picker, as it cannot dismiss itself.
        viewController.dismiss(animated: true, completion: nil)
        print("No place selected")
    }
}
