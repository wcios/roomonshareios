//
//  MoreOptionsViewController.swift
//  RoomOnShare
//
//  Created by mac on 17/08/18.
//  Copyright © 2018 WestCoast. All rights reserved.
//

import UIKit

class MoreOptionsViewController: UIViewController {
    
    @IBOutlet weak var tblOptions: UITableView!
    
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        // Do any additional setup after loading the view.
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    override func viewWillAppear(_ animated: Bool) {
        self.navigationController?.isNavigationBarHidden = true
        rdv_tabBarController?.setTabBarHidden(false, animated: true)
    }
}

extension MoreOptionsViewController: UITableViewDelegate, UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return Array_File.arrOptions.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = self.tblOptions.dequeueReusableCell(withIdentifier: "Option") as! OptionTableViewCell
        cell.lblName.text = Array_File.arrOptions[indexPath.row]
        cell.imgIcons.image = UIImage(named: Array_File.arrMenuOptionsImg[indexPath.row])
        if indexPath.row == 0 || indexPath.row == 3 {
            cell.lblLine.isHidden = false
        }else{
            cell.lblLine.isHidden = true
        }
        
        return cell
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 50
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        switch indexPath.row {
        case 0:
            let story = UIStoryboard(name: "Main", bundle: nil)
            let profile = story.instantiateViewController(withIdentifier: "ContactInfoViewController") as! ContactInfoViewController
            profile.checkvc = "Setting"
            self.navigationController?.pushViewController(profile, animated: true)
        case 1:
            Common.PushMethod(VC: self, identifier: "myAppoinmnetViewController")
        case 2:
            Common.PushMethod(VC: self, identifier: "ServicePriceViewController") // Upgrate Servcie
        case 3:
            print("vjjb")
        //Common.PushMethod(VC: self, identifier: "")  // Invite & Refer
        case 4:
            print("vjjb")
        //Common.PushMethod(VC: self, identifier: "")  // Rewards
        case 5:
            Common.PushMethod(VC: self, identifier: "SettingViewController") // Setting
        case 6:
            print("vjjb")
        //Common.PushMethod(VC: self, identifier: "") // Feedback
        case 7:
            print("vjjb")
        //Common.PushMethod(VC: self, identifier: "") // Help
        case 8:
            print("vjjb")
        //Common.PushMethod(VC: self, identifier: "") // Contact Us
        case 9:
            SharedPreference.setIsUserLogin(false)
            let story = UIStoryboard(name: "Main", bundle: nil)
            let login = story.instantiateViewController(withIdentifier: "LoginViewController") as! LoginViewController
            self.navigationController?.pushViewController(login, animated: true)
        //            Common.PushMethod(VC: self, identifier: "") // Logout
        default:
            Common.PushMethod(VC: self, identifier: "")
        }
    }
    
    
}
