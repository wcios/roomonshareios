//
//  ResetPasswordViewController.swift
//  RoomOnShare
//
//  Created by mac on 01/08/18.
//  Copyright © 2018 WestCoast. All rights reserved.
//

import UIKit

class ResetPasswordViewController: UIViewController {

    @IBOutlet weak var txtOTP: UITextField!
    @IBOutlet weak var txtPassword: UITextField!
    @IBOutlet weak var txtConfirmPassword: UITextField!
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    @IBAction func BackMethod(_ sender: Any) {
        Common.PopMethod(VC: self)
    }
    
    @IBAction func ResetPasswordMethod(_ sender: Any) {
        guard let email = self.txtOTP.text, email != ""  else {
            return Common.ShowAlert(Title: Common.Title, Message: "Please enter OTP", VC: self)
        }
        
        guard let pass = self.txtPassword.text, pass != ""  else {
            return Common.ShowAlert(Title: Common.Title, Message: "Please enter password", VC: self)
        }
        
        guard let conpass = self.txtConfirmPassword.text, conpass != ""  else {
            return Common.ShowAlert(Title: Common.Title, Message: "Please enter confirmpassword", VC: self)
        }
        
        guard  self.txtPassword.text == self.txtConfirmPassword.text   else {
            return Common.ShowAlert(Title: Common.Title, Message: "Password not matched", VC: self)
        }
    }
    
    func ResetPasswordService(){
        self.startActivityIndicator()
        
        let param = [ "otp": self.txtOTP.text!,
                      "password" : self.txtPassword.text!] as [String : Any]
        
        CommunicationManager().getResponseForPost(strUrl: Url_File.ResetPassword, parameters: param as NSDictionary) { ( result , data) in
            if(result == "success") {
                let dataDict = data as! NSDictionary
                let strMessage = dataDict["message"] as! String
                self.stopActivityIndicator()
                Common.ShowAlert(Title: Common.Title, Message: strMessage, VC: self)
                //self.GoNext()
            }else if (result == "error") {
                let strMessage = (data as! NSDictionary)["message"] as! String
                Common.ShowAlert(Title: Common.Title, Message: strMessage, VC: self)
                self.stopActivityIndicator()
            }else if (result == "Network") {
                let strMessage = (data as! String)
                Common.ShowAlert(Title: Common.Title, Message: strMessage, VC: self)
                self.stopActivityIndicator()
            }
        }
    }
    
    func GoNext() {
        
    }
}
