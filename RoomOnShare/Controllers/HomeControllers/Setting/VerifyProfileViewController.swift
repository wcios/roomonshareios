//
//  VerifyProfileViewController.swift
//  RoomOnShare
//
//  Created by mac on 06/08/18.
//  Copyright © 2018 WestCoast. All rights reserved.
//

import UIKit
import FBSDKCoreKit
import FBSDKLoginKit
import GoogleSignIn
import LinkedinSwift

class VerifyProfileViewController: UIViewController {
    
    @IBOutlet weak var btnEmail: UIButton!
    @IBOutlet weak var btnMobile: UIButton!
    @IBOutlet weak var btnFacebook: UIButton!
    @IBOutlet weak var btngoogle: UIButton!
    @IBOutlet weak var btnLinkedin: UIButton!
    
    var linkedinid = ""
    var facebookid = ""
    var googleid = ""
    var tag = 0
    
    let linkedinHelper = LinkedinSwiftHelper(configuration: LinkedinSwiftConfiguration(clientId: "81zhiuczjmy4v9", clientSecret: "5nKx1TeI0JEYjsmF", state: "linkedin\(Int(Date().timeIntervalSince1970))", permissions: ["r_basicprofile", "r_emailaddress"], redirectUrl: "https://www.linkedin.com/oauth/v2/accessToken"))

    override func viewDidLoad() {
        super.viewDidLoad()
        GIDSignIn.sharedInstance().uiDelegate = self
        GIDSignIn.sharedInstance().delegate = self
        // Do any additional setup after loading the view.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    @IBAction func BackMethod(_ sender: Any) {
           Common.PopMethod(VC: self)
    }
    
    @IBAction func EmailMethod(_ sender: Any) {
        self.VerifyEmailService()
    }
    
    @IBAction func MobileMethod(_ sender: Any) {
    }
    
    @IBAction func FacebookMethod(_ sender: Any) {
        self.tag = 1
        let fbLoginManager : FBSDKLoginManager = FBSDKLoginManager()
        fbLoginManager.logIn(withReadPermissions: ["email"], from: self) {
            (result, error) in
            if (error == nil) {
                let fbloginresult : FBSDKLoginManagerLoginResult = result!
                if fbloginresult.grantedPermissions != nil {
                    if (fbloginresult.grantedPermissions.contains("email")){
                        self.getFBUserData()
                        fbLoginManager.logOut()
                    }
                }
            }
        }
    }
    
    func getFBUserData() {
        if ((FBSDKAccessToken.current()) != nil) {
            FBSDKGraphRequest(graphPath: "me", parameters: ["fields": "id, name, first_name, last_name, picture.type(large), email"]).start(completionHandler: { (connection, result,error) -> Void in
                if (error == nil){
                    print(result!)
                    let dict = result as! NSDictionary
                    self.facebookid = dict.value(forKey: "id") as? String ?? ""
                    self.VerifyProfileService()
                }
            })
        }
    }
    
    @IBAction func GoogleMethod(_ sender: Any) {
        self.tag = 2
        GIDSignIn.sharedInstance().signIn()
    }
    
    @IBAction func LinkedinMethod(_ sender: Any) {
        self.tag = 3
        linkedinHelper.authorizeSuccess({ [unowned self] (lsToken) -> Void in
            print("token----\(lsToken)")
            // let strToken = lsToken.accessToken
            self.linkedinHelper.requestURL("https://api.linkedin.com/v1/people/~:(id,first-name,last-name,email-address,picture-url,picture-urls::(original),positions,date-of-birth,phone-numbers,location)?format=json", requestType: LinkedinSwiftRequestGet, success: { (response) -> Void in
                print("Request success with response: \(response)")
                DispatchQueue.main.async {
                    
                    let dict = response.jsonObject! as NSDictionary
                    self.linkedinid = dict.value(forKey: "id") as? String ?? ""
                    self.VerifyProfileService()
                }
            }) { (error) -> Void in
                print("Request success with response: \(error)") //[unowned self]
            }
            
            }, error: {  (error) -> Void in
                //self.writeConsoleLine("Encounter error: \(error.localizedDescription)") [unowned self]
        }, cancel: {  () -> Void in
            // self.writeConsoleLine("User Cancelled!") //[unowned self]
        })
    }
    
    
    
    ////////////// Verify Profile Service ///////////
    func VerifyProfileService(){
        self.startActivityIndicator()
        var param : [String:Any] = [:]
        if self.tag == 1 {
            param = [ "facebook_id"  : self.facebookid
                ] as [String : Any]
        }else if self.tag == 2{
            param = [ "google_id"  : self.googleid
                ] as [String : Any]
        }else if self.tag == 3{
            param = [ "linkedin_id"  : self.linkedinid
                ] as [String : Any]
        }
        print(param)
        
        CommunicationManager().getResponseForPost(strUrl: Url_File.VerifyProfile , parameters: param as NSDictionary) { ( result , data) in
            if(result == "success") {
                let dataDict = data as! NSDictionary
                print(dataDict)
                let dic = dataDict["response"] as! NSDictionary
                print(dic)
                self.stopActivityIndicator()
            }else if (result == "error") {
                let strMessage = (data as! NSDictionary)["message"] as! String
                Common.ShowAlert(Title: Common.Title, Message: strMessage, VC: self)
                self.stopActivityIndicator()
            }else if (result == "Network") {
                let strMessage = (data as! String)
                Common.ShowAlert(Title: Common.Title, Message: strMessage, VC: self)
                self.stopActivityIndicator()
            }
        }
    }
    
    ////////////// Verify Email Service ///////////
    func VerifyEmailService(){
        self.startActivityIndicator()
        CommunicationManager().getResponseForPost(strUrl: Url_File.VerifyEmail , parameters: nil) { ( result , data) in
            if(result == "success") {
                let dataDict = data as! NSDictionary
                print(dataDict)
                let strMessage = dataDict["message"] as! String
                Common.ShowAlert(Title: Common.Title, Message: strMessage, VC: self)
                self.stopActivityIndicator()
            }else if (result == "error") {
                let strMessage = (data as! NSDictionary)["message"] as! String
                Common.ShowAlert(Title: Common.Title, Message: strMessage, VC: self)
                self.stopActivityIndicator()
            }else if (result == "Network") {
                let strMessage = (data as! String)
                Common.ShowAlert(Title: Common.Title, Message: strMessage, VC: self)
                self.stopActivityIndicator()
            }
        }
    }
}


extension VerifyProfileViewController :  GIDSignInDelegate, GIDSignInUIDelegate {
    func sign(_ signIn: GIDSignIn!, didSignInFor user: GIDGoogleUser!, withError error: Error!) {
        if (error == nil) {
            let userId = user.userID
            self.googleid = userId!
            print("\(userId!)")
            self.VerifyProfileService()
        } else {
            print("\(error.localizedDescription)")
        }
    }
    
    // Present a view that prompts the user to sign in with Google
    func sign(_ signIn: GIDSignIn!,
              present viewController: UIViewController!) {
        self.present(viewController, animated: true, completion: nil)
    }
    
    // Dismiss the "Sign in with Google" view
    func sign(_ signIn: GIDSignIn!,
              dismiss viewController: UIViewController!) {
        self.dismiss(animated: true, completion: nil)
    }
}
