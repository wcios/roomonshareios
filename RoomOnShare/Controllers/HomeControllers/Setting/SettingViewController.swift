//
//  SettingViewController.swift
//  RoomOnShare
//
//  Created by mac on 06/08/18.
//  Copyright © 2018 WestCoast. All rights reserved.
//

import UIKit

class SettingViewController: UIViewController {

    @IBOutlet weak var lblEmail: UILabel!
    @IBOutlet weak var txtCountry: UITextField!
    @IBOutlet weak var txtMessage: UILabel!
    @IBOutlet weak var txtMacthes: UILabel!
    
    var countries : [country] = [country]()
    var arrCountryName : [String] = []
    var countr : String = ""
    
    override func viewDidLoad() {        
        super.viewDidLoad()
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    override func viewWillAppear(_ animated: Bool) {
        self.navigationController?.isNavigationBarHidden = true
       // rdv_tabBarController?.setTabBarHidden(false, animated: true)
        self.lblEmail.text = SharedPreference.getUserData().user_email
        self.txtCountry.text = SharedPreference.getUserData().user_country
        self.txtCountry.delegate = self
        self.countries = countrydetail.readCountryJson()
        for item in self.countries {           
            self.arrCountryName.append(item.Country_name)
        }
    }
    
    @IBAction func BackMethod(_ sender: Any) {
        Common.PopMethod(VC: self)
    }
    
    @IBAction func ChangeEmailMethod(_ sender: Any) {
        Common.PushMethod(VC: self, identifier: "ChangeEmailViewController")
    }
    
    @IBAction func ChangePasswordMethod(_ sender: Any) {
        Common.PushMethod(VC: self, identifier: "ChangePasswordViewController")
    }
    
    @IBAction func VerifyMethod(_ sender: Any) {
        Common.PushMethod(VC: self, identifier: "VerifyProfileViewController")
    }
    
    ////////////// Change Country Service ///////////
    func ChangeCountryService(){
        self.startActivityIndicator()
        var param : [String:Any] = [:]
        param = [ "country"  : self.countr
            ] as [String : Any]
        
        print(param)
        
        CommunicationManager().getResponseForPost(strUrl: Url_File.ChangeCountry , parameters: param as NSDictionary) { ( result , data) in
            if(result == "success") {
                let dataDict = data as! NSDictionary
                print(dataDict)
                let strMessage = dataDict["message"] as! String
                Common.ShowAlert(Title: Common.Title, Message: strMessage, VC: self)
                SharedPreference.Setcountry(country: self.txtCountry.text!)
                self.stopActivityIndicator()
            }else if (result == "error") {
                let strMessage = (data as! NSDictionary)["message"] as! String
                Common.ShowAlert(Title: Common.Title, Message: strMessage, VC: self)
                self.stopActivityIndicator()
            }else if (result == "Network") {
                let strMessage = (data as! String)
                Common.ShowAlert(Title: Common.Title, Message: strMessage, VC: self)
                self.stopActivityIndicator()
            }
        }
    }
}


extension SettingViewController : UITextFieldDelegate, MyPickerViewProtocol{
    func textFieldDidBeginEditing(_ textField: UITextField) {
         if textField == self.txtCountry{
            let myPickerView =  MyPickerView.init(withDictionary: self.arrCountryName, Tag:1)
            myPickerView.delegate = myPickerView
            myPickerView.MyPickerDelegate = self
            self.txtCountry.inputView = myPickerView
        }
    }
    
    func myPickerDidSelectRow(Index:Int?, Tag:Int) {
        if Tag == 1{
            let item = self.countries[Index!]
            self.countr = item.Country_code
            self.txtCountry.text = item.Country_name
            self.ChangeCountryService()
        }
    }
}
