//
//  UpgradeeViewController.swift
//  RoomOnShare
//
//  Created by mac on 07/12/18.
//  Copyright © 2018 WestCoast. All rights reserved.
//

import UIKit

class ServicePriceViewController: UIViewController , UITableViewDataSource , UITableViewDelegate , UIScrollViewDelegate , PayPalPaymentDelegate {
    
    
    
    var environment:String = PayPalEnvironmentNoNetwork {
        willSet(newEnvironment) {
            if (newEnvironment != environment) {
                PayPalMobile.preconnect(withEnvironment: newEnvironment)
            }
        }
    }
    var payPalConfig = PayPalConfiguration() // default
    @IBOutlet weak var OfferingTableView: UITableView!
    @IBOutlet weak var lookingTableView: UITableView!
    @IBOutlet weak var btnPay: UIButton!
    
    @IBOutlet weak var scrollView: UIScrollView!
    @IBOutlet weak var contentView: UIView!
    
    
    @IBOutlet weak var tblHeightConstraint : NSLayoutConstraint!
    @IBOutlet weak var OfferViewHeightConstraint : NSLayoutConstraint!
    
    
    @IBOutlet weak var tblLookHeightConstraint : NSLayoutConstraint!
    @IBOutlet weak var LookViewHeightConstraint : NSLayoutConstraint!
    
    @IBOutlet weak var contentViewHeightConstraint : NSLayoutConstraint!
    var resultText = "" // empty
    var arrOffering : [NSDictionary] = []
    var arrLooking : [NSDictionary] = []
    
    // PayPalPaymentDelegate
    
    func payPalPaymentDidCancel(_ paymentViewController: PayPalPaymentViewController) {
        print("PayPal Payment Cancelled")
        resultText = ""
        // successView.isHidden = true
        paymentViewController.dismiss(animated: true, completion: nil)
    }
    
    func payPalPaymentViewController(_ paymentViewController: PayPalPaymentViewController, didComplete completedPayment: PayPalPayment) {
        print("PayPal Payment Success !")
        paymentViewController.dismiss(animated: true, completion: { () -> Void in
            // send completed confirmaion to your server
            print("Here is your proof of payment:\n\n\(completedPayment.confirmation)\n\nSend this to your server for confirmation and fulfillment.")
            
            self.resultText = completedPayment.description
            // self.showSuccess()
        })
    }
    
    
    override func viewDidLoad()
    {
        super.viewDidLoad()
        // Do any additional setup after loading the view.
        title = "PayPal SDK Demo"
        // successView.isHidden = true
        
        // Set up payPalConfig
        payPalConfig.acceptCreditCards = false
        payPalConfig.merchantName = "Awesome Shirts, Inc."
        payPalConfig.merchantPrivacyPolicyURL = URL(string: "https://www.paypal.com/webapps/mpp/ua/privacy-full")
        payPalConfig.merchantUserAgreementURL = URL(string: "https://www.paypal.com/webapps/mpp/ua/useragreement-full")
        payPalConfig.payPalShippingAddressOption = .none;
        
        print("PayPal iOS SDK Version: \(PayPalMobile.libraryVersion())")
        
        
        
        btnPay.isHidden = true
        OfferingTableView.tableFooterView = UIView()
        lookingTableView.tableFooterView = UIView()
        
        self.callGetServicePriceApi()
        
    }
    @IBAction func ActionPay(_ sender: Any)
    {
        resultText = ""
        // Remove our last completed payment, just for demo purposes.
        
        // Note: For purposes of illustration, this example shows a payment that includes
        //       both payment details (subtotal, shipping, tax) and multiple items.
        //       You would only specify these if appropriate to your situation.
        //       Otherwise, you can leave payment.items and/or payment.paymentDetails nil,
        //       and simply set payment.amount to your total charge.
        
        // Optional: include multiple items
        let item1 = PayPalItem(name: "Old jeans with holes", withQuantity: 2, withPrice: NSDecimalNumber(string: "84.99"), withCurrency: "USD", withSku: "Hip-0037")
        let item2 = PayPalItem(name: "Free rainbow patch", withQuantity: 1, withPrice: NSDecimalNumber(string: "0.00"), withCurrency: "USD", withSku: "Hip-00066")
        let item3 = PayPalItem(name: "Long-sleeve plaid shirt (mustache not included)", withQuantity: 1, withPrice: NSDecimalNumber(string: "37.99"), withCurrency: "USD", withSku: "Hip-00291")
        
        let items = [item1, item2, item3]
        let subtotal = PayPalItem.totalPrice(forItems: items)
        
        // Optional: include payment details
        let shipping = NSDecimalNumber(string: "5.99")
        let tax = NSDecimalNumber(string: "2.50")
        let paymentDetails = PayPalPaymentDetails(subtotal: subtotal, withShipping: shipping, withTax: tax)
        
        let total = subtotal.adding(shipping).adding(tax)
        
        let payment = PayPalPayment(amount: total, currencyCode: "USD", shortDescription: "Hipster Clothing", intent: .sale)
        
        payment.items = items
        payment.paymentDetails = paymentDetails
        
        if (payment.processable) {
            let paymentViewController = PayPalPaymentViewController(payment: payment, configuration: payPalConfig, delegate: self)
            present(paymentViewController!, animated: true, completion: nil)
        }
        else {
            // This particular payment will always be processable. If, for
            // example, the amount was negative or the shortDescription was
            // empty, this payment wouldn't be processable, and you'd want
            // to handle that here.
            print("Payment not processalbe: \(payment)")
        }
        
    }
    
    
    override func viewWillAppear(_ animated: Bool) {
        self.navigationController?.isNavigationBarHidden = true
        rdv_tabBarController?.setTabBarHidden(false, animated: true)
    }
    
    @IBAction func BackMethod(_ sender: Any) {
        Common.PopMethod(VC: self)
    }
    func callGetServicePriceApi(){
        self.startActivityIndicator()
        CommunicationManager().getResponseFor(strUrl: Url_File.getServicPrice_Url, parameters: nil) { ( result , data) in
            
            if(result == "success") {
                //  print(data)
                self.stopActivityIndicator()
                let dataDict = data as! NSDictionary
                let response = dataDict["response"] as! NSDictionary
                //print(response)
                self.arrOffering = response["offering_package"] as! [NSDictionary]
                print(self.arrOffering)
                self.arrLooking = response["looking_package"] as! [NSDictionary]
                
                self.OfferingTableView.reloadData()
                self.lookingTableView.reloadData()
                
                self.scrollView.delegate = self
                
            }else if (result == "error") {
                let strMessage = (data as! NSDictionary)["message"] as! String
                Common.ShowAlert(Title: Common.Title, Message: strMessage, VC: self)
                self.stopActivityIndicator()
            }else if (result == "Network") {
                let strMessage = (data as! String)
                Common.ShowAlert(Title: Common.Title, Message: strMessage, VC: self)
                self.stopActivityIndicator()
            }
        }
    }
    
    
    
    
    //MARK:- TABLE VIEW
    func numberOfSections(in tableView: UITableView) -> Int
    {
        return 1
    }
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int
    {
        if tableView == OfferingTableView
        {
            return self.arrOffering.count
            
            
        }else if tableView == lookingTableView
        {
            return self.arrLooking.count
        }
        return 0
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        if(tableView == OfferingTableView){
            if(self.arrOffering.count == 1){
                self.tblLookHeightConstraint.constant = 65
                self.LookViewHeightConstraint.constant = 105
                return 60
            }
            if(self.arrOffering.count > 1){
                self.tblHeightConstraint.constant = CGFloat(65 * (self.arrOffering.count))
                self.OfferViewHeightConstraint.constant = CGFloat((65 * (self.arrOffering.count)) + 20)
                return 60
            }
        }
        if(tableView == lookingTableView){
            if(self.arrLooking.count == 1){
                self.tblLookHeightConstraint.constant = 65
                self.LookViewHeightConstraint.constant = 105
                return 60
            }
            if(self.arrLooking.count > 1){
                self.tblLookHeightConstraint.constant = CGFloat(65 * (self.arrLooking.count))
                self.LookViewHeightConstraint.constant = CGFloat((65 * (self.arrLooking.count)) + 20)
                return 60
            }
        }
        return 0
    }
    
    func scrollViewDidScroll(_ scrollView: UIScrollView) {
        print(contentView.frame.height)
        if(contentView.frame.height == 750){
            self.contentViewHeightConstraint.constant = self.OfferViewHeightConstraint.constant + self.LookViewHeightConstraint.constant + 100
        }
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell{
        let cell = UITableViewCell()
        
        if tableView == OfferingTableView{
            let cell = OfferingTableView.dequeueReusableCell(withIdentifier: "OfferingtableViewCell") as! OfferingtableViewCell
            let dict = self.arrOffering[indexPath.row]
            cell.lblValidityOff.text = "\(dict["month_validity"] as! Int) Month"
            cell.lblPriceOff.text = "\("USD") \(dict["price"] as! String)"
            return cell
        }
        
        if tableView == lookingTableView{
            let cell1 = lookingTableView.dequeueReusableCell(withIdentifier: "LookingtableViewCell") as! LookingtableViewCell
            let dict = self.arrLooking[indexPath.row]
            cell1.lblValiditylooking.text = "\(dict["month_validity"] as! Int) Month"
            cell1.lblPricelooking.text = "\("USD") \(dict["price"] as! String)"
            return cell1
        }
        return cell
    }
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath)
    {
        btnPay.isHidden = false
        
    }
    
    override func didReceiveMemoryWarning()
    {
        super.didReceiveMemoryWarning()
    }
}

class OfferingtableViewCell:UITableViewCell
{
    @IBOutlet weak var lblValidityOff : UILabel!
    @IBOutlet weak var lblPriceOff : UILabel!
}
class LookingtableViewCell:UITableViewCell
{
    @IBOutlet weak var lblValiditylooking : UILabel!
    @IBOutlet weak var lblPricelooking : UILabel!
}
