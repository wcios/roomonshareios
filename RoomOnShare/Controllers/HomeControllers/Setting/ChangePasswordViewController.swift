//
//  ChangePasswordViewController.swift
//  RoomOnShare
//
//  Created by mac on 06/08/18.
//  Copyright © 2018 WestCoast. All rights reserved.
//

import UIKit

class ChangePasswordViewController: UIViewController {

    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    @IBAction func BackMethod(_ sender: Any) {
        Common.PopMethod(VC: self)
    }
    
    @IBAction func RequestOTPMethod(_ sender: Any) {
        //self.SettingSendOtpService()
        Common.PushMethod(VC: self, identifier: "ResetPasswordViewController")
    }
    
    /////////// SettingSendOtp ///////////
    func SettingSendOtpService(){
        self.startActivityIndicator()
        
        let param = ["mobile": SharedPreference.getUserData().user_mobile_no]
        
        CommunicationManager().getResponseForPost(strUrl: Url_File.SettingSendOtp, parameters: param as NSDictionary) { ( result , data) in
            
            if(result == "success") {
                let dataDict = data as! NSDictionary
                print(dataDict)
                let strMessage = dataDict["message"] as! String
                Common.ShowAlert(Title: Common.Title, Message: strMessage, VC: self)
                self.GoNext()
                self.stopActivityIndicator()
            }else if (result == "error") {
                let strMessage = (data as! NSDictionary)["message"] as! String
                Common.ShowAlert(Title: Common.Title, Message: strMessage, VC: self)
                self.stopActivityIndicator()
            }else if (result == "Network") {
                let strMessage = (data as! String)
                Common.ShowAlert(Title: Common.Title, Message: strMessage, VC: self)
                self.stopActivityIndicator()
            }
        }
    }
    
    func GoNext() {
         Common.PushMethod(VC: self, identifier: "ResetPasswordViewController")
    }
    
}
