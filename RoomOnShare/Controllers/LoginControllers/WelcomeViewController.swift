//
//  WelcomeViewController.swift
//  RoomOnShare
//
//  Created by mac on 30/08/18.
//  Copyright © 2018 WestCoast. All rights reserved.
//

import UIKit

class WelcomeViewController: UIViewController {
    
    @IBOutlet weak var lblName: UILabel!
    var userdata = SharedPreference.getUserData()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.lblName.text = "Welcome \(userdata.user_fullname)"
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    @IBAction func OkMethod(_ sender: Any) {
        Common.setupTabViewController(Vc : self)
    }
    

}
