//
//  LoginViewController.swift
//  RoomOnShare
//
//  Created by mac on 27/07/18.
//  Copyright © 2018 WestCoast. All rights reserved.
//

import UIKit
import Crashlytics
import CoreLocation
//import RDVTabBarControllerSwift


class LoginViewController: UIViewController {

    @IBOutlet weak var btnFacebook: UIButton!    
    @IBOutlet weak var btnGoogle: UIButton!
    @IBOutlet weak var btnlogin: UIButton!
    @IBOutlet weak var txtEmail: UITextField!
    @IBOutlet weak var txtPassword: UITextField!
    @IBOutlet weak var btnShowPassword: UIButton!

    override func viewDidLoad() {
        super.viewDidLoad()
//        self.padding(txt: self.txtEmail)
//        self.padding(txt: self.txtPassword)
        
      // txtEmail.text = "akansha.n@westcoastinfotech.com"
      //   txtEmail.text = "text3@gmail.com"
      //  txtPassword.text = "1234567"
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    override func viewWillAppear(_ animated: Bool) {
        self.navigationController?.isNavigationBarHidden = true
        rdv_tabBarController?.setTabBarHidden(true, animated: true)
    }
    
    @IBAction func ShowPasswordMethod(_ sender: Any) {
        if self.txtPassword.isSecureTextEntry {
            self.txtPassword.isSecureTextEntry = false
            //self.btnShowPassword.setImage(" ", for: .normal)
        }else{
            self.txtPassword.isSecureTextEntry = true            
            //self.btnShowPassword.setImage(" ", for: .normal)
        }
    }

    @IBAction func ForgotPasswordMethod(_ sender: Any) {
        Common.PushMethod(VC: self, identifier: "ForgotPasswordViewController")
    }
    
    @IBAction func LoginMethod(_ sender: Any) {
        
        
        guard let email = self.txtEmail.text, email != ""  else {
            return Common.ShowAlert(Title: Common.Title, Message: "Please enter E-mail", VC: self)
        }

        guard Common.isValidEmail(testStr: self.txtEmail.text!)  else {
            return Common.ShowAlert(Title: Common.Title, Message: "Please enter valid E-mail", VC: self)
        }

        guard let password = self.txtPassword.text, password != ""  else {
            return Common.ShowAlert(Title: Common.Title, Message: "Please enter password", VC: self)
        }

        guard Common.isCheckPasswordLength(password: self.txtPassword.text!)  else {
            return Common.ShowAlert(Title:  Common.Title, Message: "Your password is too short", VC: self)
        }
        
        self.LoginService()
    }
    
    @IBAction func NotRegistredMethod(_ sender: Any) {
        Common.PushMethod(VC: self, identifier: "SignUpViewController")        
    }
    
    func LoginService(){
        self.startActivityIndicator()
        
        let param = ["email" : self.txtEmail.text!,
                     "password" : self.txtPassword.text! ]
        
        CommunicationManager().getResponseForPost(strUrl: Url_File.Login, parameters: param as NSDictionary) { ( result , data) in
            
            if(result == "success") {
                let dataDict = data as! NSDictionary
                print(dataDict)
                let dic = dataDict["response"] as! NSDictionary
                SharedPreference.setIsUserLogin(true)
                let userdata = UserData.init(withDictionary: dic as! [String : Any])
                SharedPreference.saveUserData(userdata)
                print(userdata)
                CreateProifleViewController.looking_id = dic.value(forKey: "looking_home_id") as! Int
                SharedPreference.setLooking_id(dic.value(forKey: "looking_home_id") as! Int)
                self.GoNext(dicdata: dic)
                self.stopActivityIndicator()
            }else if (result == "error") {
                let strMessage = (data as! NSDictionary)["message"] as! String
                Common.ShowAlert(Title: Common.Title, Message: strMessage, VC: self)
                self.stopActivityIndicator()
            }else if (result == "Network") {
                let strMessage = (data as! String)
                Common.ShowAlert(Title: Common.Title, Message: strMessage, VC: self)
                self.stopActivityIndicator()
            }            
        }
    }
    
    func GoNext(dicdata : NSDictionary){
        print(SharedPreference.getUserData().mobile_verified)
        if  SharedPreference.getUserData().mobile_verified{
            Common.setupTabViewController(Vc : self)
        }else{
            Common.PushMethod(VC: self, identifier: "VerifyViewController")
        }
    }
    
    func padding(txt : UITextField) {
        let paddingView = UIView(frame: CGRect(x:0, y:0, width :50, height: txt.frame.height))
        txt.leftView = paddingView
        txt.leftViewMode = UITextFieldViewMode.always
    }
}

