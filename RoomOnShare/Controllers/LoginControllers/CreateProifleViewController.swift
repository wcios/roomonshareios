//
//  CreateProifleViewController.swift
//  RoomOnShare
//
//  Created by mac on 31/07/18.
//  Copyright © 2018 WestCoast. All rights reserved.
//

import UIKit

class CreateProifleViewController: UIViewController {
    
    static var looking_id = 0

    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    override func viewWillAppear(_ animated: Bool) {
        self.navigationController?.isNavigationBarHidden = true
        //rdv_tabBarController?.setTabBarHidden(false, animated: true)
    }
    
    
    @IBAction func LookingHomeMethod(_ sender: Any) {
        if CreateProifleViewController.looking_id == 0{
            SharedPreference.setLooking_id(0)
            LookingDescriptionViewController.SGender = 0
            IntroduceSelfViewController.date = ""
            AddPhotoViewController.SarrPhoto = []
            PreferredLoactionViewController.Slatitude = 0000
            HomePreferencesViewController.SRent = 0
            FlatmatePreferenceViewController.Sid_Gender = []
            Common.PushMethod(VC: self, identifier: "LookingDescriptionViewController")
        }else{
            Common.ShowAlert(Title: Common.Title, Message: "Profile already created", VC: self)
        }
    }
    
    @IBAction func BackMethod(_ sender: Any) {
        Common.PopMethod(VC: self)
    }
   
    @IBAction func clickOnOfferingRoomBtn(_ sender : UIButton){
        userDef.set(nil, forKey: "bedroomarray")
        SharedPreference.setOffering_id(0)
        offeringHomeDescriptionVC.Shometype = 0
        AboutOccupantsViewController.Sid_Gender = []
        AddPhotoViewController.SarrPhoto = []
        FlatmatePreferenceViewController.Sid_Gender = []
        Common.PushMethod(VC: self, identifier: "offeringHomeDescriptionVC")
    }

    
}
