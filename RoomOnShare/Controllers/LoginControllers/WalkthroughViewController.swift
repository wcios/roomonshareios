//
//  WalkthroughViewController.swift
//  RoomOnShare
//
//  Created by mac on 11/08/18.
//  Copyright © 2018 WestCoast. All rights reserved.
//

import UIKit
import Crashlytics

class WalkthroughViewController: UIViewController, UIScrollViewDelegate {
    
    @IBOutlet weak var scrollview: UIScrollView!
    @IBOutlet weak var pageControl: UIPageControl!
    @IBOutlet weak var btnStart: UIButton!
    
    var frame: CGRect = CGRect(x:0, y:0, width:0, height:0)
    
    override func viewDidLoad() {
        super.viewDidLoad()
        if SharedPreference.getIsUserLogin(){
            if SharedPreference.getUserData().user_login_type == 1 {
                if SharedPreference.getUserData().mobile_verified{
                    Common.setupTabViewController(Vc: self)
                }else{
                    Common.PushMethod(VC: self, identifier: "VerifyViewController")
                }
            }else{
                if SharedPreference.getUserData().user_mobile_no != ""{
                    if SharedPreference.getUserData().mobile_verified{
                        Common.setupTabViewController(Vc: self)
                    }else{
                        Common.PushMethod(VC: self, identifier: "VerifyViewController")
                    }
                }else{
                    Common.PushMethod(VC: self, identifier: "ContactInfoViewController")
                }
            }
        }
        
        configurePageControl()
        scrollview.delegate = self
        self.SetupPages()
    }

    func SetupPages(){
        let screenSize = UIScreen.main.bounds
        let width = screenSize.width
        let hight = screenSize.height - 157
        
        for index in 0..<4 {
            frame.origin.x = screenSize.width * CGFloat(index) //self.scrollview.frame.size.width * CGFloat(index)
            //frame.size = self.scrollview.frame.size
            let myView = Bundle.main.loadNibNamed("Add", owner: nil, options: nil)![0] as! Add
            myView.frame = CGRect.init(x: frame.origin.x, y: 0, width: width, height: hight)
            myView.imgCover.image = UIImage(named: Array_File.Warrimage[index])
            myView.lblName.text = Array_File.WarrName[index]
            myView.lblHeading.text = Array_File.WarrHeading[index]
            myView.lblDesc.text = Array_File.WarrDesc[index]
            self.scrollview.addSubview(myView)
        }
        self.scrollview.contentSize = CGSize(width: width * 4,height: hight -  hight)
        pageControl.addTarget(self, action: #selector(self.changePage(sender:)), for: UIControlEvents.valueChanged)
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

    func configurePageControl() {
        // The total number of pages that are available is based on how many available colors we have.
        self.pageControl.numberOfPages = 4
        self.pageControl.currentPage = 0
        self.pageControl.tintColor = UIColor.red
        //self.pageControl.pageIndicatorTintColor = UIColor.white
        self.pageControl.currentPageIndicatorTintColor = Common.hexStringToUIColor("009EA5")
    }
    
    // MARK : TO CHANGE WHILE CLICKING ON PAGE CONTROL
    @objc func changePage(sender: AnyObject) -> () {
        let x = CGFloat(pageControl.currentPage) * scrollview.frame.size.width
        scrollview.setContentOffset(CGPoint(x:x, y:0), animated: true)
    }
    
    func scrollViewDidEndDecelerating(_ scrollView: UIScrollView) {
        let pageNumber = round(scrollView.contentOffset.x / scrollView.frame.size.width)
        pageControl.currentPage = Int(pageNumber)
    }
    
    @IBAction func GetStartedMethod(_ sender: Any) {
        Common.PushMethod(VC: self, identifier: "StartViewController")
    }

}
