//
//  ContactInfoViewController.swift
//  RoomOnShare
//
//  Created by mac on 01/08/18.
//  Copyright © 2018 WestCoast. All rights reserved.
//

import UIKit
import SDWebImage
import PhoneNumberKit

class ContactInfoViewController: UIViewController {

    @IBOutlet weak var imgProfile: UIImageView!
    @IBOutlet weak var txtFullName: UITextField!
    @IBOutlet weak var txtAge: UITextField!    
    @IBOutlet weak var txtCountry: UITextField!
    @IBOutlet weak var txtCountryCode: UITextField!
    @IBOutlet weak var txtMobileNumbe: UITextField!
    @IBOutlet weak var txtEmail: UITextField!    
    @IBOutlet weak var txtAdvice: UITextField!
    @IBOutlet weak var btnCheckBox: UIButton!
    @IBOutlet var btnGender: [UIButton]!    
    @IBOutlet weak var btnNext: UIButton!
    
    @IBOutlet weak var btnBack: UIButton!
    @IBOutlet weak var btnLogout: UIButton!
    
    
    var Pickimage : UIImage? = nil
    var checkvc = ""
    var checkGender : Int = 0
    var is_contact_private : Bool = false
    var Imagepicker = UIImagePickerController()
    var arrcountries : [country] = [country]()
   // var arrcountriesCode : [countryCode] = [countryCode]()
    var arrCountryName : [String] = []
    var arrSCountryCode : [String] = []
    var arrAge : [String] = []
     var valid_filed : Bool = false
    let phoneNumberKit = PhoneNumberKit()
    var dial_code = ""
    
    var strcountryname : String = ""
    var strcountrycode : String = ""
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.txtAge.delegate = self
        Imagepicker.delegate = self
        self.txtCountry.delegate = self
        self.txtCountryCode.delegate = self
        self.setdata()
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    override func viewWillAppear(_ animated: Bool) {
        self.navigationController?.isNavigationBarHidden = true
       // rdv_tabBarController?.setTabBarHidden(false, animated: true)
    
        self.txtFullName.delegate = self
        self.txtCountry.delegate = self
        self.txtCountryCode.delegate = self
        
        if self.checkvc == "Setting" {
            self.txtEmail.isEnabled = false
            self.txtMobileNumbe.isEnabled = false
            self.txtCountry.isEnabled = false
            self.txtCountryCode.isEnabled = false
            self.btnBack.isHidden = false
            self.btnLogout.isHidden = true
            self.btnNext.setTitle("Submit", for: .normal)
        }else{
            self.txtEmail.isEnabled = true
            self.txtMobileNumbe.isEnabled = true
            self.txtCountry.isEnabled = true
            self.txtCountryCode.isEnabled = true
            self.btnLogout.isHidden = false
            self.btnBack.isHidden = true
            self.btnNext.setTitle("Next", for: .normal)
        }

        self.arrcountries = countrydetail.readCountryJson()
        for item in self.arrcountries {
            self.arrCountryName.append(item.Country_name)
        }
        for item in self.arrcountries {
            let code = String("\((item.Country_dialcode)) \(item.Country_name)")
            self.arrSCountryCode.append(code)
        }
        for i in 18..<101{
            self.arrAge.append(String(i))
        }
    }    
    
    func setdata()  {
        self.txtFullName.text = SharedPreference.getUserData().user_fullname
        self.txtAge.text = String(SharedPreference.getUserData().user_age)
        self.txtCountry.text = SharedPreference.getUserData().user_country
        self.txtCountryCode.text = SharedPreference.getUserData().user_country_code
        self.txtMobileNumbe.text = SharedPreference.getUserData().user_mobile_no
        self.txtEmail.text = SharedPreference.getUserData().user_email
        self.txtAdvice.text = SharedPreference.getUserData().user_contactadvice
        let gender = SharedPreference.getUserData().user_gender
        let btn = UIButton()
        btn.tag = gender
        self.GenderMethod(btn)
        let img_pro = SharedPreference.getUserData().user_profilepic
        self.imgProfile.sd_setImage(with: URL(string: img_pro), placeholderImage: UIImage(named: "Banner"))
    }
    
    @IBAction func BackMethod(_ sender: Any) {
         Common.PopMethod(VC: self)
    }
    
    @IBAction func LogoutMethod(_ sender: Any) {
        SharedPreference.setIsUserLogin(false)
        let story = UIStoryboard(name: "Main", bundle: nil)
        let login = story.instantiateViewController(withIdentifier: "LoginViewController") as! LoginViewController
        self.navigationController?.pushViewController(login, animated: true)
    }
    
    @IBAction func NextMethod(_ sender: Any) {
       
        guard let FirstN = self.txtFullName.text, FirstN != ""  else {
            return Common.ShowAlert(Title: Common.Title, Message: "Please enter first name", VC: self)
        }
        
        if self.valid_filed {
        }else{
            return Common.ShowAlert(Title: Common.Title, Message: "Name field accepts only alphabatics", VC: self)
        }
        
        guard let age = self.txtEmail.text, age != ""  else {
            return Common.ShowAlert(Title: Common.Title, Message: "Please enter E-mail", VC: self)
        }
        
        guard Common.isValidEmail(testStr: self.txtEmail.text!)  else {
            return Common.ShowAlert(Title: Common.Title, Message: "Please enter valid E-mail", VC: self)
        }
        
        guard  self.checkGender != 0 else {
            return Common.ShowAlert(Title: Common.Title, Message: "Please select gender", VC: self)
        }
        
        guard let coun = self.txtCountry.text, coun != ""  else {
            return Common.ShowAlert(Title: Common.Title, Message: "Please select country", VC: self)
        }
        
        guard let CountryCode = self.txtCountryCode.text, CountryCode != ""  else {
            return Common.ShowAlert(Title: Common.Title, Message: "Please select country code", VC: self)
        }
        
        guard let Mobile = self.txtMobileNumbe.text, Mobile != ""  else {
            return Common.ShowAlert(Title: Common.Title, Message: "Please enter mobile number", VC: self)
        }
        
        let numbre = String("\(self.dial_code) \(self.txtMobileNumbe.text!)")
        do {
            let phoneNumberCustomDefaultRegion = try phoneNumberKit.parse(numbre, withRegion: self.strcountrycode, ignoreType: false)
            print(phoneNumberCustomDefaultRegion)
        }
        catch {
            return Common.ShowAlert(Title: Common.Title, Message: "Please Check mobile number", VC: self)
        }
        
//        guard Common.isValidateNumber(value: self.txtMobileNumbe.text!) else {
//            return Common.ShowAlert(Title: Common.Title, Message: "Please enter valid mobile number", VC: self)
//        }
        
        //guard let Advice = self.txtMobileNumbe.text, Advice != ""  else {
        //  return Common.ShowAlert(Title: Common.Title, Message: "Please enter mobile number", VC: self)
        //}
        
        self.AboutYouService()
        
    }
    
    @IBAction func GenderMethod(_ sender: UIButton) {
        let Selectedtag = sender.tag
        Common.CheckUnceck(arrButton: self.btnGender, Selected: Selectedtag)
        self.checkGender = Selectedtag
    }
    
    
    @IBAction func CkeckBoxMethod(_ sender: UIButton) {
        let tag = sender.tag
        if tag == 1{
            self.btnCheckBox.setImage(UIImage(named: "squareCheck"), for: .normal)
            self.is_contact_private = true
            self.btnCheckBox.tag = 2
        }else{
            self.btnCheckBox.setImage(UIImage(named: "square"), for: .normal)
            self.is_contact_private = false
            self.btnCheckBox.tag = 1
        }
    }
    
    @IBAction func EditImageMethod(_ sender: Any) {
        Common.ActionSheetForGallaryAndCamera(Picker: Imagepicker, VC: self)
    }
    
    
    func UploadProfileImage() {
        let img = self.imgProfile.image
        let imgData = UIImageJPEGRepresentation(img!, 0.5)!
        //let arrimage = [imgData as Data] as NSMutableArray
        
        CommunicationManager().getResponseForMultipartType(strUrl: Url_File.ProfilePic, parameters: nil, imagesData: [imgData], imageKey: "profile_pic",check :"Profile", arrphotoid: []) { ( result , data) in
            if(result == "success") {
                self.stopActivityIndicator()
                let dataDict = data as! NSDictionary
                print(dataDict)
                self.Pickimage = nil
                let dic = dataDict["response"] as! NSDictionary
                self.setProfilePic(dicdata: dic)
            }else if (result == "error") {
                let strMessage = (data as! NSDictionary)["message"] as! String
                Common.ShowAlert(Title: Common.Title, Message: strMessage, VC: self)
                self.stopActivityIndicator()
            }else if (result == "Network") {
                let strMessage = (data as! String)
                Common.ShowAlert(Title: Common.Title, Message: strMessage, VC: self)
                self.stopActivityIndicator()
            }
        }
    }
    
    func setProfilePic(dicdata : NSDictionary){
        SharedPreference.profilepic(pic: dicdata.value(forKey: "profile_pic") as! String)
        print(SharedPreference.getUserData().user_profilepic)
        let img_pro = SharedPreference.getUserData().user_profilepic 
        self.imgProfile.sd_setImage(with: URL(string: img_pro), placeholderImage: UIImage(named: "Banner"))
        self.GoNext()
    }
    
    func AboutYouService(){
        self.startActivityIndicator()
        
        let param = ["email"    : self.txtEmail.text!,
                     "user_id"  : SharedPreference.getUserData().user_id,
                     "name"     : self.txtFullName.text!,
                     "gender"   : self.checkGender,
                     "age"      : Int(self.txtAge.text!)!,
                     "country"  : self.strcountryname,
                     "country_code" : self.strcountrycode,
                     "mobile"   : self.txtMobileNumbe.text!,
                     "contact_advice" : self.txtAdvice.text!,
                     "is_contact_private" : self.is_contact_private] as [String : Any]
        
        CommunicationManager().getResponseForPost(strUrl: Url_File.AboutYou, parameters: param as NSDictionary) { ( result , data) in
            
            if(result == "success") {
                self.stopActivityIndicator()
                let dataDict = data as! NSDictionary
                print(dataDict)
                let dic = dataDict["response"] as! NSDictionary
                self.setUserdata(dicdata: dic)
            }else if (result == "error") {
                let strMessage = (data as! NSDictionary)["message"] as! String
                Common.ShowAlert(Title: Common.Title, Message: strMessage, VC: self)
                self.stopActivityIndicator()
            }else if (result == "Network") {
                let strMessage = (data as! String)
                Common.ShowAlert(Title: Common.Title, Message: strMessage, VC: self)
                self.stopActivityIndicator()
            }
        }
    }

    
    func setUserdata(dicdata : NSDictionary){

        SharedPreference.ContactUserData(name: self.txtFullName.text!, email: self.txtEmail.text!, age: Int(self.txtAge.text!)!, gender: self.checkGender, country: self.txtCountry.text!, country_code: self.txtCountryCode.text!, advice: self.txtAdvice.text!, mobile_V: dicdata.value(forKey: "mobile_verified") as! Bool, email_v: dicdata.value(forKey: "email_verified") as! Bool)

        if self.Pickimage != nil{
            self.UploadProfileImage()
        }else{
          self.GoNext()
        }
    }
    
    func GoNext() {
        print(SharedPreference.getUserData().mobile_verified)
        if SharedPreference.getUserData().mobile_verified{
            Common.setupTabViewController(Vc: self)
        }else if SharedPreference.getUserData().email_verified{
            Common.PushMethod(VC: self, identifier: "VerifyViewController")
        }
        else{
            Common.PushMethod(VC: self, identifier: "VerifyViewController")
        }
    }
}

extension ContactInfoViewController : UITextFieldDelegate, MyPickerViewProtocol {
    
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        let characterSet = CharacterSet.alphanumerics
        if string.rangeOfCharacter(from: characterSet.inverted) != nil {
            self.valid_filed = false
        }else{
            self.valid_filed = true
        }
        return true
    }
    
    func textFieldDidBeginEditing(_ textField: UITextField) {
        if textField == self.txtAge{
            let myPickerView =  MyPickerView.init(withDictionary: self.arrAge, Tag:3)
            myPickerView.delegate = myPickerView
            myPickerView.MyPickerDelegate = self
            self.txtAge.inputView = myPickerView
        }else if textField == self.txtCountry{
            let myPickerView =  MyPickerView.init(withDictionary: self.arrCountryName, Tag:1)
            myPickerView.delegate = myPickerView
            myPickerView.MyPickerDelegate = self
            self.txtCountry.inputView = myPickerView
        }else if textField == self.txtCountryCode{
            let myPickerView =  MyPickerView.init(withDictionary: self.arrSCountryCode, Tag:2)
            myPickerView.delegate = myPickerView
            myPickerView.MyPickerDelegate = self
            self.txtCountryCode.inputView = myPickerView
        }
    }
    
    func myPickerDidSelectRow(Index:Int?, Tag:Int) {
        if Tag == 1{
            let item = self.arrcountries[Index!]
            self.strcountryname = item.Country_code
            self.txtCountry.text = item.Country_name
            self.dial_code = item.Country_dialcode
            self.strcountrycode = item.Country_code
            self.txtCountryCode.text = self.arrSCountryCode[Index!]
        }else if Tag == 2{
            let item = self.arrcountries[Index!]
            self.strcountrycode = item.Country_code
            self.txtCountryCode.text = self.arrSCountryCode[Index!]
        }else{
            self.txtAge.text = self.arrAge[Index!]
        }
    }
}

extension ContactInfoViewController : UIImagePickerControllerDelegate, UINavigationControllerDelegate,CropViewControllerDelegate{
    func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [String : Any]) {
        let image = info[UIImagePickerControllerOriginalImage]
        self.Pickimage = image as? UIImage
        //self.imgProfile.image = image as? UIImage
        picker.dismiss(animated: true, completion: nil)
        self.OpenEditor()
    }
    
    func imagePickerControllerDidCancel(_ picker: UIImagePickerController) {
        picker.dismiss(animated: true, completion: nil)
    }
    
    func OpenEditor(){
        guard let image = self.Pickimage else {
            return
        }
        // Use view controller
        let controller = CropViewController()
        controller.delegate = self
        controller.image = image
        let navController = UINavigationController(rootViewController: controller)
        present(navController, animated: false, completion: nil)
    }
    
    // MARK: - CropView
    func cropViewController(_ controller: CropViewController, didFinishCroppingImage image: UIImage) {
    }
    
    func cropViewController(_ controller: CropViewController, didFinishCroppingImage image: UIImage, transform: CGAffineTransform, cropRect: CGRect) {
        controller.dismiss(animated: true, completion: nil)
        self.Pickimage = image
        self.imgProfile.image = image
    }
    
    func cropViewControllerDidCancel(_ controller: CropViewController) {
        controller.dismiss(animated: true, completion: nil)
    }
}


