//
//  ForgotPasswordViewController.swift
//  RoomOnShare
//
//  Created by mac on 06/08/18.
//  Copyright © 2018 WestCoast. All rights reserved.
//

import UIKit

class ForgotPasswordViewController: UIViewController {

    
    //ForgotPassword
    @IBOutlet weak var txtEmail: UITextField!
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

    @IBAction func SubmitMethod(_ sender: Any) {
        guard let email = self.txtEmail.text, email != ""  else {
            return Common.ShowAlert(Title: Common.Title, Message: "Please enter E-mail", VC: self)
        }
        
        guard Common.isValidEmail(testStr: self.txtEmail.text!)  else {
            return Common.ShowAlert(Title:Common.Title, Message: "Please enter valid E-mail", VC: self)
        }
    }
    
    @IBAction func BackMethod(_ sender: Any) {
        Common.PopMethod(VC: self)
    }
    
    func ForgotPasswordService(){
        self.startActivityIndicator()
        
        let param = ["email": self.txtEmail.text!]
        
        CommunicationManager().getResponseForPost(strUrl: Url_File.ForgotPassword, parameters: param as NSDictionary) { ( result , data) in            
            if(result == "success") {
                let dataDict = data as! NSDictionary
                let strMessage = dataDict["message"] as! String
                self.stopActivityIndicator()
                Common.ShowAlert(Title: Common.Title, Message: strMessage, VC: self)
                //self.GoNext()                
            }else if (result == "error") {
                let strMessage = (data as! NSDictionary)["message"] as! String
                Common.ShowAlert(Title: Common.Title, Message: strMessage, VC: self)
                self.stopActivityIndicator()
            }else if (result == "Network") {
                let strMessage = (data as! String)
                Common.ShowAlert(Title: Common.Title, Message: strMessage, VC: self)
                self.stopActivityIndicator()
            }
        }
    }
    
    func GoNext() {
        
    }
}
