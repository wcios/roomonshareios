//
//  VerifyViewController.swift
//  RoomOnShare
//
//  Created by mac on 11/08/18.
//  Copyright © 2018 WestCoast. All rights reserved.
//

import UIKit
import PhoneNumberKit

class VerifyViewController: UIViewController {

    @IBOutlet weak var txtOTP: UITextField!
    @IBOutlet weak var txtMobileNumber: UITextField!
    @IBOutlet weak var viewEdit: UIView!    
    @IBOutlet weak var txtCountryCode: UITextField!
    @IBOutlet weak var txtEditMobilenumber: ACFloatingTextfield!
    
    var userdata = SharedPreference.getUserData()
    var arrcountries : [country] = [country]()
    var arrSCountryCode : [String] = []
    var update_country_code = ""
    var strcountrycode : String = ""
    var Oldmobile : String = ""
    
    let phoneNumberKit = PhoneNumberKit()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.viewEdit.isHidden = true
        self.txtCountryCode.delegate = self
        self.Oldmobile = self.userdata.user_mobile_no
       
        self.txtEditMobilenumber.text = self.userdata.user_mobile_no
        self.arrcountries = countrydetail.readCountryJson()
        for item in self.arrcountries {
            if(self.userdata.user_country_code == item.Country_code){
                 self.txtMobileNumber.text = String("\(item.Country_dialcode) \(self.userdata.user_mobile_no)")
                self.txtCountryCode.text = "(\(item.Country_dialcode)) \(item.Country_name)"
            }
            let code = String("\((item.Country_dialcode)) \(item.Country_name)")
            self.arrSCountryCode.append(code)
        }
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    @IBAction func BackMethod(_ sender: Any) {
        //Common.PopMethod(VC: self)
     }
    
    @IBAction func VerifyMethod(_ sender: Any) {
        guard let email = self.txtOTP.text, email != ""else {
            return Common.ShowAlert(Title: Common.Title, Message: "Please enter OTP", VC: self)
        }
        self.VerifyOtpService()
    }
    
    @IBAction func ResendMethod(_ sender: Any) {
        self.ResendOtpService()
    }
    
    
    @IBAction func EditNumberMethod(_ sender: Any) {
        self.viewEdit.isHidden = false
    }
    
    @IBAction func LogoutMethod(_ sender: Any) {
        SharedPreference.setIsUserLogin(false)
        let story = UIStoryboard(name: "Main", bundle: nil)
        let login = story.instantiateViewController(withIdentifier: "LoginViewController") as! LoginViewController
        self.navigationController?.pushViewController(login, animated: true)
    }
    
    @IBAction func SaveChangesMethod(_ sender: Any) {
        SharedPreference.SetMobileNumber(mob: self.txtEditMobilenumber.text!)
        guard let code = self.txtCountryCode.text, code != "" else {
            return  Common.ShowAlert(Title: Common.Title, Message: "Please enter country code", VC: self)
        }
        guard let number = self.txtEditMobilenumber.text, number != "" else {
            return  Common.ShowAlert(Title: Common.Title, Message: "Please enter mobile number", VC: self)
        }
        
        print(self.update_country_code)
        
        let numbre = String("\(self.update_country_code) \(self.txtEditMobilenumber.text!)")
        do {
            let phoneNumberCustomDefaultRegion = try phoneNumberKit.parse(numbre, withRegion: self.strcountrycode, ignoreType: false)
            print(phoneNumberCustomDefaultRegion)
        }
        catch {
            return Common.ShowAlert(Title: Common.Title, Message: "Please Check mobile number", VC: self)
        }
        
        if self.Oldmobile == self.txtEditMobilenumber.text{
            self.viewEdit.isHidden = true
            self.txtMobileNumber.text = String("\(self.update_country_code) \(self.txtEditMobilenumber.text!)")
        }else{
            
            self.UpdateNumberService()
        }
    }
    
    /////////// verify otp ///////////
    func VerifyOtpService(){
        self.startActivityIndicator()
        
        let param = ["mobile": SharedPreference.getUserData().user_mobile_no,
                     "otp"   : self.txtOTP.text! ]
        
        CommunicationManager().getResponseForPost(strUrl: Url_File.VerifyOtp, parameters: param as NSDictionary) { ( result , data) in
            if(result == "success") {
                let dataDict = data as! NSDictionary
                self.GoNext()
                self.stopActivityIndicator()
            }else if (result == "error") {
                let strMessage = (data as! NSDictionary)["message"] as! String
                Common.ShowAlert(Title: Common.Title, Message: strMessage, VC: self)
                self.stopActivityIndicator()
            }else if (result == "Network") {
                let strMessage = (data as! String)
                Common.ShowAlert(Title: Common.Title, Message: strMessage, VC: self)
                self.stopActivityIndicator()
            }
        }
    }
    
    func GoNext() {
        SharedPreference.MobileVerify(mob: true)
        print(SharedPreference.getUserData().mobile_verified)
        Common.PushMethod(VC: self, identifier: "WelcomeViewController")        
    }
    
    /////////// resend otp ///////////
    func ResendOtpService(){
        self.startActivityIndicator()
        
        let param = ["mobile": self.userdata.user_mobile_no]
        
        CommunicationManager().getResponseForPost(strUrl: Url_File.ResendOtp, parameters: param as NSDictionary) { ( result , data) in
            
            if(result == "success") {                
                let dataDict = data as! NSDictionary
                print(dataDict)
                let strMessage = dataDict["message"] as! String
                Common.ShowAlert(Title:Common.Title, Message: strMessage, VC: self)
                self.stopActivityIndicator()
            }else if (result == "error") {
                let strMessage = (data as! NSDictionary)["message"] as! String
                Common.ShowAlert(Title: Common.Title, Message: strMessage, VC: self)
                self.stopActivityIndicator()
            }else if (result == "Network") {
                let strMessage = (data as! String)
                Common.ShowAlert(Title: Common.Title, Message: strMessage, VC: self)
                self.stopActivityIndicator()
            }
        }
    }
    
    
    /////////// update mobile number ///////////
    func UpdateNumberService(){
        self.startActivityIndicator()
        print(SharedPreference.getUserData().user_mobile_no)
        let param = ["mobile": self.txtEditMobilenumber.text!,
                     "country_code": self.update_country_code]
        
        CommunicationManager().getResponseForPost(strUrl: Url_File.UpdateMobileNumber, parameters: param as NSDictionary) { ( result , data) in
            if(result == "success") {
                let dataDict = data as! NSDictionary
                SharedPreference.SetMobileNumber(mob: self.txtEditMobilenumber.text!)
                self.txtMobileNumber.text = "\(self.update_country_code) \(self.txtEditMobilenumber.text!)"
                self.viewEdit.isHidden = true
                self.stopActivityIndicator()
            }else if (result == "error") {
                let strMessage = (data as! NSDictionary)["message"] as! String
                Common.ShowAlert(Title: Common.Title, Message: strMessage, VC: self)
                self.stopActivityIndicator()
            }else if (result == "Network") {
                let strMessage = (data as! String)
                Common.ShowAlert(Title: Common.Title, Message: strMessage, VC: self)
                self.stopActivityIndicator()
            }
        }
    }
    
}


extension VerifyViewController : UITextFieldDelegate, MyPickerViewProtocol {
    func textFieldDidBeginEditing(_ textField: UITextField) {
        if textField == self.txtCountryCode{
            let myPickerView =  MyPickerView.init(withDictionary: self.arrSCountryCode, Tag:1)
            myPickerView.delegate = myPickerView
            myPickerView.MyPickerDelegate = self
            self.txtCountryCode.inputView = myPickerView
        }
    }
    
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        if textField == txtOTP {
            let maxLength = 4
            let currentString: NSString = textField.text! as NSString
            let newString: NSString =
                currentString.replacingCharacters(in: range, with: string) as NSString
            return newString.length <= maxLength
        }
        return true
    }
    
   
    
    
    func myPickerDidSelectRow(Index:Int?, Tag:Int) {
        if Tag == 1{
             let coun = self.arrcountries[Index!]
            self.update_country_code = coun.Country_dialcode
            self.strcountrycode = coun.Country_code
            self.txtCountryCode.text = self.arrSCountryCode[Index!] //coun.Country_dialcode
        }
    }
}
