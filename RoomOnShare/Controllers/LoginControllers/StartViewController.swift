//
//  StartViewController.swift
//  RoomOnShare
//
//  Created by mac on 31/07/18.
//  Copyright © 2018 WestCoast. All rights reserved.
//

import UIKit
import FBSDKCoreKit
import FBSDKLoginKit
import GoogleSignIn
import LinkedinSwift

class StartViewController: UIViewController {
    
    @IBOutlet weak var btnlogin: UIButton!

    var facebook_Id : String = ""
    var google_Id : String = ""
    var linkedin_Id : String = ""
    var login_type : String = ""
    var social_email : String = ""
    var social_name : String = ""
    var social_id : String = ""

    let linkedinHelper = LinkedinSwiftHelper(configuration: LinkedinSwiftConfiguration(clientId: "81zhiuczjmy4v9", clientSecret: "5nKx1TeI0JEYjsmF", state: "linkedin\(Int(Date().timeIntervalSince1970))", permissions: ["r_basicprofile", "r_emailaddress"], redirectUrl: "https://www.linkedin.com/oauth/v2/accessToken"))

    var dict : [String:AnyObject] = [:]
    
    override func viewDidLoad() {
        super.viewDidLoad()
        GIDSignIn.sharedInstance().uiDelegate = self
        GIDSignIn.sharedInstance().delegate = self
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    //////// Signup /////////
    @IBAction func EmailMethod(_ sender: Any) {
        Common.PushMethod(VC: self, identifier: "SignUpViewController")
    }
    
    //////// Facebook /////////
    @IBAction func FacebookMethod(_ sender: Any) {
//        SocialClass.Facebook(self.btnlogin as! FBSDKLoginButton, Vc: self) { (result) in
//            print(result)
//
        let fbLoginManager : FBSDKLoginManager = FBSDKLoginManager()
        fbLoginManager.logIn(withReadPermissions: ["email"], from: self) {
            (result, error) in
            if (error == nil) {
                let fbloginresult : FBSDKLoginManagerLoginResult = result!
                if fbloginresult.grantedPermissions != nil {
                    if (fbloginresult.grantedPermissions.contains("email")){
                        self.getFBUserData()
                        //let myFbToken = (FBSDKAccessToken.current().tokenString) as! String
                        //print(myFbToken)
                        //print(FBSDKAccessToken.current().tokenString)
                        fbLoginManager.logOut()
                    }
                }
            }
        }
    }
    
    func getFBUserData() {
        if ((FBSDKAccessToken.current()) != nil) {
            FBSDKGraphRequest(graphPath: "me", parameters: ["fields": "id, name, first_name, last_name, picture.type(large), email"]).start(completionHandler: { (connection, result,error) -> Void in
                if (error == nil){
                    print(result!)
                    let dict = result as! NSDictionary
                    self.login_type = "3"
                    self.facebook_Id = dict.value(forKey: "id") as? String ?? ""
                    self.google_Id = ""
                    self.linkedin_Id = ""
                    self.social_id = dict.value(forKey: "id") as? String ?? ""
                    self.social_email = dict.value(forKey: "email") as? String ?? ""
                    self.social_name = dict.value(forKey: "name") as? String ?? ""
                    
                   self.SocilaService()
                }
            })
        }
    }
    
    //////// Google /////////
    @IBAction func GoogleMethod(_ sender: Any) {
        GIDSignIn.sharedInstance().signIn()
        //Common.setupTabViewController(Vc: self)
    }
    
    //////// LinkedIn /////////
    @IBAction func LinkedInMethod(_ sender: Any) {
//        SocialClass.LinkedIn(vc: self) { (result) in
//            print(result)
//        }
                linkedinHelper.authorizeSuccess({ [unowned self] (lsToken) -> Void in
                    print("token----\(lsToken)")
                    // let strToken = lsToken.accessToken
                    self.linkedinHelper.requestURL("https://api.linkedin.com/v1/people/~:(id,first-name,last-name,email-address,picture-url,picture-urls::(original),positions,date-of-birth,phone-numbers,location)?format=json", requestType: LinkedinSwiftRequestGet, success: { (response) -> Void in
                        print("Request success with response: \(response)")
                        DispatchQueue.main.async {
                            
                            let dict = response.jsonObject! as NSDictionary
                            self.login_type = "4"
                            self.facebook_Id = ""
                            self.google_Id = ""
                            self.linkedin_Id = dict.value(forKey: "id") as? String ?? ""
                            
                             let fullname = String("\(dict.value(forKey: "firstName") as AnyObject) \(dict.value(forKey: "lastName") as AnyObject)")
                            
                            self.social_id = dict.value(forKey: "id") as? String ?? ""
                            self.social_email = dict.value(forKey: "emailAddress") as? String ?? ""
                            self.social_name = fullname
                            self.SocilaService()
                        }
                    }) { (error) -> Void in
                        print("Request success with response: \(error)") //[unowned self]
                    }
                    
                    }, error: {  (error) -> Void in
                        //self.writeConsoleLine("Encounter error: \(error.localizedDescription)") [unowned self]
                    }, cancel: {  () -> Void in
                        // self.writeConsoleLine("User Cancelled!") //[unowned self]
                })
        
        
    }
    
    func SocilaService(){
        self.startActivityIndicator()
        
        //let Url = "http://18.223.28.186/api/register"
        let param = ["email"      : self.social_email,
                     "name"       : self.social_name,
                     "mobile"     : "",
                     "fcm_token"  : "asjkdskdlskdlkd",
                     "login_type" : self.login_type,
                     "facebook_id": self.facebook_Id,
                     "google_id"  : self.google_Id,
                     "linkedin_id": self.linkedin_Id ]
        
        print(param)                
        CommunicationManager().getResponseForPost(strUrl: Url_File.SignUp, parameters: param as NSDictionary) { ( result , data) in            
            if(result == "success") {
                let dataDict = data as! NSDictionary
                print(dataDict)
                let dic = dataDict["response"] as! NSDictionary
                print(dic)
                SharedPreference.setIsUserLogin(true) /// check
                let userdata = UserData.init(withDictionary: dic as! [String : Any])
                SharedPreference.saveUserData(userdata)
                self.stopActivityIndicator()
                self.GoNext(dic: dic)
            }else if (result == "error") {
                let strMessage = (data as! NSDictionary)["message"] as! String
                Common.ShowAlert(Title: Common.Title, Message: strMessage, VC: self)
                self.stopActivityIndicator()
            }else if (result == "Network") {
                let strMessage = (data as! String)
                Common.ShowAlert(Title: Common.Title, Message: strMessage, VC: self)
                self.stopActivityIndicator()
            }
        }
    }
    
    
    func GoNext(dic : NSDictionary) {
       if SharedPreference.getUserData().user_mobile_no != ""{
            if SharedPreference.getUserData().mobile_verified{
                Common.setupTabViewController(Vc: self)
            }else{
                Common.PushMethod(VC: self, identifier: "VerifyViewController")
            }
        }else{
            Common.PushMethod(VC: self, identifier: "ContactInfoViewController")
        }
        
        //        self.dict.updateValue(self.social_name as AnyObject, forKey: "FullName")
        //        self.dict.updateValue(self.social_email as AnyObject, forKey: "Email")
        //        self.dict.updateValue(self.social_id as AnyObject, forKey: "Id")
        //        print(self.dict)
//        let storyboard = UIStoryboard(name: "Main", bundle: nil)
//        let vc = storyboard.instantiateViewController(withIdentifier: "ContactInfoViewController") as! ContactInfoViewController
//        vc.dict = self.dict as NSDictionary
//        self.navigationController?.pushViewController(vc, animated: true)
    }
    
    //////// Login /////////
    @IBAction func LoginMethod(_ sender: Any) {
        Common.PushMethod(VC: self, identifier: "LoginViewController")
    }
    
}

extension StartViewController :  GIDSignInDelegate, GIDSignInUIDelegate {
    func sign(_ signIn: GIDSignIn!, didSignInFor user: GIDGoogleUser!, withError error: Error!) {
        if (error == nil) {
            let userId = user.userID                  // For client-side use only!
            let idToken = user.authentication.idToken // Safe to send to the server
            let fullName = user.profile.name
            let givenName = user.profile.givenName
            let familyName = user.profile.familyName
            let email = user.profile.email
            
            self.login_type = "2"
            self.facebook_Id = ""
            self.google_Id = userId!
            self.linkedin_Id = ""
            self.social_id = fullName!
            self.social_email = email!
            self.social_name = userId!
            self.SocilaService()
            print("\(userId!), \(idToken!), \(fullName!), \(givenName!), \(familyName!), \(email!)")
        } else {
            print("\(error.localizedDescription)")
        }
    }
    
    
    // Present a view that prompts the user to sign in with Google
    func sign(_ signIn: GIDSignIn!,
              present viewController: UIViewController!) {
        self.present(viewController, animated: true, completion: nil)
    }
    
    // Dismiss the "Sign in with Google" view
    func sign(_ signIn: GIDSignIn!,
              dismiss viewController: UIViewController!) {
        self.dismiss(animated: true, completion: nil)
    }
}

//print(Userdata)

//        {
//            email = "wcinfotechtest@gmail.com";
//            "first_name" = Westcoast;
//            id = 131531871106201;
//            "last_name" = Infotech;
//            name = "Westcoast Infotech";
//            picture =     {
//                data =         {
//                    height = 200;
//                    "is_silhouette" = 1;
//                    url = "https://platform-lookaside.fbsbx.com/platform/profilepic/?asid=131531871106201&height=200&width=200&ext=1535889893&hash=AeQCAe1zEltuLyzo";
//                    width = 200;
//                };
//            };
//            }



//<LSResponse - data: {
//    emailAddress = "wcinfotechtest@gmail.com";
//    firstName = WestCoast;
//    id = "Mv-sWvHmCR";
//    lastName = Infotech;
//    location =     {
//        country =         {
//            code = in;
//        };
//        name = "Indore Area, India";
//    };
//    positions =     {
//        "_total" = 1;
//        values =         (
//            {
//                company =                 {
//                    name = westcoastinfotech;
//                };
//                id = 1342227772;
//                isCurrent = 1;
//                location =                 {
//                };
//                title = "iOS Developer";
//            }
//        );
//    };
//}, status code: 200>


 //  let linkedinHelper = LinkedinSwiftHelper(configuration: LinkedinSwiftConfiguration(clientId: "81zhiuczjmy4v9", clientSecret: "5nKx1TeI0JEYjsmF", state: "linkedin\(Int(Date().timeIntervalSince1970))", permissions: ["r_basicprofile", "r_emailaddress"], redirectUrl: "https://www.linkedin.com/oauth/v2/accessToken"))

//        linkedinHelper.authorizeSuccess({ [unowned self] (lsToken) -> Void in
//            print("token----\(lsToken)")
//            // let strToken = lsToken.accessToken
//            }, error: { [unowned self] (error) -> Void in
//                //self.writeConsoleLine("Encounter error: \(error.localizedDescription)")
//            }, cancel: { [unowned self] () -> Void in
//                // self.writeConsoleLine("User Cancelled!")
//        })
//
//        linkedinHelper.requestURL("https://api.linkedin.com/v1/people/~:(id,first-name,last-name,email-address,picture-url,picture-urls::(original),positions,date-of-birth,phone-numbers,location)?format=json", requestType: LinkedinSwiftRequestGet, success: { (response) -> Void in
//            print("Request success with response: \(response)")
//            DispatchQueue.main.async {
//                //  let login = self.storyboard?.instantiateViewController(withIdentifier: "HOMEViewController") as! HOMEViewController
//                //self.present(login, animated: true, completion: nil)
//            }
//        }) { [unowned self] (error) -> Void in
//        }

