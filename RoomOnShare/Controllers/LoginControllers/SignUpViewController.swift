//
//  SignUpViewController.swift
//  RoomOnShare
//
//  Created by mac on 03/08/18.
//  Copyright © 2018 WestCoast. All rights reserved.
//

import UIKit
import PhoneNumberKit

class SignUpViewController: UIViewController {
    
    @IBOutlet weak var txtFullName: UITextField!
    @IBOutlet weak var txtAge: UITextField!
    @IBOutlet weak var txtEmail: UITextField!
    @IBOutlet weak var txtPassword: UITextField!
    @IBOutlet weak var txtCountryCode: UITextField!
    @IBOutlet weak var txtMobileNumber: UITextField!
    @IBOutlet weak var txtCountry: UITextField!    
    @IBOutlet var Gender: [UIButton]!
    @IBOutlet weak var btnShowPassword: UIButton!
    @IBOutlet weak var btnPrivacyPolicy: UIButton!
    
    var arrcountries : [country] = [country]()
    var arrCountryName : [String] = []
    var arrSCountryCode : [String] = []
    var arrAge : [String] = []
    var checkGender : Int = 0
    var strGender : String = ""
    var strcountryname : String = ""
    var strcountrycode : String = ""
    var valid_filed : Bool = false
    
    var dial_code = ""
    let phoneNumberKit = PhoneNumberKit()
    
    override func viewDidLoad() {
        super.viewDidLoad()
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        self.txtFullName.delegate = self
        self.txtAge.delegate = self
        self.txtCountry.delegate = self
        self.txtCountryCode.delegate = self
        self.arrcountries = countrydetail.readCountryJson()
        for item in self.arrcountries {
            self.arrCountryName.append(item.Country_name)
        }
        for item in self.arrcountries {
            let code = String("(\(item.Country_dialcode)) \(item.Country_name)")
            self.arrSCountryCode.append(code)
        }
        for i in 18..<101{
            self.arrAge.append(String(i))
        }
    }
    
    //////// Gender /////////
    @IBAction func GenderMethod(_ sender: UIButton) {
        let Selectedtag = sender.tag
        Common.CheckUnceck(arrButton: self.Gender, Selected: Selectedtag)
        self.checkGender = Selectedtag
    }
    
    @IBAction func ShowPasswordMethod(_ sender: Any) {
        if self.txtPassword.isSecureTextEntry {
            self.txtPassword.isSecureTextEntry = false
            //self.btnShowPassword.setImage(" ", for: .normal)
        }else{
            self.txtPassword.isSecureTextEntry = true
            //self.btnShowPassword.setImage(" ", for: .normal)
        }
    }
    
    //////// Signup /////////
    @IBAction func SignUpMethod(_ sender: Any) {
        
//        guard let Fname = self.txtFullName.text, Fname != ""  else {
//            return Common.ShowAlert(Title: Common.Title, Message: "Please enter fullname", VC: self)
//        }
//        guard let age = self.txtAge.text, age != ""  else {
//            return Common.ShowAlert(Title: Common.Title, Message: "Please enter age", VC: self)
//        }
//
//        guard  self.checkGender != 0 else {
//            return Common.ShowAlert(Title: Common.Title, Message: "Please select gender", VC: self)
//        }
        
        guard let Country = self.txtCountry.text, Country != ""  else {
            return Common.ShowAlert(Title: Common.Title, Message: "Please enter country name", VC: self)
        }

        guard let CountryCode = self.txtCountryCode.text, CountryCode != ""  else {
            return Common.ShowAlert(Title: Common.Title, Message: "Please select country code", VC: self)
        }

        guard let Mobile = self.txtMobileNumber.text, Mobile != ""  else {
            return Common.ShowAlert(Title: Common.Title, Message: "Please enter mobile number", VC: self)
        }

        let numbre = String("\(self.dial_code) \(self.txtMobileNumber.text!)")
        do {
            let phoneNumberCustomDefaultRegion = try phoneNumberKit.parse(numbre, withRegion: self.strcountrycode, ignoreType: false)
            print(phoneNumberCustomDefaultRegion)
        }
        catch {
            return Common.ShowAlert(Title: Common.Title, Message: "Please Check mobile number", VC: self)
        }
        
//        guard Common.isValidateNumber(value: self.txtMobileNumber.text!) else {
//            return Common.ShowAlert(Title: "Alert", Message: "Please enter valid mobile number", VC: self)
//        }
        
//        guard let email = self.txtEmail.text, email != ""  else {
//            return Common.ShowAlert(Title: Common.Title, Message: "Please enter E-mail", VC: self)
//        }
//        
//        guard Common.isValidEmail(testStr: self.txtEmail.text!)  else {
//            return Common.ShowAlert(Title: Common.Title, Message: "Please enter valid E-mail", VC: self)
//        }
//
//        guard let password = self.txtPassword.text, password != ""  else {
//            return Common.ShowAlert(Title: Common.Title, Message: "Please enter password", VC: self)
//        }
//
//        guard Common.isCheckPasswordLength(password: self.txtPassword.text!)  else {
//            return Common.ShowAlert(Title: Common.Title, Message: "Password must be minimum 6 character", VC: self)
//        }
//
//        guard self.btnPrivacyPolicy.tag == 2 else{
//            return Common.ShowAlert(Title: Common.Title, Message: "Please select Privacy policy", VC: self)
//        }
        
       // self.SignupService()
    }
    
    
    func SignupService(){
        self.startActivityIndicator()
        
        let param = ["email"    : self.txtEmail.text!,
                     "password" : self.txtPassword.text!,
                     "name"     : self.txtFullName.text!,
                     "gender"   : self.checkGender,
                     "age"      : Int(self.txtAge.text!)!,
                     "country"  : self.strcountryname,
                     "country_code" : self.strcountrycode,
                     "mobile"   : self.txtMobileNumber.text!,
                     "fcm_token" : "asjkdskdlskdlkd",
                     "login_type": "1"] as [String : Any]
        print(Url_File.SignUp)
        
        CommunicationManager().getResponseForPost(strUrl: Url_File.SignUp, parameters: param as NSDictionary) { ( result , data) in
            
            if(result == "success") {
                let dataDict = data as! NSDictionary
                print(dataDict)
                let dic = dataDict["response"] as! NSDictionary
                SharedPreference.setIsUserLogin(true)
                let userdata = UserData.init(withDictionary: dic as! [String : Any])
                SharedPreference.saveUserData(userdata)
                print(userdata)                
                self.GoNext()
                self.stopActivityIndicator()
            }else if (result == "error") {
                let strMessage = (data as! NSDictionary)["message"] as! String
                Common.ShowAlert(Title: Common.Title, Message: strMessage, VC: self)
                self.stopActivityIndicator()
            }else if (result == "Network") {
                let strMessage = (data as! String)
                Common.ShowAlert(Title: Common.Title, Message: strMessage, VC: self)
                self.stopActivityIndicator()
            }
        }
    }
    
    func GoNext() {
        Common.PushMethod(VC: self, identifier: "VerifyViewController")
    }
    
    //////// Login /////////
    @IBAction func Login(_ sender: Any) {
        Common.PushMethod(VC: self, identifier: "LoginViewController")
    }
    
    @IBAction func PrivacyPolicyMethod(_ sender: UIButton) {
        let tag = sender.tag
        if tag == 1{
            self.btnPrivacyPolicy.setImage(UIImage(named: "squareCheck"), for: .normal)
            self.btnPrivacyPolicy.tag = 2
        }else{
            self.btnPrivacyPolicy.setImage(UIImage(named: "square"), for: .normal)
            self.btnPrivacyPolicy.tag = 1
        }
    }
}

extension SignUpViewController : UITextFieldDelegate, MyPickerViewProtocol{
    
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        if textField == txtFullName {
            let allowedCharacters = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz "
            let allowedCharacterSet = CharacterSet(charactersIn: allowedCharacters)
            let typedCharacterSet = CharacterSet(charactersIn: string)
            let alphabet = allowedCharacterSet.isSuperset(of: typedCharacterSet)
            return alphabet && txtFullName.text!.count <= 22
        }
        else{
            let characterSet = CharacterSet.alphanumerics
            if string.rangeOfCharacter(from: characterSet.inverted) != nil {
                self.valid_filed = false
            }else{
                self.valid_filed = true
            }
            return true
        }
    }
    
    func textFieldDidBeginEditing(_ textField: UITextField) {
        if textField == self.txtAge{
            let myPickerView =  MyPickerView.init(withDictionary: self.arrAge, Tag:3)
            myPickerView.delegate = myPickerView
            myPickerView.MyPickerDelegate = self
            self.txtAge.inputView = myPickerView
        }else if textField == self.txtCountry{
            let myPickerView =  MyPickerView.init(withDictionary: self.arrCountryName, Tag:1)
            myPickerView.delegate = myPickerView
            myPickerView.MyPickerDelegate = self
            self.txtCountry.inputView = myPickerView
        }else if textField == self.txtCountryCode{
            let myPickerView =  MyPickerView.init(withDictionary: self.arrSCountryCode, Tag:2)
            myPickerView.delegate = myPickerView
            myPickerView.MyPickerDelegate = self
            self.txtCountryCode.inputView = myPickerView
        }
    }
    
    func myPickerDidSelectRow(Index:Int?, Tag:Int) {
        if Tag == 1{
            let item = self.arrcountries[Index!]
            self.strcountryname = item.Country_code
            self.txtCountry.text = item.Country_name
            self.dial_code = item.Country_dialcode
            self.strcountrycode = item.Country_code
            self.txtCountryCode.text = self.arrSCountryCode[Index!]
        }else if Tag == 2{
            let item = self.arrcountries[Index!]
            self.strcountrycode = item.Country_code
            self.txtCountryCode.text = self.arrSCountryCode[Index!]
            //self.txtCountryCode.text = item.Country_dialcode
        }else{
            self.txtAge.text = self.arrAge[Index!]
        }
    }
}


