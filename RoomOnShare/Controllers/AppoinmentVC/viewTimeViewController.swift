import UIKit

class viewTimeViewController: UIViewController{
   
    
    @IBOutlet weak var viewNoSlots : UIView!
    @IBOutlet weak var viewSlots : UIView!
    @IBOutlet weak var viewConfirmPopUp : UIView!
    
    @IBOutlet weak var viewDate1 : UIView!
    @IBOutlet weak var lblDate1 : UILabel!
    @IBOutlet weak var lblTime1 : UILabel!
    @IBOutlet weak var lblProposedBy1 : UILabel!
    @IBOutlet weak var btn1 : UIButton!
    
    @IBOutlet weak var viewDate2 : UIView!
    @IBOutlet weak var lblDate2 : UILabel!
    @IBOutlet weak var lblTime2 : UILabel!
    @IBOutlet weak var lblProposedBy2 : UILabel!
    @IBOutlet weak var btn2 : UIButton!
    
    @IBOutlet weak var viewDate3 : UIView!
    @IBOutlet weak var lblDate3 : UILabel!
    @IBOutlet weak var lblTime3 : UILabel!
    @IBOutlet weak var lblProposedBy3 : UILabel!
    @IBOutlet weak var btn3 : UIButton!
    @IBOutlet weak var lblConfirm : UILabel!
    @IBOutlet weak var btnConfirm : UIButton!
    
    var arrAppoinmentSlot : [NSDictionary] = []
    var arrOfferinghomeData : NSDictionary = [:]
    var appo_id : Int = 0
    var looking_id = 0
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.viewConfirmPopUp.isHidden=true
    }
 
    override func viewWillAppear(_ animated: Bool) {
        self.navigationController?.isNavigationBarHidden = true
        //rdv_tabBarController?.setTabBarHidden(false, animated: true)
        
        if let appoinmtId = userDef.value(forKey: "appoinment_id") as? Int {
            self.appo_id=appoinmtId
        }
        
        if(self.appo_id==0){
            self.viewSlots.isHidden=true
            self.viewNoSlots.isHidden=false
        }
        else{
            self.call_GetAppoinmentDetailApi(appoinmnet_id: self.appo_id)
        }
    }
    
    
    @IBAction func clickOnProposedNewBtn(_ sender : UIButton){
        if(self.appo_id==0){
            let story = AppStoryboard.Home.instance
            let vc = story.instantiateViewController(withIdentifier: "ViewSettingUpVC")as! ViewSettingUpVC
            vc.isEditTrue="save"
            vc.lookingId = self.looking_id
            self.navigationController?.pushViewController(vc, animated: true)
        }else{
            
            let story = AppStoryboard.Home.instance
            let vc = story.instantiateViewController(withIdentifier: "ViewSettingUpVC")as! ViewSettingUpVC
            vc.isEditTrue="update"
            vc.lookingId = self.looking_id
            vc.appoinmntSlotArr=arrAppoinmentSlot
            self.navigationController?.pushViewController(vc, animated: true)
            
        }
        
    }
    
    func call_GetAppoinmentDetailApi(appoinmnet_id : Int){
        
        self.startActivityIndicator()
        let param = ["appoinment_id" : appoinmnet_id
            ] as NSDictionary
    
           print(param)
        
        CommunicationManager().getResponseForPost(strUrl: Url_File.url_appoinment_detail, parameters: param) { ( result , data) in
            
            if(result == "success") {
                let dataDict = data as! NSDictionary
                print(dataDict)
                let responseData = dataDict["response"] as! NSDictionary
                print(responseData)
                
                let arrOffer = responseData["offering_home"] as! NSDictionary
                self.arrOfferinghomeData=arrOffer
                let arr = responseData["appoinment_slot"] as! [NSDictionary]
                self.arrAppoinmentSlot=arr
                self.handleAppoinmentSlotData(arrData: arr)
                self.stopActivityIndicator()
                
            }else if (result == "error") {
                let strMessage = (data as! NSDictionary)["message"] as! String
                Common.ShowAlert(Title: Common.Title, Message: strMessage, VC: self)
                self.navigationController?.popViewController(animated: true)
                self.stopActivityIndicator()
            }else if (result == "Network") {
                let strMessage = (data as! String)
                Common.ShowAlert(Title: Common.Title, Message: strMessage, VC: self)
                self.stopActivityIndicator()
            }
        }
    }
    
    
    func handleAppoinmentSlotData(arrData : [NSDictionary]){
        
        self.viewSlots.isHidden=false
        self.viewNoSlots.isHidden=true
        print(arrData)
        for i in 0...arrData.count-1{
            let dict = arrData[i]
            let confirmed = dict["confirmed"] as! Int
            if(i==0){
                if(confirmed==1){
                    self.viewDate1.backgroundColor=UIColor.init(red: 54/255, green: 163/255, blue: 42/255, alpha: 1.0)
                }
                else{
                    self.viewDate1.backgroundColor=UIColor.init(red: 48/255, green: 85/255, blue: 254/255, alpha: 1.0)
                }
                    
                let datetime = dict["datetime"] as! String
                if(datetime==""){
                    self.viewDate1.isHidden=true
                }else{
                    self.viewDate1.isHidden=false
                    self.btn1.tag=i
                    let datetimeArr = datetime.components(separatedBy: " ")
                    let date = datetimeArr[0]
                    let strrr : String = ""
                    self.lblDate1.text = strrr.changeDateFormat(strDate:date)
                    let timeAsString = datetimeArr[1]
                    let strtime : String = ""
                    self.lblTime1.text = strtime.changeTimeFormat(strDate: timeAsString)
                    self.lblProposedBy1.text = "Proposed by \(dict["proposed_by"] as! String)"
                }
            }
            if(i==1){
                if(confirmed==1){
                     self.viewDate2.backgroundColor=UIColor.init(red: 54/255, green: 163/255, blue: 42/255, alpha: 1.0)
                }
                else{
                    self.viewDate2.backgroundColor=UIColor.init(red: 48/255, green: 85/255, blue: 254/255, alpha: 1.0)
                }
                let datetime = dict["datetime"] as! String
                if(datetime==""){
                    self.viewDate2.isHidden=true
                }else{
                    self.viewDate2.isHidden=false
                    self.btn2.tag=i
                    let datetimeArr = datetime.components(separatedBy: " ")
                    let date = datetimeArr[0]
                    let strrr : String = ""
                    self.lblDate2.text = strrr.changeDateFormat(strDate:date)
                    let timeAsString = datetimeArr[1]
                    let strtime : String = ""
                    self.lblTime2.text = strtime.changeTimeFormat(strDate: timeAsString)
                    self.lblProposedBy2.text = "Proposed by \(dict["proposed_by"] as! String)"
                }
            }
            if(i==2){
                if(confirmed==1){
                    self.viewDate3.backgroundColor=UIColor.init(red: 54/255, green: 163/255, blue: 42/255, alpha: 1.0)
                }
                else{
                     self.viewDate3.backgroundColor=UIColor.init(red: 48/255, green: 85/255, blue: 254/255, alpha: 1.0)
                }
                let datetime = dict["datetime"] as! String
                if(datetime==""){
                    self.viewDate3.isHidden=true
                }else{
                    self.viewDate3.isHidden=false
                    self.btn3.tag=i
                    let datetimeArr = datetime.components(separatedBy: " ")
                    let date = datetimeArr[0]
                    let strrr : String = ""
                    self.lblDate3.text = strrr.changeDateFormat(strDate:date)
                    let timeAsString = datetimeArr[1]
                    let strtime : String = ""
                    self.lblTime3.text = strtime.changeTimeFormat(strDate: timeAsString)
                    self.lblProposedBy3.text = "Proposed by \(dict["proposed_by"] as! String)"
                }
            }
        }
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    
    
    @IBAction func clickOnViewFirstBtn(_ sender : UIButton){
        
        let dict = arrAppoinmentSlot[sender.tag]
        print(dict)
         self.viewConfirmPopUp.isHidden=false
        
        let datetime = dict["datetime"] as! String
        let datetimeArr = datetime.components(separatedBy: " ")
        let date = datetimeArr[0]
        let strrr : String = ""
        let dateNew = strrr.changeDateFormat(strDate:date)
        let timeAsString = datetimeArr[1]
        let strtime : String = ""
        let timeNew = strtime.changeTimeFormat(strDate: timeAsString)
        
        self.lblConfirm.text="Do you want to confirm viewing time on \(dateNew) at \(timeNew) with \(dict["proposed_by"] as! String)"
        self.btnConfirm.tag=sender.tag
        
        
        
    }
    
   
    @IBAction func clickOnViewSecBtn(_ sender : UIButton){
        let dict = arrAppoinmentSlot[sender.tag]
        print(dict)
        
        self.viewConfirmPopUp.isHidden=false
        
        let datetime = dict["datetime"] as! String
        let datetimeArr = datetime.components(separatedBy: " ")
        let date = datetimeArr[0]
        let strrr : String = ""
        let dateNew = strrr.changeDateFormat(strDate:date)
        let timeAsString = datetimeArr[1]
        let strtime : String = ""
        let timeNew = strtime.changeTimeFormat(strDate: timeAsString)
        
        self.lblConfirm.text="Do you want to confirm viewing time on \(dateNew) at \(timeNew) with \(dict["proposed_by"] as! String)"
        self.btnConfirm.tag=sender.tag
    }
    
    @IBAction func clickOnViewThirdBtn(_ sender : UIButton){
        let dict = arrAppoinmentSlot[sender.tag]
        print(dict)
        
        self.viewConfirmPopUp.isHidden=false
        
        let datetime = dict["datetime"] as! String
        let datetimeArr = datetime.components(separatedBy: " ")
        let date = datetimeArr[0]
        let strrr : String = ""
        let dateNew = strrr.changeDateFormat(strDate:date)
        let timeAsString = datetimeArr[1]
        let strtime : String = ""
        let timeNew = strtime.changeTimeFormat(strDate: timeAsString)
        
        self.lblConfirm.text="Do you want to confirm viewing time on \(dateNew) at \(timeNew) with \(dict["proposed_by"] as! String)"
        self.btnConfirm.tag=sender.tag
    }
    
    @IBAction func clickOnConfirmBtn(_ sender : UIButton){
        
        let dict = arrAppoinmentSlot[sender.tag]
        print(dict)
        let appoinmntId = dict["appoinment_id"] as! Int
        let dateSlotId = dict["date_slot_id"] as! Int
        
        self.startActivityIndicator()
        let param = ["appoinment_id" : appoinmntId,
                     "date_slot_id" : dateSlotId
            ] as NSDictionary
        
        print(param)
        
        CommunicationManager().getResponseForPost(strUrl: Url_File.url_appoinment_confirmed, parameters: param) { ( result , data) in
            
            if(result == "success") {
                let dataDict = data as! NSDictionary
                print(dataDict)
                let responseData = dataDict["response"] as! NSDictionary
                print(responseData)
                
                let appoId = responseData["appoinment_id"] as! Int
                let confirmed_appoinment = responseData["confirmed_appoinment"] as! Int
                
                self.goNext(appoinment_dict: dict)
                
                self.stopActivityIndicator()
                
            }else if (result == "error") {
                let strMessage = (data as! NSDictionary)["message"] as! String
                Common.ShowAlert(Title: Common.Title, Message: strMessage, VC: self)
                
                self.navigationController?.popViewController(animated: true)
                
                self.stopActivityIndicator()
            }else if (result == "Network") {
                let strMessage = (data as! String)
                Common.ShowAlert(Title: Common.Title, Message: strMessage, VC: self)
                self.stopActivityIndicator()
            }
        }
        
        
    }
    
    func goNext(appoinment_dict : NSDictionary){
        
        let story = AppStoryboard.Home.instance
        let meetingVc = story.instantiateViewController(withIdentifier: "meetingConfirmationViewController") as!meetingConfirmationViewController
        meetingVc.arrData=self.arrOfferinghomeData
        meetingVc.appoinmntSlotData=appoinment_dict
        self.navigationController?.pushViewController(meetingVc, animated: true)
    }
    

    
}
