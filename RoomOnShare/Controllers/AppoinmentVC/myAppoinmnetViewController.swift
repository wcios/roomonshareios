
import UIKit
import CoreLocation
import MapKit

class myAppoinmnetViewController: UIViewController {

    @IBOutlet weak var tblAppointment : UITableView!
    @IBOutlet weak var viewNoSlot : UIView!
    var arrMyAppointment : [NSDictionary] = []
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.tblAppointment.isHidden = true
        self.viewNoSlot.isHidden = true
        self.getAllApoinmnets()
    }
    
    
    override func viewWillAppear(_ animated: Bool) {
        self.navigationController?.isNavigationBarHidden = true
        rdv_tabBarController?.setTabBarHidden(true, animated: true)
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    
    @IBAction func clickOnBackBtn(_ sender : UIButton){
        
        Common.PopMethod(VC: self)
    }
    
    func getAllApoinmnets(){
        
        self.startActivityIndicator()
        
        CommunicationManager().getResponseFor(strUrl: Url_File.url_getAllAppoinment, parameters: nil, completion: { ( result , data) in
            if(result == "success") {
                CreateProifleViewController.looking_id = 0
                self.stopActivityIndicator()
                let dataDict = data as! NSDictionary
                self.arrMyAppointment = dataDict["response"] as! [NSDictionary]
                if(self.arrMyAppointment.count==0){
                    self.tblAppointment.isHidden = true
                    self.viewNoSlot.isHidden = false
                }else{
                    self.viewNoSlot.isHidden = true
                    self.tblAppointment.isHidden = false
                    self.tblAppointment.reloadData()
                }
            }else if (result == "error") {
                let strMessage = (data as! NSDictionary)["message"] as! String
                Common.ShowAlert(Title: Common.Title, Message: strMessage, VC: self)
                self.stopActivityIndicator()
            }else if (result == "Network") {
                let strMessage = (data as! String)
                Common.ShowAlert(Title: Common.Title, Message: strMessage, VC: self)
                self.stopActivityIndicator()
            }
        })
    }
    

}

extension myAppoinmnetViewController : UITableViewDelegate , UITableViewDataSource , UIGestureRecognizerDelegate{
    
    func numberOfSections(in tableView: UITableView) -> Int {
        
        return self.arrMyAppointment.count
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell = tblAppointment.dequeueReusableCell(withIdentifier: "appoinmntTblCell") as! appoinmntTblCell
        let dict = self.arrMyAppointment[indexPath.section]
        let offerDict = dict["offering_home"] as! NSDictionary
        cell.lblTitle.text = (offerDict["home_location"] as! String)
        cell.lblSubTitle.text = (offerDict["home_address"] as! String)
        cell.lblNAme.text = (dict["appointment_with_name"] as! String)
        let imgUrl = (dict["appointment_with_image"] as! String)
        cell.imgProfile.sd_setImage(with: URL(string: imgUrl), placeholderImage: UIImage(named: "user"))
        
        cell.btnAddress.tag = indexPath.section
        cell.btnAddress.addTarget(self, action: #selector(clickOnAddress(_:)), for: .touchUpInside)
        
       
        cell.btnConfirm.tag = indexPath.section
        cell.btnConfirm.addTarget(self, action: #selector(clickOnConfiemBtn(_:)), for: .touchUpInside)
        
        return cell
    }
    
    
    @objc func clickOnConfiemBtn(_ sender : UIButton){
        let dict = self.arrMyAppointment[sender.tag]
        print(dict)
        let appoin = dict["appoinment_slot"] as! [NSDictionary]
    }
    
    @objc func clickOnAddress ( _ sender : UIButton){
        let dict = self.arrMyAppointment[sender.tag]
        let offerDict = dict["offering_home"] as! NSDictionary
        let lat = offerDict["latitude"] as! Double
        let long = offerDict["longitude"] as! Double
        
        let coords = CLLocation(latitude: lat, longitude: long)
        CLGeocoder().reverseGeocodeLocation(coords) { (placemark, error) in
            if error != nil {
                print("Hay un error")
            } else {
                
                let place = placemark! as [CLPlacemark]
                if place.count > 0 {
                    let place = placemark![0]
                    var adressString : String = ""
                    if place.thoroughfare != nil {
                        adressString = adressString + place.thoroughfare! + ", "
                    }
                    if place.subThoroughfare != nil {
                        adressString = adressString + place.subThoroughfare! + "\n"
                    }
                    if place.locality != nil {
                        adressString = adressString + place.locality! + " - "
                    }
                    if place.postalCode != nil {
                        adressString = adressString + place.postalCode! + "\n"
                    }
                    if place.subAdministrativeArea != nil {
                        adressString = adressString + place.subAdministrativeArea! + " - "
                    }
                    if place.country != nil {
                        adressString = adressString + place.country!
                    }
                    
                    print(adressString)
                    
                    let geocoder = CLGeocoder()
                    geocoder.geocodeAddressString(adressString) { (placemarksOptional, error) -> Void in
                        if let placemarks = placemarksOptional {
                            if let location = placemarks.first?.location {
                                let query = "?ll=\(location.coordinate.latitude),\(location.coordinate.longitude)"
                                _ = "http://maps.apple.com/" + query
                                
                                let coordinates = CLLocationCoordinate2DMake(location.coordinate.latitude,location.coordinate.longitude)
                                let regionSpan =   MKCoordinateRegionMakeWithDistance(coordinates, 1000, 1000)
                                let placemark = MKPlacemark(coordinate: location.coordinate, addressDictionary: nil)
                                let mapItem = MKMapItem(placemark: placemark)
                                mapItem.openInMaps(launchOptions:[
                                    MKLaunchOptionsMapCenterKey: NSValue(mkCoordinate: regionSpan.center)
                                    ] as [String : Any])
                                
                                //                    if let url =  NSURL(string: path) {
                                //                       // UIApplication.shared.open(url as URL, options: [:], completionHandler: nil)
                                //                    //    let request:MKDirectionsRequest = MKDirectionsRequest()
                                //                     //   mapView.addOverlay(route.polyline, level: MKOverlayLevel.AboveRoads)
                                //                    } else {
                                //                        // Could not construct url. Handle error.
                                //                    }
                            } else {
                                // Could not get a location from the geocode request. Handle error.
                            }
                        } else {
                            // Didn't get any placemarks. Handle error.
                        }
                    }
                }
            }
        }
        
    }
}

class  appoinmntTblCell : UITableViewCell {
    @IBOutlet weak var imgProfile : UIImageView!
    @IBOutlet weak var lblNAme : UILabel!
    @IBOutlet weak var lblTitle : UILabel!
     @IBOutlet weak var lblSubTitle : UILabel!
    @IBOutlet weak var btnAddress : UIButton!
    @IBOutlet weak var btnConfirm : UIButton!
}
