import UIKit

class ViewSettingUpVC: UIViewController {
    
    
    var isEditTrue = ""
    var appoinmntSlotArr : [NSDictionary] = []
    var datePicker_tag = 0
    
    var strdateAvailable1 : String = ""
    var strdateAvailable2 : String = ""
    var strdateAvailable3 : String = ""
    
    var strTime1 : String = ""
    var strTime2 : String = ""
    var strTime3 : String = ""
    
    @IBOutlet weak var txtDateAvailable1 : UITextField!
    @IBOutlet weak var txtDateAvailable2 : UITextField!
    @IBOutlet weak var txtDateAvailable3 : UITextField!
    
    @IBOutlet weak var txtTime1 : UITextField!
    @IBOutlet weak var txtTime2 : UITextField!
    @IBOutlet weak var txtTime3 : UITextField!
    
    
    @IBOutlet weak var lblProposedby1 : UILabel!
    @IBOutlet weak var lblProposedby2 : UILabel!
    @IBOutlet weak var lblProposedby3 : UILabel!
    
    var dateSlotId_1 : Int = 0
    var dateSlotId_2 : Int = 0
    var dateSlotId_3 : Int = 0
    
    var lookingId = 0
    
    var datepicker = UIDatePicker()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        let todaysDate = Date()
        datepicker.minimumDate = todaysDate
        
        if(isEditTrue=="update"){
            print(appoinmntSlotArr.count)
            for i in 0...appoinmntSlotArr.count-1{
                let dict = appoinmntSlotArr[i]
                if(i==0){
                    self.dateSlotId_1=dict["date_slot_id"] as! Int
                    let datetime = dict["datetime"] as! String
                    if(datetime==""){
                    }else{
                        let datetimeArr = datetime.components(separatedBy: " ")
                        let date = datetimeArr[0]
                        let strrr : String = ""
                        self.txtDateAvailable1.text = strrr.changeDateFormat(strDate:date)
                        let timeAsString = datetimeArr[1]
                        let strtime : String = ""
                        self.txtTime1.text = strtime.changeTimeFormat(strDate: timeAsString)
                        self.lblProposedby1.text = "Proposed by \(dict["proposed_by"] as! String)"
                    }
                }
                if(i==1){
                    self.dateSlotId_2=dict["date_slot_id"] as! Int
                    let datetime = dict["datetime"] as! String
                    if(datetime==""){
                    }else{
                        let datetimeArr = datetime.components(separatedBy: " ")
                        let date = datetimeArr[0]
                        let strrr : String = ""
                        self.txtDateAvailable2.text = strrr.changeDateFormat(strDate:date)
                        let timeAsString = datetimeArr[1]
                        let strtime : String = ""
                        self.txtTime2.text = strtime.changeTimeFormat(strDate: timeAsString)
                        self.lblProposedby2.text = "Proposed by \(dict["proposed_by"] as! String)"
                        
                        
                    }
                }
                if(i==2){
                    self.dateSlotId_3=dict["date_slot_id"] as! Int
                    let datetime = dict["datetime"] as! String
                    if(datetime==""){
                    }else{
                        let datetimeArr = datetime.components(separatedBy: " ")
                        let date = datetimeArr[0]
                        let strrr : String = ""
                        self.txtDateAvailable3.text = strrr.changeDateFormat(strDate:date)
                        let timeAsString = datetimeArr[1]
                        let strtime : String = ""
                        self.txtTime3.text = strtime.changeTimeFormat(strDate: timeAsString)
                        self.lblProposedby3.text = "Proposed by \(dict["proposed_by"] as! String)"
                    }
                }
                
            }
        }
        
        
        
        
    }
    
    override func viewWillAppear(_ animated: Bool) {
        self.navigationController?.isNavigationBarHidden = true
        rdv_tabBarController?.setTabBarHidden(false, animated: true)
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        
    }
    
    
    
    
    @IBAction func clickOnSaveBtn(_ sender : UIButton){
        self.startActivityIndicator()
        
        var param : [String : Any] = [:]
        let arrAppoinmentslot :NSMutableArray = NSMutableArray()
        
        if(isEditTrue=="save"){
            
            let para1:NSMutableDictionary = NSMutableDictionary()
            
            if(strdateAvailable1=="" && strTime1==""){
                para1.setValue("\(self.strdateAvailable1)\(self.strTime1)", forKey: "date0")
            }else{
                para1.setValue("\(self.strdateAvailable1) \(self.strTime1)", forKey: "date0")
            }
            
            let para2:NSMutableDictionary = NSMutableDictionary()
            if(strdateAvailable2=="" && strTime2==""){
                para2.setValue("\(self.strdateAvailable2)\(self.strTime2)", forKey: "date1")
            }else{
                para2.setValue("\(self.strdateAvailable2) \(self.strTime2)", forKey: "date1")
            }
            
            let para3:NSMutableDictionary = NSMutableDictionary()
            
            if(strdateAvailable3=="" && strTime3==""){
                para3.setValue("\(self.strdateAvailable3)\(self.strTime3)", forKey: "date2")
            }else{
                para3.setValue("\(self.strdateAvailable3) \(self.strTime3)", forKey: "date2")
            }
            
            
            arrAppoinmentslot.add(para1)
            arrAppoinmentslot.add(para2)
            arrAppoinmentslot.add(para3)
            
            
            print(arrAppoinmentslot)
            
            param = ["looking_home_id" : self.lookingId,
                     "offering_home_id" : SharedPreference.getOffering_id(),
                     "appoinment_slot": arrAppoinmentslot
                ] as [String : Any]
            
            //        let param = ["looking_home_id" : SharedPreference.getLooking_id(),
            //                     "offering_home_id" : SharedPreference.getOffering_id(),
            //                     "appoinment_slot": arrAppoinmentslot
            //            ] as [String : Any]
            
            print(param)
            
            
        }else{
            
            let para1:NSMutableDictionary = NSMutableDictionary()
            let para2:NSMutableDictionary = NSMutableDictionary()
            let para3:NSMutableDictionary = NSMutableDictionary()
            
            
            if(strdateAvailable1=="" && strTime1==""){
                para1.setValue("\(self.strdateAvailable1)\(self.strTime1)",     forKey: "date0")
                para1.setValue(self.dateSlotId_1, forKey: "date_slot_id")
            }else{
                para1.setValue("\(self.strdateAvailable1) \(self.strTime1)", forKey: "date0")
                para1.setValue(self.dateSlotId_1, forKey: "date_slot_id")
            }
            
            
            if(strdateAvailable2=="" && strTime2==""){
                para2.setValue("\(self.strdateAvailable2)\(self.strTime2)", forKey: "date1")
                para2.setValue(self.dateSlotId_2, forKey: "date_slot_id")
            }else{
                para2.setValue("\(self.strdateAvailable2) \(self.strTime2)", forKey: "date1")
                para2.setValue(self.dateSlotId_2, forKey: "date_slot_id")
            }
            
            if(strdateAvailable3=="" && strTime3==""){
                para3.setValue("\(self.strdateAvailable3)\(self.strTime3)", forKey: "date2")
                para3.setValue(self.dateSlotId_3, forKey: "date_slot_id")
            }else{
                para3.setValue("\(self.strdateAvailable3) \(self.strTime3)", forKey: "date2")
                para3.setValue(self.dateSlotId_3, forKey: "date_slot_id")
            }
            
            arrAppoinmentslot.add(para1)
            arrAppoinmentslot.add(para2)
            arrAppoinmentslot.add(para3)
            
            print(arrAppoinmentslot)
            
            
            let appoinMntId = userDef.value(forKey: "appoinment_id") as? Int
            param = ["appoinment_id" : appoinMntId!,
                     "looking_home_id" : 1,
                     "offering_home_id" : 1,
                     "appoinment_slot": arrAppoinmentslot
                ] as [String : Any]
            
            
            //        let param = ["looking_home_id" : SharedPreference.getLooking_id(),
            //                     "offering_home_id" : SharedPreference.getOffering_id(),
            //                     "appoinment_slot": arrAppoinmentslot
            //            ] as [String : Any]
            
            print(param)
            
            
        }
        
        
        CommunicationManager().getResponseForPost(strUrl: Url_File.url_appoinment, parameters: param as NSDictionary) { ( result , data) in
            
            if(result == "success") {
                let dataDict = data as! NSDictionary
                let responseData = dataDict["response"] as! NSDictionary
                print(responseData)
                
                let appoint_id = responseData["appoinment_id"] as! Int
                self.goNext(strAppoinmntId: appoint_id)
                self.stopActivityIndicator()
                
                
                
            }else if (result == "error") {
                let strMessage = (data as! NSDictionary)["message"] as! String
                Common.ShowAlert(Title: Common.Title, Message: strMessage, VC: self)
                
                self.navigationController?.popViewController(animated: true)
                
                self.stopActivityIndicator()
            }else if (result == "Network") {
                let strMessage = (data as! String)
                Common.ShowAlert(Title: Common.Title, Message: strMessage, VC: self)
                self.stopActivityIndicator()
            }
        }
    }
    
    func goNext(strAppoinmntId : Int){
        userDef.set(strAppoinmntId, forKey: "appoinment_id")
        Common.PopMethod(VC: self)
    }
}


extension ViewSettingUpVC : UITextFieldDelegate{
    func textFieldDidBeginEditing(_ textField: UITextField) {
        if textField == self.txtDateAvailable1{
            
            datePicker_tag=1
            datepicker.datePickerMode = .date
            txtDateAvailable1.inputView = datepicker
            datepicker.addTarget(self, action: #selector(handleDatePicker(sender:)), for: .valueChanged)
            
        }
        else if textField == self.txtDateAvailable2{
            
            datePicker_tag=2
            datepicker.datePickerMode = .date
            txtDateAvailable2.inputView = datepicker
            datepicker.addTarget(self, action: #selector(handleDatePicker(sender:)), for: .valueChanged)
            
        }
        else if textField == self.txtDateAvailable3{
            
            datePicker_tag=3
            datepicker.datePickerMode = .date
            txtDateAvailable3.inputView = datepicker
            datepicker.addTarget(self, action: #selector(handleDatePicker(sender:)), for: .valueChanged)
            
        }
        if textField == self.txtTime1{
            
            datePicker_tag=4
            datepicker.datePickerMode = .time
            txtTime1.inputView = datepicker
            datepicker.addTarget(self, action: #selector(handleDatePicker(sender:)), for: .valueChanged)
            
        }
        else if textField == self.txtTime2{
            
            datePicker_tag=5
            datepicker.datePickerMode = .time
            self.txtTime2.inputView = datepicker
            datepicker.addTarget(self, action: #selector(handleDatePicker(sender:)), for: .valueChanged)
            
        }
        else if textField == self.txtTime3{
            
            datePicker_tag=6
            datepicker.datePickerMode = .time
            self.txtTime3.inputView = datepicker
            datepicker.addTarget(self, action: #selector(handleDatePicker(sender:)), for: .valueChanged)
            
        }
        
        
    }
    
    @objc func handleDatePicker(sender: UIDatePicker) {
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "dd MMM yyyy"
        
        
        let timeFormatter = DateFormatter()
        timeFormatter.dateFormat = "hh:mm, a"
        
        if(datePicker_tag==1){
            txtDateAvailable1.text = dateFormatter.string(from: sender.date)
            self.strdateAvailable1 = self.ChangeDateFormat(Date: self.txtDateAvailable1.text!)
        }
        else if(datePicker_tag==2){
            txtDateAvailable2.text = dateFormatter.string(from: sender.date)
            self.strdateAvailable2 = self.ChangeDateFormat(Date: self.txtDateAvailable2.text!)
        }
        else if(datePicker_tag==3){
            txtDateAvailable3.text = dateFormatter.string(from: sender.date)
            self.strdateAvailable3 = self.ChangeDateFormat(Date: self.txtDateAvailable3.text!)
        }
        else if(datePicker_tag==4){
            txtTime1.text = timeFormatter.string(from: sender.date)
            self.strTime1 = self.ChangeTimeFormat(Time: self.txtTime1.text!)
        }
        else if(datePicker_tag==5){
            txtTime2.text = timeFormatter.string(from: sender.date)
            self.strTime2 = self.ChangeTimeFormat(Time: self.txtTime2.text!)
        }
        else if(datePicker_tag==6){
            txtTime3.text = timeFormatter.string(from: sender.date)
            self.strTime3 = self.ChangeTimeFormat(Time: self.txtTime3.text!)
        }
        
    }
    
    
    func ChangeDateFormat(Date: String) -> String  {
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "dd MMM yyyy"
        let newdate = dateFormatter.date(from: Date)
        dateFormatter.dateFormat = "yyyy-MM-dd"
        return dateFormatter.string(from: newdate!)
    }
    
    func ChangeTimeFormat(Time: String) -> String  {
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "hh:mm, a"
        let newdate = dateFormatter.date(from: Time)
        dateFormatter.dateFormat = "hh:mm:ss"
        return dateFormatter.string(from: newdate!)
    }
    
}
