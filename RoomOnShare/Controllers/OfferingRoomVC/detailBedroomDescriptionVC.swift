import UIKit
class detailBedroomDescriptionVC: UIViewController {
    
    @IBOutlet weak var tblBedroomDescription : UITableView!
    var arrBedroomData : Array<Any> = []
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        tblBedroomDescription.tableFooterView=UIView()
        tblBedroomDescription.register(UINib(nibName: "detailBedrromDescrptnTblCell", bundle: nil), forCellReuseIdentifier: "detailBedrromDescrptnTblCell")
    
        // Do any additional setup after loading the view.
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    override func viewWillAppear(_ animated: Bool) {
        if let arrBedroomData = userDef.value(forKey: "bedroomarray") as? Array<Any> {
            self.arrBedroomData = arrBedroomData
        }
    }
    
    @IBAction func clickOnBackBtn(_ sender : UIButton){
        Common.PopMethod(VC: self)
    }
    
}

extension detailBedroomDescriptionVC : UITableViewDelegate , UITableViewDataSource{
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return arrBedroomData.count
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell = self.tblBedroomDescription.dequeueReusableCell(withIdentifier: "detailBedrromDescrptnTblCell") as! detailBedrromDescrptnTblCell
        
        let dict = arrBedroomData[indexPath.section] as! NSDictionary
        cell.lblDateAvail.text = "Date Available : \(dict["available_date"] as! String)"
        
        let min = dict["minimum_period"] as! Int
        cell.lblMin.text = "Minimum : \(Array_File.arrMinWeek[min - 1])"
        
        let max = dict["maximum_period"] as! Int
        cell.lblMax.text = "Maximum : \(Array_File.arrMaxWeek[max - 1])"
        
        cell.lblSinglePerson.text = "Single : \(dict["single_person_rent"] as! String)"
        cell.lblCouple.text = "Couple : \(dict["couple_rent"] as! String)"
        
        let bedroom_type = dict["bedroom_type"] as! Int
        cell.lblBedroomtype.text = Array_File.arrBedroomtype[bedroom_type - 1]
        if(bedroom_type==2){
            cell.lblBedroomSize.isHidden=true
            cell.lblBedroomFurniture.isHidden=true
            cell.lblBathroomFacility.isHidden=true
            cell.lblNoOfBeds.isHidden=false
            cell.lblNoOfBeds.text="Number of Beds : \(Array_File.arrNoOfBeds[dict["no_of_beds"] as! Int - 1]) beds"
            
        }else{
            cell.lblBedroomSize.isHidden=false
            cell.lblBedroomFurniture.isHidden=false
            cell.lblBathroomFacility.isHidden=false
            cell.lblNoOfBeds.isHidden=true
            
            
            cell.lblBedroomSize.text="Bedroom Size : \(Array_File.arrBedroomSize[dict["bedroom_size"] as! Int - 1])"
            cell.lblBedroomFurniture.text="Bedroom Furniture : \(Array_File.arrBedroomFurniture[dict["bedroom_furniture"] as! Int - 1])"
            
            let bath = dict["bathroom_facilities"] as! Int
            if bath == 1{
                cell.lblBathroomFacility.text="Bathroom Facility : Shared bathroom"
            }else{
                cell.lblBathroomFacility.text="Bathroom Facility : Own bathroom"
            }
            
            
        }
        
        cell.lblRoomFeature.text = "Bedroom Feature : "
        cell.lblOtherInfo.text = "Other Information : \(dict["other_information"] as! String)"
        
        cell.btnEdit.tag = indexPath.section
        cell.btnDelete.tag = indexPath.section
        
        cell.btnDelete.addTarget(self, action: #selector(clickOnDeleteBtn(_:)), for: .touchUpInside)
        cell.btnEdit.addTarget(self, action: #selector(clickOnEditBtn(_:)), for: .touchUpInside)
        
        return cell
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 240
    }
    
    @objc func clickOnDeleteBtn(_ sender : UIButton){
        let alert = UIAlertController(title: "Room On Share", message:
            "Are you sure, you want to delete this profile", preferredStyle: UIAlertControllerStyle.alert)
        let okAction = UIAlertAction(title: "Yes", style: UIAlertActionStyle.default) { (action) in
            self.arrBedroomData.remove(at: sender.tag)
            if(self.arrBedroomData.count > 0){
                userDef.set(self.arrBedroomData, forKey: "bedroomarray")
            }else{
                userDef.removeObject(forKey: "bedroomarray")
            }
            self.tblBedroomDescription.reloadData()
        }
        
        let CancleAction = UIAlertAction(title: "No", style: UIAlertActionStyle.default) { (action) in
            
        }
        
        alert.addAction(okAction)
        alert.addAction(CancleAction)
        self.present(alert, animated: true, completion: nil)
    }
    @objc func clickOnEditBtn(_ sender : UIButton){
        let story = UIStoryboard(name: "Main", bundle: nil)
        let detail = story.instantiateViewController(withIdentifier: "BedroomDescriptionViewController") as! BedroomDescriptionViewController
        detail.arrIndex = sender.tag
        detail.strIsEdit = "true"
        self.navigationController?.pushViewController(detail, animated: true)
    }
}
