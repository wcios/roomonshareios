//
//  BedroomDescriptionViewController.swift
//  RoomOnShare
//
//  Created by mac on 07/08/18.
//  Copyright © 2018 WestCoast. All rights reserved.
//

import UIKit

class BedroomDescriptionViewController: UIViewController {
    
    var strIsEdit : String = ""
    var arrIndex : Int = 0
    
    @IBOutlet weak var mainviewHight: NSLayoutConstraint!
    
    @IBOutlet weak var txtDateAvailable : UITextField!
    @IBOutlet weak var txtMinWeek : UITextField!
    @IBOutlet weak var txtMaxWeek : UITextField!
    @IBOutlet weak var txtBedroomRentWeek : UITextField!
    @IBOutlet weak var txtSinglePrsnRent : UITextField!
    @IBOutlet weak var txtCoupleRent : UITextField!
    @IBOutlet weak var txtDeposit : UITextField!
    @IBOutlet weak var txtTypeBedroom : UITextField!
    @IBOutlet weak var txtBedroomSize : UITextField!
    @IBOutlet weak var txtBedroomfurniture : UITextField!
    @IBOutlet weak var txtNoOfBeds : UITextField!
    @IBOutlet weak var txtOtherInformation : UITextField!
    @IBOutlet weak var viewBedroomType : UIView!
    @IBOutlet weak var viewNoOfBeds : UIView!
    @IBOutlet weak var viewBathroomFacility : UIView!
    @IBOutlet weak var viewBedroomHeight: NSLayoutConstraint!
    @IBOutlet weak var viewRoomFeatureTop: NSLayoutConstraint!
    @IBOutlet weak var collRoomFeatures : UICollectionView!
    @IBOutlet var btnBillsIncluded : [UIButton]!
    @IBOutlet var btnBathroomFacility : [UIButton]!
    
    var arrBedroomData : Array<Any> = []
    var _selectedCells : NSMutableArray = []
    var strRoomFeaturesArr = ""
    var id_bedroom : Int = 0
    var id_Bills : Int = 0
    var id_BathroomFacility : Int = 0
    var id_MinWeek : Int = 0
    var id_MaxWeek : Int = 0
    var id_Deposit : Int = 0
    var id_TypeBedroom : Int = 0
    var id_BedroomSize : Int = 0
    var id_Bedroomfurniture : Int = 0
    var id_NoOfBeds : Int = 0
    var id_Bedroomrentweek : Int = 0
    var datepicker = UIDatePicker()
    var strdateAvailable : String = ""
    var country_code = ""
    
    override func viewDidLoad() {
        collRoomFeatures.allowsMultipleSelection=true
        self.ShowData()
        
        let todaysDate = Date()
        datepicker.minimumDate = todaysDate
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        self.navigationController?.isNavigationBarHidden = true
        rdv_tabBarController?.setTabBarHidden(false, animated: true)
        if let arrBedroomData = userDef.value(forKey: "bedroomarray") as? Array<Any> {
            self.arrBedroomData = arrBedroomData
            print(self.arrBedroomData)
        }
    }
    
    func ShowData() {
        if(strIsEdit == "true"){
            let arrBedroomData = userDef.value(forKey: "bedroomarray") as! Array<Any>
            let dict = arrBedroomData[arrIndex] as! NSDictionary
            print(dict)
            if SharedPreference.getOffering_id() != 0{
                self.id_bedroom = dict["id"] as! Int
            }
            self.strdateAvailable = dict["available_date"] as! String
            self.id_MinWeek = dict["minimum_period"] as! Int
            self.id_MaxWeek = dict["maximum_period"] as! Int
            self.id_Bedroomrentweek = dict["rent_duration"] as! Int
            self.id_Bills = dict["bills"] as! Int
            self.id_Deposit = dict["deposit"] as! Int
            self.id_TypeBedroom = dict["bedroom_type"] as! Int
            self.id_BedroomSize = dict["bedroom_size"] as! Int
            self.id_Bedroomfurniture = dict["bedroom_furniture"] as! Int
            self.id_BathroomFacility = dict["bathroom_facilities"] as! Int
            self.strRoomFeaturesArr = dict["room_features"] as! String
            self.id_NoOfBeds = dict["no_of_beds"] as! Int
            self.country_code = dict["country_code"] as! String
            
            let arr = self.strRoomFeaturesArr.components(separatedBy: ",")
            for item in arr {
                self._selectedCells.add(item)
            }
            self.collRoomFeatures.reloadData()
            
           // "room_features": self.strRoomFeaturesArr,
            
            self.txtDateAvailable.text = dict["available_date"] as? String
            self.txtMinWeek.text = Array_File.arrMinWeek[dict["minimum_period"] as! Int - 1]
            self.txtMaxWeek.text = Array_File.arrMaxWeek[dict["maximum_period"] as! Int - 1]
            self.txtSinglePrsnRent.text = "\(dict["single_person_rent"] as! String)"
            self.txtCoupleRent.text = "\(dict["couple_rent"] as! String)"
            //self.strdateAvailable = self.ChangeDateFormat(Date: self.txtDateAvailable.text!)
            
            self.txtDeposit.text = Array_File.arrDeposit[dict.value(forKey: "deposit") as! Int - 1] 
            self.txtBedroomRentWeek.text = Array_File.arrBedrromrentWeek[dict["rent_duration"] as! Int - 1]
            let bills = dict["bills"] as! Int
            let btn = UIButton()
            btn.tag = bills
            self.BillsMethod(btn)
            
            let bedroom_type = dict["bedroom_type"] as! Int
            self.txtTypeBedroom.text = Array_File.arrBedroomtype[bedroom_type - 1]
            
            if(bedroom_type==2){
                self.viewBathroomFacility.isHidden = true
                self.viewBedroomType.isHidden = true
                self.viewNoOfBeds.isHidden = false
                self.viewBedroomHeight.constant=195
                self.viewRoomFeatureTop.constant = -108
                self.mainviewHight.constant=1320
                self.txtNoOfBeds.text = Array_File.arrNoOfBeds[dict["no_of_beds"] as! Int - 1]
            }else{
                self.viewBathroomFacility.isHidden = false
                self.viewBedroomType.isHidden = false
                self.viewNoOfBeds.isHidden = true
                self.viewBedroomHeight.constant=245
                self.viewRoomFeatureTop.constant=12
                self.mainviewHight.constant=1450
                
                self.txtBedroomSize.text = Array_File.arrBedroomSize[dict["bedroom_size"] as! Int - 1]
                self.txtBedroomfurniture.text=Array_File.arrBedroomFurniture[dict["bedroom_furniture"] as! Int - 1]
                let bath = dict["bathroom_facilities"] as! Int
                let btn = UIButton()
                btn.tag = bath
                self.BathroomFacilityMethod(btn)
            }
            self.txtOtherInformation.text = dict["other_information"] as? String
            
        }else{
            print("add")
            if(txtTypeBedroom.text == "Select Bedroom Type"){
                self.viewBedroomType.isHidden=true
                self.viewNoOfBeds.isHidden=true
                self.viewBedroomHeight.constant=130
                self.mainviewHight.constant=1300
            }
        }
    }
    
    func ChangeDateFormat(Date: String) -> String  {
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "dd MMM yyyy"
        let newdate = dateFormatter.date(from: Date)
        dateFormatter.dateFormat = "yyyy-MM-dd"
        return dateFormatter.string(from: newdate!)
    }
    
    @IBAction func BackMethod(_ sender: UIButton) {
        Common.PopMethod(VC: self)
    }
    
    @IBAction func BillsMethod(_ sender: UIButton) {
        let Selectedtag = sender.tag
        id_Bills = Selectedtag
        self.CheckUnceck(arrButton: self.btnBillsIncluded, Selected: Selectedtag)
    }
    
    @IBAction func BathroomFacilityMethod(_ sender: UIButton) {
        let Selectedtag = sender.tag
        id_BathroomFacility = Selectedtag
        self.CheckUnceck(arrButton: self.btnBathroomFacility, Selected: Selectedtag)
    }
    
    func CheckUnceck(arrButton : [UIButton], Selected : Int) {
        for button in arrButton {
            let tag = button.tag
            if Selected == tag{
                button.isSelected = true
            }else{
                button.isSelected = false
            }
        }
    }
    
    @IBAction func clckOnSaveBedroom(_ sender: Any) {
        
        var dict : [String : Any] = [:]
        if SharedPreference.getOffering_id() != 0{
            dict = [
                "id" : self.id_bedroom,
                "available_date": self.strdateAvailable,
                "minimum_period": id_MinWeek,
                "maximum_period": id_MaxWeek,
                "single_person_rent": "\(txtSinglePrsnRent.text!)",
                "couple_rent": "\(txtCoupleRent.text!)",
                "rent_duration": self.id_Bedroomrentweek,
                "bills": id_Bills,
                "deposit": id_Deposit,
                "bedroom_type": id_TypeBedroom,
                "bedroom_size": id_BedroomSize,
                "bedroom_furniture": id_Bedroomfurniture,
                "bathroom_facilities": id_BathroomFacility,
                "room_features": self.strRoomFeaturesArr,
                "no_of_beds": id_NoOfBeds,
                "other_information": self.txtOtherInformation.text!,
                "country_code":self.country_code
                ] as [String : Any]
            
            print(dict)
        }else{
            dict = [
                "available_date": self.strdateAvailable,
                "minimum_period": id_MinWeek,
                "maximum_period": id_MaxWeek,
                "single_person_rent": "\(txtSinglePrsnRent.text!)",
                "couple_rent": "\(txtCoupleRent.text!)",
                "rent_duration": self.id_Bedroomrentweek,
                "bills": id_Bills,
                "deposit": id_Deposit,
                "bedroom_type": id_TypeBedroom,
                "bedroom_size": id_BedroomSize,
                "bedroom_furniture": id_Bedroomfurniture,
                "bathroom_facilities": id_BathroomFacility,
                "room_features": self.strRoomFeaturesArr,
                "no_of_beds": id_NoOfBeds,
                "other_information": self.txtOtherInformation.text!,
                "country_code":SharedPreference.getUserData().user_country
                ] as [String : Any]
            
            print(dict)
        }
        
        
       
        
         if(strIsEdit == "true"){
            self.arrBedroomData = userDef.value(forKey: "bedroomarray") as! Array<Any>
            self.arrBedroomData.remove(at: arrIndex)
            self.arrBedroomData.insert(dict, at: arrIndex)
         }else{
            self.arrBedroomData.append(dict)  //adding(dict) //add(dict)
         }
         userDef.set(self.arrBedroomData, forKey: "bedroomarray")
        
        Common.PopMethod(VC: self)
    }
    
}

extension BedroomDescriptionViewController : UICollectionViewDataSource,UICollectionViewDelegate,UICollectionViewDelegateFlowLayout {
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return Array_File.arrroomFeaturesImgs.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collRoomFeatures.dequeueReusableCell(withReuseIdentifier: "collRoomFeatures", for: indexPath) as! collRoomFeatures
        let dict = Array_File.arrroomFeaturesImgs[indexPath.row]
        cell.imgFeature.image=UIImage(named: (dict["image"])!)
        cell.lblIconName.text=(dict["name"]!)
        
        if _selectedCells.contains(indexPath.row + 1) {
            cell.isSelected=true
            collRoomFeatures.selectItem(at: indexPath, animated: true, scrollPosition: UICollectionViewScrollPosition.top)
            cell.imgFeature.tintImageColor(color: UIColorFromHex(rgbValue: 0x05C6CF, alpha: 1.0))
        }
        else{
            cell.isSelected=false
            cell.imgFeature.tintImageColor(color: .lightGray)
        }
        
        if(strIsEdit == "true"){
            let arrBedroomData = userDef.value(forKey: "bedroomarray") as! Array<Any>
            let dict = arrBedroomData[arrIndex] as! NSDictionary
            let roomFeature = dict["room_features"] as? String
            let fullArr = roomFeature?.components(separatedBy: ",")
            for i in 0...(fullArr?.count)! - 1{
                let id = Int(fullArr![i] )
                if indexPath.row + 1 == id {
                    cell.isSelected=true
                    collRoomFeatures.selectItem(at: indexPath, animated: true, scrollPosition: UICollectionViewScrollPosition.top)
                    cell.imgFeature.tintImageColor(color: UIColorFromHex(rgbValue: 0x05C6CF, alpha: 1.0))
                }
            }
        }
        return cell
    }
    
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath){
        _selectedCells.add(indexPath.row + 1)
        self.strRoomFeaturesArr = _selectedCells.componentsJoined(by: ",")
        collRoomFeatures.reloadItems(at: [indexPath])
    }
    
    func collectionView(_ collectionView: UICollectionView, didDeselectItemAt indexPath: IndexPath){
        _selectedCells.remove(indexPath.row + 1)
        self.strRoomFeaturesArr = _selectedCells.componentsJoined(by: ",")
        collRoomFeatures.reloadItems(at: [indexPath])
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        return CGSize(width: collectionView.bounds.width/4.0, height: 110.0)
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, insetForSectionAt section: Int) -> UIEdgeInsets {
        return UIEdgeInsets.zero
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumInteritemSpacingForSectionAt section: Int) -> CGFloat {
        return 0
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumLineSpacingForSectionAt section: Int) -> CGFloat {
        return 0
    }
}

class collRoomFeatures: UICollectionViewCell {
    @IBOutlet weak var imgFeature : UIImageView!
    @IBOutlet weak var lblIconName : UILabel!
}


extension BedroomDescriptionViewController : UITextFieldDelegate, MyPickerViewProtocol{
    func textFieldDidBeginEditing(_ textField: UITextField) {
        if textField == self.txtDateAvailable{
            
            datepicker.datePickerMode = .date
            txtDateAvailable.inputView = datepicker
            datepicker.addTarget(self, action: #selector(handleDatePicker(sender:)), for: .valueChanged)
        }else if textField == self.txtMinWeek{
            let myPickerView =  MyPickerView.init(withDictionary: Array_File.arrMinWeek, Tag:1)
            myPickerView.delegate = myPickerView
            myPickerView.MyPickerDelegate = self
            self.txtMinWeek.inputView = myPickerView
            self.txtMinWeek.text=Array_File.arrMinWeek[0]
            self.id_MinWeek = 1
        }else if textField == self.txtMaxWeek{
            let myPickerView =  MyPickerView.init(withDictionary: Array_File.arrMaxWeek, Tag:2)
            myPickerView.delegate = myPickerView
            myPickerView.MyPickerDelegate = self
            self.txtMaxWeek.inputView = myPickerView
            self.txtMaxWeek.text=Array_File.arrMaxWeek[0]
            self.id_MaxWeek = 1
        }else if textField == self.txtBedroomRentWeek{
            let myPickerView =  MyPickerView.init(withDictionary: Array_File.arrBedrromrentWeek, Tag:3)
            myPickerView.delegate = myPickerView
            myPickerView.MyPickerDelegate = self
            self.txtBedroomRentWeek.inputView = myPickerView
            self.txtBedroomRentWeek.text=Array_File.arrBedrromrentWeek[0]
            self.id_Bedroomrentweek = 1
        }else if textField == self.txtDeposit{
            let myPickerView =  MyPickerView.init(withDictionary: Array_File.arrDeposit, Tag:4)
            myPickerView.delegate = myPickerView
            myPickerView.MyPickerDelegate = self
            self.txtDeposit.inputView = myPickerView
            self.txtDeposit.text=Array_File.arrDeposit[0]
            self.id_Deposit = 1
        
        }else if textField == self.txtTypeBedroom{
            let myPickerView =  MyPickerView.init(withDictionary: Array_File.arrBedroomtype, Tag:5)
            myPickerView.delegate = myPickerView
            myPickerView.MyPickerDelegate = self
            self.txtTypeBedroom.inputView = myPickerView
        }
        else if textField == self.txtBedroomSize{
            let myPickerView =  MyPickerView.init(withDictionary: Array_File.arrBedroomSize, Tag:6)
            myPickerView.delegate = myPickerView
            myPickerView.MyPickerDelegate = self
            self.txtBedroomSize.inputView = myPickerView
            self.txtBedroomSize.text=Array_File.arrBedroomSize[0]
            self.id_BedroomSize = 1
        }else if textField == self.txtBedroomfurniture{
            let myPickerView =  MyPickerView.init(withDictionary: Array_File.arrBedroomFurniture, Tag:7)
            myPickerView.delegate = myPickerView
            myPickerView.MyPickerDelegate = self
            self.txtBedroomfurniture.inputView = myPickerView
            self.txtBedroomfurniture.text=Array_File.arrBedroomFurniture[0]
            self.id_Bedroomfurniture = 1
        }
        else if textField == self.txtNoOfBeds{
            let myPickerView =  MyPickerView.init(withDictionary: Array_File.arrNoOfBeds, Tag:8)
            myPickerView.delegate = myPickerView
            myPickerView.MyPickerDelegate = self
            self.txtNoOfBeds.inputView = myPickerView
            self.txtNoOfBeds.text=Array_File.arrNoOfBeds[0]
            self.id_NoOfBeds = 1
        }
    }
    
    @objc func handleDatePicker(sender: UIDatePicker) {
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "dd MMM yyyy"
        txtDateAvailable.text = dateFormatter.string(from: sender.date)
        self.strdateAvailable = self.ChangeDateFormat(Date: self.txtDateAvailable.text!)
    }
    
    func myPickerDidSelectRow(Index:Int?, Tag:Int) {
        if Tag == 1{
            id_MinWeek = Index! + 1
            self.txtMinWeek.text = Array_File.arrMinWeek[Index!]
        }else if Tag == 2{
            id_MaxWeek = Index! + 1
            self.txtMaxWeek.text = Array_File.arrMaxWeek[Index!]
        }else if Tag == 3{
            id_Bedroomrentweek = Index! + 1
            self.txtBedroomRentWeek.text = Array_File.arrBedrromrentWeek[Index!]
        }else if Tag == 4{
            id_Deposit = Index! + 1
            self.txtDeposit.text = Array_File.arrDeposit[Index!]
        }else if Tag == 5{
            id_TypeBedroom = Index! + 1
            let item = Array_File.arrBedroomtype[Index!]
            self.txtTypeBedroom.text = item
            if(item=="Private Bedroom"){
                self.viewBathroomFacility.isHidden=false
                self.viewBedroomType.isHidden=false
                self.viewNoOfBeds.isHidden=true
                self.viewBedroomHeight.constant=245
                self.viewRoomFeatureTop.constant=12
                self.mainviewHight.constant=1450
            }else {
                self.viewBathroomFacility.isHidden=true
                self.viewBedroomType.isHidden=true
                self.viewNoOfBeds.isHidden=false
                self.viewBedroomHeight.constant=195
                self.viewRoomFeatureTop.constant = -108
                self.mainviewHight.constant=1320
            }
        }
        else if Tag == 6{
            id_BedroomSize = Index! + 1
            self.txtBedroomSize.text = Array_File.arrBedroomSize[Index!]
        }else if Tag == 7{
            id_Bedroomfurniture = Index! + 1
            self.txtBedroomfurniture.text = Array_File.arrBedroomFurniture[Index!]
        }else if Tag == 8{
            id_NoOfBeds = Index! + 1
            self.txtNoOfBeds.text = Array_File.arrNoOfBeds[Index!]
        }
    }
}
