//
//  AboutOccupantsViewController.swift
//  RoomOnShare
//
//  Created by mac on 07/08/18.
//  Copyright © 2018 WestCoast. All rights reserved.
//

import UIKit

class AboutOccupantsViewController: UIViewController , tblSelection{
    
    
    @IBOutlet weak var collAboutOccupants : UICollectionView!
    var collDataArr : NSMutableArray = []
    @IBOutlet weak var viewInterestHeight : NSLayoutConstraint!
    
    @IBOutlet var Gender: [UIButton]!
    @IBOutlet var AgeGroup: [UIButton]!
    @IBOutlet var Smoking: [UIButton]!
    @IBOutlet var Pets: [UIButton]!
    @IBOutlet weak var txtPersonalIntro: UITextView!
    @IBOutlet weak var txtPersonalQuality: UITextView!
    @IBOutlet weak var `switch`: UISwitch!
    
    @IBOutlet weak var view1: UIView!
    @IBOutlet weak var view2: UIView!
    @IBOutlet weak var view3: UIView!
    @IBOutlet weak var view4: UIView!
    @IBOutlet weak var view5: UIView!
    @IBOutlet weak var view6: UIView!
    
    var id_Gender = ""
    var id_AgeGroup = ""
    var id_Smoking = ""
    var id_Pet  = 0
    var strMoreinterest = ""
    
    var arrGender : NSMutableArray = []
    var arrAge : NSMutableArray = []
    var arrSmoking : NSMutableArray = []
    var switch_state : Bool = false
    var moreinterest : NSMutableArray = []

    static var Sid_Gender : Array<Any> = []
    static var Sid_AgeGroup : Array<Any> = []
    static var Sid_Pet = 0
    static var Sid_Smoking : Array<Any> = []
    static var SPersonalInfo = ""
    static var SPersonalquality = ""
    static var SarrMoreinterest : Array<Any> = []
    
    var userdata = SharedPreference.getUserData()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.txtPersonalIntro.delegate = self
        self.txtPersonalIntro.text = "Personal Introduction"
        self.txtPersonalIntro.textColor = UIColor.lightGray
        self.txtPersonalQuality.delegate = self
        self.txtPersonalQuality.text = "Important Personal Qualities"
        self.txtPersonalQuality.textColor = UIColor.lightGray
        collAboutOccupants.isHidden=true
        viewInterestHeight.constant=50
    }
    
    override func viewWillAppear(_ animated: Bool) {
        self.navigationController?.isNavigationBarHidden = true
        rdv_tabBarController?.setTabBarHidden(false, animated: true)
        self.Showdata()
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    func Showdata(){
        if AboutOccupantsViewController.Sid_Gender.count != 0{
            
            let arrG = AboutOccupantsViewController.Sid_Gender
            for item in arrG{
                if item as! Int != 0{
                    self.arrGender.add(item)
                }
            }
            self.id_Gender = self.arrGender.componentsJoined(by: ",")
            
            let arrAG = AboutOccupantsViewController.Sid_AgeGroup
            for item in arrAG{
                if item as! Int != 0{
                    self.arrAge.add(item)
                }
            }
            self.id_AgeGroup = self.arrAge.componentsJoined(by: ",")
            
            let arrSM = AboutOccupantsViewController.Sid_Smoking
            for item in arrSM{
                if item as! Int != 0{
                    self.arrSmoking.add(item)
                }
            }
            self.id_Smoking = self.arrSmoking.componentsJoined(by: ",")
            
            self.id_Pet = AboutOccupantsViewController.Sid_Pet
            
            let arrmi = AboutOccupantsViewController.SarrMoreinterest
            for item in arrmi{
                if item as! Int != 0{
                    self.moreinterest.add(item)
                }
            }
            self.strMoreinterest = self.moreinterest.componentsJoined(by: ",")
            
            let arrgender = AboutOccupantsViewController.Sid_Gender
            for item in arrgender{
                if item as! Int != 0{
                    self.Check(arrButton: self.Gender, Selected: item as! Int)
                }
            }
            
            let arrage = AboutOccupantsViewController.Sid_AgeGroup
            for item in arrage{
                if item as! Int != 0{
                    self.Check(arrButton: self.AgeGroup, Selected: item as! Int)
                }
            }
            
            let arrsmok = AboutOccupantsViewController.Sid_Smoking
            for item in arrsmok{
                if item as! Int != 0{
                    self.Check(arrButton: self.Smoking, Selected: item as! Int)
                }
            }
    
            self.CheckUnceck(arrButton: self.Pets, Selected: self.id_Pet)
            self.txtPersonalIntro.text = AboutOccupantsViewController.SPersonalInfo
            self.txtPersonalQuality.text = AboutOccupantsViewController.SPersonalquality
            
            let arr = AboutOccupantsViewController.SarrMoreinterest                        
            for i in 0...Array_File.arrIntesrestOptionImgs.count{
                for item in arr{
                    let j = item as! Int
                    if j == i{
                        let dict = Array_File.arrIntesrestOptionImgs[i]
                        self.collDataArr.add(dict)
                    }
                }
            }
            collAboutOccupants.isHidden=false
            viewInterestHeight.constant=170
            self.collAboutOccupants.reloadData()
        }
    }
    
    func Check(arrButton : [UIButton], Selected : Int) {
        for button in arrButton {
            let tag = button.tag
            if Selected == tag{
                button.isSelected = true
            }
        }
    }
    
    @IBAction func BackMethod(_ sender: Any) {
        Common.PopMethod(VC: self)
    }
    
    @IBAction func SwitchMethod(_ sender: UISwitch) {
        if sender.isOn{
            self.switch_state = true
            self.view1.isHidden = false
            self.view2.isHidden = false
            self.view3.isHidden = false
            self.view4.isHidden = false
            self.view5.isHidden = false
            self.view6.isHidden = false
        }else{
            self.switch_state = false
            self.view1.isHidden = true
            self.view2.isHidden = true
            self.view3.isHidden = true
            self.view4.isHidden = true
            self.view5.isHidden = true
            self.view6.isHidden = true
        }
    }
    
    @IBAction func GenderMethod(_ sender: UIButton) {
        let Selectedtag = sender.tag
        if sender.isSelected {
            sender.isSelected = false
            arrGender.remove(Selectedtag)
        }else{
            sender.isSelected = true
            arrGender.add(Selectedtag)
        }
        self.id_Gender = arrGender.componentsJoined(by: ",")
        print(self.id_Gender)
    }
    
    @IBAction func AgeMethod(_ sender: UIButton) {
        let Selectedtag = sender.tag
        if sender.isSelected {
            sender.isSelected = false
            arrAge.remove(Selectedtag)
        }else{
            sender.isSelected = true
            arrAge.add(Selectedtag)
        }
        self.id_AgeGroup = arrAge.componentsJoined(by: ",")
        print(self.id_AgeGroup)
    }
    
    @IBAction func SmokingMethod(_ sender: UIButton) {
        let Selectedtag = sender.tag
        if sender.isSelected {
            sender.isSelected = false
            arrSmoking.remove(Selectedtag)
        }else{
            sender.isSelected = true
            arrSmoking.add(Selectedtag)
        }
        self.id_Smoking = arrSmoking.componentsJoined(by: ",")
        print(self.id_Smoking)
    }
    
    @IBAction func PetsMethod(_ sender: UIButton) {
        let Selectedtag = sender.tag
        self.id_Pet = Selectedtag
        CheckUnceck(arrButton: self.Pets, Selected: Selectedtag)
    }
    
        func CheckUnceck(arrButton : [UIButton], Selected : Int) {
            for button in arrButton {
                let tag = button.tag
                if Selected == tag{
                    button.isSelected = true
                }else{
                    button.isSelected = false
                }
            }
        }
    
    @IBAction func NextMethod(_ sender: UIButton) {
        
        guard self.id_Gender != ""  else {
            return Common.ShowAlert(Title: Common.Title, Message: "Please select rent budget", VC: self)
        }
        
        guard self.id_AgeGroup != ""   else {
            return Common.ShowAlert(Title: Common.Title, Message: "Please select bedroom type", VC: self)
        }
        
        guard self.id_Smoking != ""   else {
            return Common.ShowAlert(Title: Common.Title, Message: "Please select home size", VC: self)
        }
        
        guard self.id_Pet != 0   else {
            return Common.ShowAlert(Title: Common.Title, Message: "Please select bedroom size", VC: self)
        }
        
        guard let personaminfo = self.txtPersonalIntro.text, personaminfo != ""  else {
            return Common.ShowAlert(Title: Common.Title, Message: "Please enter personal information", VC: self)
        }
        
        guard let personalquality = self.txtPersonalQuality.text, personalquality != ""  else {
            return Common.ShowAlert(Title: Common.Title, Message: "Please enter personal quality", VC: self)
        }
        
        guard self.strMoreinterest != ""  else {
            return Common.ShowAlert(Title: Common.Title, Message: "Please select your interest", VC: self)
        }
        
        self.AboutOccupantsService()
    }
    
    @IBAction func clicknInterestBtn(_ sender : UIButton){
        let storyboard = UIStoryboard(name: "Main", bundle: nil)
        let vc = storyboard.instantiateViewController(withIdentifier: "moreInterestViewController") as! moreInterestViewController
        vc.delegate = self
        self.navigationController?.pushViewController(vc, animated: true)
    }
    
    func onSelectPassData(arrData: NSMutableArray) {
        self.strMoreinterest = arrData.componentsJoined(by: ",")
        print(arrData)
        for i in 0...Array_File.arrIntesrestOptionImgs.count{
            if(arrData.contains(i)){
                let dict = Array_File.arrIntesrestOptionImgs[i]
                collDataArr.add(dict)
            }
        }
        print(collDataArr)
        
        // if collDataArr.count
        collAboutOccupants.isHidden=false
        viewInterestHeight.constant=170
        
        collAboutOccupants.reloadData()
    }
    
    func AboutOccupantsService(){
        self.startActivityIndicator()
        let param = ["user_id"    : userdata.user_id,
                     "profile_id" : 1,
                     "is_anyone_living" : self.switch_state,
                     "offering_home_id" : SharedPreference.getOffering_id(),
                     "gender_and_sexuality" : self.id_Gender,
                     "age_group"     : self.id_AgeGroup,
                     "smoking"  : self.id_Smoking,
                     "pets"     : self.id_Pet,
                     "main_interest" : self.strMoreinterest,
                     "personal_introduction" : self.txtPersonalIntro.text!,
                     "personal_qualities" : self.txtPersonalQuality.text!
            ] as [String : Any]
        
        print(param)
        
        CommunicationManager().getResponseForPost(strUrl: Url_File.Offering_AboutOccupants, parameters: param as NSDictionary) { ( result , data) in
            
            if(result == "success") {
                let dataDict = data as! NSDictionary
                print(dataDict)
                let dic = dataDict["response"] as! NSDictionary
                self.GoNext()
                self.stopActivityIndicator()
            }else if (result == "error") {
                let strMessage = (data as! NSDictionary)["message"] as! String
                Common.ShowAlert(Title: Common.Title, Message: strMessage, VC: self)
                self.stopActivityIndicator()
            }else if (result == "Network") {
                let strMessage = (data as! String)
                Common.ShowAlert(Title: Common.Title, Message: strMessage, VC: self)
                self.stopActivityIndicator()
            }
        }
    }
    
    func GoNext() {
        strCheckType="OfferringRoom"
        
        AboutOccupantsViewController.Sid_Gender = self.arrGender as! Array<Any>
        AboutOccupantsViewController.Sid_AgeGroup = self.arrAge as! Array<Any>
        AboutOccupantsViewController.Sid_Smoking = self.arrSmoking as! Array<Any>
        AboutOccupantsViewController.Sid_Pet = self.id_Pet
        AboutOccupantsViewController.SPersonalInfo = self.txtPersonalIntro.text!
        AboutOccupantsViewController.SPersonalquality = self.txtPersonalQuality.text!
        
        Common.PushMethod(VC: self, identifier: "AddPhotoViewController")
    }
}

extension AboutOccupantsViewController : UICollectionViewDelegate , UICollectionViewDataSource , UICollectionViewDelegateFlowLayout{
    
    func numberOfSections(in collectionView: UICollectionView) -> Int {
        return 1
    }
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return collDataArr.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        
        let cell = collAboutOccupants.dequeueReusableCell(withReuseIdentifier: "collAboutOccupantsCell", for: indexPath) as! collAboutOccupantsCell
        
        let dict = collDataArr[indexPath.row] as! [String : String]
        cell.imgSelect.image=UIImage(named: (dict["image"])!)?.imageWithInsets(insetDimen: 8)
        cell.lblName.text=(dict["name"]!)
        
        cell.imgSelect.tintImageColor(color: UIColorFromHex(rgbValue: 0x05C6CF, alpha: 1.0))
        
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        let yourWidth = collectionView.bounds.width/4.0
        return CGSize(width: yourWidth, height: 110.0)
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, insetForSectionAt section: Int) -> UIEdgeInsets {
        return UIEdgeInsets.zero
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumInteritemSpacingForSectionAt section: Int) -> CGFloat {
        return 0
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumLineSpacingForSectionAt section: Int) -> CGFloat {
        return 0
    }
}

class collAboutOccupantsCell: UICollectionViewCell {
    
    @IBOutlet weak var lblName : UILabel!
    @IBOutlet weak var imgSelect : UIImageView!
    
}

extension AboutOccupantsViewController : UITextViewDelegate{
    func textViewShouldBeginEditing(_ textView: UITextView) -> Bool {
        if self.txtPersonalIntro.textColor == UIColor.lightGray {
            self.txtPersonalIntro.text = ""
            self.txtPersonalIntro.textColor = UIColor.black
        }        
        if self.txtPersonalQuality.textColor == UIColor.lightGray {
            self.txtPersonalQuality.text = ""
            self.txtPersonalQuality.textColor = UIColor.black
        }
        return true
    }
}
