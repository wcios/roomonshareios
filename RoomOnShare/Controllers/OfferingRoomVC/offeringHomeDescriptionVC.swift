
import UIKit
import GooglePlacePicker
import CoreLocation



class offeringHomeDescriptionVC: UIViewController , CLLocationManagerDelegate{
    
    
    var locationManager:CLLocationManager!
    
    @IBOutlet weak var collViewHomeFeatures : UICollectionView!
    @IBOutlet weak var viewBedroom : UIView!
    @IBOutlet weak var viewBedroomHeight : NSLayoutConstraint!
    @IBOutlet weak var btnMore : UIButton!
    @IBOutlet weak var viewMain : UIView!
    @IBOutlet weak var viewMainHeight : NSLayoutConstraint!
    @IBOutlet weak var lblHomeLocation : UILabel!
    @IBOutlet weak var txtHomeType : UITextField!
    @IBOutlet weak var txtHomeSize : UITextField!
    @IBOutlet weak var txtHomeAddress : UITextField!
    @IBOutlet weak var txtHomeDescription : UITextView!
    
    @IBOutlet weak var txtBillsRequired : UITextField!
    @IBOutlet var btnBill : [UIButton]!
    @IBOutlet var btnParking : [UIButton]!
    
    var lat : Double = 0000
    var long : Double = 0000

    //Bedroom description
    
    @IBOutlet weak var txtbedroomtype: UITextField!
    @IBOutlet weak var lblDateAvail : UILabel!
    @IBOutlet weak var lblMin : UILabel!
    @IBOutlet weak var lblMax : UILabel!
    @IBOutlet weak var lblSinglePerson : UILabel!
    @IBOutlet weak var lblCouple : UILabel!
    @IBOutlet weak var lblRentInclude : UILabel!
    @IBOutlet weak var lblBillInclude : UILabel!
    @IBOutlet weak var lblBedroomSize : UILabel!
    @IBOutlet weak var lblBedroomFurniture : UILabel!
    @IBOutlet weak var lblNoOfBeds : UILabel!
    @IBOutlet weak var lblBathroomFacility : UILabel!
    @IBOutlet weak var lblDeposit : UILabel!
    @IBOutlet weak var lblRoomFeature : UILabel!
    @IBOutlet weak var lblOtherInfo : UILabel!
    
    @IBOutlet weak var Stakbedroomsize: UIStackView!
    @IBOutlet weak var Stakbedroomfurniture: UIStackView!
    @IBOutlet weak var Stakbathroomfacility: UIStackView!
    @IBOutlet weak var StakNoofbeds: UIStackView!
    
    @IBOutlet weak var viewbilldetail: UIView!
    
    @IBOutlet weak var Constraintbilldetail: NSLayoutConstraint!
    
    
    var id_Bill : Int = 0
    var id_Parking : Int = 0
    var id_HomeType : Int = 0
    var id_HomeSize : Int = 0
    var strHomeFeatures : String = ""
    var _selectedCells : NSMutableArray = []
    var dictBedroomDescription : [String : Any] = [:]
    var arrBedroomData : Array<Any> = []
    
    static var Shomeaddress = ""
    static var Shomelocation = ""
    static var Shometype = 0
    static var Shomesize = 0
    static var Shomefeature : Array<Any> = []
    static var SParking  = 0
    static var Shomedescription = ""
    static var Sbill = 0
    static var Sbillinclude = ""
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.txtHomeDescription.delegate = self
        self.txtHomeDescription.text = "Home Description(Excluded Bedroom)"
        self.txtHomeDescription.textColor = UIColor.lightGray
        
        collViewHomeFeatures.allowsMultipleSelection=true
        self.determineMyCurrentLocation()
        let tap = UITapGestureRecognizer(target: self, action: #selector(self.handleTap(_:)))
        lblHomeLocation.addGestureRecognizer(tap)
        lblHomeLocation.isUserInteractionEnabled = true
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        self.navigationController?.isNavigationBarHidden = true
        rdv_tabBarController?.setTabBarHidden(false, animated: true)
        
        if userDef.value(forKey: "bedroomarray") as? Array<Any> != nil{
        }
        self.callBedroomData()
        self.showData()
    }
    
    func showData(){
        
        if offeringHomeDescriptionVC.Shometype != 0 {
            self.txtHomeAddress.text = offeringHomeDescriptionVC.Shomeaddress
            self.lblHomeLocation.text = offeringHomeDescriptionVC.Shomelocation
            self.id_HomeType = offeringHomeDescriptionVC.Shometype
            self.id_HomeSize = offeringHomeDescriptionVC.Shomesize
            self.id_Parking = offeringHomeDescriptionVC.SParking
            self.txtHomeDescription.text = offeringHomeDescriptionVC.Shomedescription
            self.id_Bill = offeringHomeDescriptionVC.Sbill
            self.txtBillsRequired.text = offeringHomeDescriptionVC.Sbillinclude
            
            let arrG = offeringHomeDescriptionVC.Shomefeature
            for item in arrG{
                if item as! Int != 0{
                    self._selectedCells.add(item)
                }
            }
            self.strHomeFeatures = self._selectedCells.componentsJoined(by: ",")
            self.collViewHomeFeatures.reloadData()
            
            self.txtHomeType.text = Array_File.arrHomeType[self.id_HomeType - 1]
            self.txtHomeSize.text = Array_File.arrHomeSize[self.id_HomeSize - 1]
            
            if self.id_Bill == 1{
                self.Constraintbilldetail.constant = 75
                self.viewbilldetail.isHidden = false
            }else{
                self.Constraintbilldetail.constant = 0
                self.viewbilldetail.isHidden = true
            }
            self.CheckUnceck(arrButton: self.btnBill, Selected: self.id_Bill)
            self.CheckUnceck(arrButton: self.btnParking, Selected: self.id_Parking)
        }
        
    }
    
    // function which is triggered when handleTap is called
    @objc func handleTap(_ sender: UITapGestureRecognizer) {
        let center = CLLocationCoordinate2D(latitude: lat, longitude: long)
        let northEast = CLLocationCoordinate2D(latitude: center.latitude + 0.001, longitude: center.longitude + 0.001)
        let southWest = CLLocationCoordinate2D(latitude: center.latitude - 0.001, longitude: center.longitude - 0.001)
        let viewport = GMSCoordinateBounds(coordinate: northEast, coordinate: southWest)
                
        let config = GMSPlacePickerConfig(viewport: viewport)
        let placePicker = GMSPlacePickerViewController(config: config)
        placePicker.delegate = self
        self.present(placePicker, animated: true, completion: nil)
    }
    
    @IBAction func clickOnReadMoreBtn(_ sender : UIButton){
        Common.PushMethod(VC: self, identifier: "detailBedroomDescriptionVC")
    }
    
    @IBAction func clickOnBAckBtn(_ sender : UIButton){
        Common.PopMethod(VC: self)
    }
    
    ///////// delete bedroom ///////
    @IBAction func clickOnBedRoomDel(_ sender : UIButton){
        let alert = UIAlertController(title: "Room On Share", message:
            "Are you sure, you want to delete this profile", preferredStyle: UIAlertControllerStyle.alert)
        let okAction = UIAlertAction(title: "Yes", style: UIAlertActionStyle.default) { (action) in
            self.arrBedroomData.remove(at: 0)
            if(self.arrBedroomData.count > 0){
                userDef.set(self.arrBedroomData, forKey: "bedroomarray")
            }else{
                userDef.removeObject(forKey: "bedroomarray")
            }
            self.callBedroomData()
        }
        
        let CancleAction = UIAlertAction(title: "No", style: UIAlertActionStyle.default) { (action) in
            
        }
        
        alert.addAction(okAction)
        alert.addAction(CancleAction)
        self.present(alert, animated: true, completion: nil)
    }
    
    
    func determineMyCurrentLocation() {
        locationManager = CLLocationManager()
        locationManager.delegate = self
        locationManager.desiredAccuracy = kCLLocationAccuracyBest
        locationManager.requestAlwaysAuthorization()
        
        if CLLocationManager.locationServicesEnabled() {
            locationManager.startUpdatingLocation()
        }
        
        locationManager.startUpdatingLocation()
    }
    
    
    func locationManager(_ manager: CLLocationManager, didUpdateLocations locations: [CLLocation]) {
        let userLocation:CLLocation = locations[0] as CLLocation
        
        lat=userLocation.coordinate.latitude
        long=userLocation.coordinate.longitude
        
        print("user latitude = \(userLocation.coordinate.latitude)")
        print("user longitude = \(userLocation.coordinate.longitude)")
        
        locationManager.stopUpdatingLocation()
    }
    
    func locationManager(_ manager: CLLocationManager, didFailWithError error: Error)
    {
        print("Error \(error.localizedDescription)")
    }
    
    func callBedroomData(){
        
        if userDef.value(forKey: "bedroomarray") as? Array<Any> != nil{
            self.arrBedroomData = userDef.value(forKey: "bedroomarray") as! Array<Any>
            
            self.viewBedroom.isHidden=false
            if self.viewMainHeight.constant == 1600{
                self.viewBedroomHeight.constant=340
                self.viewMainHeight.constant = self.viewMainHeight.constant + self.viewBedroomHeight.constant
            }
            if((arrBedroomData.count) == 1){
                btnMore.isHidden=true
            }else {
                btnMore.isHidden=false
            }
            
            let dict = arrBedroomData[0] as! NSDictionary
            print(dict)
            self.lblDateAvail.text = "\(dict["available_date"] as! String)"
            self.lblMin.text = "\(Array_File.arrMinWeek[dict["minimum_period"] as! Int - 1])"
            self.lblMax.text = "\(Array_File.arrMaxWeek[dict["maximum_period"] as! Int - 1])"
            self.lblSinglePerson.text = "\(dict["single_person_rent"] as! String)"
            self.lblCouple.text = "\(dict["couple_rent"] as! String)"
            
            let bills = dict["bills"] as! Int
            if bills == 1{
                self.lblRentInclude.text="yes"
            }else{
                self.lblRentInclude.text="no"
            }
            
            let bedroom_type = dict["bedroom_type"] as! Int
            self.txtbedroomtype.text = Array_File.arrBedroomtype[bedroom_type - 1]
            if(bedroom_type==2){
                self.Stakbedroomsize.isHidden=true
                self.Stakbedroomfurniture.isHidden=true
                self.Stakbathroomfacility.isHidden=true
                self.StakNoofbeds.isHidden=false
                lblNoOfBeds.text="\(Array_File.arrNoOfBeds[dict["no_of_beds"] as! Int - 1]) beds"
            }else{
                self.Stakbedroomsize.isHidden=false
                self.Stakbedroomfurniture.isHidden=false
                self.Stakbathroomfacility.isHidden=false
                self.StakNoofbeds.isHidden=true
                lblBedroomSize.text="\(Array_File.arrBedroomSize[dict["bedroom_size"] as! Int - 1])"
                lblBedroomFurniture.text="\(Array_File.arrBedroomFurniture[dict["bedroom_furniture"] as! Int - 1])"
                let bath = dict["bathroom_facilities"] as! Int
                if bath == 1{
                    lblBathroomFacility.text="Shared bathroom"
                }else{
                    lblBathroomFacility.text="Own bathroom"
                }
            }
            
            let roomFeature = dict["room_features"] as! String
            let fullArr = roomFeature.components(separatedBy: ",")
            print(fullArr)
            let  arrname : NSMutableArray = []
            
            for i in 0...Array_File.arrroomFeaturesImgs.count - 1{
                for item in fullArr{
                    let id = Int(item as String)
                    if i == id {
                        let dic = Array_File.arrroomFeaturesImgs[i-1] as NSDictionary
                        let name = dic.value(forKey: "name") as! String
                        arrname.add(name)
                        print(arrname)
                    }
                }
            }
            self.lblRoomFeature.text = arrname.componentsJoined(by: ",")
            self.lblDeposit.text = Array_File.arrDeposit[dict["deposit"] as! Int - 1]
            self.lblOtherInfo.text = "\(dict["other_information"] as! String)"
        }
        else{
            self.viewBedroom.isHidden=true
            self.viewBedroomHeight.constant=0
            self.viewMainHeight.constant=1600
        }
    }
    
    @IBAction func clickOnBedRoomEdit(_ sender : UIButton){
        let story = UIStoryboard(name: "Main", bundle: nil)
        let detail = story.instantiateViewController(withIdentifier: "BedroomDescriptionViewController") as! BedroomDescriptionViewController
        detail.arrIndex = 0
        detail.strIsEdit = "true"
        self.navigationController?.pushViewController(detail, animated: true)
    }
    
    @IBAction func clickOnNextBtn(_ sender : UIButton){
        
        if(txtHomeAddress.text == "" || txtHomeAddress.text?.count==0){
            Common.ShowAlert(Title: Common.Title, Message: "Please enter a valid address.", VC: self)
        }else if(txtHomeType.text == "Select Home Type"){
            Common.ShowAlert(Title: Common.Title, Message: "Home type - Please make a selection.", VC: self)
        }else if(txtHomeSize.text == "Select Home Size"){
            Common.ShowAlert(Title: Common.Title, Message: "Home size - Please make a selection.", VC: self)
        }else if(self.strHomeFeatures == ""){
            Common.ShowAlert(Title: Common.Title, Message: "Home features - Please make a selection.", VC: self)
        }else if(self.id_Parking==0){
            Common.ShowAlert(Title: Common.Title, Message: "Parking facilities - Please make a selection.", VC: self)
        }else if(self.txtHomeDescription.text == "" || self.txtHomeDescription.text?.count==0){
            Common.ShowAlert(Title: Common.Title, Message: "Describe the home (excluding bedrooms) - Please answer this question. You can edit your profile to add more information later.", VC: self)
        }else if(self.id_Bill==0){
            Common.ShowAlert(Title: Common.Title, Message: "Bill details - Please make a selection.", VC: self)
        }else if(self.txtBillsRequired.text == "" || self.txtBillsRequired.text?.count==0){
            Common.ShowAlert(Title: Common.Title, Message: "What are the bills? - Please answer this question. You can edit your profile to add more information later.", VC: self)
        }else if self.arrBedroomData.count==0{
            Common.ShowAlert(Title: Common.Title, Message: "Please add atleast one bedroom", VC: self)
        }else if reach.isReachable || reach.isReachableViaWiFi || reach.isReachableViaWWAN{
            self.callOfferingHomeApi()
        }
        else{
            Common.ShowAlert(Title: Common.Title, Message: "Please check your internet connection.", VC: self)
        }
    }
    
    @IBAction func clickOnAddBedroonBtn(_ sender : UIButton){
        if self.id_HomeSize != 0{
            if self.id_HomeSize == 1 || self.id_HomeSize == 2 || self.id_HomeSize == 3{
                if self.arrBedroomData.count < self.id_HomeSize {
                    self.Addbedrooms()
                }else{
                    Common.ShowAlert(Title: Common.Title, Message: "Maximum bedrooms added", VC: self)
                }
            }else{
                self.Addbedrooms()
            }
        }else{
            Common.ShowAlert(Title: Common.Title, Message: "Please select home size", VC: self)
        }
    }
    
    func Addbedrooms()  {
        let story = UIStoryboard(name: "Main", bundle: nil)
        let detail = story.instantiateViewController(withIdentifier: "BedroomDescriptionViewController") as! BedroomDescriptionViewController
        detail.strIsEdit = "false"
        self.navigationController?.pushViewController(detail, animated: true)
    }
    
    @IBAction func BillMethod(_ sender: UIButton) {
        let Selectedtag = sender.tag
        self.id_Bill=Selectedtag
        
        if Selectedtag == 1{
            self.Constraintbilldetail.constant = 75
            self.viewbilldetail.isHidden = false
        }else{
            self.Constraintbilldetail.constant = 0
            self.viewbilldetail.isHidden = true
        }
        
        self.CheckUnceck(arrButton: self.btnBill, Selected: Selectedtag)
    }
    
    @IBAction func ParkingMethod(_ sender: UIButton) {
        let Selectedtag = sender.tag
        self.id_Parking=Selectedtag
        self.CheckUnceck(arrButton: self.btnParking, Selected: Selectedtag)
    }
    
    func CheckUnceck(arrButton : [UIButton], Selected : Int) {
        for button in arrButton {
            let tag = button.tag
            if Selected == tag{
                button.isSelected = true
            }else{
                button.isSelected = false
            }
        }
    }
    
    func callOfferingHomeApi(){
        self.startActivityIndicator()
        
        let arrBedroomData = userDef.value(forKey: "bedroomarray") as? Array<Any>
        let userid = SharedPreference.getUserData().user_id
        var param : [String : Any] = [:]
        
        
        if SharedPreference.getOffering_id() != 0{
            param = ["user_id" : userid,
                     "profile_id" : 1,
                     "offering_home_id" : SharedPreference.getOffering_id(),
                     "home_address": "\(txtHomeAddress.text!)",
                "home_type": self.id_HomeType,
                "home_size": self.id_HomeSize,
                "home_features": self.strHomeFeatures,
                "home_description": "\(txtHomeDescription.text!)",
                "parking_facility": self.id_Parking,
                "bills": self.id_Bill,
                "bills_included": "\(txtBillsRequired.text!)",
                "latitude" : self.lat,
                "longitude" : self.long,
                "home_location" : self.lblHomeLocation.text!,
                "bedroom": arrBedroomData!
                ] as [String : Any]
        }else{
        param = ["user_id" : userid,
                     "profile_id" : 1,
                     "home_address": "\(txtHomeAddress.text!)",
            "home_type": self.id_HomeType,
            "home_size": self.id_HomeSize,
            "home_features": self.strHomeFeatures,
            "home_description": "\(txtHomeDescription.text!)",
            "parking_facility": self.id_Parking,
            "bills": self.id_Bill,
            "bills_included": "\(txtBillsRequired.text!)",
            "latitude" : self.lat,
            "longitude" : self.long,
            "home_location" : self.lblHomeLocation.text!,
            "bedroom": arrBedroomData!
            ] as [String : Any]
        }
        print(param)
        
        CommunicationManager().getResponseForPost(strUrl: Url_File.url_offering_home, parameters: param as NSDictionary) { ( result , data) in
            
            if(result == "success") {
                let dataDict = data as! NSDictionary
                print(dataDict)
                let responseData = dataDict["response"] as! NSDictionary
                SharedPreference.setOffering_id(responseData.value(forKey: "offering_home_id") as! Int)
                print(SharedPreference.getOffering_id())
                self.GoNext()
                self.stopActivityIndicator()
            }else if (result == "error") {
                let strMessage = (data as! NSDictionary)["message"] as! String
                Common.ShowAlert(Title: Common.Title, Message: strMessage, VC: self)
                self.stopActivityIndicator()
            }else if (result == "Network") {
                let strMessage = (data as! String)
                Common.ShowAlert(Title: Common.Title, Message: strMessage, VC: self)
                self.stopActivityIndicator()
            }
        }
    }
    
    func GoNext() {
        
        offeringHomeDescriptionVC.Shomeaddress = self.txtHomeAddress.text!
        offeringHomeDescriptionVC.Shomelocation = self.lblHomeLocation.text!
        offeringHomeDescriptionVC.Shometype = self.id_HomeType
        offeringHomeDescriptionVC.Shomesize = self.id_HomeSize
        offeringHomeDescriptionVC.Shomefeature = self._selectedCells as! Array<Any>
        offeringHomeDescriptionVC.SParking = self.id_Parking
        offeringHomeDescriptionVC.Shomedescription = self.txtHomeDescription.text!
        offeringHomeDescriptionVC.Sbill = self.id_Bill
        offeringHomeDescriptionVC.Sbillinclude = self.txtBillsRequired.text!
        
        Common.PushMethod(VC: self, identifier: "AboutOccupantsViewController")
    }
    
}

extension offeringHomeDescriptionVC : UICollectionViewDelegate , UICollectionViewDataSource ,UICollectionViewDelegateFlowLayout{
    
    func numberOfSections(in collectionView: UICollectionView) -> Int {
        return 1
    }
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return Array_File.arrHomeDescriptionOptionImgs.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        
        let cell = collViewHomeFeatures.dequeueReusableCell(withReuseIdentifier: "homeFeaturesCollCell", for: indexPath) as! homeFeaturesCollCell
        let dict = Array_File.arrHomeDescriptionOptionImgs[indexPath.row]
        cell.imgFeatures.image=UIImage(named: (dict["image"])!)?.imageWithInsets(insetDimen: 8)
        cell.lblIconName.text=(dict["name"]!)
        
        if _selectedCells.contains(indexPath.row + 1) {
            cell.isSelected=true
            collViewHomeFeatures.selectItem(at: indexPath, animated: true, scrollPosition: UICollectionViewScrollPosition.top)
            cell.imgFeatures.tintImageColor(color: UIColorFromHex(rgbValue: 0x05C6CF, alpha: 1.0))
        }else{
            cell.isSelected=false
            cell.imgFeatures.tintImageColor(color: .lightGray)
        }
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath){
        _selectedCells.add(indexPath.row + 1)
        print(_selectedCells)
        self.strHomeFeatures = _selectedCells.componentsJoined(by: ",")
        collViewHomeFeatures.reloadItems(at: [indexPath])
    }
    
    func collectionView(_ collectionView: UICollectionView, didDeselectItemAt indexPath: IndexPath) {
        _selectedCells.remove(indexPath.row + 1)
        print(_selectedCells)
        self.strHomeFeatures = _selectedCells.componentsJoined(by: ",")
        collViewHomeFeatures.reloadItems(at: [indexPath])
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        let yourWidth = collectionView.bounds.width/4.0
        return CGSize(width: yourWidth, height: 100.0)
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, insetForSectionAt section: Int) -> UIEdgeInsets {
        return UIEdgeInsets.zero
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumInteritemSpacingForSectionAt section: Int) -> CGFloat {
        return 0
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumLineSpacingForSectionAt section: Int) -> CGFloat {
        return 0
    }
}

class homeFeaturesCollCell: UICollectionViewCell {
    @IBOutlet weak var imgFeatures : UIImageView!
    @IBOutlet weak var lblIconName : UILabel!
}
extension offeringHomeDescriptionVC : UITextFieldDelegate, MyPickerViewProtocol{
    func textFieldDidBeginEditing(_ textField: UITextField) {
        if textField == self.txtHomeType{
            let myPickerView =  MyPickerView.init(withDictionary: Array_File.arrHomeType, Tag:1)
            myPickerView.delegate = myPickerView
            myPickerView.MyPickerDelegate = self
            self.txtHomeType.inputView = myPickerView
        }else if textField == self.txtHomeSize{
            let myPickerView =  MyPickerView.init(withDictionary: Array_File.arrHomeSize, Tag:2)
            myPickerView.delegate = myPickerView
            myPickerView.MyPickerDelegate = self
            self.txtHomeSize.inputView = myPickerView
        }
    }
    
    func myPickerDidSelectRow(Index:Int?, Tag:Int) {
        if Tag == 1{
            self.id_HomeType = Index! + 1
            let item = Array_File.arrHomeType[Index!]
            self.txtHomeType.text = item
        }else if Tag == 2{
            if Index! + 1 < self.arrBedroomData.count {
                Common.ShowAlert(Title: Common.Title, Message: "Already bedroom added", VC: self)
            }else{
                self.id_HomeSize = Index! + 1
                let item = Array_File.arrHomeSize[Index!]
                self.txtHomeSize.text = item
            }
        }
    }
}

//place api code
extension offeringHomeDescriptionVC : GMSPlacePickerViewControllerDelegate{
    func placePicker(_ viewController: GMSPlacePickerViewController, didPick place: GMSPlace) {
        self.lblHomeLocation.text=place.formattedAddress!
        lat=place.coordinate.latitude
        long=place.coordinate.longitude
        print(place.coordinate.latitude)
        print(place.coordinate.longitude)
        viewController.dismiss(animated: true, completion: nil)
    }
    
    func placePickerDidCancel(_ viewController: GMSPlacePickerViewController) {
        // Dismiss the place picker, as it cannot dismiss itself.
        viewController.dismiss(animated: true, completion: nil)
        print("No place selected")
    }
}

extension offeringHomeDescriptionVC : UITextViewDelegate{
    func textViewShouldBeginEditing(_ textView: UITextView) -> Bool {
        if self.txtHomeDescription.textColor == UIColor.lightGray {
            self.txtHomeDescription.text = ""
            self.txtHomeDescription.textColor = UIColor.black
        }
        return true
    }
}

//                let strMsg = dataDict.object(forKey: "message") as! String

//                let alert = UIAlertController(title: "", message: strMsg, preferredStyle: UIAlertControllerStyle.alert)
//                let okAction = UIAlertAction(title: "OK", style: UIAlertActionStyle.default) { (action) in
//                    SharedPreference.setOffering_id(responseData.value(forKey: "offering_home_id") as! Int)
//                    print(SharedPreference.getOffering_id())
//                    self.GoNext()
//                    self.stopActivityIndicator()
//
//                }
//                alert.addAction(okAction)
//                self.present(alert, animated: true, completion: nil)
