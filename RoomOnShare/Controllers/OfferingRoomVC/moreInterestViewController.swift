

import UIKit

protocol tblSelection {
    func onSelectPassData(arrData : NSMutableArray)
    
}

struct Model {
    var title: String
}




class moreInterestViewController: UIViewController {
    
    //tblcode
    @IBOutlet weak var tblInterest : UITableView!
    @IBOutlet weak var doneButton: UIButton!
    
    var delegate: tblSelection? = nil
    var arrSelectData : [String] = []
    var viewModel = ViewModel()
    
    var dataArray:[Model] = []
    
    
    //coll code
    var _selectedCells : NSMutableArray = []
    @IBOutlet weak var collInterest : UICollectionView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        collInterest.allowsMultipleSelection=true
        
        //           tblInterest.register(UINib(nibName: "interestTableCell", bundle: nil), forCellReuseIdentifier: "interestTableCell")
        //        tblInterest.estimatedRowHeight = 100
        //        tblInterest.rowHeight = UITableViewAutomaticDimension
        //        tblInterest.allowsMultipleSelection = true
        //        tblInterest.dataSource = viewModel
        //        tblInterest.delegate = viewModel
        //        tblInterest.separatorStyle = .none
        //
        //        viewModel.didToggleSelection = { [weak self] hasSelection in
        //            self?.doneButton.isEnabled = hasSelection
        //        }
        
    }
    
    @IBAction func sendDataBackButton(sender: UIButton) {
        
        //        print(viewModel.selectedItems.map { $0.title })
        //        tblInterest.reloadData()
       
        delegate?.onSelectPassData(arrData: _selectedCells)
        self.navigationController?.popViewController(animated: true)
    }
    
    @IBAction func clickOnBack(_ sender : UIButton){
        self.navigationController?.popViewController(animated: true)
    }
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    
    
}

extension moreInterestViewController : UICollectionViewDataSource,UICollectionViewDelegate,UICollectionViewDelegateFlowLayout{
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return Array_File.arrIntesrestOptionImgs.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collInterest.dequeueReusableCell(withReuseIdentifier: "collIntersetCell", for: indexPath) as! collIntersetCell
        
        let dict = Array_File.arrIntesrestOptionImgs[indexPath.row]
        cell.imgInterest.image=UIImage(named: (dict["image"])!)?.imageWithInsets(insetDimen: 8)
        cell.lblIconName.text=(dict["name"]!)
        
        
        if _selectedCells.contains(indexPath.row) {
            cell.isSelected=true
            collInterest.selectItem(at: indexPath, animated: true, scrollPosition: UICollectionViewScrollPosition.top)
            cell.imgInterest.tintImageColor(color: UIColorFromHex(rgbValue: 0x05C6CF, alpha: 1.0))
        }
        else{
            cell.isSelected=false
            cell.imgInterest.tintImageColor(color: .lightGray)
        }
        return cell
        
    }
    
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath){
        
        //add the selected cell contents to _selectedCells arr when cell is selected
        
        _selectedCells.add(indexPath.row)
        print(_selectedCells)
        collInterest.reloadItems(at: [indexPath])
    }
    
    func collectionView(_ collectionView: UICollectionView, didDeselectItemAt indexPath: IndexPath) {
        
        //remove the selected cell contents from _selectedCells arr when cell is De-Selected
        
        _selectedCells.remove(indexPath.row)
        collInterest.reloadItems(at: [indexPath])
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        let yourWidth = collectionView.bounds.width/4.0
        return CGSize(width: yourWidth, height: 110.0)
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, insetForSectionAt section: Int) -> UIEdgeInsets {
        return UIEdgeInsets.zero
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumInteritemSpacingForSectionAt section: Int) -> CGFloat {
        return 0
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumLineSpacingForSectionAt section: Int) -> CGFloat {
        return 0
    }
}

class collIntersetCell: UICollectionViewCell {
    
    @IBOutlet weak var imgInterest : UIImageView!
    @IBOutlet weak var lblIconName : UILabel!
}


