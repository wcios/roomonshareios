//
//  PreferredLoactionViewController.swift
//  RoomOnShare
//
//  Created by mac on 03/08/18.
//  Copyright © 2018 WestCoast. All rights reserved.
//

import UIKit
import GoogleMaps
import GooglePlaces
import CoreLocation
import GooglePlacePicker

class PreferredLoactionViewController: UIViewController {

    @IBOutlet weak var imgLocationLogo: UIImageView!
    @IBOutlet weak var viewBG: UIView!
    @IBOutlet weak var viewMap: GMSMapView!
    @IBOutlet weak var lblAddress: UILabel!
    
    var userdata = SharedPreference.getUserData()
    var locationManager:CLLocationManager!
    var check : String = ""
    
    var latitude : Double = 0000
    var longitude : Double = 0000
    
    static var Slatitude : Double = 0000
    static var Slongitude : Double = 0000
    static var Saddress = ""

    
    override func viewDidLoad() {
        super.viewDidLoad()
        
       // placePickerController.delegate = self
        
        determineMyCurrentLocation()
        
        if self.check == "AddPhoto" {
         self.viewBG.isHidden = true
        }else{
         self.viewBG.isHidden = false
        }
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    override func viewWillAppear(_ animated: Bool) {
        if PreferredLoactionViewController.Slatitude != 0000 {
            self.viewBG.isHidden = false
            self.latitude = PreferredLoactionViewController.Slatitude
            self.longitude = PreferredLoactionViewController.Slongitude
            self.lblAddress.text = PreferredLoactionViewController.Saddress
            ShowMarker(lati: self.latitude, long: self.longitude)
        }
    }
    
    @IBAction func BackMethod(_ sender: Any) {
        Common.PopMethod(VC: self)
    }
    
    @IBAction func SelectLocationMethod(_ sender: Any) {
        self.handleTap()
       //present(placePickerController, animated: true, completion: nil)
    }
    
    @IBAction func EditLocationMethod(_ sender: Any) {
        self.handleTap()
        //present(placePickerController, animated: true, completion: nil)
    }
    
    
    func handleTap() {
        let center = CLLocationCoordinate2D(latitude: self.latitude, longitude: self.longitude)
        let northEast = CLLocationCoordinate2D(latitude: center.latitude + 0.001, longitude: center.longitude + 0.001)
        let southWest = CLLocationCoordinate2D(latitude: center.latitude - 0.001, longitude: center.longitude - 0.001)
        let viewport = GMSCoordinateBounds(coordinate: northEast, coordinate: southWest)
        
        let config = GMSPlacePickerConfig(viewport: viewport)
        let placePicker = GMSPlacePickerViewController(config: config)
        placePicker.delegate = self
        self.present(placePicker, animated: true, completion: nil)
    }
    
    @IBAction func NextMethod(_ sender: Any) {
        
        //Common.PushMethod(VC: self, identifier: "HomePreferencesViewController")
        
        guard self.latitude != 0000 && self.longitude != 0000 else {
            return Common.ShowAlert(Title: Common.Title, Message: "Please select preferred location", VC: self)
        }

        self.PreferredLocationService()
    }
    
////////////// Preferred Location Service   ///////////
    func PreferredLocationService(){
        self.startActivityIndicator()
        let param = ["user_id"    : userdata.user_id,
                     "profile_id" : 2,
                     "looking_home_id" : SharedPreference.getLooking_id(),
                     "latitude"   : self.latitude,
                     "longitude"  : self.longitude,
                     "address"    : self.lblAddress.text!
            ] as [String : Any]
    
        CommunicationManager().getResponseForPost(strUrl: Url_File.Looking_PreferredLocation, parameters: param as NSDictionary) { ( result , data) in
            
            if(result == "success") {
                let dataDict = data as! NSDictionary
                print(dataDict)
                let dic = dataDict["response"] as! NSDictionary
                self.GoNext()
                self.stopActivityIndicator()
            }else if (result == "error") {
                let strMessage = (data as! NSDictionary)["message"] as! String
                Common.ShowAlert(Title: Common.Title, Message: strMessage, VC: self)
                self.stopActivityIndicator()
            }else if (result == "Network") {
                let strMessage = (data as! String)
                Common.ShowAlert(Title: Common.Title, Message: strMessage, VC: self)
                self.stopActivityIndicator()
            }
        }
    }
    
    func GoNext() {
         PreferredLoactionViewController.Slatitude = self.latitude
         PreferredLoactionViewController.Slongitude = self.longitude
         PreferredLoactionViewController.Saddress = self.lblAddress.text!
         Common.PushMethod(VC: self, identifier: "HomePreferencesViewController")
    }
}

////////////// Location   ///////////
extension PreferredLoactionViewController : CLLocationManagerDelegate{
    func determineMyCurrentLocation() {
        locationManager = CLLocationManager()
        locationManager.delegate = self
        locationManager.desiredAccuracy = kCLLocationAccuracyBest
        locationManager.requestAlwaysAuthorization()
        if CLLocationManager.locationServicesEnabled() {
            locationManager.startUpdatingLocation()
        }
    }
    
    func locationManager(_ manager: CLLocationManager, didUpdateLocations locations: [CLLocation]) {
        let userLocation:CLLocation = locations[0] as CLLocation
        print("user latitude = \(userLocation.coordinate.latitude)")
        print("user longitude = \(userLocation.coordinate.longitude)")
        self.latitude = userLocation.coordinate.latitude
        self.longitude = userLocation.coordinate.longitude
    }
    
    func locationManager(_ manager: CLLocationManager, didFailWithError error: Error)
    {
        print("Error \(error)")
    }
}

////////////// Place picker   ///////////
extension PreferredLoactionViewController : GMSPlacePickerViewControllerDelegate{
    
    func placePicker(_ viewController: GMSPlacePickerViewController, didPick place: GMSPlace) {
        
        self.lblAddress.text = place.formattedAddress
        ShowMarker(lati: place.coordinate.latitude, long: place.coordinate.longitude)        
        self.viewBG.isHidden = false
        print(place.coordinate.latitude)
        print(place.coordinate.longitude)
        viewController.dismiss(animated: true, completion: nil)
    }

    func placePickerDidCancel(_ viewController: GMSPlacePickerViewController) {
        // Dismiss the place picker, as it cannot dismiss itself.
        viewController.dismiss(animated: true, completion: nil)
        print("No place selected")
    }
    
    func ShowMarker(lati : Double, long : Double) {
        let camera = GMSCameraPosition.camera(withLatitude: lati, longitude: long, zoom: 6.0)
        self.viewMap.camera = camera
        
        // Creates a marker in the center of the map.
        let marker = GMSMarker()
        marker.position = CLLocationCoordinate2D(latitude: lati, longitude: long)
        marker.map = self.viewMap
    }
}



//extension PreferredLoactionViewController: GMSPlacePickerViewControllerDelegate {
//
//    // Handle the user's selection.
//    func viewController(_ viewController: GMSAutocompleteViewController, didAutocompleteWith place: GMSPlace) {
//        print("Place name: \(place.coordinate.latitude)")
//        print("Place name: \(place.coordinate.longitude)")
//        print("Place name: \(place.name)")
//        print("Place address: \(place.formattedAddress)")
//       // print("Place attributions: \(place.attributions)")
//
//        self.lblAddress.text = place.formattedAddress
//        ShowMarker(lati: place.coordinate.latitude, long: place.coordinate.longitude)
//
//        self.viewBG.isHidden = false
//        dismiss(animated: true, completion: nil)
//    }
//
//    func viewController(_ viewController: GMSAutocompleteViewController, didFailAutocompleteWithError error: Error) {
//        // TODO: handle the error.
//        print("Error: ", error.localizedDescription)
//    }
//
//    // User canceled the operation.
//    func wasCancelled(_ viewController: GMSAutocompleteViewController) {
//        dismiss(animated: true, completion: nil)
//    }
//
//    // Turn the network activity indicator on and off again.
//    func didRequestAutocompletePredictions(_ viewController: GMSAutocompleteViewController) {
//        UIApplication.shared.isNetworkActivityIndicatorVisible = true
//    }
//
//    func didUpdateAutocompletePredictions(_ viewController: GMSAutocompleteViewController) {
//        UIApplication.shared.isNetworkActivityIndicatorVisible = false
//    }
//
//
//    func ShowMarker(lati : Double, long : Double) {
//        let camera = GMSCameraPosition.camera(withLatitude: lati, longitude: long, zoom: 6.0)
//        self.viewMap.camera = camera
//
//        // Creates a marker in the center of the map.
//        let marker = GMSMarker()
//        marker.position = CLLocationCoordinate2D(latitude: lati, longitude: long)
//        marker.map = self.viewMap
//    }
//
//}
