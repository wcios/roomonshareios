//
//  HomePreferencesViewController.swift
//  RoomOnShare
//
//  Created by mac on 08/08/18.
//  Copyright © 2018 WestCoast. All rights reserved.
//

import UIKit

class HomePreferencesViewController: UIViewController {

    @IBOutlet weak var sliderRent: UISlider!
    @IBOutlet weak var lblShowRent: UILabel!
    @IBOutlet weak var txtBedroomType: UITextField!
    @IBOutlet var HomeSize: [UIButton]!
    @IBOutlet var BedroomSize: [UIButton]!
    @IBOutlet var btnBathroomFacility: [UIButton]!
    @IBOutlet var btnParking: [UIButton]!
    
    var Rent : Int = 0
    var Bedroom_type : Int = 0
    var Home_size = ""
    var Bedroom_size = ""
    var Bathroom_facilities : Int = 0
    var Parking_facility : Int = 0
    
    var arrHomesize : NSMutableArray = []
    var arrBedroomsize : NSMutableArray = []
    
    static var SRent : Int = 0
    static var SBedroom_type : Int = 0
    static var SHome_size : Array<Any> = []
    static var SBedroom_size : Array<Any> = []
    static var SBathroom_facilities : Int = 0
    static var SParking_facility : Int = 0
    
    var userdata = SharedPreference.getUserData()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.txtBedroomType.delegate = self
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    override func viewWillAppear(_ animated: Bool) {
        
        if HomePreferencesViewController.SRent != 0{
            
            self.Rent = HomePreferencesViewController.SRent
            self.Bedroom_type = HomePreferencesViewController.SBedroom_type
            self.Bathroom_facilities = HomePreferencesViewController.SBathroom_facilities
            self.Parking_facility = HomePreferencesViewController.SParking_facility
            let arrhoems = HomePreferencesViewController.SHome_size
            
            for item in arrhoems{
                if item as! Int != 0{
                   self.arrHomesize.add(item)
                }
            }
            self.Home_size = self.arrHomesize.componentsJoined(by: ",")
            
            let arrbedd = HomePreferencesViewController.SHome_size
            for item in arrbedd{
                if item as! Int != 0{
                    self.arrBedroomsize.add(item)
                }
            }
            self.Bedroom_size = self.arrBedroomsize.componentsJoined(by: ",")
            
            
            self.sliderRent.setValue(Float(HomePreferencesViewController.SRent), animated: true)
            if HomePreferencesViewController.SBedroom_type != 0{
                self.txtBedroomType.text = Array_File.arrBedroom_type[HomePreferencesViewController.SBedroom_type - 1]
            }
            
            self.lblShowRent.text = "$\(HomePreferencesViewController.SRent)/week"
            
            self.ShowCheckUnceck(arrButton: self.btnBathroomFacility, Selected: HomePreferencesViewController.SBathroom_facilities)
          
            self.ShowCheckUnceck(arrButton: self.btnParking, Selected: HomePreferencesViewController.SParking_facility)
            
            let arrhoem = HomePreferencesViewController.SHome_size
            for item in arrhoem{
                if item as! Int != 0{
                    self.ShowCheckUnceck(arrButton: self.HomeSize, Selected: item as! Int)
                }
            }
            
            let arrbed = HomePreferencesViewController.SBedroom_size //.components(separatedBy: ",")
            for item in arrbed{
                if item as! Int != 0{
                    self.ShowCheckUnceck(arrButton: self.BedroomSize, Selected: item as! Int)
                }
            }
        }
    }
    
    func ShowCheckUnceck(arrButton : [UIButton], Selected : Int) {
        for button in arrButton {
            let tag = button.tag
            if Selected == tag{
                button.isSelected = true
            }
        }
    }
    
    @IBAction func BackMethod(_ sender: Any) {
        Common.PopMethod(VC: self)
    }
    
    @IBAction func RentMethod(_ sender: UISlider) {
        let rent = sender.value
        self.Rent = Int(rent)
        self.lblShowRent.text = "$\(rent)/month"
        print(rent)
    }
    
    @IBAction func HomeSizeMethod(_ sender: UIButton) {
        let Selectedtag = sender.tag
        if sender.isSelected {
            sender.isSelected = false
            arrHomesize.remove(Selectedtag)
        }else{
            sender.isSelected = true
            arrHomesize.add(Selectedtag)
        }
        self.Home_size = arrHomesize.componentsJoined(by: ",")
        print(self.Home_size)
        
        //        self.Home_size = Selectedtag
        //        self.CheckUnceck(arrButton: self.HomeSize, Selected: Selectedtag)
    }
    
    @IBAction func BedroomSizeMethod(_ sender: UIButton) {
        let Selectedtag = sender.tag
        
        if sender.isSelected {
            sender.isSelected = false
            arrBedroomsize.remove(Selectedtag)
        }else{
            sender.isSelected = true
            arrBedroomsize.add(Selectedtag)
        }
        self.Bedroom_size = arrBedroomsize.componentsJoined(by: ",")
        print(self.Bedroom_size)
        
//        self.Bedroom_size = Selectedtag
//        self.CheckUnceck(arrButton: self.BedroomSize, Selected: Selectedtag)
    }
    
    @IBAction func clickOnBtnBathrrom(_ sender: UIButton) {
        let Selectedtag = sender.tag
        self.Bathroom_facilities = Selectedtag
        self.CheckUnceck(arrButton: self.btnBathroomFacility, Selected: Selectedtag)
    }
    
    @IBAction func clickOnParking(_ sender: UIButton) {
        let Selectedtag = sender.tag
        self.Parking_facility = Selectedtag
        self.CheckUnceck(arrButton: self.btnParking, Selected: Selectedtag)
    }
    
    func CheckUnceck(arrButton : [UIButton], Selected : Int) {
        for button in arrButton {
            let tag = button.tag
            if Selected == tag{
                button.isSelected = true
            }else{
                button.isSelected = false
            }
        }
    }
    
    @IBAction func NextMethod(_ sender: Any) {
        
      //  Common.PushMethod(VC: self, identifier: "FlatmatePreferenceViewController")

        guard self.Rent != 0  else {
            return Common.ShowAlert(Title: Common.Title, Message: "Please select rent budget", VC: self)
        }

        guard self.Bedroom_type != 0   else {
            return Common.ShowAlert(Title: Common.Title, Message: "Please select bedroom type", VC: self)
        }

        guard self.Home_size != ""   else {
            return Common.ShowAlert(Title: Common.Title, Message: "Please select home size", VC: self)
        }

        guard self.Bedroom_size != ""   else {
            return Common.ShowAlert(Title: Common.Title, Message: "Please select bedroom size", VC: self)
        }

        guard self.Bathroom_facilities != 0   else {
            return Common.ShowAlert(Title: Common.Title, Message: "Please select bathroom facility", VC: self)
        }

        guard self.Parking_facility != 0   else{
            return Common.ShowAlert(Title: Common.Title, Message: "Please select parking facility", VC: self)
        }

        self.HomePreferrenceService()
    }
    
    ////////////// Home Preferrence Service   ///////////
    func HomePreferrenceService(){
        self.startActivityIndicator()
        let param = ["user_id"    : userdata.user_id,
                     "profile_id" : 2,
                     "rent_budget" : self.Rent,
                     "bedroom_type" : self.Bedroom_type,
                     "home_size"  : self.Home_size,
                     "bedroom_size" : self.Bedroom_size,
                     "bathroom_facilities" : self.Bathroom_facilities,
                     "parking_facility" : self.Parking_facility,
                     "country_code":SharedPreference.getUserData().user_country,
                     "looking_home_id" : SharedPreference.getLooking_id()
            ] as [String : Any]
        
        CommunicationManager().getResponseForPost(strUrl: Url_File.Looking_HomePreferrence, parameters: param as NSDictionary) { ( result , data) in
            
            if(result == "success") {
                let dataDict = data as! NSDictionary
                print(dataDict)
                let dic = dataDict["response"] as! NSDictionary
                
                self.GoNext()
                self.stopActivityIndicator()
            }else if (result == "error") {
                let strMessage = (data as! NSDictionary)["message"] as! String
                Common.ShowAlert(Title: Common.Title, Message: strMessage, VC: self)
                self.stopActivityIndicator()
            }else if (result == "Network") {
                let strMessage = (data as! String)
                Common.ShowAlert(Title: Common.Title, Message: strMessage, VC: self)
                self.stopActivityIndicator()
            }
        }
    }
    
    func GoNext() {
        HomePreferencesViewController.SRent = self.Rent
        HomePreferencesViewController.SBedroom_type = self.Bedroom_type
        HomePreferencesViewController.SBathroom_facilities = self.Bathroom_facilities
        HomePreferencesViewController.SParking_facility = self.Parking_facility
        HomePreferencesViewController.SHome_size = self.arrHomesize as! Array<Any>
        HomePreferencesViewController.SBedroom_size = self.arrBedroomsize as! Array<Any>
        
        let Vc = self.storyboard?.instantiateViewController(withIdentifier: "FlatmatePreferenceViewController") as! FlatmatePreferenceViewController
        Vc.isCheckFlat="Looking"
        self.navigationController?.pushViewController(Vc, animated: false)
        
    }
    
}

////////////// TextField delegate   ///////////
extension HomePreferencesViewController : UITextFieldDelegate, MyPickerViewProtocol{
    func textFieldDidBeginEditing(_ textField: UITextField) {
        if textField == self.txtBedroomType{
            let myPickerView =  MyPickerView.init(withDictionary: Array_File.arrBedroom_type, Tag:1)
            myPickerView.delegate = myPickerView
            myPickerView.MyPickerDelegate = self
            self.txtBedroomType.inputView = myPickerView
        }
    }
    
    func myPickerDidSelectRow(Index:Int?, Tag:Int) {
        if Tag == 1{
            self.Bedroom_type = Index! + 1
            self.txtBedroomType.text = Array_File.arrBedroom_type[Index!]
        }
    }
}

