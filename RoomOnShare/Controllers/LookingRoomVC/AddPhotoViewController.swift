//
//  AddPhotoViewController.swift
//  RoomOnShare
//
//  Created by mac on 02/08/18.
//  Copyright © 2018 WestCoast. All rights reserved.
//

import UIKit

class AddPhotoViewController: UIViewController, UIImagePickerControllerDelegate, UINavigationControllerDelegate, CropViewControllerDelegate {
    
    @IBOutlet weak var img1: UIImageView!
    @IBOutlet weak var img2: UIImageView!
    @IBOutlet weak var img3: UIImageView!
    @IBOutlet weak var img4: UIImageView!
    @IBOutlet weak var img5: UIImageView!
    
    @IBOutlet weak var btn1: UIButton!
    @IBOutlet weak var btn2: UIButton!
    @IBOutlet weak var btn3: UIButton!
    @IBOutlet weak var btn4: UIButton!
    @IBOutlet weak var btn5: UIButton!
    
    var Pickimage : UIImage? = nil
    var id_profile = 0
    var picker = UIImagePickerController()
    var state : Int = 0
    var arrimages : Array<Any> = []
    var userdata = SharedPreference.getUserData()
    
    var arrPhoto : Array<Any> = []
    var arrPhotoID : Array<Any> = []
    static var SarrPhoto : Array<Any> = []
    static var SarrImages : [Images] = []
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.picker.delegate = self
         if (strCheckType=="LookingRoom"){
            self.id_profile = 2
         }else{
            self.id_profile = 1
        }
        
        self.btn1.isHidden = false
        self.btn2.isHidden = true
        self.btn3.isHidden = true
        self.btn4.isHidden = true
        self.btn5.isHidden = true
        ShowImages()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        
    }
    
    func ShowImages()  {
        if AddPhotoViewController.SarrPhoto.count != 0 {
            let arr = AddPhotoViewController.SarrPhoto
            for i in 0...arr.count-1 {
                let dic = arr[i] as! NSDictionary
                let pfotoUrl = dic.value(forKey: "photo") as! String
                switch i + 1{
                case 1 :
                    self.img1.sd_setImage(with: URL(string: pfotoUrl), placeholderImage: UIImage(named: "Banner"))
                    self.btn2.isHidden = false
                case 2 :
                    self.img2.sd_setImage(with: URL(string: pfotoUrl), placeholderImage: UIImage(named: "Banner"))
                    self.btn3.isHidden = false
                case 3 :
                    self.img3.sd_setImage(with: URL(string: pfotoUrl), placeholderImage: UIImage(named: "Banner"))
                    self.btn4.isHidden = false
                case 4 :
                    self.img4.sd_setImage(with: URL(string: pfotoUrl), placeholderImage: UIImage(named: "Banner"))
                    self.btn5.isHidden = false
                case 5 :
                    self.img5.sd_setImage(with: URL(string: pfotoUrl), placeholderImage: UIImage(named: "Banner"))
                default:
                    self.img5.sd_setImage(with: URL(string: ""), placeholderImage: UIImage(named: "Banner"))
                }
            }
        }
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    @IBAction func Btn1Method(_ sender: Any) {
        self.state = 1
        Common.ActionSheetForGallaryAndCamera(Picker: picker, VC: self)
    }
    
    @IBAction func btn2Method(_ sender: Any) {
        self.state = 2
        Common.ActionSheetForGallaryAndCamera(Picker: picker, VC: self)
    }
    
    @IBAction func Btn3Method(_ sender: Any) {
        self.state = 3
        Common.ActionSheetForGallaryAndCamera(Picker: picker, VC: self)
    }
    
    @IBAction func Btn4Method(_ sender: Any) {
        self.state = 4
        Common.ActionSheetForGallaryAndCamera(Picker: picker, VC: self)
    }
    
    @IBAction func Btn5Method(_ sender: Any) {
        self.state = 5
        Common.ActionSheetForGallaryAndCamera(Picker: picker, VC: self)
    }
    
    @IBAction func BackMethod(_ sender: Any) {
        Common.PopMethod(VC: self)
    }
    
    @IBAction func NextMethod(_ sender: Any) {
        
//        if (strCheckType=="LookingRoom"){
//            let Vc = self.storyboard?.instantiateViewController(withIdentifier: "PreferredLoactionViewController") as! PreferredLoactionViewController
//            Vc.check = "AddPhoto"
//            self.navigationController?.pushViewController(Vc, animated: false)
//        }else  if (strCheckType=="OfferringRoom"){
//            let Vc = self.storyboard?.instantiateViewController(withIdentifier: "FlatmatePreferenceViewController") as! FlatmatePreferenceViewController
//            self.navigationController?.pushViewController(Vc, animated: false)
//        }
        
        if AddPhotoViewController.SarrPhoto.count == 0 {
            guard self.arrimages.count > 1 else {
                return Common.ShowAlert(Title: Common.Title, Message: "Please select minimum 2 images", VC: self)
            }
        }
        self.AddYourPhotoService()
    }
    
    ////////////// Add Your Photo Service   ///////////
    func AddYourPhotoService(){
        self.startActivityIndicator()
        var param : [String : Any] = [:]
        if (strCheckType=="LookingRoom"){
            param = ["user_id"          : userdata.user_id,
                     "profile_id"       : self.id_profile,
                     "looking_home_id"  : SharedPreference.getLooking_id()
                ] as [String : Any]
        }else{
            param = ["user_id"          : userdata.user_id,
                     "profile_id"       : self.id_profile,
                     "offering_home_id"  : SharedPreference.getOffering_id()
                ] as [String : Any]
        }
        
        print(self.arrimages.count)
        
        print(param, self.arrPhotoID, self.arrimages)
        CommunicationManager().getResponseForMultipartType(strUrl: Url_File.AddYourPhoto, parameters: param as NSDictionary, imagesData: self.arrimages as! [Data], imageKey: "photo[]",check :"AddPhoto", arrphotoid: self.arrPhotoID) { ( result , data) in
            if(result == "success") {
                self.stopActivityIndicator()
                let dataDict = data as! NSDictionary
                print(dataDict)
                let dic = dataDict["response"] as! NSDictionary
                self.arrPhoto = dic.value(forKey: "upload_photo") as! Array<Any>
                self.GoNext()
            }else if (result == "error") {
                let strMessage = (data as! NSDictionary)["message"] as! String
                Common.ShowAlert(Title: Common.Title, Message: strMessage, VC: self)
                self.stopActivityIndicator()
            }else if (result == "Network") {
                let strMessage = (data as! String)
                Common.ShowAlert(Title: Common.Title, Message: strMessage, VC: self)
                self.stopActivityIndicator()
            }
        }
    }
    
    func GoNext() {
        AddPhotoViewController.SarrPhoto = self.arrPhoto
        self.arrimages = []
        if (strCheckType=="LookingRoom"){
            let Vc = self.storyboard?.instantiateViewController(withIdentifier: "PreferredLoactionViewController") as! PreferredLoactionViewController
            Vc.check = "AddPhoto"
            self.navigationController?.pushViewController(Vc, animated: false)
        }else  if (strCheckType=="OfferringRoom"){
            let Vc = self.storyboard?.instantiateViewController(withIdentifier: "FlatmatePreferenceViewController") as! FlatmatePreferenceViewController
            Vc.isCheckFlat="Offer"
            self.navigationController?.pushViewController(Vc, animated: false)
        }
    }
    
    ////////////// Image picker   ///////////
    func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [String : Any]) {
        
        let image = info[UIImagePickerControllerOriginalImage]
        self.Pickimage = image as? UIImage
        dismiss(animated: true, completion: nil)
        self.OpenEditor()
    }
    
    func Addimage(data : Data, index : Int) {
    
        if self.arrimages.count != 0 {
            if self.arrimages.count >= index{
                self.arrimages.remove(at: index - 1)
                self.arrimages.insert(data, at: index - 1)
            }else{
                self.arrimages.append(data)
            }
        }else{
            self.arrimages.append(data)
        }
        
        if AddPhotoViewController.SarrPhoto.count != 0 {   ////// Testing //////
            let arr = AddPhotoViewController.SarrPhoto
            if self.arrimages.count >= index{
                let dic = arr[index-1] as! NSDictionary
                let photoid = dic.value(forKey: "id") as! Int
                self.arrPhotoID.append(photoid)
            }
        }
    }
    
    func imagePickerControllerDidCancel(_ picker: UIImagePickerController) {
        picker.dismiss(animated: true, completion: nil)
    }
    
    func OpenEditor(){
        guard let image = self.Pickimage else {
            return
        }
        // Use view controller
        let controller = CropViewController()
        controller.delegate = self
        controller.image = image
        let navController = UINavigationController(rootViewController: controller)
        present(navController, animated: false, completion: nil)
    }
    
    // MARK: - CropView
    func cropViewController(_ controller: CropViewController, didFinishCroppingImage image: UIImage) {
    }
    
    func cropViewController(_ controller: CropViewController, didFinishCroppingImage image: UIImage, transform: CGAffineTransform, cropRect: CGRect) {
        
        switch self.state {
        case 1:
            self.img1.image = image as UIImage?
            self.btn2.isHidden = false
        case 2:
            self.img2.image = image as UIImage?
            self.btn3.isHidden = false
        case 3:
            self.img3.image = image as UIImage?
            self.btn4.isHidden = false
        case 4:
            self.img4.image = image as UIImage?
            self.btn5.isHidden = false
        case 5:
            self.img5.image = image as UIImage?
        default:
            self.img1.image =  UIImage(named: "")
        }
        let imgData = UIImageJPEGRepresentation(image , 0.5)!
        self.Addimage(data: imgData, index: self.state)
        
        controller.dismiss(animated: true, completion: nil)
    }
    
    func cropViewControllerDidCancel(_ controller: CropViewController) {
        controller.dismiss(animated: true, completion: nil)
    }
    
}


//{
//    message = "Successfully Submit.";
//    response =     {
//        "upload_photo" =         (
//            {
//                photo = "http://18.223.28.186/photogallry/photogallry/CHAIiQHgSlyeGIpuUEQ0N4eATWEvJ0JTjBtlqbTt.jpeg";
//                "photo_id" = 22;
//        },
//            {
//                photo = "http://18.223.28.186/photogallry/photogallry/zsiMIvxoN1IPFS9v9yd7B1gw5P9fVFgiMK9vcuLA.jpeg";
//                "photo_id" = 23;
//        }
//        );
//    };
//    status = 1;
//}

