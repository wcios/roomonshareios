//
//  IntroduceSelfViewController.swift
//  RoomOnShare
//
//  Created by mac on 01/08/18.
//  Copyright © 2018 WestCoast. All rights reserved.
//

import UIKit

class IntroduceSelfViewController: UIViewController {

    @IBOutlet weak var txtAvalibality: UITextField!
    @IBOutlet weak var txtStay: UITextField!
    @IBOutlet weak var txtQuality: UITextView!    
    
    var userdata = SharedPreference.getUserData()
    var id_Stay : Int = 0
    var strdate = ""
    
    static var date = ""
    static var stay = 0
    static var quality = ""
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.txtQuality.delegate = self
        self.txtAvalibality.delegate = self
        self.txtStay.delegate = self
        self.txtQuality.text = "Important personal quality"
        self.txtQuality.textColor = UIColor.lightGray
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    override func viewWillAppear(_ animated: Bool) {
        if IntroduceSelfViewController.date != ""  {
            
            self.strdate = IntroduceSelfViewController.date
            self.txtAvalibality.text = Common.ChangeDateFormat(Date: IntroduceSelfViewController.date, fromFormat: "yyyy-MM-dd", toFormat: "dd MMM yyyy")
            self.id_Stay = IntroduceSelfViewController.stay
            self.txtStay.text = Array_File.arrLength_of_stay[ self.id_Stay - 1]
            self.txtQuality.text = IntroduceSelfViewController.quality
        }
    }

    ////////////// back  ///////////
    @IBAction func BackMethod(_ sender: Any) {
        Common.PopMethod(VC: self)
    }
    
    ////////////// Next  ///////////
    @IBAction func NextMethod(_ sender: Any) {
        
        strCheckType="LookingRoom"
//        Common.PushMethod(VC: self, identifier: "AddPhotoViewController")
        
        guard let Ava = self.txtAvalibality.text, Ava != ""  else {
            return Common.ShowAlert(Title: Common.Title, Message: "Please select availability date", VC: self)
        }

        guard let Stay = self.txtStay.text, Stay != ""  else {
            return Common.ShowAlert(Title: Common.Title, Message: "Please select preferred length of stay", VC: self)
        }

        guard let quality = self.txtQuality.text, quality != ""  else {
            return Common.ShowAlert(Title: Common.Title, Message: "Please enter personal quality", VC: self)
        }

        self.IntroduceYourSelfService()
    }
    
    ////////////// Introduce YourSelf Service   ///////////
    func IntroduceYourSelfService(){
        self.startActivityIndicator()
        let param = ["user_id"    : userdata.user_id,
                     "profile_id" : 2,
                     "available_date" : self.strdate,
                     "stay_duration"     : self.id_Stay,
                     "personal_qualities"  : self.txtQuality.text!,
                     "looking_home_id" : SharedPreference.getLooking_id()
            ] as [String : Any]
        
        print(param)
        
        CommunicationManager().getResponseForPost(strUrl: Url_File.Looking_IntroduceYourSelf, parameters: param as NSDictionary) { ( result , data) in
            
            if(result == "success") {
                let dataDict = data as! NSDictionary
                print(dataDict)
                _ = dataDict["response"] as! NSDictionary
                self.GoNext()
                self.stopActivityIndicator()
            }else if (result == "error") {
                let strMessage = (data as! NSDictionary)["message"] as! String
                Common.ShowAlert(Title: Common.Title, Message: strMessage, VC: self)
                self.stopActivityIndicator()
            }else if (result == "Network") {
                let strMessage = (data as! String)
                Common.ShowAlert(Title: Common.Title, Message: strMessage, VC: self)
                self.stopActivityIndicator()
            }
        }
    }
        
    func GoNext() {
        IntroduceSelfViewController.date = self.txtAvalibality.text!
        IntroduceSelfViewController.stay = self.id_Stay
        IntroduceSelfViewController.quality = self.txtQuality.text!
        strCheckType="LookingRoom"
        Common.PushMethod(VC: self, identifier: "AddPhotoViewController")
    }
    
}

////////////// textField delegate   ///////////
extension IntroduceSelfViewController : UITextFieldDelegate, MyPickerViewProtocol{
    func textFieldDidBeginEditing(_ textField: UITextField) {
        if textField == self.txtAvalibality{
            let datePickerView = UIDatePicker()
            datePickerView.datePickerMode = .date
            datePickerView.minimumDate = datePickerView.date
            self.txtAvalibality.inputView = datePickerView
            datePickerView.addTarget(self, action: #selector(handleDatePicker(sender:)), for: .valueChanged)
        }
        else if textField == self.txtStay{
            let myPickerView =  MyPickerView.init(withDictionary: Array_File.arrLength_of_stay, Tag:1)
            myPickerView.delegate = myPickerView
            myPickerView.MyPickerDelegate = self
            self.txtStay.inputView = myPickerView
        }
    }
    
    func myPickerDidSelectRow(Index: Int?, Tag: Int) {
        if Tag == 1{
            self.id_Stay = Index! + 1
            self.txtStay.text = Array_File.arrLength_of_stay[Index!]
        }
    }
    
    @objc func handleDatePicker(sender: UIDatePicker) {
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "dd MMM yyyy"
        self.txtAvalibality.text = dateFormatter.string(from: sender.date)
        self.strdate = Common.ChangeDateFormat(Date: self.txtAvalibality.text!, fromFormat: "dd MMM yyyy", toFormat: "yyyy-MM-dd")
    }
}

extension IntroduceSelfViewController : UITextViewDelegate{
    func textViewShouldBeginEditing(_ textView: UITextView) -> Bool {
        if self.txtQuality.textColor == UIColor.lightGray {
            self.txtQuality.text = ""
            self.txtQuality.textColor = UIColor.black
        }
        return true
    }
}

