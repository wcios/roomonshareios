//
//  FlatmatePreferenceViewController.swift
//  RoomOnShare
//
//  Created by mac on 06/08/18.
//  Copyright © 2018 WestCoast. All rights reserved.
//

import UIKit

class FlatmatePreferenceViewController: UIViewController {
    
    @IBOutlet var Gender: [UIButton]!
    @IBOutlet var AgeGroup: [UIButton]!
    @IBOutlet var Smoking: [UIButton]!
    @IBOutlet var Pets: [UIButton]!
    
    var id_Gender = ""
    var id_AgeGroup = ""
    var id_Pet  = ""
    var id_Smoking = ""
    
    var isCheckFlat = ""
    var id_profile = 0
    
    var arrGender : NSMutableArray = []
    var arrAge : NSMutableArray = []
    var arrSmoking : NSMutableArray = []
    var arrPet : NSMutableArray = []
    
    static var Sid_Gender : Array<Any> = []
    static var Sid_AgeGroup : Array<Any> = []
    static var Sid_Pet : Array<Any> = []
    static var Sid_Smoking : Array<Any> = []
    
    var userdata = SharedPreference.getUserData()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        if isCheckFlat=="Looking"{
            self.id_profile = 2
        }else{
            self.id_profile = 1
        }
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    override func viewWillAppear(_ animated: Bool) {
        self.navigationController?.isNavigationBarHidden = true
       // rdv_tabBarController?.setTabBarHidden(false, animated: true)
        
        if FlatmatePreferenceViewController.Sid_Gender.count != 0{
            
            let arrG = FlatmatePreferenceViewController.Sid_Gender
            for item in arrG{
                if item as! Int != 0{
                    self.arrGender.add(item)
                }
            }
            self.id_Gender = self.arrGender.componentsJoined(by: ",")
            
            let arrAG = FlatmatePreferenceViewController.Sid_AgeGroup
            for item in arrAG{
                if item as! Int != 0{
                    self.arrAge.add(item)
                }
            }
            self.id_AgeGroup = self.arrAge.componentsJoined(by: ",")
            
            let arrSM = FlatmatePreferenceViewController.Sid_Smoking
            for item in arrSM{
                if item as! Int != 0{
                    self.arrSmoking.add(item)
                }
            }
            self.id_Smoking = self.arrSmoking.componentsJoined(by: ",")
            
            let arrP = FlatmatePreferenceViewController.Sid_Pet
            for item in arrP{
                if item as! Int != 0{
                    self.arrPet.add(item)
                }
            }
            self.id_Pet = self.arrPet.componentsJoined(by: ",")
            
            
            
            let arrgender = FlatmatePreferenceViewController.Sid_Gender
            for item in arrgender{
                if item as! Int != 0{
                    self.ShowCheckUnceck(arrButton: self.Gender, Selected: item as! Int)
                }
            }
            
            let arrage = FlatmatePreferenceViewController.Sid_AgeGroup
            for item in arrage{
                if item as! Int != 0{
                    self.ShowCheckUnceck(arrButton: self.AgeGroup, Selected: item as! Int)
                }
            }
            
            let arrsmok = FlatmatePreferenceViewController.Sid_Smoking
            for item in arrsmok{
                if item as! Int != 0{
                    self.ShowCheckUnceck(arrButton: self.Smoking, Selected: item as! Int)
                }
            }
            
            let arrpets = FlatmatePreferenceViewController.Sid_Pet
            for item in arrpets{
                if item as! Int != 0{
                    self.ShowCheckUnceck(arrButton: self.Pets, Selected: item as! Int)
                }
            }
        }
    }
    
    func ShowCheckUnceck(arrButton : [UIButton], Selected : Int) {
        for button in arrButton {
            let tag = button.tag
            if Selected == tag{
                button.isSelected = true
            }
        }
    }
    
    @IBAction func BackMethod(_ sender: Any) {
        Common.PopMethod(VC: self)
    }
    
    @IBAction func GenderMethod(_ sender: UIButton) {
        let Selectedtag = sender.tag
        if sender.isSelected {
            sender.isSelected = false
            arrGender.remove(Selectedtag)
        }else{
            sender.isSelected = true
            arrGender.add(Selectedtag)
        }
        self.id_Gender = arrGender.componentsJoined(by: ",")
        print(self.id_Gender)
    }
    
    @IBAction func AgeMethod(_ sender: UIButton) {
        let Selectedtag = sender.tag
        if sender.isSelected {
            sender.isSelected = false
            arrAge.remove(Selectedtag)
        }else{
            sender.isSelected = true
            arrAge.add(Selectedtag)
        }
        self.id_AgeGroup = arrAge.componentsJoined(by: ",")
        print(self.id_AgeGroup)
    }
    
    @IBAction func SmokingMethod(_ sender: UIButton) {
        let Selectedtag = sender.tag
        if sender.isSelected {
            sender.isSelected = false
            arrSmoking.remove(Selectedtag)
        }else{
            sender.isSelected = true
            arrSmoking.add(Selectedtag)
        }
        self.id_Smoking = arrSmoking.componentsJoined(by: ",")
        print(self.id_Smoking)
    }
    
    @IBAction func PetsMethod(_ sender: UIButton) {
        let Selectedtag = sender.tag
        if sender.isSelected {
            sender.isSelected = false
            arrPet.remove(Selectedtag)
        }else{
            sender.isSelected = true
            arrPet.add(Selectedtag)
        }
        self.id_Pet = arrPet.componentsJoined(by: ",")
        print(self.id_Pet)
    }
    
    @IBAction func clickOnSubmitBtn(_ sender: UIButton) {
        
        guard self.id_Gender != ""  else {
            return Common.ShowAlert(Title: Common.Title, Message: "Please select Gender sexuality", VC: self)
        }
        
        guard self.id_AgeGroup != ""   else {
            return Common.ShowAlert(Title: Common.Title, Message: "Please select age group", VC: self)
        }
        
        guard self.id_Smoking != ""   else {
            return Common.ShowAlert(Title: Common.Title, Message: "Please select smoking type", VC: self)
        }
        
        guard self.id_Pet != ""   else {
            return Common.ShowAlert(Title: Common.Title, Message: "Please select pets type", VC: self)
        }
        
        self.FlatematePreferrenceService()
    }
    
////////////// Flatemate Preferrence Service   ///////////
    func FlatematePreferrenceService(){
        self.startActivityIndicator()
        
        var param : [String : Any] = [:]
        if isCheckFlat == "Looking"{
        param = ["user_id"    : userdata.user_id,
                     "profile_id" : self.id_profile,
                     "gender_and_sexuality" : self.id_Gender,
                     "age_group"     : self.id_AgeGroup,
                     "smoking"  : self.id_Smoking,
                     "pets"     : self.id_Pet,
                     "looking_home_id" : SharedPreference.getLooking_id()
            ] as [String : Any]
        }else{
            param = ["user_id"    : userdata.user_id,
                     "profile_id" : self.id_profile,
                     "gender_and_sexuality" : self.id_Gender,
                     "age_group"     : self.id_AgeGroup,
                     "smoking"  : self.id_Smoking,
                     "pets"     : self.id_Pet,
                     "offering_home_id" : SharedPreference.getOffering_id()
                ] as [String : Any]
        }
        
        CommunicationManager().getResponseForPost(strUrl: Url_File.FlatematePreferrence, parameters: param as NSDictionary) { ( result , data) in
            
            if(result == "success") {
                let dataDict = data as! NSDictionary
                print(dataDict)
                let dic = dataDict["response"] as! NSDictionary                
                self.GoNext()
                self.stopActivityIndicator()
            }else if (result == "error") {
                let strMessage = (data as! NSDictionary)["message"] as! String
                Common.ShowAlert(Title: Common.Title, Message: strMessage, VC: self)
                self.stopActivityIndicator()
            }else if (result == "Network") {
                let strMessage = (data as! String)
                Common.ShowAlert(Title: Common.Title, Message: strMessage, VC: self)
                self.stopActivityIndicator()
            }
        }
    }
    
    func GoNext() {
        FlatmatePreferenceViewController.Sid_Gender = self.arrGender as! Array<Any>
        FlatmatePreferenceViewController.Sid_AgeGroup = self.arrAge as! Array<Any>
        FlatmatePreferenceViewController.Sid_Smoking = self.arrSmoking as! Array<Any>
        FlatmatePreferenceViewController.Sid_Pet = self.arrPet as! Array<Any>
        
        let storyboard = UIStoryboard(name: "Home", bundle: nil)
        let Vc = storyboard.instantiateViewController(withIdentifier: "HomeViewController") as! HomeViewController
        self.navigationController?.pushViewController(Vc, animated: false)
    }
    
}

