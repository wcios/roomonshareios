
import UIKit

class LookingDescriptionViewController: UIViewController , tblSelection{
    
    
    @IBOutlet weak var txtAdults: UITextField!
    @IBOutlet weak var txtGender: UITextField!
    @IBOutlet weak var txtSextulity: UITextField!
    @IBOutlet weak var txtAgeGroup: UITextField!
    @IBOutlet weak var txtEmployment: UITextField!
    @IBOutlet weak var viewInterestHeight : NSLayoutConstraint!
    @IBOutlet weak var contentViewHeight : NSLayoutConstraint!
    
    @IBOutlet var btnSmoke : [UIButton]!
    @IBOutlet var btnPets : [UIButton]!
    @IBOutlet weak var collInterest : UICollectionView!
    
    var id_Adults : Int = 0
    var id_Gender : Int = 0
    var id_Sextulity : Int = 0
    var id_AgeGroup : Int = 0
    var id_Employment : Int = 0
    var id_Pet : Int = 0
    var id_Smoking : Int = 0
    var strMoreinterest = ""
    
    static var SAdults = 0
    static var SGender = 0
    static var SSextulity = 0
    static var SAgeGroup = 0
    static var SEmployment = 0
    static var Sid_Pet : Int = 0
    static var Sid_Smoking : Int = 0
    static var SstrMoreinterest : Array<Any> = []
    
    var collDataArr : NSMutableArray = []
    var userdata = SharedPreference.getUserData()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        collInterest.isHidden=true
        viewInterestHeight.constant=35
        contentViewHeight.constant=667
       
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        self.txtAdults.delegate = self
        self.txtGender.delegate = self
        self.txtSextulity.delegate = self
        self.txtAgeGroup.delegate = self
        self.txtEmployment.delegate = self
        
        self.navigationController?.isNavigationBarHidden = true
        rdv_tabBarController?.setTabBarHidden(false, animated: true)
        self.Showdata()
    }
    
    ////////////// Show Data ///////////
    func Showdata()  {
        if LookingDescriptionViewController.SGender != 0 {
            
            self.id_Adults = LookingDescriptionViewController.SAdults
            self.id_Gender = LookingDescriptionViewController.SGender
            self.id_Sextulity = LookingDescriptionViewController.SSextulity
            self.id_AgeGroup = LookingDescriptionViewController.SAgeGroup
            self.id_Employment = LookingDescriptionViewController.SEmployment
            self.id_Pet = LookingDescriptionViewController.Sid_Pet
            self.id_Smoking = LookingDescriptionViewController.Sid_Smoking
            let arrInt : NSMutableArray = []
            let arrInte = LookingDescriptionViewController.SstrMoreinterest
            if arrInte.count != 0{
                for item in arrInte{
                    arrInt.add(item)
                }
                self.strMoreinterest = arrInt.componentsJoined(by: ",")
            }
            
            self.txtAdults.text = Array_File.arrAdults[self.id_Adults - 1]
            self.txtGender.text = Array_File.arrGender[self.id_Gender - 1]
            self.txtSextulity.text = Array_File.arrSexuality[self.id_Sextulity - 1]
            self.txtAgeGroup.text = Array_File.arrAgeGroup[self.id_AgeGroup - 1]
            self.txtEmployment.text = Array_File.arrEmployment[self.id_Employment - 1]
            self.ShowCheckUnceck(arrButton: self.btnPets, Selected: self.id_Pet)
            self.ShowCheckUnceck(arrButton: self.btnSmoke, Selected: self.id_Smoking)
            
            let arr = LookingDescriptionViewController.SstrMoreinterest
            if arr.count != 0{
                self.collDataArr = []
                for i in 0...Array_File.arrIntesrestOptionImgs.count{
                    for item in arr{
                        let j = item as! Int
                        if j == i{
                            let dict = Array_File.arrIntesrestOptionImgs[i]
                            self.collDataArr.add(dict)
                        }
                    }
                }
                self.collInterest.isHidden=false
                viewInterestHeight.constant=170
                self.collInterest.reloadData()
            }
        }
    }
    
    func ShowCheckUnceck(arrButton : [UIButton], Selected : Int) {
        for button in arrButton {
            let tag = button.tag
            if Selected == tag{
                button.isSelected = true
            }else{
                button.isSelected = false
            }
        }
    }
    
    ////////////// Back ///////////
    @IBAction func BackMethod(_ sender: Any) {
        Common.PopMethod(VC: self)
    }
    
    ////////////// Next Method ///////////
    @IBAction func clickOnNextBtn(_ sender : UIButton){
    
        guard let adults = self.txtAdults.text, adults != ""  else {
            return Common.ShowAlert(Title: Common.Title, Message: "Please select number of adults", VC: self)
        }

        guard let gender = self.txtGender.text, gender != ""  else {
            return Common.ShowAlert(Title: Common.Title, Message: "Please select gender", VC: self)
        }

        guard let sextulity = self.txtSextulity.text, sextulity != ""  else {
            return Common.ShowAlert(Title: Common.Title, Message: "Please select sextulity", VC: self)
        }

        guard let age = self.txtAgeGroup.text, age != ""  else {
            return Common.ShowAlert(Title: Common.Title, Message: "Please select age group", VC: self)
        }

        guard let employment = self.txtEmployment.text, employment != ""  else {
            return Common.ShowAlert(Title: Common.Title, Message: "Please select employment situation", VC: self)
        }

        guard self.id_Smoking != 0 else {
            return Common.ShowAlert(Title: Common.Title, Message: "Please select your smoking", VC: self)
        }

        guard self.id_Pet != 0 else {
            return Common.ShowAlert(Title: Common.Title, Message: "Please select your pets", VC: self)
        }

        self.YourDescriptionService()
        
    }
    
    func onSelectPassData(arrData: NSMutableArray) {
        print(arrData)
        self.strMoreinterest = arrData.componentsJoined(by: ",")
        print(self.strMoreinterest)
        self.collDataArr = []
        for i in 0...Array_File.arrIntesrestOptionImgs.count{
            if(arrData.contains(i)){
                let dict = Array_File.arrIntesrestOptionImgs[i]
                collDataArr.add(dict)
            }
        }
        print(collDataArr)
        collInterest.isHidden=false
        viewInterestHeight.constant=150
        contentViewHeight.constant=contentViewHeight.constant + viewInterestHeight.constant + 10
        collInterest.reloadData()
    }
    
    ////////////// More intrerest ///////////
    @IBAction func clickOnPlusBtn(_ sender: UIButton) {
        LookingDescriptionViewController.SstrMoreinterest = []
        let storyboard = UIStoryboard(name: "Main", bundle: nil)
        let vc = storyboard.instantiateViewController(withIdentifier: "moreInterestViewController") as! moreInterestViewController
        vc.delegate = self
        self.navigationController?.pushViewController(vc, animated: true)
    }
    
    ////////////// Smoking ///////////
    @IBAction func SmokingMethod(_ sender: UIButton) {
        let Selectedtag = sender.tag
        self.id_Smoking = Selectedtag
        self.CheckUnceck(arrButton: self.btnSmoke, Selected: Selectedtag)
    }
    
    ////////////// Pets ///////////
    @IBAction func PetsMethod(_ sender: UIButton) {
        let Selectedtag = sender.tag
        self.id_Pet = Selectedtag
        self.CheckUnceck(arrButton: self.btnPets, Selected: Selectedtag)
    }
    
    func CheckUnceck(arrButton : [UIButton], Selected : Int) {
        for button in arrButton {
            let tag = button.tag
            if Selected == tag{
                button.isSelected = true
            }else{
                button.isSelected = false
            }
        }
    }
    
    ////////////// Your Description Service  ///////////
    func YourDescriptionService(){
        self.startActivityIndicator()
        
        var param : [String:Any] = [:]
        
        if SharedPreference.getLooking_id() != 0{
            param = ["user_id"    : userdata.user_id,
                     "profile_id" : 2,
                     "no_of_adults" : self.id_Adults,
                     "gender"     : self.id_Gender,
                     "sexuality"  : self.id_Sextulity,
                     "age_group"  : self.id_AgeGroup,
                     "employment_situation" : self.id_Employment,
                     "main_interest" : self.strMoreinterest,
                     "pets" : self.id_Pet,
                     "smoking" : self.id_Smoking,
                     "looking_home_id" : SharedPreference.getLooking_id()
                ] as [String : Any]
        }else{
        param = ["user_id"    : userdata.user_id,
                     "profile_id" : 2,
                     "no_of_adults" : self.id_Adults,
                     "gender"     : self.id_Gender,
                     "sexuality"  : self.id_Sextulity,
                     "age_group"  : self.id_AgeGroup,
                     "employment_situation" : self.id_Employment,
                     "main_interest" : self.strMoreinterest,
                     "pets" : self.id_Pet,
                     "smoking" : self.id_Smoking
                     //"looking_home_id" : 0
            ] as [String : Any]
        }
        print(param)
        
        CommunicationManager().getResponseForPost(strUrl: Url_File.Looking_YourDescription, parameters: param as NSDictionary) { ( result , data) in
            
            if(result == "success") {
                let dataDict = data as! NSDictionary
                print(dataDict)
                let dic = dataDict["response"] as! NSDictionary
                SharedPreference.setLooking_id(dic.value(forKey: "looking_home_id") as! Int)
                print(SharedPreference.getLooking_id())
                self.GoNext()
                self.stopActivityIndicator()
            }else if (result == "error") {
                let strMessage = (data as! NSDictionary)["message"] as! String
                Common.ShowAlert(Title: Common.Title, Message: strMessage, VC: self)
                self.stopActivityIndicator()
            }else if (result == "Network") {
                let strMessage = (data as! String)
                Common.ShowAlert(Title: Common.Title, Message: strMessage, VC: self)
                self.stopActivityIndicator()
            }
        }
    }
    
    func GoNext() {
        LookingDescriptionViewController.SAdults = self.id_Adults
        LookingDescriptionViewController.SGender = self.id_Gender
        LookingDescriptionViewController.SSextulity = self.id_Sextulity
        LookingDescriptionViewController.SAgeGroup = self.id_AgeGroup
        LookingDescriptionViewController.SEmployment = self.id_Employment
        LookingDescriptionViewController.Sid_Pet = self.id_Pet
        LookingDescriptionViewController.Sid_Smoking = self.id_Smoking
        print(self.strMoreinterest)
        let arrin = self.strMoreinterest.components(separatedBy: ",")
        print(arrin)
        if arrin.count > 0{
        LookingDescriptionViewController.SstrMoreinterest = self.separate(arr: arrin)
        }        
        
        Common.PushMethod(VC: self, identifier: "IntroduceSelfViewController")
    }
    
    func separate(arr : [String]) -> Array<Any>{
        var arrInt : Array<Any> = []
        for item in arr{
            arrInt.append(Int(item)!)
        }
        return arrInt
    }
}



////////////// textField delegate  ///////////
extension LookingDescriptionViewController : UITextFieldDelegate, MyPickerViewProtocol{
    func textFieldDidBeginEditing(_ textField: UITextField) {
        if textField == self.txtAdults{
            let myPickerView =  MyPickerView.init(withDictionary: Array_File.arrAdults, Tag:1)
            myPickerView.delegate = myPickerView
            myPickerView.MyPickerDelegate = self
            self.txtAdults.inputView = myPickerView
        }else if textField == self.txtGender{
            let myPickerView =  MyPickerView.init(withDictionary: Array_File.arrGender, Tag:2)
            myPickerView.delegate = myPickerView
            myPickerView.MyPickerDelegate = self
            self.txtGender.inputView = myPickerView
        }else if textField == self.txtSextulity{
            let myPickerView =  MyPickerView.init(withDictionary: Array_File.arrSexuality, Tag:3)
            myPickerView.delegate = myPickerView
            myPickerView.MyPickerDelegate = self
            self.txtSextulity.inputView = myPickerView
        }else if textField == self.txtAgeGroup{
            let myPickerView =  MyPickerView.init(withDictionary: Array_File.arrAgeGroup, Tag:4)
            myPickerView.delegate = myPickerView
            myPickerView.MyPickerDelegate = self
            self.txtAgeGroup.inputView = myPickerView
        }else if textField == self.txtEmployment{
            let myPickerView =  MyPickerView.init(withDictionary: Array_File.arrEmployment, Tag:5)
            myPickerView.delegate = myPickerView
            myPickerView.MyPickerDelegate = self
            self.txtEmployment.inputView = myPickerView
        }
    }
    
    func myPickerDidSelectRow(Index:Int?, Tag:Int) {
        if Tag == 1{
            self.id_Adults = Index! + 1
            self.txtAdults.text = Array_File.arrAdults[Index!]
        }else if Tag == 2{
            self.id_Gender = Index! + 1
            self.txtGender.text = Array_File.arrGender[Index!]
        }else if Tag == 3{
            self.id_Sextulity = Index! + 1
            self.txtSextulity.text = Array_File.arrSexuality[Index!]
        }else if Tag == 4{
            self.id_AgeGroup = Index! + 1
            self.txtAgeGroup.text = Array_File.arrAgeGroup[Index!]
        }else if Tag == 5{
            self.id_Employment = Index! + 1
           self.txtEmployment.text = Array_File.arrEmployment[Index!]
        }
    }
}

////////////// collectionview  ///////////
extension LookingDescriptionViewController : UICollectionViewDelegate , UICollectionViewDataSource , UICollectionViewDelegateFlowLayout{
    
    func numberOfSections(in collectionView: UICollectionView) -> Int {
        return 1
    }
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        
        return collDataArr.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        
        let cell = collInterest.dequeueReusableCell(withReuseIdentifier: "moreInterestCollCell", for: indexPath) as! moreInterestCollCell
        
        let dict = collDataArr[indexPath.row] as! [String : String]
        cell.imgSelect.image=UIImage(named: (dict["image"])!)?.imageWithInsets(insetDimen: 8)
        cell.lblName.text=(dict["name"]!)
        cell.imgSelect.tintImageColor(color: UIColorFromHex(rgbValue: 0x05C6CF, alpha: 1.0))
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        let yourWidth = collectionView.bounds.width/4.0
        return CGSize(width: yourWidth, height: 110.0)
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, insetForSectionAt section: Int) -> UIEdgeInsets {
        return UIEdgeInsets.zero
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumInteritemSpacingForSectionAt section: Int) -> CGFloat {
        return 0
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumLineSpacingForSectionAt section: Int) -> CGFloat {
        return 0
    }
}

class moreInterestCollCell: UICollectionViewCell {
    
    @IBOutlet weak var lblName : UILabel!
    @IBOutlet weak var imgSelect : UIImageView!
    
}
