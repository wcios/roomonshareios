
import UIKit

class createGroupViewController: UIViewController {
    
    @IBOutlet weak var tblGroupMembers : UITableView!
    @IBOutlet weak var txtGroupName : UITextField!
    @IBOutlet weak var btnSave : UIButton!
    @IBOutlet weak var btnDelete : UIButton!
    @IBOutlet weak var lblTitle : UILabel!
    @IBOutlet weak var viewNoMember: UIView!
    @IBOutlet var btnGroupType: [UIButton]!
    @IBOutlet weak var viewPopBg: UIView!
    @IBOutlet weak var txtEmail: ACFloatingTextfield!
    
    var dicGroup : NSDictionary = [:]
    var arrGroupMember : Array<Any> = []
    var isNavigateFrom = ""
    var id_Grouptype : Int = 0
    var Group_id : Int = 0
    var user_id : Int = 0
    

    override func viewDidLoad() {
        super.viewDidLoad()
        tblGroupMembers.tableFooterView=UIView()
        self.viewPopBg.isHidden = true
        if(isNavigateFrom=="create"){
            self.tblGroupMembers.isHidden = true
            self.viewNoMember.isHidden = false
            self.btnSave.setTitle("Save", for: .normal)
            self.lblTitle.text = "Create Group"
            self.btnDelete.isHidden=true
        }else if(isNavigateFrom=="update"){
            self.tblGroupMembers.isHidden = false
            self.viewNoMember.isHidden = true
            self.btnSave.setTitle("Update", for: .normal)
            self.lblTitle.text = "Group Settings"
            self.btnDelete.isHidden=false
        }
        
        if self.dicGroup != [:]{
            self.Group_id =  self.dicGroup.value(forKey:"id") as! Int
            self.txtGroupName.text = self.dicGroup.value(forKey:"name") as? String
            let  type = self.dicGroup.value(forKey:"type") as! Int
            self.CheckUnceck(arrButton: self.btnGroupType, Selected: type)
            self.GroupMemberListService()
        }
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    override func viewWillAppear(_ animated: Bool) {
        self.navigationController?.isNavigationBarHidden = true
       // rdv_tabBarController?.setTabBarHidden(false, animated: true)
    }
    
    override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {
        self.viewPopBg.isHidden = true
    }
    
    @IBAction func clickOnBackBtn(_ sender : UIButton){
        Common.PopMethod(VC: self)
    }
    
    @IBAction func GroupTypeMethod(_ sender: UIButton) {
        let Selectedtag = sender.tag
        self.id_Grouptype = Selectedtag
        self.CheckUnceck(arrButton: self.btnGroupType, Selected: Selectedtag)
    }
    
    func CheckUnceck(arrButton : [UIButton], Selected : Int) {
        for button in arrButton {
            let tag = button.tag
            if Selected == tag{
                button.isSelected = true
                button.tintColor = self.UIColorFromHex(rgbValue: 0x05C6CF, alpha: 1.0)
            }else{
                button.isSelected = false
            }
        }
    }
    
    @IBAction func AddPersonMethod(_ sender: Any) {
        self.viewPopBg.isHidden = false
    }
    
    @IBAction func SaveMethod(_ sender: Any) {
        guard let Fname = self.txtGroupName.text, Fname != ""  else {
            return Common.ShowAlert(Title: "Alert", Message: "Please enter group name", VC: self)
        }
        self.CreateGroupService()
    }
    
    @IBAction func DeleteGroupMethod(_ sender: Any) {
        let alert = UIAlertController(title: "Room On Share", message:
            "Are you sure, you want to delete this group", preferredStyle: UIAlertControllerStyle.alert)
        let okAction = UIAlertAction(title: "Yes", style: UIAlertActionStyle.default) { (action) in
            self.DeleteGroupService()
        }
        
        let CancleAction = UIAlertAction(title: "No", style: UIAlertActionStyle.default) { (action) in
            
        }
        alert.addAction(okAction)
        alert.addAction(CancleAction)
        self.present(alert, animated: true, completion: nil)
    }
    
    
    /////// Popup //////
    
    @IBAction func ContactListMethod(_ sender: Any) {
        
    }
    
    @IBAction func CancelMethod(_ sender: Any) {
        self.viewPopBg.isHidden = true
    }
    
    @IBAction func AddMethod(_ sender: Any) {
        guard let email = self.txtEmail.text, email != ""  else {
            return Common.ShowAlert(Title: Common.Title, Message: "Please enter E-mail", VC: self)
        }
        
        guard Common.isValidEmail(testStr: self.txtEmail.text!)  else {
            return Common.ShowAlert(Title: Common.Title, Message: "Please enter valid E-mail", VC: self)
        }
        
        self.AddMemberService()
    }
    
    
    /////////// Create Group ///////////
    func CreateGroupService(){
        self.startActivityIndicator()
        
        let param = ["group_name": self.txtGroupName.text!,
                     "group_type" : self.id_Grouptype] as [String : Any]
        print(param)
        CommunicationManager().getResponseForPost(strUrl: Url_File.CreateGroup, parameters: param as NSDictionary) { ( result , data) in
            if(result == "success") {
                let dataDict = data as! NSDictionary
                print(dataDict)
                let response = dataDict.value(forKey: "response") as! NSDictionary
                self.Group_id = response.value(forKey: "id") as! Int
                Common.ShowAlert(Title: "Success", Message:"Now you can add member in this group", VC: self)
                self.stopActivityIndicator()
            }else if (result == "error") {
                let strMessage = (data as! NSDictionary)["message"] as! String
                Common.ShowAlert(Title: Common.Title, Message: strMessage, VC: self)
                self.stopActivityIndicator()
            }else if (result == "Network") {
                let strMessage = (data as! String)
                Common.ShowAlert(Title: Common.Title, Message: strMessage, VC: self)
                self.stopActivityIndicator()
            }
        }
    }

    
    /////////// Add Group member ///////////
    func AddMemberService(){
        self.startActivityIndicator()
        
        let param = ["group_id": self.Group_id ,
                     "user_email" : self.txtEmail.text!] as [String : Any]
        
        CommunicationManager().getResponseForPost(strUrl: Url_File.AddMemberInGroup, parameters: param as NSDictionary) { ( result , data) in
            if(result == "success") {
                let dataDict = data as! NSDictionary
                print(dataDict)
                self.viewPopBg.isHidden = true
                self.GroupMemberListService()
                self.stopActivityIndicator()
            }else if (result == "error") {
                let strMessage = (data as! NSDictionary)["message"] as! String
                Common.ShowAlert(Title: Common.Title, Message: strMessage, VC: self)
                self.stopActivityIndicator()
            }else if (result == "Network") {
                let strMessage = (data as! String)
                Common.ShowAlert(Title: Common.Title, Message: strMessage, VC: self)
                self.stopActivityIndicator()
            }
        }
    }
    
    /////////// Delete Group member ///////////
    func DeleteGroupMemberService(userid : Int ){
        self.startActivityIndicator()
        
        let param = ["group_id": self.Group_id ,
                     "user_id" : userid] as [String : Any]
        
        CommunicationManager().getResponseForPost(strUrl: Url_File.DeleteGroupMember, parameters: param as NSDictionary) { ( result , data) in
            if(result == "success") {
                let dataDict = data as! NSDictionary
                print(dataDict)
                let strMessage = dataDict.value(forKey: "message") as! String
                Common.ShowAlert(Title: "Room On Share", Message: strMessage, VC: self)
                self.GroupMemberListService()
                self.stopActivityIndicator()
            }else if (result == "error") {
                let strMessage = (data as! NSDictionary)["message"] as! String
                Common.ShowAlert(Title: Common.Title, Message: strMessage, VC: self)
                self.stopActivityIndicator()
            }else if (result == "Network") {
                let strMessage = (data as! String)
                Common.ShowAlert(Title: Common.Title, Message: strMessage, VC: self)
                self.stopActivityIndicator()
            }
        }
    }
    
    
    /////////// Group Member List ///////////
    func GroupMemberListService(){
        self.startActivityIndicator()
        
        let param = ["group_id": self.Group_id] as [String : Any]
        
        CommunicationManager().getResponseForPost(strUrl: Url_File.GroupMemberList, parameters: param as NSDictionary) { ( result , data) in
            if(result == "success") {
                let dataDict = data as! NSDictionary
                print(dataDict)
                let response = dataDict.value(forKey: "response") as! NSDictionary
                self.arrGroupMember = response.value(forKey: "members") as! Array<Any>
                self.tblGroupMembers.reloadData()
                self.stopActivityIndicator()
            }else if (result == "error") {
                let strMessage = (data as! NSDictionary)["message"] as! String
                Common.ShowAlert(Title: Common.Title, Message: strMessage, VC: self)
                self.stopActivityIndicator()
            }else if (result == "Network") {
                let strMessage = (data as! String)
                Common.ShowAlert(Title: Common.Title, Message: strMessage, VC: self)
                self.stopActivityIndicator()
            }
        }
    }
    
    /////////// Delete Group ///////////
    func DeleteGroupService(){
        self.startActivityIndicator()
        
        let param = ["group_id": self.Group_id] as [String : Any]
        
        CommunicationManager().getResponseForPost(strUrl: Url_File.DeleteGroup, parameters: param as NSDictionary) { ( result , data) in
            if(result == "success") {
                let dataDict = data as! NSDictionary
                print(dataDict)
                Common.PopRootMethod(VC: self)                
                self.stopActivityIndicator()
            }else if (result == "error") {
                let strMessage = (data as! NSDictionary)["message"] as! String
                Common.ShowAlert(Title: Common.Title, Message: strMessage, VC: self)
                self.stopActivityIndicator()
            }else if (result == "Network") {
                let strMessage = (data as! String)
                Common.ShowAlert(Title: Common.Title, Message: strMessage, VC: self)
                self.stopActivityIndicator()
            }
        }
    }
    
    
}

extension createGroupViewController : UITableViewDelegate , UITableViewDataSource{
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.arrGroupMember.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = self.tblGroupMembers.dequeueReusableCell(withIdentifier: "grupMemberTblCell") as! grupMemberTblCell
        let dic = self.arrGroupMember[indexPath.row] as! NSDictionary
        let user = dic.value(forKey: "user") as! NSDictionary
        
        cell.lblusername.text = user.value(forKey: "name") as? String
        cell.lblUserEmail.text = user.value(forKey: "email") as? String
        let img_pro = user.value(forKey: "profile_pic") as! String
        cell.imgUser.sd_setImage(with: URL(string: img_pro), placeholderImage: UIImage(named: "UserPlaceholder"))
        cell.btnDelete.tag = indexPath.row
        cell.btnDelete.addTarget(self, action: #selector(DeleteMethod(btn:)), for: .touchUpInside)
        return cell
    }
    
    ////////////// delete button ///////////
    @objc func DeleteMethod(btn : UIButton) {
        
        let Index = btn.tag
        let dic = self.arrGroupMember[Index] as! NSDictionary
        
        let alert = UIAlertController(title: "Room On Share", message:
            "Are you sure, you want to delete this member", preferredStyle: UIAlertControllerStyle.alert)
        let okAction = UIAlertAction(title: "Yes", style: UIAlertActionStyle.default) { (action) in
            let user_id =  dic.value(forKey: "user_id") as! Int
            self.DeleteGroupMemberService(userid: user_id)
        }
        
        let CancleAction = UIAlertAction(title: "No", style: UIAlertActionStyle.default) { (action) in
            
        }
        
        alert.addAction(okAction)
        alert.addAction(CancleAction)
        self.present(alert, animated: true, completion: nil)
    }
}

class grupMemberTblCell: UITableViewCell {
    @IBOutlet weak var imgUser: UIImageView!
    @IBOutlet weak var lblusername: UILabel!
    @IBOutlet weak var lblUserEmail: UILabel!
    @IBOutlet weak var btnDelete: UIButton!
}
