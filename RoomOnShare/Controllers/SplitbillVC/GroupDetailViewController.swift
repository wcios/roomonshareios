import UIKit

class GroupDetailViewController: UIViewController {
    
    @IBOutlet weak var tblGroupDetail : UITableView!
   
    @IBOutlet weak var lblOwes: UILabel!
    @IBOutlet weak var lblOwed: UILabel!
    @IBOutlet weak var lblBalance: UILabel!
    @IBOutlet weak var lblGroupName: UILabel!
    @IBOutlet weak var imgGroupName: UIImageView!

    var dicGroupDetail : NSDictionary = [:]
    var arrGroupDetail : [NSDictionary] = []

    override func viewDidLoad() {
        super.viewDidLoad()
        self.tblGroupDetail.tableFooterView=UIView()
        self.tblGroupDetail.estimatedRowHeight = 120.0
        self.tblGroupDetail.rowHeight = UITableViewAutomaticDimension
        self.lblGroupName.text = self.dicGroupDetail.value(forKey: "name") as? String
        
        self.GroupDetailService()
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        self.navigationController?.isNavigationBarHidden = true
        rdv_tabBarController?.setTabBarHidden(true, animated: true)
    }
    
    @IBAction func clickOnEditBtn(_ sender : UIButton){
        let story = AppStoryboard.Home.instance
        let activity = story.instantiateViewController(withIdentifier: "createGroupViewController") as! createGroupViewController
        activity.isNavigateFrom="update"
        activity.dicGroup = self.dicGroupDetail
        self.navigationController?.pushViewController(activity, animated: true)
    }

    @IBAction func clickOnBackBtn(_ sender : UIButton){
        Common.PopMethod(VC: self)
    }
    
    @IBAction func clickOnAddExpense(_ sender : UIButton){
        
        for i in 0...self.arrGroupDetail.count - 1{
            let dict = self.arrGroupDetail[i]
            if let _ = dict["group_members"] as? NSArray{
                Common.ShowAlert(Title: "Alert", Message: "Please add first group members.", VC: self)
            }
            if let _ = dict["group_members"] as? NSDictionary{
                let story = AppStoryboard.Home.instance
                let activity = story.instantiateViewController(withIdentifier: "createExpenseViewController") as! createExpenseViewController
                activity.dicGroup = self.dicGroupDetail
                self.navigationController?.pushViewController(activity, animated: true)
            }
        }
        
    }
    
    
    
    /////////// Group Detail  ///////////
    func GroupDetailService(){
        self.startActivityIndicator()
        
        let param = ["group_id": self.dicGroupDetail.value(forKey:"id") as! Int] as [String : Any]
        
        CommunicationManager().getResponseForPost(strUrl: Url_File.GroupDetail, parameters: param as NSDictionary) { ( result , data) in
            if(result == "success") {
                self.stopActivityIndicator()
                let dataDict = data as! NSDictionary
                print(dataDict)
                self.Setdata(dicData: dataDict.value(forKey: "response") as! NSDictionary)
                
            }else if (result == "error") {
                let strMessage = (data as! NSDictionary)["message"] as! String
                Common.ShowAlert(Title: Common.Title, Message: strMessage, VC: self)
                self.stopActivityIndicator()
            }else if (result == "Network") {
                let strMessage = (data as! String)
                Common.ShowAlert(Title: Common.Title, Message: strMessage, VC: self)
                self.stopActivityIndicator()
            }
        }
    }
    
    func Setdata(dicData : NSDictionary){
        self.lblOwed.text = String(dicData.value(forKey: "owed") as! Int)
        self.lblOwes.text = String(dicData.value(forKey: "owes") as! Int)
        self.lblBalance.text = String(dicData.value(forKey: "balance") as! Int)
        self.arrGroupDetail = dicData.value(forKey: "group_detail") as! [NSDictionary]
        for i in 0...self.arrGroupDetail.count - 1{
            let dict = self.arrGroupDetail[i]
            if let arrGrupDetail = dict["group_members"] as? NSArray{
                if(arrGrupDetail.count == 0){
                    self.tblGroupDetail.isHidden = true
                }else{
                    self.tblGroupDetail.isHidden = false
                    self.tblGroupDetail.reloadData()
                }
            }
        }
        
    }
    
}

extension GroupDetailViewController : UITableViewDelegate , UITableViewDataSource{
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.arrGroupDetail.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell{
        let cell = self.tblGroupDetail.dequeueReusableCell(withIdentifier: "groupDetailTblCell") as! groupDetailTblCell
        let dic = self.arrGroupDetail[indexPath.row]
        cell.lblGroupName.text = dic.value(forKey: "expense_title") as? String
        cell.lblPrice.text = dic.value(forKey: "expense_amount") as? String
        let type = dic.value(forKey: "expense_type") as! Int
        cell.imgGroup.image = UIImage(named: Array_File.arrExpenseTypeList[type - 1]["image"]!)
        
        let user = dic["user"] as! NSDictionary
        let userID = user["user_id"] as! Int
        if(userID == SharedPreference.getUserData().user_id){
            cell.lblDescription.text = "Added by me"
        }else{
            cell.lblDescription.text = "Added by \(user["name"] as! String)"
        }
        
        cell.btnNext.tag=indexPath.row
        cell.btnNext.addTarget(self, action: #selector(clickOnNextBtn(_:)), for: .touchUpInside)
        
        return cell
    }
    
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let dic = self.arrGroupDetail[indexPath.row] as! NSDictionary
        let story = AppStoryboard.Home.instance
         let activity = story.instantiateViewController(withIdentifier: "ExpenseDetailViewController") as! ExpenseDetailViewController
        activity.dictDetail = dic
          self.navigationController?.pushViewController(activity, animated: true)
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        
        return 89
    }
    
   @objc func clickOnNextBtn(_ sender : UIButton){
        
    }
    
    
}

class groupDetailTblCell: UITableViewCell {
    
    @IBOutlet weak var imgGroup : UIImageView!
    @IBOutlet weak var lblGroupName : UILabel!
    @IBOutlet weak var lblPrice : UILabel!
    @IBOutlet weak var lblDescription : UILabel!
    @IBOutlet weak var btnNext : UIButton!
}



//{
//    message = "Group Detail";
//    response =     {
//        balance = 25;
//        "group_detail" =         (
//            {
//                "country_code" = IN;
//                "created_date" = "2018-10-10";
//                "expense_amount" = 50;
//                "expense_image" = "";
//                "expense_title" = "rental bill";
//                "expense_type" = 1;
//                "group_id" = 15;
//                "group_members" =                 (
//                    {
//                        "created_at" = "2018-10-03 12:26:20";
//                        "group_id" = 15;
//                        id = 5;
//                        "make_group_id" = 15;
//                        "updated_at" = "2018-10-03 12:26:20";
//                        user =                         {
//                            age = 24;
//                            city = "";
//                            "contact_advice" = "";
//                            country = "";
//                            "country_code" = "";
//                            "country_latitude" = 0;
//                            "country_longitude" = 0;
//                            "created_at" = "2018-09-10 09:56:28";
//                            email = "test@test.com";
//                            "email_verified" = 0;
//                            "facebook_id" = "";
//                            "fcm_token" = "";
//                            gender = 1;
//                            "google_id" = "";
//                            id = 2;
//                            "is_active" = 0;
//                            "is_admin_verified" = 0;
//                            "is_contact_private" = 0;
//                            "linkedin_id" = "";
//                            "login_type" = 1;
//                            mobile = 1234567890;
//                            "mobile_verified" = 1;
//                            name = "test test";
//                            "profile_pic" = "";
//                            state = "";
//                            status = 0;
//                            "updated_at" = "2018-10-11 10:58:43";
//                            "user_id" = 2;
//                            "user_latitude" = 0;
//                            "user_longitude" = 0;
//                        };
//                        "user_id" = 2;
//                },
//                    {
//                        "created_at" = "2018-10-04 07:50:33";
//                        "group_id" = 15;
//                        id = 8;
//                        "make_group_id" = 15;
//                        "updated_at" = "2018-10-04 07:50:33";
//                        user =                         {
//                            age = 21;
//                            city = "";
//                            "contact_advice" = "";
//                            country = BN;
//                            "country_code" = IN;
//                            "country_latitude" = 0;
//                            "country_longitude" = 0;
//                            "created_at" = "2018-09-10 09:58:00";
//                            email = "bansal@gmail.com";
//                            "email_verified" = 0;
//                            "facebook_id" = "";
//                            "fcm_token" = "dORaaDlFeVw:APA91bHLOiPgmKNWgEeksynUtC_KjC72uFOTnzn-ivReidJEtTC-uqGROiLIwsdSEYoCiqmWqM4KZ9zA6V-tBZzVhpjEFVUWifaNV2Aqj8L8SUE_Kpd0bcBnWzXOOdkg1XCYBm-kutq4";
//                            gender = 2;
//                            "google_id" = 109070728450137512091;
//                            id = 3;
//                            "is_active" = 0;
//                            "is_admin_verified" = 0;
//                            "is_contact_private" = 0;
//                            "linkedin_id" = "";
//                            "login_type" = 1;
//                            mobile = 7987191186;
//                            "mobile_verified" = 1;
//                            name = "julee ban";
//                            "profile_pic" = "http://18.223.28.186/upload/profile_pic/1536578821.jpg";
//                            state = "";
//                            status = 0;
//                            "updated_at" = "2018-10-11 06:56:54";
//                            "user_id" = 3;
//                            "user_latitude" = 0;
//                            "user_longitude" = 0;
//                        };
//                        "user_id" = 3;
//                }
//                );
//                id = 10;
//                "updated_at" = "2018-10-10 09:39:33";
//                user =                 {
//                    age = 21;
//                    city = "";
//                    "contact_advice" = "";
//                    country = BN;
//                    "country_code" = IN;
//                    "country_latitude" = 0;
//                    "country_longitude" = 0;
//                    "created_at" = "2018-09-10 09:58:00";
//                    email = "bansal@gmail.com";
//                    "email_verified" = 0;
//                    "facebook_id" = "";
//                    "fcm_token" = "dORaaDlFeVw:APA91bHLOiPgmKNWgEeksynUtC_KjC72uFOTnzn-ivReidJEtTC-uqGROiLIwsdSEYoCiqmWqM4KZ9zA6V-tBZzVhpjEFVUWifaNV2Aqj8L8SUE_Kpd0bcBnWzXOOdkg1XCYBm-kutq4";
//                    gender = 2;
//                    "google_id" = 109070728450137512091;
//                    id = 3;
//                    "is_active" = 0;
//                    "is_admin_verified" = 0;
//                    "is_contact_private" = 0;
//                    "linkedin_id" = "";
//                    "login_type" = 1;
//                    mobile = 7987191186;
//                    "mobile_verified" = 1;
//                    name = "julee ban";
//                    "profile_pic" = "http://18.223.28.186/upload/profile_pic/1536578821.jpg";
//                    state = "";
//                    status = 0;
//                    "updated_at" = "2018-10-11 06:56:54";
//                    "user_id" = 3;
//                    "user_latitude" = 0;
//                    "user_longitude" = 0;
//                };
//                "user_expense_share" =                 (
//                    {
//                        amount = 25;
//                        "created_at" = "2018-10-10 09:39:33";
//                        "expense_id" = 10;
//                        "expense_share_id" = 20;
//                        id = 20;
//                        status = 5;
//                        "updated_at" = "2018-10-11 06:49:32";
//                        user =                         {
//                            age = 24;
//                            city = "";
//                            "contact_advice" = "";
//                            country = "";
//                            "country_code" = "";
//                            "country_latitude" = 0;
//                            "country_longitude" = 0;
//                            "created_at" = "2018-09-10 09:56:28";
//                            email = "test@test.com";
//                            "email_verified" = 0;
//                            "facebook_id" = "";
//                            "fcm_token" = "";
//                            gender = 1;
//                            "google_id" = "";
//                            id = 2;
//                            "is_active" = 0;
//                            "is_admin_verified" = 0;
//                            "is_contact_private" = 0;
//                            "linkedin_id" = "";
//                            "login_type" = 1;
//                            mobile = 1234567890;
//                            "mobile_verified" = 1;
//                            name = "test test";
//                            "profile_pic" = "";
//                            state = "";
//                            status = 0;
//                            "updated_at" = "2018-10-11 10:58:43";
//                            "user_id" = 2;
//                            "user_latitude" = 0;
//                            "user_longitude" = 0;
//                        };
//                        "user_id" = 2;
//                },
//                    {
//                        amount = 25;
//                        "created_at" = "2018-10-10 09:39:33";
//                        "expense_id" = 10;
//                        "expense_share_id" = 21;
//                        id = 21;
//                        status = 0;
//                        "updated_at" = "2018-10-10 09:39:33";
//                        user =                         {
//                            age = 21;
//                            city = "";
//                            "contact_advice" = "";
//                            country = BN;
//                            "country_code" = IN;
//                            "country_latitude" = 0;
//                            "country_longitude" = 0;
//                            "created_at" = "2018-09-10 09:58:00";
//                            email = "bansal@gmail.com";
//                            "email_verified" = 0;
//                            "facebook_id" = "";
//                            "fcm_token" = "dORaaDlFeVw:APA91bHLOiPgmKNWgEeksynUtC_KjC72uFOTnzn-ivReidJEtTC-uqGROiLIwsdSEYoCiqmWqM4KZ9zA6V-tBZzVhpjEFVUWifaNV2Aqj8L8SUE_Kpd0bcBnWzXOOdkg1XCYBm-kutq4";
//                            gender = 2;
//                            "google_id" = 109070728450137512091;
//                            id = 3;
//                            "is_active" = 0;
//                            "is_admin_verified" = 0;
//                            "is_contact_private" = 0;
//                            "linkedin_id" = "";
//                            "login_type" = 1;
//                            mobile = 7987191186;
//                            "mobile_verified" = 1;
//                            name = "julee ban";
//                            "profile_pic" = "http://18.223.28.186/upload/profile_pic/1536578821.jpg";
//                            state = "";
//                            status = 0;
//                            "updated_at" = "2018-10-11 06:56:54";
//                            "user_id" = 3;
//                            "user_latitude" = 0;
//                            "user_longitude" = 0;
//                        };
//                        "user_id" = 3;
//                }
//                );
//                "user_id" = 3;
//            }
//        );
//        owed = 0;
//        owes = 25;
//    };
//    status = 1;
//}

