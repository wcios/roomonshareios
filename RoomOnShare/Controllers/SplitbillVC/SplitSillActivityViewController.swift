//
//  SplitSillActivityViewController.swift
//  RoomOnShare
//
//  Created by mac on 06/09/18.
//  Copyright © 2018 WestCoast. All rights reserved.
//

import UIKit

class SplitSillActivityViewController: UIViewController {
    
    @IBOutlet weak var tblActivity : UITableView!

    override func viewDidLoad() {
        super.viewDidLoad()
        tblActivity.tableFooterView=UIView()
        tblActivity.estimatedRowHeight = 85.0
        tblActivity.rowHeight = UITableViewAutomaticDimension
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    override func viewWillAppear(_ animated: Bool) {
        self.navigationController?.isNavigationBarHidden = true
        rdv_tabBarController?.setTabBarHidden(false, animated: true)
    }
    
    @IBAction func clickOnBackBtn(_ sender : UIButton){
        
        Common.PopMethod(VC: self)
    }
    
}

extension SplitSillActivityViewController : UITableViewDelegate , UITableViewDataSource{
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 5
    }
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tblActivity.dequeueReusableCell(withIdentifier: "activityBillTblCell") as! activityBillTblCell
        cell.lblDescription.text = "Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco."
        
        
        return cell
    }
    
}

class activityBillTblCell: UITableViewCell {
    
    @IBOutlet weak var lblTitle : UILabel!
    @IBOutlet weak var lblDescription : UILabel!
    @IBOutlet weak var lblDateTime : UILabel!
}

