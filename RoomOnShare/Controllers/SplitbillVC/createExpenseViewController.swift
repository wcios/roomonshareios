
import UIKit
import Alamofire
import SwiftyJSON

class createExpenseViewController: UIViewController {
    
    @IBOutlet weak var tblUserList : UITableView!
    @IBOutlet weak var imgDetail : UIImageView!
    @IBOutlet weak var txtExpenseTitle : UITextField!
    @IBOutlet weak var txtAmount : UITextField!
    @IBOutlet var ExpenseType: [UIButton]!
    
    var dicGroup : NSDictionary = [:]
    var Imagepicker = UIImagePickerController()
    var Pickimage : UIImage? = nil
    var Expanse_type : Int = 0
    var Group_id : Int = 0
    var arrGroupMember : Array<Any> = []
    var UserExpanseShare : Array<Any> = []
    var amount = "0"
    var countrycode = ""
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.txtAmount.delegate = self
        self.Imagepicker.delegate = self
        self.Imagepicker.allowsEditing = true
    
        self.tblUserList.tableFooterView=UIView()
        self.tblUserList.estimatedRowHeight = 120.0
        self.tblUserList.rowHeight = UITableViewAutomaticDimension
        self.GroupMemberListService()
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    
    @IBAction func clickOnBtnBack(_ sender : UIButton){
        Common.PopMethod(VC: self)
    }
    
    @IBAction func PickImageMethod(_ sender: Any) {
        Common.ActionSheetForGallaryAndCamera(Picker: Imagepicker, VC: self)
    }
    
    @IBAction func ExpanceTypeMethod(_ sender: UIButton) {
        let Selectedtag = sender.tag
        self.Expanse_type = Selectedtag
        self.CheckUnceck(arrButton: self.ExpenseType, Selected: Selectedtag)
    }
    
    func CheckUnceck(arrButton : [UIButton], Selected : Int) {
        for button in arrButton {
            let tag = button.tag
            if Selected == tag{
                button.isSelected = true
                button.tintColor = self.UIColorFromHex(rgbValue: 0x05C6CF, alpha: 1.0)
            }else{
                button.isSelected = false
            }
        }
    }
    
    @IBAction func UpdateExpanseMethod(_ sender: Any) {
        guard let title = self.txtExpenseTitle.text, title != ""  else {
            return Common.ShowAlert(Title: "Alert", Message: "Please enter expense title", VC: self)
        }
        
        guard self.amount != "0" else {
            return Common.ShowAlert(Title: "Alert", Message: "Please enter amount", VC: self)
        }
        
        guard self.Expanse_type != 0 else {
            return Common.ShowAlert(Title: "Alert", Message: "Please select expense type", VC: self)
        }
        
        self.createExpenseAlamofire()
        
    }
    
    /////////// Group Member List ///////////
    func GroupMemberListService(){
        self.startActivityIndicator()
        
        let param = ["group_id": self.dicGroup.value(forKey:"id") as! Int] as [String : Any]
        
        CommunicationManager().getResponseForPost(strUrl: Url_File.GroupMemberList, parameters: param as NSDictionary) { ( result , data) in
            if(result == "success") {
                let dataDict = data as! NSDictionary
                print(dataDict)
                let response = dataDict.value(forKey: "response") as! NSDictionary
                self.arrGroupMember = response.value(forKey: "members") as! Array<Any>
                self.tblUserList.reloadData()
                self.stopActivityIndicator()
            }else if (result == "error") {
                let strMessage = (data as! NSDictionary)["message"] as! String
                Common.ShowAlert(Title: Common.Title, Message: strMessage, VC: self)
                self.stopActivityIndicator()
            }else if (result == "Network") {
                let strMessage = (data as! String)
                Common.ShowAlert(Title: Common.Title, Message: strMessage, VC: self)
                self.stopActivityIndicator()
            }
        }
    }
    
    /////////// Create Expance ///////////
    
    
    
    
    func createExpenseAlamofire(){
        
      //  self.startActivityIndicator()
       
        var secretKey = ""
        if SharedPreference.getIsUserLogin() {
            secretKey = SharedPreference.getUserData().user_authtoken
        }
        
        let headers: HTTPHeaders = [
            /* "Authorization": "your_access_token",  in case you need authorization header */
           // "Content-type": "multipart/form-data",
            "Authorization" : "Bearer \(secretKey)"
        ]
        
        let parameters = [
                    "group_id": "\(self.dicGroup.value(forKey:"id") as! Int)",
                    "expense_title" : self.txtExpenseTitle.text!,
                    "expense_amount" :self.txtAmount.text!,
                    "expense_type" : self.Expanse_type,
                    "country_code" : self.countrycode
            ] as [String : AnyObject]   //"user_expense_share" : self.UserExpanseShare ,
        
        print(parameters)
        
        if(self.imgDetail.image == nil){
            self.sendImageToServerWithURL(Url_File.CreateExpance, method: .post, headers: headers, parameters: parameters, imageData: nil, imageName: "")
        }else{
           let image_data = UIImageJPEGRepresentation(self.imgDetail.image!, 0.5)
            self.sendImageToServerWithURL(Url_File.CreateExpance, method: .post, headers: headers, parameters: parameters, imageData: image_data, imageName: "")
        }
        
    }
    
    func sendImageToServerWithURL(_ URLString: URLConvertible, method: HTTPMethod, headers: [String : String]?, parameters: [String: AnyObject]?, imageData : Data?,imageName:String) {
        
        Alamofire.upload(multipartFormData: { (multipartFormData) in
            
            if((imageData) != nil)
            {
                multipartFormData.append(imageData!, withName: "expense_pic", fileName: "file_name",mimeType: "image/jpeg")
                
            }
            
            for (key, value) in parameters!
            {
                multipartFormData.append("\(value)".data(using: String.Encoding(rawValue: String.Encoding.utf8.rawValue))!, withName: key)
            }
        }, to:URLString ,headers : headers)
        { (result) in
            switch result {
            case .success(let upload, _, _):
                
                upload.responseJSON { response in
                    print (response.result)
                }
                
            case .failure( _): break
            }
        }
    }
}

extension createExpenseViewController : UITableViewDelegate , UITableViewDataSource{
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.arrGroupMember.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell{
        let cell = self.tblUserList.dequeueReusableCell(withIdentifier: "expenseTblCell") as! expenseTblCell
        let dic = self.arrGroupMember[indexPath.row] as! NSDictionary
        let user = dic.value(forKey: "user") as! NSDictionary
        
        cell.lblName.text = user.value(forKey: "name") as? String
        cell.lblDescription.text = user.value(forKey: "email") as? String
        let img_pro = user.value(forKey: "profile_pic") as! String
        cell.imgGroup.sd_setImage(with: URL(string: img_pro), placeholderImage: UIImage(named: "UserPlaceholder"))
        cell.lblPrice.text = self.amount
        self.countrycode = user.value(forKey: "country_code") as! String
        let dicShare : NSDictionary = ["user_id" : dic.value(forKey: "user_id") as! Int, "amount" : self.amount] as NSDictionary
        self.UserExpanseShare.append(dicShare)
        print(UserExpanseShare)
        return cell
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 89
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
     //   let story = AppStoryboard.Home.instance
       // let activity = story.instantiateViewController(withIdentifier: "ExpenseDetailViewController") as! ExpenseDetailViewController
      //  self.navigationController?.pushViewController(activity, animated: true)
    }
    
}

extension createExpenseViewController : UIImagePickerControllerDelegate, UINavigationControllerDelegate,CropViewControllerDelegate{
    func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [String : Any]) {
        let image = info[UIImagePickerControllerOriginalImage]
        self.Pickimage = image as? UIImage
        picker.dismiss(animated: true, completion: nil)
        self.OpenEditor()
    }
    
    func imagePickerControllerDidCancel(_ picker: UIImagePickerController) {
        picker.dismiss(animated: true, completion: nil)
    }
    
    func OpenEditor(){
        guard let image = self.Pickimage else {
            return
        }
        // Use view controller
        let controller = CropViewController()
        controller.delegate = self
        controller.image = image
        let navController = UINavigationController(rootViewController: controller)
        present(navController, animated: false, completion: nil)
    }
    
    // MARK: - CropView
    func cropViewController(_ controller: CropViewController, didFinishCroppingImage image: UIImage) {
    }
    
    func cropViewController(_ controller: CropViewController, didFinishCroppingImage image: UIImage, transform: CGAffineTransform, cropRect: CGRect) {
        controller.dismiss(animated: true, completion: nil)
        self.Pickimage = image
        self.imgDetail.image = image
    }
    
    func cropViewControllerDidCancel(_ controller: CropViewController) {
        controller.dismiss(animated: true, completion: nil)
    }
}

class expenseTblCell: UITableViewCell {
    
    @IBOutlet weak var imgGroup : UIImageView!
    @IBOutlet weak var lblName : UILabel!
    @IBOutlet weak var lblPrice : UILabel!
    @IBOutlet weak var lblDescription : UILabel!
    @IBOutlet weak var lblOwes : UILabel!
}

extension createExpenseViewController : UITextFieldDelegate{
    
    func textFieldDidEndEditing(_ textField: UITextField) {
        if textField == self.txtAmount{
            if self.txtAmount.text != ""{
                let totel = Float(self.txtAmount.text!)
                let member = Float(self.arrGroupMember.count)
                print(totel)
                print(member)
                self.amount = String(totel!/member)
                print(amount)
                self.UserExpanseShare.removeAll()
                self.tblUserList.reloadData()
            }
        }
    }
    
}

// Uncomment to use crop view directly
//        let imgView = UIImageView(image: image)
//        imgView.clipsToBounds = true
//        imgView.contentMode = .ScaleAspectFit
//
//        let cropView = CropView(frame: imageView.frame)
//
//        cropView.opaque = false
//        cropView.clipsToBounds = true
//        cropView.backgroundColor = UIColor.clearColor()
//        cropView.imageView = imgView
//        cropView.showCroppedArea = true
//        cropView.cropAspectRatio = 1.0
//        cropView.keepAspectRatio = true
//
//        view.insertSubview(cropView, aboveSubview: imageView)
