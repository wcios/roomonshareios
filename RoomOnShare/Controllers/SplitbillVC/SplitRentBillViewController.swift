
import UIKit

class SplitRentBillViewController: UIViewController {
    
    @IBOutlet weak var viewNoGroup : UIView!
    @IBOutlet weak var viewGroup : UIView!
    @IBOutlet weak var btnQRPay : UIButton!
    @IBOutlet weak var tblGroupList : UITableView!
    @IBOutlet weak var lblOwes: UILabel!
    @IBOutlet weak var lblOwed: UILabel!
    @IBOutlet weak var lblBalance: UILabel!
    
    var arrGroupListing : Array<Any> = []
    
    override func viewDidLoad() {
        super.viewDidLoad()
        tblGroupList.tableFooterView=UIView()
        tblGroupList.estimatedRowHeight = 120.0
        tblGroupList.rowHeight = UITableViewAutomaticDimension
        self.ViewSetup()
        
    }
    override func viewWillAppear(_ animated: Bool) {
        self.navigationController?.isNavigationBarHidden = true
        rdv_tabBarController?.setTabBarHidden(false, animated: true)
        self.GroupListService()
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    
    func ViewSetup() {
        if(self.arrGroupListing.count==0){
            self.viewNoGroup.isHidden=false
            self.viewGroup.isHidden=true
            self.btnQRPay.isHidden=true
        }else{
            self.viewNoGroup.isHidden=true
            self.viewGroup.isHidden=false
            self.btnQRPay.isHidden=false
        }
    }
    
    @IBAction func clickOnNotificationBtn(_ sender : UIButton){
        let story = AppStoryboard.Home.instance
        let activity = story.instantiateViewController(withIdentifier: "SplitSillActivityViewController") as! SplitSillActivityViewController
        self.navigationController?.pushViewController(activity, animated: true)
    }
    
    @IBAction func clickOnCreateGroupBtn(_ sender : UIButton){
        let story = AppStoryboard.Home.instance
        let activity = story.instantiateViewController(withIdentifier: "createGroupViewController") as! createGroupViewController
        activity.isNavigateFrom="create"
        self.navigationController?.pushViewController(activity, animated: true)
    }
    
    //GroupListListing
    func GroupListService() {
        
        self.startActivityIndicator()
        
        CommunicationManager().getResponseForPost(strUrl: Url_File.GroupList, parameters: nil, completion: { ( result , data) in
            
            if(result == "success") {
                let dataDict = data as! NSDictionary
                print(dataDict)
                let dic = dataDict["response"] as! NSDictionary
                self.Setdata(dicData: dataDict.value(forKey: "response") as! NSDictionary)                
                self.ViewSetup()
                self.stopActivityIndicator()
            }else if (result == "error") {
                let strMessage = (data as! NSDictionary)["message"] as! String
                Common.ShowAlert(Title: Common.Title, Message: strMessage, VC: self)
                self.stopActivityIndicator()
            }else if (result == "Network") {
                let strMessage = (data as! String)
                Common.ShowAlert(Title: Common.Title, Message: strMessage, VC: self)
                self.stopActivityIndicator()
            }
        })
    }
    
    func Setdata(dicData : NSDictionary)  {
        self.lblOwed.text = String(dicData.value(forKey: "owed") as! Int)
        self.lblOwes.text = String(dicData.value(forKey: "owes") as! Int)
        self.lblBalance.text = String(dicData.value(forKey: "balance") as! Int)
        
        self.arrGroupListing = dicData.value(forKey: "group_lists") as! Array<Any>
        self.tblGroupList.reloadData()
    }
}

extension SplitRentBillViewController : UITableViewDelegate , UITableViewDataSource{
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.arrGroupListing.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell{
        let cell = tblGroupList.dequeueReusableCell(withIdentifier: "groupTblCell") as! groupTblCell
        let dic  = self.arrGroupListing[indexPath.row] as! NSDictionary
        cell.lblGroupName.text = dic.value(forKey: "name") as? String
        let type = dic.value(forKey: "type") as! Int
        cell.imgGroup.image = UIImage(named: Array_File.arrGroupList[type - 1]["image"]!)
        let arrUser = dic.value(forKey: "group_user") as! Array<Any>
        if arrUser.count  != 0 {
            cell.lblMemberCount.text = String(arrUser.count)
        }else{
            cell.lblMemberCount.text = "0"
        }
        
        cell.btnNext.tag=indexPath.row
        cell.btnNext.addTarget(self, action: #selector(clickOnNextBtn(_:)), for: .touchUpInside)
        return cell
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 89
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let dic  = self.arrGroupListing[indexPath.row] as! NSDictionary
        let story = AppStoryboard.Home.instance
        let activity = story.instantiateViewController(withIdentifier: "GroupDetailViewController") as! GroupDetailViewController
        activity.dicGroupDetail = dic
        self.navigationController?.pushViewController(activity, animated: true)
    }
    
    @objc func clickOnNextBtn(_ sender : UIButton){
        let story = AppStoryboard.Home.instance
        let activity = story.instantiateViewController(withIdentifier: "GroupDetailViewController") as! GroupDetailViewController
        self.navigationController?.pushViewController(activity, animated: true)
    }
}

class groupTblCell: UITableViewCell {
    
    @IBOutlet weak var lblMemberCount: UILabel!
    @IBOutlet weak var imgGroup : UIImageView!
    @IBOutlet weak var lblGroupName : UILabel!
    @IBOutlet weak var btnNext : UIButton!
}
