import UIKit

class ExpenseDetailViewController: UIViewController {
    
    @IBOutlet weak var tblExpenseDetailList : UITableView!
    
    @IBOutlet weak var imgBil : UIImageView!
    @IBOutlet weak var imgIcon : UIImageView!
    @IBOutlet weak var lblBillName : UILabel!
    @IBOutlet weak var lblBillPrice : UILabel!
    @IBOutlet weak var lblAddedBy : UILabel!
    @IBOutlet weak var lblDate : UILabel!
    
    
    var dictDetail : NSDictionary = [:]
    var arrGroupMem : [NSDictionary] = []
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        
       self.showExpenseDataOnHeader()
        
        self.tblExpenseDetailList.tableFooterView=UIView()
        self.tblExpenseDetailList.estimatedRowHeight = 150.0
        self.tblExpenseDetailList.rowHeight = UITableViewAutomaticDimension
    }
    
    
    override func viewWillAppear(_ animated: Bool) {
        self.navigationController?.isNavigationBarHidden = true
        rdv_tabBarController?.setTabBarHidden(true, animated: true)
    }
    
    
    func showExpenseDataOnHeader(){
         print(dictDetail)
        
        self.arrGroupMem = dictDetail["group_members"] as! [NSDictionary]
        
        self.lblBillName.text = dictDetail["expense_title"] as? String
        let type = dictDetail["expense_type"] as! Int
        self.imgIcon.image = UIImage(named: Array_File.arrGroupList[type - 1]["image"]!)
        let arrcountries = countrydetail.readCountryJson()
        let countryCode = dictDetail["country_code"] as! String
        for item in arrcountries {
            if(countryCode == item.Country_code){
                let price = dictDetail["expense_amount"] as! String
                self.lblBillPrice.text = String("\(item.currency_symbol) \(price)")
            }
        }
        let user = dictDetail["user"] as! NSDictionary
        let userID = user["user_id"] as! Int
        if(userID == SharedPreference.getUserData().user_id){
           self.lblAddedBy.text = "Added by me"
        }else{
            self.lblAddedBy.text = "Added by \(user["name"] as! String)"
        }
        let date = self.dictDetail["created_date"] as! String
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "yyyy-MM-dd"
        self.lblDate.text = Common.ChangeDateFormat(Date: date, fromFormat: "yyyy-MM-dd", toFormat: "dd MMM yyyy")
        
        
        self.tblExpenseDetailList.reloadData()
    }
    
    @IBAction func clickOnBtnBack(_ sender : UIButton){
        Common.PopMethod(VC: self)
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
}

extension ExpenseDetailViewController : UITableViewDelegate , UITableViewDataSource{
    func numberOfSections(in tableView: UITableView) -> Int {
        return self.arrGroupMem.count
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell{
        let cell = self.tblExpenseDetailList.dequeueReusableCell(withIdentifier: "expenseDetailTblCell") as! expenseDetailTblCell
        let dict = self.arrGroupMem[indexPath.section]
        print(dict)
        let userDict = dict["user"] as! NSDictionary
        
        cell.lblName.text = userDict["name"] as? String
        cell.lblDescription.text = userDict["email"] as? String
        
        let status = userDict["email"] as! String
        switch status {
        case "0":
            cell.lblOwes.text = "owes"
        case "1":
            cell.lblOwes.text = "paid"
        case "2":
            cell.lblOwes.text = "deny"
        case "3":
            cell.lblOwes.text = "Paid in cash"
        case "4":
            cell.lblOwes.text = ""
        case "5":
            cell.lblOwes.text = "confirm"
            
        default:
            print("default")
        }
        
        let img_pro = userDict["profile_pic"] as? String
        cell.imgGroup.sd_setImage(with: URL(string: img_pro!), placeholderImage: UIImage(named: "placeholder"))
        
        cell.btnPay.tag=indexPath.row
        cell.btnPay.addTarget(self, action: #selector(clickOnPayBtn(_:)), for: .touchUpInside)
        
        cell.btnQRCode.tag=indexPath.row
        cell.btnQRCode.addTarget(self, action: #selector(clickOnQrCodeBtn(_:)), for: .touchUpInside)
        return cell
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 115
    }
    
    @objc func clickOnPayBtn(_ sender : UIButton){
        let story = AppStoryboard.Home.instance
        let activity = story.instantiateViewController(withIdentifier: "MakePaymentViewController") as! MakePaymentViewController
        self.navigationController?.pushViewController(activity, animated: true)
    }
    
    
    @objc func clickOnQrCodeBtn(_ sender : UIButton){
        
        let userExpnS = dictDetail["user_expense_share"] as! [NSDictionary]
        let dict = userExpnS[sender.tag] as! NSDictionary
        
        let story = AppStoryboard.Home.instance
        let activity = story.instantiateViewController(withIdentifier: "QRCodeGeneatorVC") as! QRCodeGeneatorVC
        activity.dataDict = dict
        activity.expenseTitle = dictDetail["expense_title"] as! String
        self.navigationController?.pushViewController(activity, animated: true)
        
    }
    
    
}

class expenseDetailTblCell: UITableViewCell {
    
    @IBOutlet weak var imgGroup : UIImageView!
    @IBOutlet weak var lblName : UILabel!
    @IBOutlet weak var lblPrice : UILabel!
    @IBOutlet weak var lblDescription : UILabel!
    @IBOutlet weak var lblOwes : UILabel!
    @IBOutlet weak var btnDeny : UIButton!
    @IBOutlet weak var btnPay : UIButton!
    @IBOutlet weak var btnQRCode : UIButton!
}
