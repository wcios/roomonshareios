

import UIKit

class QRCodeGeneatorVC: UIViewController {
    
    @IBOutlet weak var imgQRCode: UIImageView!
    @IBOutlet weak var btnAction: UIButton!
    @IBOutlet weak var slider: UISlider!
    
    
    var qrcodeImage: CIImage!
    
    var expenseTitle = ""
    var dataDict : NSDictionary = [:]

    override func viewDidLoad() {
        super.viewDidLoad()
       
    }
    
    
    override func viewWillAppear(_ animated: Bool) {
        self.navigationController?.isNavigationBarHidden = true
        rdv_tabBarController?.setTabBarHidden(true, animated: true)
         self.generateQRCode(dict: self.dataDict)
    }
    
    func generateQRCode(dict : NSDictionary){
        if qrcodeImage == nil {
            let expense_id = dict["expense_id"] as! Int
            let expense_amount = dict["amount"] as! String
            let expense_share_id = dict["expense_share_id"] as! Int
            
            let userDict = dict["user"] as! NSDictionary
            let email = userDict["email"] as! String
            let name = userDict["name"] as! String
            
            let json = ["full_name":name,"amount":expense_amount,"expense_id":expense_id,"expense_share_id":expense_share_id,"expense_title":expenseTitle,"email":email] as [String : Any]
            do {
                let data1 =  try JSONSerialization.data(withJSONObject: json, options: JSONSerialization.WritingOptions.prettyPrinted) // first of all convert json to the data
                let convertedString = String(data: data1, encoding: String.Encoding.utf8)
                let data = convertedString?.data(using: String.Encoding.isoLatin1, allowLossyConversion: false)
                let filter = CIFilter(name: "CIQRCodeGenerator")
                filter?.setValue(data, forKey: "inputMessage")
                filter?.setValue("Q", forKey: "inputCorrectionLevel")
                qrcodeImage = filter?.outputImage
                displayQRCodeImage()
            }
            catch let myJSONError {
                print(myJSONError)
            }
        }
        else {
            imgQRCode.image = nil
            qrcodeImage = nil
            btnAction.setTitle("Generate", for: .normal)
            
        }
    }
    
    
    func displayQRCodeImage() {
        let scaleX = imgQRCode.frame.size.width / qrcodeImage.extent.size.width
        let scaleY = imgQRCode.frame.size.height / qrcodeImage.extent.size.height
        let transformedImage = qrcodeImage.transformed(by: CGAffineTransform(scaleX: scaleX, y: scaleY))
        imgQRCode.image = UIImage(ciImage: transformedImage)
        
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    
    @IBAction func shareImageButton(_ sender: UIButton) {
        
        // image to share
        // set up activity view controller
        let imageToShare = [ imgQRCode! ]
        let activityViewController = UIActivityViewController(activityItems: imageToShare, applicationActivities: nil)
        activityViewController.popoverPresentationController?.sourceView = self.view // so that iPads won't crash
        
        // exclude some activity types from the list (optional)
     //   activityViewController.excludedActivityTypes = [ UIActivityType.airDrop, UIActivityType.postToFacebook ]
        
        // present the view controller
        self.present(activityViewController, animated: true, completion: nil)
    }

    
    @IBAction func clickOnBackBtn(_ sender : UIButton){
        Common.PopMethod(VC: self)
    }


}
