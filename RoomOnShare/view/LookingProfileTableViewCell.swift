//
//  LookingProfileTableViewCell.swift
//  RoomOnShare
//
//  Created by mac on 17/08/18.
//  Copyright © 2018 WestCoast. All rights reserved.
//

import UIKit

class LookingProfileTableViewCell: UITableViewCell {
    
    
    @IBOutlet weak var imgProfile: UIImageView!
    @IBOutlet weak var lblName: UILabel!
    @IBOutlet weak var lblAddress: UILabel!
    @IBOutlet weak var lblStatus: UILabel!
    @IBOutlet weak var lblDate: UILabel!    
    @IBOutlet weak var lblviewsCount: UILabel!
    @IBOutlet weak var btnEdit: UIButton!
    @IBOutlet weak var btnDelete: UIButton!
    @IBOutlet weak var btnMsg: UIButton!
    @IBOutlet weak var btnMatches: UIButton!
    @IBOutlet weak var lblAvailable: UILabel!
    @IBOutlet weak var lblActive: UILabel!
    
    
    @IBOutlet weak var viewoffer: UIView!
    @IBOutlet weak var imgHometype: UIImageView!
    @IBOutlet weak var imgParkingtype: UIImageView!
    
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
}
