//
//  detailBedrromDescrptnTblCell.swift
//  RoomOnShare
//
//  Created by mac on 28/08/18.
//  Copyright © 2018 WestCoast. All rights reserved.
//

import UIKit

class detailBedrromDescrptnTblCell: UITableViewCell {

    //Bedroom description
    
    
    @IBOutlet weak var lblBedroomtype: UILabel!
    @IBOutlet weak var lblDateAvail : UILabel!
    @IBOutlet weak var lblMin : UILabel!
    @IBOutlet weak var lblMax : UILabel!
    @IBOutlet weak var lblSinglePerson : UILabel!
    @IBOutlet weak var lblCouple : UILabel!
    @IBOutlet weak var lblRentInclude : UILabel!
    @IBOutlet weak var lblBillInclude : UILabel!
    @IBOutlet weak var lblBedroomSize : UILabel!
    @IBOutlet weak var lblBedroomFurniture : UILabel!
    @IBOutlet weak var lblNoOfBeds : UILabel!
    @IBOutlet weak var lblBathroomFacility : UILabel!
    @IBOutlet weak var lblDeposit : UILabel!
    @IBOutlet weak var lblRoomFeature : UILabel!
    @IBOutlet weak var lblOtherInfo : UILabel!
    
    @IBOutlet weak var btnDelete : UIButton!
    @IBOutlet weak var btnEdit : UIButton!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
