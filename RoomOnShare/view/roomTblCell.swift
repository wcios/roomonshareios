//
//  MatchTblCellView.swift
//  RoomOnShare
//
//  Created by mac on 14/08/18.
//  Copyright © 2018 WestCoast. All rights reserved.
//

import UIKit

class roomTblCell: UITableViewCell {
    
    @IBOutlet weak var imgRoom: UIImageView!
    @IBOutlet weak var imgProfile: UIImageView!
    @IBOutlet weak var imgV1: UIImageView!
    @IBOutlet weak var imgV2: UIImageView!
    @IBOutlet weak var imgV3: UIImageView!
    @IBOutlet weak var imgV4: UIImageView!
    @IBOutlet weak var imgV5: UIImageView!
    @IBOutlet weak var lblName: UILabel!
    @IBOutlet weak var lblStatus: UILabel!
    @IBOutlet weak var lblDate: UILabel!
    @IBOutlet weak var lblAddress: UILabel!
    @IBOutlet weak var btnLike: UIButton!
    @IBOutlet weak var lblPrice: UILabel!
    @IBOutlet weak var imgHometype: UIImageView!
    @IBOutlet weak var imgParkingtype: UIImageView!
    
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
