//
//  roomMateTblCell.swift
//  RoomOnShare
//
//  Created by mac on 14/08/18.
//  Copyright © 2018 WestCoast. All rights reserved.
//

import UIKit

class roomMateTblCell: UITableViewCell {
    
    
    @IBOutlet weak var imgV1: UIImageView!
    @IBOutlet weak var imgV2: UIImageView!
    @IBOutlet weak var imgV3: UIImageView!
    @IBOutlet weak var imgV4: UIImageView!
    @IBOutlet weak var imgV5: UIImageView!    
    @IBOutlet weak var imgProfile: UIImageView!
    @IBOutlet weak var lblName: UILabel!
    @IBOutlet weak var lblAgeGender: UILabel!
    @IBOutlet weak var lblDate: UILabel!
    @IBOutlet weak var lblViews: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
