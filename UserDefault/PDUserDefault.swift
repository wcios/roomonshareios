//
//  PDUserDefault.swift
//  TheBaby
//
//  Created by mac on 20/04/17.
//  Copyright © 2017 espsofttech. All rights reserved.
//

import UIKit

class PDUserDefault: NSObject {
    
    
    ///////// User Registration ////////
    
    static var IsLogin = DefaultsKey<Bool?>("IsLogin")
    static var CurrentLatitude = DefaultsKey<String?>("CurrentLatitude")
    static var CurrentLongitude = DefaultsKey<String?>("CurrentLongitude")
    
    static var FCMToken = DefaultsKey<String?>("FCMToken")
    static var password = DefaultsKey<String?>("password")
    static var launchCount = DefaultsKey<Int>("launchCount")
    
    
    static var EventID = DefaultsKey<String?>("EventID")
    static var username = DefaultsKey<String?>("username")
    static var UserID = DefaultsKey<String?>("UserID")
    static var UserType = DefaultsKey<String?>("UserType")
    static var FullName = DefaultsKey<String?>("FullName")
    static var UserEmail = DefaultsKey<String?>("UserEmail")
    static var PhoneNumber = DefaultsKey<String?>("PhoneNumber")
    static var Gender = DefaultsKey<String?>("Gender")
    static var Photo = DefaultsKey<String?>("Photo")
    static var ImgUrl = DefaultsKey<String?>("ImgUrl")
    static var Address = DefaultsKey<String?>("Address")
    static var Mobileverify = DefaultsKey<String?>("Mobileverify")
    static var Loginstatus = DefaultsKey<String?>("Loginstatus")
    static var ProfileImage = DefaultsKey<String?>("ProfileImage")
    
    static var ThemeID = DefaultsKey<String?>("ThemeID")
    static var ThemeColor = DefaultsKey<String?>("ThemeColor")
    
    static var UIviewControllwe = DefaultsKey<String?>("UIviewControllwe")
    
    static var dicEvent = DefaultsKey<NSDictionary>("dicEvent")
    
    static var EventStatus = DefaultsKey<String?>("EventStatus")
    
    static var ETitle = DefaultsKey<String?>("ETitle")
    static var Estartdate = DefaultsKey<String?>("Estartdate")
    static var Eenddate = DefaultsKey<String?>("Eenddate")
    static var Eaddress = DefaultsKey<String?>("Eaddress")
    static var Bannerimage = DefaultsKey<String?>("Bannerimage")
    
    
    static var SocialEmail = DefaultsKey<String?>("SocialEmail")
    static var SocialID = DefaultsKey<String?>("SocialID")
    static var SocialName = DefaultsKey<String?>("SocialName")
    static var SocialFirstName = DefaultsKey<String?>("SocialFirstName")
    static var SocialLastName = DefaultsKey<String?>("SocialLastName")
    static var SocialToken = DefaultsKey<String?>("SocialToken")
    
}




////Store
//UserDefaults.standard.set(true, forKey: "Key") //Bool
//UserDefaults.standard.set(1, forKey: "Key")  //Integer
//UserDefaults.standard.set("TEST", forKey: "Key") //setObject

//////Retrieve
//UserDefaults.standard.bool(forKey: "Key")
//UserDefaults.standard.integer(forKey: "Key")
//UserDefaults.standard.string(forKey: "Key")

///Remove
//UserDefaults.standard.removeObject(forKey: "Key")
